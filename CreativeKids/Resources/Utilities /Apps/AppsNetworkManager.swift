//
//  AppsNetworkManager swift
//  Shubham Kaliyar
//
//  Created by Shubham Kaliyar on 17/09/19.
//  Copyright © 2019 Shubham Kaliyar. All rights reserved.



import Foundation
import UIKit
import SVProgressHUD
import AVFoundation

import Alamofire


//MARK:- Globle Variable for AppsNetworkManagerInstanse
let AppsNetworkManagerInstanse = AppsNetworkManager.sharedInstanse
let iimageCache = NSCache<NSString, AnyObject>()
let documentCache = NSCache<NSString, AnyObject>()



//MARK:-Class For Network Manager For Api Send And Retrived Data To/From Server

public class AppsNetworkManager{
    
    /**
     A shared instance of `AppsNetworkManagerInstanse`, used by top-level UllSessions request methods, and suitable for use directly
     for any ad hoc requests.
     */
    
    internal static let sharedInstanse :AppsNetworkManager  = AppsNetworkManager()
    
    // MARK:- Func for Single part Api
    /**
     *  Initiates HTTPS or HTTP request over |HttpsMEthods| method and returns call back in success and failure block.
     *
     *  @param serviceurl  name of the service
     *  @param method       method type like Get and Post
     *  @param postData     parameters
     *  @param responeBlock call back in block
     */
    
    
    
    
    func requestApi(parameters : Dictionary<String , String>, serviceurl:String , methodType:httpMethod, completionClosure: @escaping (_ params : Any?,_ statusCode : Int) -> ()) -> Void {
        
        //MARK:- Check the network availability
        //        if  NetworkReachabilityManager()?.isReachable != true {
        //            showAlertMessage.alert(message: AlertMessage.knoNetwork)
        //        }
        
        
        //MARK:-Show Progress bar Hud
        self.showHudWithNoInteraction(show: true)
        
        //MARK:-Fatch URL From Strings
        let urlString = AppsNetworkManagerConstants.baseUrl + serviceurl
        guard let url = URL(string: urlString.replacingOccurrences(of: " ", with: "%20")) else { return }
        print("Connecting to Host with URL \(urlString) with parameters: \(parameters)")
        
        let accessToken = kSharedUserDefaults.getLoggedInAccessToken()
        var request = URLRequest(url: url)
        request.httpMethod = methodType.rawValue
        
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: methodType.rawValue == httpMethod.get.rawValue ?  [:] : parameters , options: []) else {
            return
        }
        
        
        request.httpBody = httpBody
        request.setValue(accessToken, forHTTPHeaderField: AppsNetworkManagerConstants.accessToken)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        URLSession.shared.dataTask(with: request){(data , response , error) in
            print("Details: \(String(describing: data)) --- ResponseIs: \(String(describing: response)) --- ErrorIs \(String(describing: error))")
            
            self.showHudWithNoInteraction(show: false)
            
            guard error == nil else {
                //MARK:-  In Case of No Internet "Request failed with error: The Internet connection appears to be offline."
                let returnMessage = "RequestFailed :->  \(String(describing: error!.localizedDescription))"
                //MARK:-  --> Show snackbar in case of no internet connection for reload page case
                DispatchQueue.main.async { self.alert(message: returnMessage)}
                return
            }
            
            if let response = response {
                print(response)
            }
            
            
            if let data = data {
                guard  let httpsresponse = response as? HTTPURLResponse else {return}
                let statusCode = httpsresponse.statusCode
//                    let data = try JSONSerialization.jsonObject(with: data, options: [])
                    
//                    let response = kSharedInstance.getDictionary(data)
                let response = self.getDataAnyFormData(data: data)
                    print(response)
                    switch statusCode{
                    case 200 :
                        DispatchQueue.main.async {completionClosure(response.responseData, Int.getInt(statusCode))}
                    case 401:
                        completionClosure(nil, Int.getInt(statusCode))
                        DispatchQueue.main.async {
                            self.alert(message: String.getString(response.error?.localizedDescription))
                        }
                    default:
                        completionClosure(nil, Int.getInt(statusCode))
                        DispatchQueue.main.async { self.alert(message: String.getString(response.error?.localizedDescription)) }
                    }
            }
            
        }.resume()
    }
    
    
    //Func for Post Api MultiPart Api to send  image ,Video and File
    /**
     *  Upload multiple images and videos via multipart
     * @param send image into perticular Key
     *  @param serviceurl  name of the service
     *  @param videosArray  array having videos file path
     *  @param postData     parameters
     *  @param responeBlock call back in block
     */
    
    func requestMultipartApi(parameters : Dictionary<String , Any> , serviceurl:String , methodType:httpMethod, completionClosure: @escaping (_ result: Any?) -> ()) -> Void{
        //MARK:- fetch Url For Apia
        self.showHudWithNoInteraction(show: true)
        let urlString = AppsNetworkManagerConstants.baseUrl + serviceurl
        guard let url = URL(string: urlString.replacingOccurrences(of: " ", with: "%20")) else { return }
        
        
        print("Connecting to Host with URL \(urlString) with parameters: \(parameters)")
        
        
        //MARK:- Fatch the Access Tokan And Create Header
        let accessToken = kSharedUserDefaults.getLoggedInAccessToken()
        var request = URLRequest(url: url)
        
        request.httpMethod = methodType.rawValue
        request.setValue(accessToken, forHTTPHeaderField: kAccessToken)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        //MARK:-  Boundary For Multipart Api
        let boundary =  "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! createBody(parameters: parameters, boundary: boundary, mimeType: "image/jpeg/png/jpg/docx/doc/mp4/mov/movie")
        
        
        URLSession.shared.dataTask(with: request){(data , response , error) in
            print("Details: \(String(describing: data)) --- ResponseIs: \(String(describing: response)) --- ErrorIs \(String(describing: error))")
            self.showHudWithNoInteraction(show: false)
            
            guard error == nil else {
                //MARK:-  In Case of No Internet "Request failed with error: The Internet connection appears to be offline."
                let returnMessage = "RequestFailed :->  \(String(describing: error!.localizedDescription))"
                //MARK:-  --> Show snackbar in case of no internet connection for reload page case
                DispatchQueue.main.async { self.alert(message: String.getString(returnMessage))}
                return
            }
            
            
            if let response = response {
                print(response)
            }
            
            
            if let data = data {
                guard  let httpsresponse = response as? HTTPURLResponse else {return}
                let statusCode = httpsresponse.statusCode
                do {
                    let data = try JSONSerialization.jsonObject(with: data, options: [])
                    let response = kSharedInstance.getDictionary(data)
                    print(response)
                    switch statusCode{
                    case 200 :
                        completionClosure(response[kResponse])
                    case 401:
                        self.alert(message: String.getString(response[ApiParameters.message]))
                    default:
                        DispatchQueue.main.async { self.alert(message: String.getString(response[ApiParameters.message]))}
                    }
                    
                } catch {
                    DispatchQueue.main.async { self.alert(message: String.getString(error))}
                }
            }
            
        }.resume()
    }
    
    
    
    //MARK:- Func for Create Body for multipart Api to append Video and images
    func createBody(parameters: [String: Any], boundary: String, mimeType: String) throws -> Data {
        var body = Data()
        
        for (key, value) in parameters {
            if(value is String || value is NSString) {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value)\r\n")
            } else if let imagValue = value as? UIImage {
                let r = arc4random()
                let filename = "image\(r).jpg" //MARK:  put your imagename in key
                let data: Data = imagValue.jpegData(compressionQuality: 0.5)!
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
                body.append("Content-Type: \(mimeType)\r\n\r\n")
                body.append(data)
                body.append("\r\n")
                
            }else if value is [String: String] {
                var body1 = Data()
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                for (keyy, valuee) in (value as? [String: String])! {
                    body1.append("--\(boundary)\r\n")
                    body1.append("Content-Disposition: form-data; name=\"\(keyy)\"\r\n\r\n")
                    body1.append("\(valuee)\r\n")
                }
                
                body.append(body1)
                
            } else if let images = value as? [UIImage] {
                
                for image in images {
                    let r = arc4random()
                    let filename = "image\(r).jpg" //MARK:  put your imagename in key
                    let data: Data = image.jpegData(compressionQuality: 0.5)!
                    body.append("--\(boundary)\r\n")
                    body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
                    body.append("Content-Type: \(mimeType)\r\n\r\n")
                    body.append(data)
                    body.append("\r\n")
                    
                }
            } else if let videoData = value as? Data { //MARK:  it is Used for Video and pdf send to the server
                let r = arc4random()
                let filename = "\(key)\(r).mov" //MARK:  Put you image Name in key
                let data : Data = videoData
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
                body.append("Content-Type: \(mimeType)\r\n\r\n")
                body.append(data)
                body.append("\r\n")
            } else if let multipleData = value as? [Data] { //MARK:  It is used for Multiple Data to api
                for filedata in multipleData {
                    let r = arc4random()
                    let filename = "\(key)\(r).mov" //MARK:-  put your imagename in key
                    let data: Data = filedata
                    body.append("--\(boundary)\r\n")
                    body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
                    body.append("Content-Type: \(mimeType)\r\n\r\n")
                    body.append(data)
                    body.append("\r\n")
                    
                }
            }
        }
        body.append("--\(boundary)--\r\n")
        return body
    }
    
    func getDataAnyFormData(data :Data) -> (responseData: Dictionary<String,Any>?, error:Error?){
       let responceInDict = getResponseDataDictionaryFromData(data: data)
       if responceInDict.responseData != nil{
           let returnResponce:Dict = [getResponce:responceInDict.responseData!]
           return(returnResponce,responceInDict.error)
       }else{
           let responceInArrayDict = getResponseDataArrayFromData(data: data)
           if responceInArrayDict.responseData != nil{
               let returnResponce:Dict = [getResponce:responceInArrayDict.responseData!]
               return(returnResponce,responceInArrayDict.error)
           }else{
               let responceInString = getResponseDataStringFromData(data: data)
               if responceInString.responseData != nil{
                   let returnResponce:Dict = [getResponce:responceInString.responseData!]
                   return(returnResponce,responceInArrayDict.error)
               }else{
               let error = responceInArrayDict.error
               return(nil,error)
           }
         }
       }
   }
    
    //////////
    private func getResponseDataArrayFromData(data: Data) -> (responseData: [Any]?, error: NSError?)
    {
        do
        {
            let responseData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [Any]
            print("Success with JSON: \(String(describing: responseData))")
            return (responseData, nil)
        }
        catch let error as NSError
        {
            print_debug(items: "json error: \(error.localizedDescription)")
            return (nil, error)
        }
    }
    
    private func getResponseDataDictionaryFromData(data: Data) -> (responseData: Dictionary<String, Any>?, error: Error?)
    {
        do
        {
            let responseData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? Dictionary<String, Any>
            print("Success with JSON: \(String(describing: responseData))")
            return (responseData, nil)
        }
        catch let error
        {
            print_debug(items: "json error: \(error.localizedDescription)")
            return (nil, error)
        }
    }
    
    private func getResponseDataStringFromData(data: Data) -> (responseData: Any?, error: Error?)
    {
        do
        {
            let responseData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? String
            print("Success with JSON: \(String(describing: responseData))")
            return (responseData, nil)
        }
        catch let error
        {
            print_debug(items: "json error: \(error.localizedDescription)")
            return (nil, error)
        }
    }
    
}




//MARK:- Class For Shared Utilities For AppsNetworkManagerInstanse
extension AppsNetworkManager {
    
    //MARK:- Func For Show Hud
    func showHudWithNoInteraction(show: Bool) {
        if show {
            SVProgressHUD.setDefaultMaskType(.clear)
            SVProgressHUD.setDefaultStyle(.custom)
            SVProgressHUD.setForegroundColor(UIColor.black)
            SVProgressHUD.setBackgroundColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0).withAlphaComponent(0))
            SVProgressHUD.show()
        } else {
            SVProgressHUD.dismiss()
        }
    }
    
    //MARK: - Show Alert For Error
    func alert(message:String) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        let action1 = UIAlertAction(title: AlertTitle.kOk, style: .cancel, handler: nil)
        alert.addAction(action1)
        UIApplication.shared.windows.first?.rootViewController?.present(alert , animated: true)
    }
    
}


//MARK:- Constant for Api For Url Sessions
struct AppsNetworkManagerConstants {
    static let baseUrl               = "https://creativekidssolutions.com/students/"
    static let baseUrlForimage       = "http://15.185.108.16:3000"
    
    static let accessToken           = "Authorization"
    
}

//MARK:- Enum For httpsMethos

enum httpMethod: String {
    case get  = "GET"
    case post = "POST"
    case put  = "PUT"
}
//MARK: - Extension for Downlode Image Using URl Sessions
extension UIImageView {
    
    //MARK:- Func for downlode image
    func downlodeImage(serviceurl:String?, placeHolder: UIImage?) {
        
        self.image = placeHolder
        let urlString = serviceurl ?? ""
        guard let url = URL(string: urlString.replacingOccurrences(of:  " ", with: "%20")) else { return }
        
        //MARK:- Check image Store in Cache or not
//        if let cachedImage = iimageCache.object(forKey: urlString.replacingOccurrences(of: " ", with: "%20") as NSString) {
//            if  let image = cachedImage as? UIImage {
//                self.image = image
//                print("Find image on Cache : For Key" , urlString.replacingOccurrences(of: " ", with: "%20"))
//                return
//            }
//        }
        
        print("Conecting to Host with Url:-> \(url)")
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            
            if error != nil {
                print(error!)
                DispatchQueue.main.async {
                    self.image = placeHolder
                    return
                }
            }
            if data == nil {
                DispatchQueue.main.async {
                    self.image = placeHolder
                }
                return
            }
            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    self.image = image
                    iimageCache.setObject(image, forKey: urlString.replacingOccurrences(of: " ", with: "%20") as NSString)
                }
            }
        }).resume()
    }
    //MARK:- Func for downlode Gif
    func downlodeGif(serviceurl:String?) {
        
        let urlString = serviceurl!
        guard let url = URL(string: urlString.replacingOccurrences(of:  " ", with: "%20")) else { return }
        
        print("Conecting to Host with Url:-> \(url)")
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            
            if error != nil {
                print(error!)
                DispatchQueue.main.async {
                    self.image = nil
                    return
                }
            }
            if data == nil {
                DispatchQueue.main.async {
                    self.image = nil
                }
                return
            }
            DispatchQueue.main.async {
                let gifImageData = UIImage.gifImageWithData(data!)
                if let getGif = gifImageData {
                    let imageView3 = UIImageView(image: getGif)
                    imageView3.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
                    self.addSubview(imageView3)
//                    self.image = getGif
                }
            }
        }).resume()
    }
    
}

//MARK:- Extension of Data For Apped String
extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}
