//
//  UIButtonCornerRadius.swift
//  OneClickWash
//
//  Created by RUCHIN SINGHAL on 15/09/16.
//  Copyright © 2016 Appslure. All rights reserved.
//

import UIKit
import Foundation

class UIButtonCornerRadius: UIButton {
    @IBInspectable override var cornerRadius: CGFloat {
    didSet {
      layer.cornerRadius = cornerRadius
      layer.masksToBounds = cornerRadius > 0
    }
  }
  @IBInspectable var makeCircle: Bool = false {
    didSet {
      layer.masksToBounds = cornerRadius > 0
    }
  }
}
 
class SetCornerRadiusWithBorder: UIButton{
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setCornerRoundWithWidth(color:UIColor.systemBlue.cgColor)
    }
    func setCornerRoundWithWidth(color:CGColor){
        self.layer.cornerRadius = self.height/2
        self.layer.borderWidth = 1.0
        self.layer.borderColor = color
    }
}
