//
//  UserDefaultsExtended.swift
//  OneClickWash
//
//  Created by RUCHIN SINGHAL on 23/10/16.
//  Copyright © 2016 Appslure. All rights reserved.
//

import Foundation

extension UserDefaults {

    func isTutorialShown() -> Bool {
        return self.bool(forKey: kIsTutorialAlreadyShown)
    }
    
    func setTutorialShownStatus(tutorialShown: Bool) {
        self.set(tutorialShown, forKey: kIsTutorialAlreadyShown)
        self.synchronize()
    }
    
    func isUserLoggedIn() -> Bool {
        return self.bool(forKey: kIsUserLoggedIn)
    }
    
    func setUserLoggedIn(userLoggedIn: Bool) {
        self.set(userLoggedIn, forKey: kIsUserLoggedIn)
        self.synchronize()
    }
    
    func isAppInstalled() -> Bool {
        return self.bool(forKey: kIsAppInstalled)
    }
    
    func setAppInstalled(installed: Bool) {
        self.set(installed, forKey: kIsAppInstalled)
        self.synchronize()
    }
    
    func getCategoryID() -> String {
         return String.getString(self.string(forKey: "category_id"))
     }
     
     
     func setCategoryID(category: String) {
         self.set(category, forKey: "category_id")
         self.synchronize()
     }
    
    
    func getLoggedInUserId() -> String {
        return String.getString(self.string(forKey: kLoginUserID))
    }
    
    
    func setLoggedInUserId(loggedInUserId: String) {
        self.set(loggedInUserId, forKey: kLoginUserID)
        self.synchronize()
    }
    
    func setLoggedInAccessToken(loggedInAccessToken: String) {
        self.set(loggedInAccessToken, forKey: kLoggedInAccessToken)
        self.synchronize()
    }
    
    func getLoggedInAccessToken() -> String {
        return String.getString(self.string(forKey: kLoggedInAccessToken))
    }
    
    func setUserSelectedLocation(_ latitude: Double, _ longitude: Double) {
        self.set(latitude, forKey: kLatitude)
        self.set(longitude, forKey: kLongitude)
        self.synchronize()
    }
    
    
    func getSelectedLatitudeAndLongitude() -> (Double, Double) {
        return (Double.getDouble(self.double(forKey: kLatitude)), Double.getDouble(self.double(forKey: kLongitude)))
    }
    
    
    func updateUserPics(_ dict: Dictionary<String, Any>) {
        var dictUserData = getLoggedInUserDetails()
        var arrUserImages = kSharedInstance.getArray(dictUserData[kMediafiles])
        arrUserImages.append(dict)
        
        dictUserData[kMediafiles] = arrUserImages
        setLoggedInUserDetails(loggedInUserDetails: dictUserData)
    }
    
    
    
    func updateUserLocation(withLatitude latitude: Double, andLongitude longitude: Double) {
        var dictUserData = getLoggedInUserDetails()
        var dictLocation = kSharedInstance.getDictionary(dictUserData[kLocation])
        dictLocation[kLatitude] = latitude
        dictLocation[kLongitude] = longitude
        dictUserData[kLocation] = dictLocation
        setLoggedInUserDetails(loggedInUserDetails: dictUserData)
    }
    
    
    func deleteUserPic(atIndex index: Int) {
        var dictUserData = getLoggedInUserDetails()
        var arrUserImages = kSharedInstance.getArray(dictUserData[kMediafiles])
        let dictPic = kSharedInstance.getDictionary(arrUserImages[index])
        arrUserImages.remove(at: index)
        if NSNumber.getNSNumber(message: dictPic[kIsProfilePicture]).boolValue && arrUserImages.count > 0 {
            var dictProfilePic = kSharedInstance.getDictionary(arrUserImages[0])
            dictProfilePic[kIsProfilePicture] = "1"
            dictUserData[kProfilePicture] = dictProfilePic[kMedia]
            arrUserImages.remove(at: 0)
            arrUserImages.insert(dictProfilePic, at: 0)
        }
        
        dictUserData[kMediafiles] = arrUserImages
        setLoggedInUserDetails(loggedInUserDetails: dictUserData)
    }
    
    
    func updateUserProfilePic(toIndex newProfilePicIndex: Int) {
        var dictUserData = getLoggedInUserDetails()
        var arrUserImages = kSharedInstance.getArray(dictUserData[kMediafiles])
        
        for i in 0..<arrUserImages.count {
            var dictPic = kSharedInstance.getDictionary(arrUserImages[i])
            if i == newProfilePicIndex {
                dictPic[kIsProfilePicture] = "1"
                dictUserData[kProfilePicture] = dictPic[kMedia]
            } else {
                dictPic[kIsProfilePicture] = "0"
            }
            arrUserImages.remove(at: i)
            arrUserImages.insert(dictPic, at: i)
        }
        
        dictUserData[kMediafiles] = arrUserImages
        setLoggedInUserDetails(loggedInUserDetails: dictUserData)
    }
    
    
    func updateLoggedInUserData(_ dict: Dictionary<String, Any>) {
        var dictUserData = getLoggedInUserDetails()
        for (key, value) in dict {
            dictUserData[key] = value
        }
        setLoggedInUserDetails(loggedInUserDetails: dictUserData)
    }
    
    
    func updateEmailVerifiedStatus() {
        var dictUserData = getLoggedInUserDetails()
        dictUserData[kIsEmailVerified] = NSNumber(value: true)
        setLoggedInUserDetails(loggedInUserDetails: dictUserData)
    }
    
    
    func getLoggedInUserDetails() -> Dictionary<String, Any> {
        guard let dataUser = self.object(forKey: kLoggedInUserDetails) else {
            return ["":""]
        }
        
        guard let userData = dataUser as? Data else {
            return ["":""]
        }
        
        let unarchiver = NSKeyedUnarchiver(forReadingWith: userData)
        guard let userLoggedInDetails = unarchiver.decodeObject(forKey: kLoggedInUserDetails) as? Dictionary <String, Any> else {
            unarchiver.finishDecoding()
            return ["":""]
        }
        unarchiver.finishDecoding()
        return userLoggedInDetails
    }
    
    //MARK:- GET USER SUBSCRIBE SUBJECTS
    func getUserSubscribtionDetail() -> Dictionary<String, Any> {
        guard let dataUser = self.object(forKey: kUserSubscriptionDetails) else {
            return ["":""]
        }
        
        guard let userData = dataUser as? Data else {
            return ["":""]
        }
        
        let unarchiver = NSKeyedUnarchiver(forReadingWith: userData)
        guard let userLoggedInDetails = unarchiver.decodeObject(forKey: kUserSubscriptionDetails) as? Dictionary <String, Any> else {
            unarchiver.finishDecoding()
            return ["":""]
        }
        unarchiver.finishDecoding()
        return userLoggedInDetails
    }

    
    //MARK:- SET USER SUBSCRIBE SUBJECTS
    func setUserSubscribtionDetail(subscribtionDetail: Dictionary<String, Any>) {
        if subscribtionDetail.isEmpty {
            self.set(nil, forKey: kUserSubscriptionDetails)
            self.synchronize()
            return
        }
        let userData = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: userData)
        archiver.encode(subscribtionDetail, forKey: kUserSubscriptionDetails)
        archiver.finishEncoding()
        self.set(userData, forKey: kUserSubscriptionDetails)
        self.synchronize()
    }
    
    
    func setLoggedInUserDetails(loggedInUserDetails: Dictionary<String, Any>) {
        if loggedInUserDetails.isEmpty {
            self.set(nil, forKey: kLoggedInUserDetails)
            self.synchronize()
            return
        }
        let userData = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: userData)
        archiver.encode(loggedInUserDetails, forKey: kLoggedInUserDetails)
        archiver.finishEncoding()
        self.set(userData, forKey: kLoggedInUserDetails)
        self.synchronize()
    }
    //kRecentChapterSeen
    //recentChapter
    //setRecentChapterSeen
    
    func setRecentChapterSeen(recentChapter: Dictionary<String, Any>) {
        if recentChapter.isEmpty {
            self.set(nil, forKey: kRecentChapterSeen)
            self.synchronize()
            return
        }
        let userData = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: userData)
        archiver.encode(recentChapter, forKey: kRecentChapterSeen)
        archiver.finishEncoding()
        self.set(userData, forKey: kRecentChapterSeen)
        self.synchronize()
    }
    
    func getRecentChapterSeen() -> Dictionary<String, Any> {
        guard let dataUser = self.object(forKey: kRecentChapterSeen) else {
            return ["":""]
        }
        
        guard let userData = dataUser as? Data else {
            return ["":""]
        }
        
        let unarchiver = NSKeyedUnarchiver(forReadingWith: userData)
       
    
        guard let userLoggedInDetails = unarchiver.decodeObject(forKey: kRecentChapterSeen) as? Dictionary <String, Any> else {
            unarchiver.finishDecoding()
            return ["":""]
        }
        unarchiver.finishDecoding()
        return userLoggedInDetails
    }
    
    func setDeviceToken(deviceToken: String) {
        self.set(deviceToken, forKey: kDeviceToken)
        self.synchronize()
    }
    
    func getDeviceToken() -> String {
        return String.getString(self.string(forKey: kDeviceToken))
    }
    
//    func setUserClass(userClass: String) {
//        self.set(userClass, forKey: kUserClass)
//        self.synchronize()
//    }
//
//    func getUserClass() -> String {
//        return String.getString(self.string(forKey: kUserClass))
//    }
    
  // MARK:- SET USER SUBSCRIBE SUBJECTS
    func setUserClass(userClass: Dictionary<String, Any>) {
        if userClass.isEmpty {
            self.set(nil, forKey: kUserClass)
            self.synchronize()
            return
        }
        let userData = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: userData)
        archiver.encode(userClass, forKey: kUserClass)
        archiver.finishEncoding()
        self.set(userData, forKey: kUserClass)
        self.synchronize()
    }

    //MARK:- GET USER SUBSCRIBE SUBJECTS
    func getUserClass() -> Dictionary<String, Any> {
        guard let dataUser = self.object(forKey: kUserClass) else {
            return ["":""]
        }
        guard let userData = dataUser as? Data else {
            return ["":""]
        }
        let unarchiver = NSKeyedUnarchiver(forReadingWith: userData)
        guard let userClassDetails = unarchiver.decodeObject(forKey: kUserClass) as? Dictionary <String, Any> else {
            unarchiver.finishDecoding()
            return ["":""]
        }
        unarchiver.finishDecoding()
        return userClassDetails
    }
    
    //MARK:- APP TOUR
    func setAppTourDone(tourDone: Bool) {
        self.set(tourDone, forKey: kIsTourDone)
        self.synchronize()
    }
    func isAppTourDone() -> Bool {
        return self.bool(forKey: kIsTourDone)
    }
    
    //MARK: - Chat Halper
    
    func getuserDetails() -> Dictionary<String, Any> {
        guard let dataUser = self.object(forKey: kchatUserDetails) else {
            return ["":""]
        }
        
        guard let userData = dataUser as? Data else {
            return ["":""]
        }
        
        let unarchiver = NSKeyedUnarchiver(forReadingWith: userData)
        guard let userLoggedInDetails = unarchiver.decodeObject(forKey: kchatUserDetails) as? Dictionary <String, Any> else {
            unarchiver.finishDecoding()
            return ["":""]
        }
        
        unarchiver.finishDecoding()
        return userLoggedInDetails
    }
    
    func setuserDetials(loggedInUserDetails:UsersState?) {
        if  String.getString(loggedInUserDetails?.userId) == "" {
            self.set(nil, forKey: kchatUserDetails)
            self.synchronize()
            return
        }
        let details = loggedInUserDetails?.createDictonary(objects: loggedInUserDetails)
        let userData = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: userData)
        archiver.encode(details, forKey: kchatUserDetails)
        archiver.finishEncoding()
        self.set(userData, forKey: kchatUserDetails)
        self.synchronize()
    }
}
