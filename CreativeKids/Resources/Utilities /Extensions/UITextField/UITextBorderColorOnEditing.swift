//
//  UITextBorderColorOnEditing.swift
//  Dyne
//
//  Created by Abhishek Banerjee on 22/11/17.
//  Copyright © 2017 Nitin Aggarwal. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func changeBorderColorOnTextFieldBeginEditing() {
        layer.borderColor = UIColor.init(red: 0, green: 0.5137, blue: 0.560, alpha: 1).cgColor
    }
    func changeBorderColorOnTextFieldEndEditing() {
        layer.borderColor = UIColor.init(red: 0.667, green: 0.667, blue: 0.667, alpha: 1).cgColor
    }
}

extension UITextField {
    func setRightView(image: UIImage) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        let imageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 20, height: 20))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        self.rightView = view;
        self.rightViewMode = .always
    }
}

