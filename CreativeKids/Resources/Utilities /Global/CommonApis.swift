//
//  CommonApis.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/05/22.
//

import Foundation

let commonApi = CommonAPis.shared

class CommonAPis:NSObject{
    static var shared = CommonAPis()
    
    func getClass(completionHandler: @escaping (([String])->())) {
        BaseController.shared.postToServerAPI(url: "?clss=abc", params: [:], type: .GET) { response, statusCode in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            print(returnData)
            let classData = returnData.map{String.getString($0["classname"])}
            completionHandler(classData)
        }
    }
    
    func getClassesBySchool(schoolId:String,completionHandler: @escaping (([Class])->())) {
        let serviceUrl = kStudentBaseUrl + "getClass/?schid=\(schoolId)"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            if statusCode == 200{
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            let classData = returnData.map{Class(data: $0)}
            completionHandler(classData)
        }else {
             print("something error")
        }
        }
    }
    
    func getSectionByClass(classId:String,completionHandler: @escaping (([Section])->())) {
        let serviceUrl = kStudentBaseUrl + "getsec/?clsid=\(classId)"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            if statusCode == 200{
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            let sectionData = returnData.map{Section(data:$0)}
            completionHandler(sectionData)
            }else{
               print("something error")
            }
        }
    }
}





// *************Models**************

class Class{
    var className:String
    var id:String
    init(data:Dict) {
        className = String.getstring(data["classname"])
        id = String.getstring(data["classid"])
    }
}


class Section{
    var className:String
    var id:String
    var classId:String
    var sectionName:String
    init(data:Dict) {
        className = String.getstring(data["classid"])
        id = String.getstring(data["secid"])
        sectionName = String.getstring(data["section"])
        classId = String.getstring(data["classid"])
    }
}
