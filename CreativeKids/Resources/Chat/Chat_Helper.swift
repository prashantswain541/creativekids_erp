//
//  Chat_Helper.swift
//  Chat Demo
//
//  Created by Creative Kids on 28/02/22.
//

import Foundation
import UIKit
import FirebaseCore
import FirebaseDatabase
import AVFoundation
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage


class ChatHalper {
    
    //MARK:- Object for Chat Halper
    static let shared    = ChatHalper()
    var messageReference = Database.database().reference().child(ChatParameter.Message)
    var recentReference  = Database.database().reference().child(ChatParameter.ResentMessage)
    var userReference        = Database.database().reference().child(ChatParameter.UserState)
    var groupReference       = Database.database().reference().child(ChatParameter.Groups)
    var imagesStorageRef     = Storage.storage().reference().child(ChatParameter.Images)
    var pdfStorageRef        = Storage.storage().reference().child(ChatParameter.Pdfs)
    var messageclass         = [MessageClass]()
    var chatBackupOnetoOne   = [MessageClass]()
    var resentUser           = [ResentUsers]()
    var users                = [UsersState]()
    var userState :UsersState?
    let userDetails = chatUserDefault.getuserDetails()
    
    //MARK:- Function For Send message one to one Chat
    func sendMessage(messageDic:MessageClass) {
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userState.userId) == ChatParameter.emptyString {
            print("User Id Is empty please check you save the details in chatUser Default or not.")
            return
        }
        if String.getstring(messageDic.receiverId) == ChatParameter.emptyString {
            print("Receiver id is empty please check you fill the receiver id in message on to send message or not.")
            return
        }
        let messageNode = self.createNode(senderId: userState.userId ?? "", receiverId: messageDic.receiverId ?? "")
        let sendReference = messageReference.child(messageNode).childByAutoId()
        messageDic.uid = sendReference.key
        messageDic.senderId = userState.userId
        messageDic.senderName = userState.name
        let message = messageDic.createDictonary(objects: messageDic)
        print("send message to node \(messageNode) with key \(sendReference.key ?? "") with message \(message)")
        sendReference.setValue(chatSharedInstanse.getDictionary(message))
        userReference.child(userState.userId ?? "").updateChildValues(userState.createDictonary(objects: userState))
        self.resentUser(messageDetails: messageDic)
    }
    func updateMessage(messageDic:MessageClass) {
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userState.userId) == ChatParameter.emptyString {
            print("User Id Is empty please check you save the details in chatUser Default or not.")
            return
        }
        if String.getstring(messageDic.receiverId) == ChatParameter.emptyString {
            print("Receiver id is empty please check you fill the receiver id in message on to send message or not.")
            return
        }
        let messageNode = self.createNode(senderId: userState.userId ?? "", receiverId: messageDic.receiverId ?? "")
        let sendReference = messageReference.child(messageNode)
        messageDic.senderId = userState.userId
        messageDic.senderName = userState.name
        let message = messageDic.createDictonary(objects: messageDic)
        print("send message to node \(messageNode) with key \(sendReference.key ?? "") with message \(message)")
        sendReference.child(String.getstring(messageDic.uid)).setValue(chatSharedInstanse.getDictionary(message))
         self.resentUser(messageDetails: messageDic)
    }
    
    func removeMessage(messageDic:MessageClass) {
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userState.userId) == ChatParameter.emptyString {
            print("User Id Is empty please check you save the details in chatUser Default or not.")
            return
        }
        if String.getstring(messageDic.receiverId) == ChatParameter.emptyString {
            print("Receiver id is empty please check you fill the receiver id in message on to send message or not.")
            return
        }
        let messageNode = self.createNode(senderId: userState.userId ?? "", receiverId: messageDic.receiverId ?? "")
        let sendReference = messageReference.child(messageNode)
        messageDic.senderId = userState.userId
        messageDic.senderName = userState.name
        print("send message to node \(messageNode) with key \(sendReference.key ?? "") with message \(messageDic)")
        sendReference.child(String.getstring(messageDic.uid)).removeValue()
    }
    
    //MARK:- Func For Receive Message for One To One Chat
    func receiveMessage(receiverId:String , message:@escaping (_ result: [MessageClass]? , _ chatBackup : [MessageClass]?) -> ()) -> Void {
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userState.userId) == ChatParameter.emptyString {
            print("User Id Is empty please check you save the details in chatUser Default or not.")
            return
        }
        if String.getstring(receiverId) == ChatParameter.emptyString {
            print("Receiver id is empty please check you send the receiver id or not .")
            return
        }
        let messageNode = self.createNode(senderId: userState.userId ?? "", receiverId: receiverId)

        CommonUtils.showHudWithNoInteraction(show: true)
        messageReference.child(messageNode).observe(.value) { [weak self] (snapshot) in
            CommonUtils.showHudWithNoInteraction(show: false)
            self?.chatBackupOnetoOne.removeAll()
            self?.messageclass.removeAll()
            if snapshot.exists() {
                let messages = chatSharedInstanse.getDictionary(snapshot.value)
                messages.forEach {(key, value) in
                    let dic = chatSharedInstanse.getDictionary(value)
                    let isDeleted =  String.getstring(dic[ChatParameter.isDeleted]).components(separatedBy: ChatParameter.saparaterString)
                    if !(isDeleted.contains(String.getstring(userState.userId))) {
                        self?.messageclass.append(MessageClass(uid: String.getstring(key), messageData: dic))
                        self?.messageclass.sort{ $0.sendingTime < $1.sendingTime }
                    } else {
                        self?.chatBackupOnetoOne.append(MessageClass(uid: String.getstring(key), messageData: dic))
                        self?.chatBackupOnetoOne.sort{ $0.sendingTime < $1.sendingTime}
                    }
                }
            }
            message(self?.messageclass, self?.chatBackupOnetoOne)
        }
    }
    //MARK:- Func For send Resent Users and retrived data On resent Users Submit Data to Users Every User Submit 2 Node  Sender and Receiver
    func resentUser(messageDetails:MessageClass) {
        let senderId = messageDetails.senderId ?? ""
        let receiverId = messageDetails.receiverId ?? ""
        if String.getstring(senderId) == ChatParameter.emptyString || String.getstring(receiverId) == ChatParameter.emptyString {
            print(ChatParameter.alertmessage)
            return
        }
        
        let receiverDetails = [ ChatParameter.receiverId: String.getstring(receiverId),
                                ChatParameter.senderid : String.getstring(senderId),
                                ChatParameter.uid : String.getstring(messageDetails.uid),
                                ChatParameter.readStatus : String.getstring(messageDetails.mediaType?.rawValue) == "create" ? "seen":"sent",
                                ChatParameter.lastMessage : String.getstring(messageDetails.message),
                                ChatParameter.mediaType    : String.getstring(messageDetails.mediaType?.rawValue) ,
                                ChatParameter.timeStamp : String.getstring(Int(Date().timeIntervalSince1970 * 1000))
        ]
        let senderDetails =   [ ChatParameter.receiverId: String.getstring(senderId),
                                ChatParameter.senderid : String.getstring(receiverId),
                                ChatParameter.uid : String.getstring(messageDetails.uid),
                                ChatParameter.readStatus : String.getstring(messageDetails.mediaType?.rawValue) == "create" ? "seen":"sent",
                                ChatParameter.lastMessage : String.getstring(messageDetails.message),
                                ChatParameter.mediaType    : String.getstring(messageDetails.mediaType?.rawValue),
                                ChatParameter.timeStamp : String.getstring(Int(Date().timeIntervalSince1970 * 1000))
        ]
        recentReference.child(senderId).child(receiverId).updateChildValues(receiverDetails)
        recentReference.child(receiverId).child(senderId).updateChildValues(senderDetails)
        
    }
    //MARK:- Func For Resent Users retrived on Recent Screen user User Id
    func receiveResentUsers(resentUsers:@escaping (_ result: [ResentUsers]?) -> ()) -> Void{
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userState.userId) == ChatParameter.emptyString {
            print(ChatParameter.alertmessage)
            return
        }
        self.resentUser.removeAll()
        recentReference.child(String.getstring(userState.userId)).observe(.value) { [weak self](snapshot) in
            self?.resentUser.removeAll()
            if snapshot.exists() {
                let usersDetails = chatSharedInstanse.getDictionary(snapshot.value)
                self?.resentUser.removeAll()
                usersDetails.forEach { [weak self] (key, value) in
                    let details = chatSharedInstanse.getDictionary(value)
                    self?.checkUserStateObserver(userid: String.getstring(details[ChatParameter.receiverId]), usersDetails: { [weak self] (receiverDetails) in
                        if let receiverDetail = receiverDetails {
                            self?.unreadMessage(userdetails: details, referenceType: receiverDetail.messageReferanceType, unreadCount: { [weak self] (messageCount) in
                                var resentusersDetails = ResentUsers()
                                let blockUser = self?.checkUserBlock(users:receiverDetail.blockUsers) ?? false
                                resentusersDetails.receiverId     =  String.getstring(details[ChatParameter.receiverId])
                                resentusersDetails.sendingTime    =  Int.getint(details[ChatParameter.timeStamp])
                                resentusersDetails.senderId       =  String.getstring(details[ChatParameter.senderId])
                                resentusersDetails.lastMessage    =  String.getstring(details[ChatParameter.lastMessage])
                                resentusersDetails.mediaType      =  String.getstring(details[ChatParameter.mediaType])
                                resentusersDetails.receiverName   = String.getstring(receiverDetails?.name)
                                resentusersDetails.readStatus     = String.getstring(details[ChatParameter.readStatus])
                                resentusersDetails.imageUrl       = blockUser ? "" : String.getstring(receiverDetail.profile_image)
                                resentusersDetails.isOnline       = receiverDetail.isOnline ?? false
                                resentusersDetails.createdBy = String.getstring(receiverDetail.createdBy)
                                resentusersDetails.readCount      = String.getstring(messageCount)
                                resentusersDetails.userOnlinetime = String.getstring(receiverDetail.onlineTime)
                                resentusersDetails.uid            = String.getstring(details[ChatParameter.uid])
                                print(messageCount)
                                 let firstIndex = self?.resentUser.firstIndex{$0.receiverId == resentusersDetails.receiverId}
                                                           if firstIndex != nil { self?.resentUser.remove(at: firstIndex!) }
                                                           self?.resentUser.append(resentusersDetails)
                                                           self?.resentUser.sort { $0.sendingTime > $1.sendingTime}
                                                           resentUsers(self?.resentUser)
                            })
                        } else {
                            resentUsers(self?.resentUser)
                        }
                    })
                }
            }else{
                resentUsers(self?.resentUser)
            }
        }
        print("not observed")
    }
    
    func readMessage(senderId :String , receiverId:String,messages:[MessageClass]?) {
           let userState = UsersState(userdata: chatUserDefault.getuserDetails())
           let messageNode = self.createNode(senderId: senderId, receiverId: receiverId)
          for message in messages ?? [] {
              if !senderId.isEmpty{
                  if message.readStatus != "seen" && userState.userId != message.senderId {
                  messageReference.child(messageNode).child(String.getstring(message.uid)).updateChildValues([ChatParameter.readStatus:"seen"])
              }
            }
          }
      }
    //MARK:- Function For Read message Count For Resent Screen Batch Only Read Messages
    func seenMessageCount(receiverId:String , senderId :String , readState :String) {
        if readState == "0" {return}
        if receiverId == ChatParameter.emptyString || senderId == ChatParameter.emptyString{
            print(ChatParameter.alertmessage)
            return
        }
        recentReference.child(senderId).child(receiverId).updateChildValues([ChatParameter.readState:readState])
        self.readresent(receiverId: receiverId, senderId: senderId, readState: "seen")
    }
    
    //MARK:- Function For Read message Count For Resent Screen Batch Only Read Messages
    func readresent(receiverId:String , senderId :String , readState :String) {
        if receiverId == ChatParameter.emptyString || senderId == ChatParameter.emptyString{
            print(ChatParameter.alertmessage)
            return
        }
        recentReference.child(senderId).child(receiverId).updateChildValues([ChatParameter.readStatus:readState])
    }
    //MARK:- Function For Read message Count For Resent Screen Batch
    func readMessageCount(senderId :String , receiverId:String, readstate:String , counters:@escaping (_ result: Int) -> ()) {
        let messageNode = self.createNode(senderId: senderId, receiverId: receiverId)
        messageReference.child(messageNode).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let msgs = chatSharedInstanse.getDictionary(snapshot.value)
                counters(Int(msgs.count) - Int.getint(readstate))
            }
        })
    }
    
    //MARK:- Function For Read message Count For Resent Screen Batch
    func unReadMessageCount(senderId :String , receiverId:String, readstate:String , counters:@escaping (_ result: Int) -> ()) {
        let messageNode = self.createNode(senderId: senderId, receiverId: receiverId)
        print(senderId)
        messageReference.child(messageNode).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let msgs = chatSharedInstanse.getDictionary(snapshot.value)
                let unseenMessage = msgs.filter{String.getstring(chatSharedInstanse.getDictionary($0.value)[ChatParameter.senderId]) == senderId || String.getstring(chatSharedInstanse.getDictionary($0.value)[ChatParameter.readStatus]) == "seen"}
                counters(Int(msgs.count) - Int.getint(unseenMessage.count))
            }
        })
    }
    
    //MARK:- Func For checkUserOnlineStateObserver
    func  checkUserStateObserver(userid:String , usersDetails:@escaping (_ status:UsersState?) -> ()) -> Void{
        if userid == ChatParameter.emptyString {
            print("sender Id Is empty please check you save the details in chatUser Default or not.")
            return
        }
        userReference.child(userid).observe( .value) { (snapshot) in
            if snapshot.exists() {
                let userDetails = chatSharedInstanse.getDictionary(snapshot.value)
                let userStatus =  UsersState(userdata: userDetails , isSelected: false)
                usersDetails(userStatus)
            } else {
                usersDetails(nil)
            }
        }
    }
    //MARK:- Func For Check User State For Online And Ofline State and Submit Data on Firebase On Self Node
    func onlineState(state:String) {
        var  userDetails = chatUserDefault.getuserDetails()
        let userID = String.getstring(userDetails[ChatParameter.userId])
        if userID == ChatParameter.emptyString {
            print(ChatParameter.alertmessage)
            return
        }
        let timeStamp = String.getstring(Int(Date().timeIntervalSince1970 * 1000))
        userDetails[ChatParameter.OnlineState] = String.getstring(state)
        userDetails[ChatParameter.timeStamp] = String.getstring(timeStamp)
        userReference.child(userID).updateChildValues(chatSharedInstanse.getDictionary(userDetails))
    }
    
    //MARK:- Function For Delete message One to One Only Delete Perticular Messege
    func deleteMessage(message:MessageClass?) {
        if String.getstring(message?.senderId) == ChatParameter.emptyString {
            print("sender Id Is empty please check you save the details in chatUser Default or not.")
            return
        }
        if String.getstring(message?.receiverId) == ChatParameter.emptyString {
            print("Receiver id is empty please check you send the receiver id in mesaage details  or not .")
            return
        }
        if String.getstring(message?.uid) == ChatParameter.emptyString {
            print("You can't handel this problem please contact to shubham kaliyar")
            return
        }
        let node = self.createNode(senderId: String.getstring(message?.senderId), receiverId: String.getstring(message?.receiverId))
        let reference = messageReference.child(node).child(String.getstring(message?.uid)).child(ChatParameter.isDeleted)
        var isDeleted = String()
        reference.observe(.value) { (snapshot) in
            if snapshot.exists() {
                isDeleted = String.getstring(snapshot.value)
                print(isDeleted)
            }
        }
        messageReference.child(node).observeSingleEvent(of: .value, with: { (SnapShot) in
            if SnapShot.exists() {
                if isDeleted.isEmpty {
                    reference.setValue(String.getstring(message?.senderId))
                    return
                }
                reference.setValue("\(isDeleted)_\(String.getstring(message?.senderId))")
            }
        })
    }
    
    //MARK:- Function For Star   message One to One Only Star Perticular Messege
    func starMessage(message:MessageClass?) {
        if String.getstring(message?.senderId) == ChatParameter.emptyString {
            print("sender Id Is empty please check you save the details in chatUser Default or not.")
            return
        }
        if String.getstring(message?.receiverId) == ChatParameter.emptyString {
            print("Receiver id is empty please check you send the receiver id in mesaage details  or not .")
            return
        }
        if String.getstring(message?.uid) == ChatParameter.emptyString {
            print("You can't handel this problem please contact to shubham kaliyar")
            return
        }
        let node = self.createNode(senderId: String.getstring(message?.senderId), receiverId: String.getstring(message?.receiverId))
        let reference = messageReference.child(node).child(String.getstring(message?.uid)).child(ChatParameter.starmessage)
        var starmessage = String()
        reference.observe(.value) { (snapshot) in
            if snapshot.exists() {
                starmessage = String.getstring(snapshot.value)
                print(starmessage)
            }
        }
        messageReference.child(node).observeSingleEvent(of: .value, with: { (SnapShot) in
            if SnapShot.exists() {
                if starmessage.isEmpty  {
                    reference.setValue(String.getstring(message?.senderId))
                    return
                }
                reference.setValue("\(starmessage)_\(String.getstring(message?.senderId))")
            }
        })
    }
    //MARK:- Func for Clear All Messages From One to One Chat
    func clearChat(msgclass:[MessageClass],senderId:String, receiverId:String) {
        if receiverId == ChatParameter.emptyString || senderId == ChatParameter.emptyString{
            print(ChatParameter.alertmessage)
            return
        }
        let node =  self.createNode(senderId: senderId, receiverId: receiverId)
        for index in msgclass {
            let uid = index.uid
            var isDeleted = String()
            messageReference.child(node).child(String.getstring(uid)).child(ChatParameter.isDeleted).observe(.value) { (snapshot) in
                if snapshot.exists() {
                    isDeleted = String.getstring(snapshot.value)
                    print(isDeleted)
                }
            }
            messageReference.child(node).observeSingleEvent(of: .value, with: { (SnapShot) in
                if SnapShot.exists() {
                    isDeleted.isEmpty ? self.messageReference.child(node).child(String.getstring(uid)).child(ChatParameter.isDeleted).setValue(senderId) :  self.messageReference.child(node).child(String.getstring(uid)).child(ChatParameter.isDeleted).setValue("\(isDeleted)_\(senderId)")
                }
            })
        }
    }
    //MARK:- Func for One to One Chat BackUp from remove isDeleted ID from Chat messages
    func chatBackUpOneToOne(msgclass:[ChatbackupOnetoOne] , senderid:String, receiverid:String) {
        if senderid == ChatParameter.emptyString || receiverid == ChatParameter.emptyString{
            print(ChatParameter.alertmessage)
            return
        }
        let node = self.createNode(senderId: senderid, receiverId: receiverid)
        for  index in msgclass {
            let uid = index.uid
            var isDeleted = [String]()
            messageReference.child(node).child(String.getstring(uid)).child(ChatParameter.isDeleted).observe(.value) { (snapshot) in
                if snapshot.exists() {
                    isDeleted = String.getstring(snapshot.value).components(separatedBy: ChatParameter.saparaterString)
                    if (isDeleted.firstIndex{$0 == String.getstring(senderid)} != nil) {
                        isDeleted.remove(at: isDeleted.firstIndex{$0 == String.getstring(senderid)}!)
                    }
                    print(isDeleted)
                }
            }
            messageReference.child(node).observeSingleEvent(of: .value, with: { (SnapShot) in
                if SnapShot.exists() {
                    self.messageReference.child(node).child(String.getstring(uid)).child(ChatParameter.isDeleted).setValue("\(isDeleted.joined(separator: ChatParameter.saparaterString))")
                }
            })
        }
    }
    //MARK:- Func for Block Users And Unblock Users
    func blockUnblockUser(receiverid:String ,blockCheck : Bool) {
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userState.userId) == ChatParameter.emptyString {
            print("sender Id Is empty please check you save the details in chatUser Default or not.")
            return
        }
        if String.getstring(receiverid) == ChatParameter.emptyString {
            print("Receiver id is empty please check you send the receiver id or not .")
            return
        }
        var blockUsers = [String]()
        self.userReference.child(String.getstring(userState.userId)).child(ChatParameter.blockUsers).observeSingleEvent(of: .value, with:  { (snapshot) in
            if snapshot.exists() {
                blockUsers = chatSharedInstanse.getStringArray(snapshot.value)
                print(blockUsers)
                if !blockCheck {
                    if (blockUsers.firstIndex{$0 == String.getstring(receiverid)} != nil) {
                        blockUsers.remove(at: blockUsers.firstIndex{$0 == String.getstring(receiverid)}!)
                    }
                }
                blockCheck ? blockUsers.append(receiverid) : print(blockCheck ? "User Bloacked Done" : "User Unblock Done")
                blockCheck ? self.userReference.child(String.getstring(userState.userId)).updateChildValues([ChatParameter.blockUsers: chatSharedInstanse.getStringArray(blockUsers)]) : self.userReference.child(String.getstring(userState.userId)).updateChildValues([ChatParameter.blockUsers:chatSharedInstanse.getStringArray(blockUsers)])
            } else {
                blockCheck ? blockUsers.append(receiverid) : print(blockCheck ? "User Bloacked Done" : "User Unblock Done")
                blockCheck ? self.userReference.child(String.getstring(userState.userId)).updateChildValues([ChatParameter.blockUsers: chatSharedInstanse.getStringArray(blockUsers)]) : self.userReference.child(String.getstring(userState.userId)).updateChildValues([ChatParameter.blockUsers:chatSharedInstanse.getStringArray(blockUsers)])
            }
        })
    }
    
    // MARK:- func for retrive All Users From Firebase
    func receiveAllUsers(users:@escaping (_ result: [UsersState]?) -> ()) -> Void{
        userReference.observe(.value) { [weak self](snapshot) in
            self?.users.removeAll()
            if snapshot.exists() {
                let usersDetails = chatSharedInstanse.getDictionary(snapshot.value)
                usersDetails.forEach {(key, value) in
                    let details = chatSharedInstanse.getDictionary(value)
                    let userState = UsersState(userdata: details)
                    self?.users.append(userState)
                    self?.users.sort { $0.onlineTime > $1.onlineTime }
                    self?.users = self?.users.uniqueArray(map: {$0.userId}) ?? []
                    users(self?.users)
                }
            }
        }
    }
    
    //MARK:- Func for createNode
    func createNode(senderId:String, receiverId:String) -> String {
        if senderId.isEmpty { return receiverId }
        return senderId > receiverId ? "\(senderId)_\(receiverId)" : "\(receiverId)_\(senderId)"
    }
    // MARK:- Check user Bloack or Not
    func checkUserBlock(users:[String]?) ->Bool{
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        return users?.contains(String.getstring(userState.userId)) ?? false
    }
    
    
    //MARK: - Upload one to one image
    func uploadImage(_ image:UIImage = UIImage(systemName: "shareplay")!,messageDic:MessageClass){
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userState.userId) == ChatParameter.emptyString {
            print("User Id Is empty please check you save the details in chatUser Default or not")
            return
        }
        if String.getstring(messageDic.receiverId) == ChatParameter.emptyString {
            print("Receiver id is empty please check you fill the receiver id in message on to send message or not")
            return
        }
        let messageNode = self.createNode(senderId: userState.userId ?? "", receiverId: messageDic.receiverId ?? "")
        let storageNodeRef = imagesStorageRef.child(messageNode)
        let imageName:String = String("\(NSDate().timeIntervalSince1970).png")
        if let imageData: Data = image.jpegData(compressionQuality: 0.4){
        let storageRef = storageNodeRef.child(imageName)
           storageRef.putData(imageData, metadata: nil
               , completion: { (metadata, error) in
                   if error != nil {
                       print("error")
//                        self.stopAnimating()
//                       showAlertWithTitleWithMessage(message: "Please try again later")
                       print("Please try again later")
                       return
                   }else{
//                       self.stopAnimating()
                   }
//                   let strPic:String = (metadata?.downloadURL()?.absoluteString)!
               storageRef.downloadURL { url, error in
                   if let error = error {
                    assertionFailure(error.localizedDescription)
                     }
                   messageDic.message = url?.absoluteURL.absoluteString ?? ""
                   self.sendMessage(messageDic: messageDic)
                    }
//                   print(metadata)
                   //self.imagePath = (metadata?.downloadURL()?.absoluteString)!
                   //self.sendMessageOnServer()
//                   print("\n\n\n\n\n\n ===download url : \(strPic)")

           })
     }
    }
    
    
    //MARK: - Upload one to one image
    func uploadGroupImage(_ image:UIImage = UIImage(systemName: "shareplay")!,messageDic:MessageClass, groupUsers:[String]){
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userState.userId) == ChatParameter.emptyString {
            print("User Id Is empty please check you save the details in chatUser Default or not")
            return
        }
        if String.getstring(messageDic.receiverId) == ChatParameter.emptyString {
            print("Receiver id is empty please check you fill the receiver id in message on to send message or not")
            return
        }
        let messageNode = self.createNode(senderId: userState.userId ?? "", receiverId: messageDic.receiverId ?? "")
        let storageNodeRef = imagesStorageRef.child(messageNode)
        let imageName:String = String("\(NSDate().timeIntervalSince1970).png")
        if let imageData: Data = image.jpegData(compressionQuality: 0.4){
        let storageRef = storageNodeRef.child(imageName)
           storageRef.putData(imageData, metadata: nil
               , completion: { (metadata, error) in
                   if error != nil {
                       print("error")
//                        self.stopAnimating()
//                       showAlertWithTitleWithMessage(message: "Please try again later")
                       print("Please try again later")
                       return
                   }else{
//                       self.stopAnimating()
                   }
//                   let strPic:String = (metadata?.downloadURL()?.absoluteString)!
               storageRef.downloadURL { url, error in
                   if let error = error {
                    assertionFailure(error.localizedDescription)
                     }
                   messageDic.message = url?.absoluteURL.absoluteString ?? ""
                   self.sendMessageToGroup(messageDic: messageDic, senderid: String.getstring(messageDic.senderId), groupid: String.getstring(messageDic.receiverId), users: groupUsers)
                    }
//                   print(metadata)
                   //self.imagePath = (metadata?.downloadURL()?.absoluteString)!
                   //self.sendMessageOnServer()
//                   print("\n\n\n\n\n\n ===download url : \(strPic)")

           })
     }
    }
    
    //MARK: - Upload Group Ggoup image
    func uploadGroupProfileImage(_ image:UIImage = UIImage(systemName: "person.circle")!, messageString: @escaping ((_ groupImage:String)->Void)){
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        let messageNode = "\(userState.userId ?? "")-GroupImage"
        
        let storageNodeRef = imagesStorageRef.child("GroupProfile").child(messageNode)
        let imageName:String = String("\(NSDate().timeIntervalSince1970).png")
        if let imageData: Data = image.jpegData(compressionQuality: 0.4){
        let storageRef = storageNodeRef.child(imageName)
           storageRef.putData(imageData, metadata: nil
               , completion: { (metadata, error) in
                   if error != nil {
                       print("error")
//                        self.stopAnimating()
//                       showAlertWithTitleWithMessage(message: "Please try again later")
                       print("Please try again later")
                       return
                   }else{
//                       self.stopAnimating()
                   }
//                   let strPic:String = (metadata?.downloadURL()?.absoluteString)!
               storageRef.downloadURL { url, error in
                   if let error = error {
                    assertionFailure(error.localizedDescription)
                     }
                   messageString(url?.absoluteURL.absoluteString ?? "")
                    }
//                   print(metadata)
                   //self.imagePath = (metadata?.downloadURL()?.absoluteString)!
                   //self.sendMessageOnServer()
//                   print("\n\n\n\n\n\n ===download url : \(strPic)")

           })
     }
    }
    
    //MARK: - Upload one to one image
    func uploadPdfFile(_ pdfUrl:URL,messageDic:MessageClass){
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userState.userId) == ChatParameter.emptyString {
            print("User Id Is empty please check you save the details in chatUser Default or not")
            return
        }
        if String.getstring(messageDic.receiverId) == ChatParameter.emptyString {
            print("Receiver id is empty please check you fill the receiver id in message on to send message or not")
            return
        }
        let messageNode = self.createNode(senderId: userState.userId ?? "", receiverId: messageDic.receiverId ?? "")
        let storageNodeRef = pdfStorageRef.child(messageNode)
        let pdfName:String = String("\(NSDate().timeIntervalSince1970).pdf")
        let _ = pdfUrl.startAccessingSecurityScopedResource()
        if let data = try? Data.init(contentsOf: pdfUrl) {
            let pdfData = data
            let storageRef = storageNodeRef.child(pdfName)
               storageRef.putData(pdfData, metadata: nil
                   , completion: { (metadata, error) in
                       if error != nil {
                           print("error")
    //                        self.stopAnimating()
    //                       showAlertWithTitleWithMessage(message: "Please try again later")
                           print("Please try again later")
                           return
                       }else{
    //                       self.stopAnimating()
                       }
    //                   let strPic:String = (metadata?.downloadURL()?.absoluteString)!
                   storageRef.downloadURL { url, error in
                       if let error = error {
                        assertionFailure(error.localizedDescription)
                         }
                       print(url)
                       messageDic.message = url?.absoluteURL.absoluteString ?? ""
                       self.sendMessage(messageDic: messageDic)
                        }
    //                   print(metadata)
                       //self.imagePath = (metadata?.downloadURL()?.absoluteString)!
                       //self.sendMessageOnServer()
    //                   print("\n\n\n\n\n\n ===download url : \(strPic)")

               })
            pdfUrl.stopAccessingSecurityScopedResource()
        }
    }
    
    
    //MARK: - Upload one to one Pdf
    func uploadGroupPdfFile(_ pdfUrl:URL,messageDic:MessageClass, groupUsers:[String]){
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userState.userId) == ChatParameter.emptyString {
            print("User Id Is empty please check you save the details in chatUser Default or not")
            return
        }
        if String.getstring(messageDic.receiverId) == ChatParameter.emptyString {
            print("Receiver id is empty please check you fill the receiver id in message on to send message or not")
            return
        }
        let messageNode = self.createNode(senderId: userState.userId ?? "", receiverId: messageDic.receiverId ?? "")
        let storageNodeRef = pdfStorageRef.child(messageNode)
        let pdfName:String = String("\(NSDate().timeIntervalSince1970).pdf")
        let _ = pdfUrl.startAccessingSecurityScopedResource()
        if let data = try? Data.init(contentsOf: pdfUrl) {
            let pdfData = data
            let storageRef = storageNodeRef.child(pdfName)
               storageRef.putData(pdfData, metadata: nil
                   , completion: { (metadata, error) in
                       if error != nil {
                           print("error")
    //                        self.stopAnimating()
    //                       showAlertWithTitleWithMessage(message: "Please try again later")
                           print("Please try again later")
                           return
                       }else{
    //                       self.stopAnimating()
                       }
    //                   let strPic:String = (metadata?.downloadURL()?.absoluteString)!
                   storageRef.downloadURL { url, error in
                       if let error = error {
                        assertionFailure(error.localizedDescription)
                         }
                       messageDic.message = url?.absoluteURL.absoluteString ?? ""
                       self.sendMessageToGroup(messageDic: messageDic, senderid: String.getstring(messageDic.senderId), groupid: String.getstring(messageDic.receiverId), users: groupUsers)
                        }
    //                   print(metadata)
                       //self.imagePath = (metadata?.downloadURL()?.absoluteString)!
                       //self.sendMessageOnServer()
    //                   print("\n\n\n\n\n\n ===download url : \(strPic)")

               })
            pdfUrl.stopAccessingSecurityScopedResource()
        }
    }
    
    //MARK:- Function For Delete message One to One Only Delete Perticular Messege
    func deleteGroup(user:ResentUsers?) {
        if String.getstring(user?.createdBy).isEmpty {
            print("createdBy Is empty please check you save the details in chatUser Default or not.")
            return
        }
        if String.getstring(user?.receiverId).isEmpty {
            print("Receiver id is empty please check you send the receiver id in mesaage details  or not .")
            return
        }
        let groupCreatedBy = String.getstring(user?.createdBy)
        let groupReferenceString = String.getstring(user?.receiverId)
        let groupNodeReference = groupReference.child(groupCreatedBy).child(groupReferenceString)
        let groupInfoReference = groupNodeReference.child(ChatParameter.information)
        groupInfoReference.observe(.value) { (snapshot) in
            if snapshot.exists() {
                let groupInfo = chatSharedInstanse.getDictionary(snapshot.value)
                let group = GroupModel(data: groupInfo)
                self.deleteGroupFromRecentMessage(group:group,user: group.user)
                self.deleteGroupFromRecentMessage(group:group,user: group.blockedUser)
                self.deleteGroupFromUserState(group:group)
                self.deleteGroupFromMessages(group:group)
                groupNodeReference.removeValue{ error, _ in
                    print(error)
                }
            
            }
        }
//        messageReference.child(node).observeSingleEvent(of: .value, with: { (SnapShot) in
//            if SnapShot.exists() {
//                if isDeleted.isEmpty {
//                    reference.setValue(String.getstring(message?.senderId))
//                    return
//                }
//                reference.setValue("\(isDeleted)_\(String.getstring(message?.senderId))")
//            }
//        })
    }
    
    func deleteGroupFromRecentMessage(group:GroupModel,user:[String]?){
        let allUser = chatSharedInstanse.getArray(user)
        allUser.forEach{ user in
            let userReference = recentReference.child(String.getstring(user))
            userReference.child(String.getstring(group.userid)).removeValue{ error, _ in
                print(error)
            }
        }
    }
    
    func deleteGroupFromUserState(group:GroupModel){
            let userStateReference = userReference.child(String.getstring(group.userid))
            userStateReference.removeValue{ error, _ in
                print(error)
        }
    }
    
    func deleteGroupFromMessages(group:GroupModel){
            let userStateReference = messageReference.child(String.getstring(group.userid))
            userStateReference.removeValue{ error, _ in
                print(error)
        }
    }
}


extension Date {
    func isToday() -> Bool {
        return self.isEqualToDateIgnoringTime(date: Date())
    }
    
    func isTomorrow() -> Bool {
        return self.isEqualToDateIgnoringTime(date: Date.dateTomorrow())
    }
    
    func isYesterday() -> Bool {
        return self.isEqualToDateIgnoringTime(date: Date.dateYesterday())
    }
    func toString(withFormat format: String = "EEEE ، d MMMM yyyy") -> String {
        
        let dateFormatter         = DateFormatter()
        dateFormatter.locale      = Locale.current
        dateFormatter.timeZone    = TimeZone.init(abbreviation: "LOCAL")
        dateFormatter.dateFormat  = format
        let strMonth = dateFormatter.string(from: self)
        
        return strMonth
    }
    static func dateFromString(date: String, withCurrentFormat format:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.date(from: date) ?? Date()
    }
    func toMillis() -> Int! {
        return Int(self.timeIntervalSince1970*1000)
    }
    func isEqualToDateIgnoringTime(date: Date) -> Bool {
        let dateComponent1 =  Date.currentCalendar().dateComponents([.year, .month, .day, .weekOfMonth, .hour, .minute, .second, .weekday, .weekdayOrdinal], from: self)
        
        let dateComponent2 = Date.currentCalendar().dateComponents([.year, .month, .day, .weekOfMonth, .hour, .minute, .second, .weekday, .weekdayOrdinal], from: date)
        
        return (dateComponent1.year == dateComponent2.year) && (dateComponent1.month == dateComponent2.month) && (dateComponent1.day == dateComponent2.day)
    }
    
    static func currentCalendar() -> Calendar {
        var sharedCalendar: Calendar? = nil
        if sharedCalendar == nil {
            sharedCalendar = Calendar.autoupdatingCurrent
        }
        return sharedCalendar!
    }
    static func dateTomorrow() -> Date {
        return Date.dateWithDaysFromNow(days: 1)
    }
    static func dateWithDaysFromNow(days: NSInteger) -> Date {
        return Date().dateByAddingDays(days: days)
    }
    static func dateYesterday() -> Date {
           return Date.dateWithDaysBeforeNow(days: 1)
       }
    static func dateWithDaysBeforeNow(days: NSInteger) -> Date {
           return Date().dateBySubstractingDays(days: days)
       }
    func dateBySubstractingDays(days: NSInteger) -> Date {
           return self.dateByAddingDays(days: days * -1)
       }
    func day1() -> NSInteger {
        let dateComponent = Date.currentCalendar().dateComponents([.year, .month, .day, .weekOfMonth, .hour, .minute, .second, .weekday, .weekdayOrdinal], from: self)
        return dateComponent.day!
    }
    func month1() -> NSInteger {
        let dateComponent = Date.currentCalendar().dateComponents([.year, .month, .day, .weekOfMonth, .hour, .minute, .second, .weekday, .weekdayOrdinal], from: self)
        return dateComponent.month!
    }
    func lastDayOfMonth() -> Date {
        let calendar = Calendar.current
        let dayRange = calendar.range(of: .day, in: .month, for: self)
        let dayCount = dayRange?.count
        var comp = calendar.dateComponents([.year, .month, .day], from: self)
        comp.day = dayCount
        return calendar.date(from: comp)!
    }
    func dateByAddingDays(days: NSInteger) -> Date {
        var dateComponents = DateComponents()
        dateComponents.day = days
        if self.day1() == 1 && days < 0 {
            dateComponents.month = -1
        } else if self.day1() == self.lastDayOfMonth().day1() && days >= 1 {
            dateComponents.month = 1
            switch self.month1() {
            case 2:
                dateComponents.day = dateComponents.day! + 3
            case 4,6,9,11:
                dateComponents.day = dateComponents.day! + 1
            default:
                break
            }
        }
        
        let newDate = Calendar.current.date(byAdding: dateComponents, to: self, wrappingComponents: true)
        return newDate!
    }
}

extension Array {
    func uniqueArray<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>()
        var arrayOrdered = [Element]()
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        return arrayOrdered
    }
}
