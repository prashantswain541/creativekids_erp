//
//  ChatHalperExt.swift
//  ChatHalperDemo
//
//  Created by Fluper on 10/05/20.
//  Copyright © 2020 Fluper. All rights reserved.
//

import Foundation

//MARK:- Extension for Chat_halper for Group Chat
extension ChatHalper {
    //MARK:- Function For Create Group for Group Chat
    func createGroup(groupdic:Dictionary<String, Any> , userdic:[String]) {
        var groupsDetails = groupdic
        let ref =  self.groupReference.child(String.getstring(groupdic[ChatParameter.CreatedBy])).childByAutoId()
        groupsDetails[ChatParameter.userId] = String.getstring(ref.key)
        groupsDetails[ChatParameter.receiverId] = String.getstring(ref.key)
        groupsDetails[ChatParameter.timeStamp] = String.getstring(Int(Date().timeIntervalSince1970 * 1000))
        groupsDetails[ChatParameter.lastmessage] = "you are added in this group"
        let details:[String : Any] = [ChatParameter.information : chatSharedInstanse.getDictionary(groupsDetails) , ChatParameter.Users : chatSharedInstanse.getStringArray(userdic) ]
        ref.setValue(details)
        self.recentReference.child(String.getstring(groupdic[ChatParameter.CreatedBy])).child(String.getstring(groupsDetails[ChatParameter.userId])).updateChildValues(groupsDetails)
        for users in userdic {
            self.recentReference.child(String.getstring(users)).child(String.getstring(groupsDetails[ChatParameter.userId])).updateChildValues(groupsDetails)
        }
        
    }
    //MARK:- Function For Send message to group Chat
    func sendMessagetoGroup(dic:Dictionary<String, Any> , senderid :String , groupid:String ) {
        let node = String.getstring(groupid)

        if node == ChatParameter.emptyString {
            print(ChatParameter.alertmessage)
            return
        }
        messageReference.child(node).childByAutoId().setValue(chatSharedInstanse.getDictionary(dic))
    }

    //MARK:- Func For Receive Message from Group Chat
    func receivceMessagefromGroup(groupid :String , receiverId:String, message:@escaping (_ result: [MessageClass]?, _ chatbackup : [MessageClass]?) -> ()) -> Void {
        self.messageclass.removeAll()
        let node =  String.getstring(groupid)
        if node == ChatParameter.emptyString {
            print(ChatParameter.alertmessage)
            return
        }
        messageReference.child(node).observe(.value) { (snapshot) in
            if snapshot.exists() {
                let msgs = chatSharedInstanse.getDictionary(snapshot.value)
                self.messageclass.removeAll()
                self.chatBackupOnetoOne.removeAll()
                msgs.forEach {(key, value) in
                    let dic = chatSharedInstanse.getDictionary(value)
                    let messageUser = chatSharedInstanse.getDictionary(dic[ChatParameter.groupMessageSeen])
                    if !String.getstring(messageUser[String.getstring(receiverId)]).isEmpty{
                        let isDeleted =  String.getstring(dic[ChatParameter.isDeleted]).components(separatedBy: ChatParameter.saparaterString)

                        if !(isDeleted.contains(String.getstring(self.userDetails[ChatParameter.userId]))) {

                            self.messageclass.append(MessageClass(uid: String.getstring(key), messageData: dic))
                            self.messageclass.sort{ $0.sendingTime < $1.sendingTime }
                        } else {
                            self.chatBackupOnetoOne.append(MessageClass(uid: String.getstring(key), messageData: dic))
                            self.chatBackupOnetoOne.sort{ $0.sendingTime < $1.sendingTime }
                        }
                    }
                    
                }
            }
            message(self.messageclass , self.chatBackupOnetoOne)
        }
    }
    
    //MARK:- Function For Updated Group Infromarion for Every users
    func  updateGroup(userid:String ,groupid :String, message:@escaping (_ result : GroupModel) -> ()) -> Void{
        if userid == ChatParameter.emptyString || groupid == ChatParameter.emptyString {
            print(ChatParameter.alertmessage)
            return
        }
        self.messageReference.child(userid).child(groupid).observe( .value) { (snapshot) in
            if snapshot.exists() {
                let msgs = chatSharedInstanse.getDictionary(snapshot.value)
                let status = GroupModel(adminId: String.getstring(msgs[ChatParameter.userId]), adminName: "",userid: String.getstring(msgs[ChatParameter.userId]), name: String.getstring(msgs[ChatParameter.name]), imageUrl: String.getstring(msgs[ChatParameter.profileImage]), user: chatSharedInstanse.getStringArray(msgs[ChatParameter.Users]),createdBy: String.getstring(msgs[ChatParameter.name]))
                message(status)
            }
        }
    }
    //MARK:- Func For send Resent Users For Group Chat  Create a Loop And Send Resend Users Information
    func groupResentUser(lastmessage:String, groupid:String , adminid :String  , name:String , profile_image:String , readState :String) {
        let timeStamp = String.getstring(Int(Date().timeIntervalSince1970 * 1000))
        if adminid == ChatParameter.emptyString || groupid == ChatParameter.emptyString{
            print(ChatParameter.alertmessage)
            return
        }
        let dic = [ ChatParameter.userId:      String.getstring(groupid),
                    ChatParameter.name:    String.getstring(name),
                    ChatParameter.profileImage: String.getstring(profile_image),
                    ChatParameter.lastmessage : String.getstring(lastmessage),
                    ChatParameter.timeStamp : String.getstring(timeStamp)
        ]
        self.updateGroup(userid: adminid, groupid: groupid) { (value) in
            for users in chatSharedInstanse.getStringArray(value.user) {
                self.recentReference.child(String.getstring(users)).child(String.getstring(groupid)).updateChildValues(dic)
            }
        }
    }
    //MARK:- Func For seenUnseenmessageforGroup
    func seenUnseenmessageforGroup( groupid:String , adminid :String  , userid:String, readState :String) {
        if String.getstring(groupid) == ChatParameter.emptyString || String.getstring(adminid) == ChatParameter.emptyString{
            print(ChatParameter.alertmessage)
            return
        }
        self.groupReference.child(adminid).child(groupid).child(ChatParameter.UserSeenCount).child(userid).updateChildValues([ChatParameter.readState:readState])
    }
    //MARK:- Func for Delete message from group Only One
    func deletemessagefromgroup(groupid:String , uid :String , senderid:String) {
        if String.getstring(groupid) == ChatParameter.emptyString || String.getstring(uid) == ChatParameter.emptyString{
            print(ChatParameter.alertmessage)
            return
        }
        var isDeleted = String()
        messageReference.child(groupid).child(uid).child(ChatParameter.isDeleted).observe(.value) { (snapshot) in
            if snapshot.exists() {
                isDeleted = String.getstring(snapshot.value)
                print(isDeleted)
            }
        }
        messageReference.child(groupid).observeSingleEvent(of: .value, with: { (SnapShot) in
            if SnapShot.exists() {
                isDeleted.isEmpty ? self.messageReference.child(groupid).child(uid).child(ChatParameter.isDeleted).setValue(senderid) :  self.messageReference.child(groupid).child(uid).child(ChatParameter.isDeleted).setValue("\(isDeleted)_\(senderid)")
            }
        })
    }
    //MARK:- Func for Delete message from group Only One
    func starmessageIngroup(groupid:String , uid :String , senderid:String) {
        if String.getstring(groupid) == ChatParameter.emptyString || String.getstring(uid) == ChatParameter.emptyString{
            print(ChatParameter.alertmessage)
            return
        }
        var starmessage = String()
        self.messageReference.child(groupid).child(uid).child(ChatParameter.starmessage).observe(.value) { (snapshot) in
            if snapshot.exists() {
                starmessage = String.getstring(snapshot.value)
                print(starmessage)
            }
        }
        self.messageReference.child(groupid).observeSingleEvent(of: .value, with: { (SnapShot) in
            if SnapShot.exists() {
                starmessage.isEmpty ? self.messageReference.child(groupid).child(uid).child(ChatParameter.starmessage).setValue(senderid) :  self.messageReference.child(groupid).child(uid).child(ChatParameter.starmessage).setValue("\(starmessage)_\(senderid)")
            }
        })
    }
    //MARK:- Func for Clear All Messages From group
    func clearChatFromGroup(msgclass:[MessageClass] , senderid:String, groupid:String) {
        for  index in msgclass {
            let uid = index.uid
            var isDeleted = String()
            self.messageReference.child(groupid).child(uid ?? "Shubham Kaliyar").child(ChatParameter.isDeleted).observe(.value) { (snapshot) in
                if snapshot.exists() {
                    isDeleted = String.getstring(snapshot.value)
                    print(isDeleted)
                }
            }
            self.messageReference.child(groupid).observeSingleEvent(of: .value, with: { (SnapShot) in
                if SnapShot.exists() {
                    isDeleted.isEmpty ? self.messageReference.child(groupid).child(uid ?? "Shubham Kaliyar").child(ChatParameter.isDeleted).setValue(senderid) :  self.messageReference.child(groupid).child(uid ?? "Shubham Kaliyar").child(ChatParameter.isDeleted).setValue("\(isDeleted)_\(senderid)")
                }
            })
        }
    }
    //MARK:- Func for One to One Chat BackUp
    func chatBackUpGroup(msgclass:[ChatbackupOnetoOne] , senderid:String, groupid:String) {
        if groupid == ChatParameter.emptyString || senderid == ChatParameter.emptyString{
            print(ChatParameter.alertmessage)
            return
        }
        for  index in msgclass {
            let uid = index.uid
            var isDeleted = [String]()
            self.messageReference.child(groupid).child(uid ?? "Shubham Kaliyar").child(ChatParameter.isDeleted).observe(.value) { (snapshot) in
                if snapshot.exists() {
                    isDeleted = String.getstring(snapshot.value).components(separatedBy: ChatParameter.saparaterString)
                    if (isDeleted.firstIndex{$0 == String.getstring(senderid)} != nil) {
                        isDeleted.remove(at: isDeleted.firstIndex{$0 == String.getstring(senderid)}!)
                    }
                    print(isDeleted)
                }
            }
            self.messageReference.child(groupid).observeSingleEvent(of: .value, with: { (SnapShot) in
                if SnapShot.exists() {
                    self.messageReference.child(groupid).child(uid ?? "Shubham Kaliyar").child(ChatParameter.isDeleted).setValue("\(isDeleted.joined(separator: "_"))")
                }
            })
        }
    }
    //MARK:- Function For Group Read Messages
    func readMessageforGroup(groupid :String , readstate:String , counters:@escaping (_ result: Int) -> ()) {
        if groupid == ChatParameter.emptyString {
            print(ChatParameter.alertmessage)
            return
        }
        var countreadMessage = 0
        self.messageReference.child(groupid).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                countreadMessage = 0
                let msgs = chatSharedInstanse.getDictionary(snapshot.value)
                msgs.forEach {(key, value) in
                    let dic = chatSharedInstanse.getDictionary(value)
                    let isDeleted =  String.getstring(dic[ChatParameter.isDeleted]).components(separatedBy: ChatParameter.saparaterString)
                    if !(isDeleted.contains(String.getstring(self.userDetails[ChatParameter.userId]))) {
                        countreadMessage += 1
                    }
                }
            }
            counters(Int(countreadMessage) - Int.getint(readstate))
        })
    }
    //MARK:- Function For Group Read Messages from users perticular
    func  readMessagecountforGroupusers(adminid :String , groupid:String , userid:String , message:@escaping (_ id: String, _ status: String) -> ()) -> Void{
        if userid == ChatParameter.emptyString ||  groupid == ChatParameter.emptyString || adminid == ChatParameter.emptyString {
            print(ChatParameter.alertmessage)
            return
        }
        self.groupReference.child(adminid).child(groupid).child(ChatParameter.UserSeenCount).child(userid).observe( .value) { (snapshot) in
            if snapshot.exists() {
                let msgs = chatSharedInstanse.getDictionary(snapshot.value)
                message(groupid,  String.getstring(msgs[ChatParameter.readState]))
            } }
    }
}

extension ChatHalper {
    func createGroup(group:GroupModel){
        let groupsDetails = group
        let ref =  self.groupReference.child(String.getstring(groupsDetails.createdBy)).childByAutoId()
        groupsDetails.userid = String.getstring(ref.key)
        var groupDic = groupsDetails.createGroupDic(groupObject:groupsDetails)
        groupDic[ChatParameter.receiverid] = String.getstring(ref.key)
        groupDic[ChatParameter.lastmessage] = "you are added in this group"
        let details:[String : Any] = [ChatParameter.information : chatSharedInstanse.getDictionary(groupDic) , ChatParameter.Users : chatSharedInstanse.getStringArray(groupsDetails.user), ChatParameter.blockUsers: chatSharedInstanse.getStringArray(groupsDetails.blockedUser)]
        ref.setValue(details)
        
        saveGroupReference(group:groupsDetails)
//        self.recentReference.child(String.getstring(groupDic[ChatParameter.CreatedBy])).child(String.getstring(groupDic[ChatParameter.userId])).updateChildValues(groupDic)
        let groupAdminId = String.getstring(groupsDetails.adminId)
        let groupId = String.getstring(groupsDetails.userid)
        
        let message = MessageClass()
        message.receiverId = String.getstring(groupsDetails.userid)
        message.senderId = String.getstring(groupsDetails.adminId)
        message.mediaType = .create
        message.message =  String.getstring(groupDic[ChatParameter.lastmessage])
        message.receiverName = String.getstring(groupsDetails.name)
        sendMessageToGroup(messageDic:message, senderid :groupAdminId  , groupid: groupId,users: chatSharedInstanse.getStringArray(groupsDetails.user))
    }
    func saveGroupReference(group:GroupModel){
        let gropDetails = UsersState()
        gropDetails.name = String.getstring(group.name)
        gropDetails.userId = String.getstring(group.userid)
        gropDetails.profile_image = String.getstring(group.imageUrl)
        gropDetails.messageReferanceType = MessageReferanceType.group.rawValue
        gropDetails.createdBy = String.getstring(group.createdBy)
        self.userReference.child(gropDetails.userId ?? "").setValue(gropDetails.createDictonary(objects:gropDetails))
        
    }
    
    //MARK:- Function For Send message to group Chat
    func sendMessageToGroup(messageDic:MessageClass, senderid :String , groupid:String,users:[String]) {
        let node = String.getstring(groupid)

        if node == ChatParameter.emptyString {
            print(ChatParameter.alertmessage)
            return
        }
        
        let messageReference =  messageReference.child(node).childByAutoId()
        messageDic.uid = String.getstring(messageReference.key)
        messageDic.senderId = String.getstring(senderid)
        var message = messageDic.createDictonary(objects: messageDic)
        let seenDict = userGruopMessageSeen(users:users)
        message[ChatParameter.groupMessageSeen] = seenDict
        print("send message to node \(node) with key \(messageReference.key ?? "") with message \(message)")
        messageReference.setValue(message)
        resentUserGroup(messageDetails:messageDic,user:users)
    }
    
    //MARK:- Func For send Resent Users and retrived data On resent Users Submit Data to Users Every User Submit 2 Node  Sender and Receiver
    func resentUserGroup(messageDetails:MessageClass,user:[String]) {
        let senderId = messageDetails.senderId ?? ""
        let receiverId = messageDetails.receiverId ?? ""
        if String.getstring(senderId) == ChatParameter.emptyString || String.getstring(receiverId) == ChatParameter.emptyString {
            print(ChatParameter.alertmessage)
            return
        }
        
        let receiverDetails = [ ChatParameter.receiverId: String.getstring(receiverId),
                                ChatParameter.senderid : String.getstring(senderId),
                                ChatParameter.uid : String.getstring(messageDetails.uid),
                                ChatParameter.readStatus : String.getstring(messageDetails.mediaType?.rawValue) == "create" ? "seen":"sent",
                                ChatParameter.lastMessage : String.getstring(messageDetails.message),
                                ChatParameter.mediaType    : String.getstring(messageDetails.mediaType?.rawValue) ,
                                ChatParameter.timeStamp : String.getstring(Int(Date().timeIntervalSince1970 * 1000))
        ]
        for users in chatSharedInstanse.getStringArray(user) {
            self.recentReference.child(String.getstring(users)).child(String.getstring(messageDetails.receiverId)).updateChildValues(receiverDetails)
        }
    }
    
    //MARK:- Function For Read message Count For Resent Screen Batch
    func unReadGroupMessageCount(senderId :String , receiverId:String, readstate:String , counters:@escaping (_ result: Int) -> ()) {
        let messageNode = String.getstring(receiverId)
        print(senderId)
        
        messageReference.child(messageNode).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let msgs = chatSharedInstanse.getDictionary(snapshot.value)
                let filteredMessage = msgs.filter{String.getstring(chatSharedInstanse.getDictionary($0.value)[ChatParameter.senderId]) != senderId && !([messageType.create.rawValue,messageType.join.rawValue,messageType.kickOutByAdmin.rawValue, messageType.leave.rawValue].contains(String.getstring(chatSharedInstanse.getDictionary($0.value)[ChatParameter.mediaType])))}
                var unreadmessageCount = 0
                for message in filteredMessage {
                    let messageDic = chatSharedInstanse.getDictionary(message.value)
                    let messageSeen = chatSharedInstanse.getDictionary(messageDic[ChatParameter.groupMessageSeen])
                    if !(String.getstring(messageSeen[senderId]).isEmpty){
                        if !(String.getstring(messageSeen[senderId]) == "seen"){
                            unreadmessageCount += 1
                        }
                    }
                }
                counters(unreadmessageCount)
            }
        })
    }
    
    //MARK:- Received All User References
    func retrivedAllUser(allUser: @escaping ([UsersState])->Void){
        let userDetails = UsersState(userdata: chatUserDefault.getuserDetails())
        userReference.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists(){
                let userInstances = chatSharedInstanse.getDictionary(snapshot.value)
                let userStates = userInstances.map{UsersState(userdata: chatSharedInstanse.getDictionary($0.value))}
                let filterOnlyUser = userStates.filter{String.getstring($0.messageReferanceType) == MessageReferanceType.oneToOne.rawValue && String.getstring($0.userId) != String.getstring(userDetails.userId) && String.getstring($0.schoolId) == String.getstring(userDetails.schoolId)}
                allUser(filterOnlyUser)
            }
        }
    }
    
    
    //MARK:- User Group Read message
    func userGruopMessageSeen(users:[String]) -> [String:String]{
        var seenDict:[String:String] = [:]
        for user in users{
            seenDict[user] = "sent"
        }
        return seenDict
    }

    //MARK:- User Group Read message
    func getGroupDetail(groupCreatedBy:String, groupId:String,group:@escaping (_ group: GroupModel) -> ()){
        let gruopNode = String.getstring(groupCreatedBy)
        let gruopIdNode = String.getstring(groupId)
        if gruopNode.isEmpty && gruopIdNode.isEmpty{
            print("Group created or group id can not be empty")
            return
        }
        groupReference.child(gruopNode).child(gruopIdNode).observeSingleEvent(of:.value) { (snapshot) in
            if snapshot.exists(){
                let groupDetail = chatSharedInstanse.getDictionary(snapshot.value)
                if !groupDetail.isEmpty{
                    let groupDetail = chatSharedInstanse.getDictionary(groupDetail[ChatParameter.information])
                    let groupState = GroupModel(data: groupDetail)
                    group(groupState)
                }
            }
        }
    }
    
    //MARK:- User Group Read message
    func readGroupMessage(groupid :String , receiverId:String,messages:[MessageClass]?) {
        let messageNode = String.getstring(groupid)
        let userId = String.getstring(receiverId)
          for message in messages ?? [] {
              if !userId.isEmpty{
                  if userId != message.senderId {
//                      let userDic = chatSharedInstanse.getDictionary( messageReference.child(messageNode).child(String.getstring(message.uid)).child(ChatParameter.groupMessageSeen))
                      messageReference.child(messageNode).child(String.getstring(message.uid)).child(ChatParameter.groupMessageSeen).updateChildValues([userId:"seen"])
              }
            }
          }
      }
    
    func unreadMessage(userdetails:[String:Any], referenceType:String?, unreadCount:@escaping (_ result:Int)->Void){
        let userState = UsersState(userdata: chatUserDefault.getuserDetails())
            if String.getstring(referenceType) == "oneToOne"{
                self.unReadMessageCount(senderId: String.getstring(userState.userId) , receiverId: String.getstring(userdetails[ChatParameter.receiverId]), readstate: String.getstring(userdetails[ChatParameter.readState]), counters:  { (readCount) in
                    let unreadMessageCount = readCount
                    unreadCount(unreadMessageCount)
                })
            }else if String.getstring(referenceType) == "group"{
                self.unReadGroupMessageCount(senderId: String.getstring(userState.userId), receiverId: String.getstring(userdetails[ChatParameter.receiverId]), readstate: String.getstring(userdetails[ChatParameter.readState]), counters: { (groupMessageReadCount) in
                    let unreadMessageCount = groupMessageReadCount
                    unreadCount(unreadMessageCount)
                })
            }
    }
    
    //MARK:- Add More Member into Group By admin
    func addMemberToGroup(groupCreatedBy:String, groupId:String ,addUsers:[String],completion: @escaping (()->Void)){
        let userDetails = UsersState(userdata: chatUserDefault.getuserDetails())
        let userId = String.getstring(userDetails.userId)
        if userId.isEmpty{
            print("user id can not be empty")
            return completion()
        }
        let gruopNode = String.getstring(groupCreatedBy)
        let gruopIdNode = String.getstring(groupId)
        if gruopNode.isEmpty && gruopIdNode.isEmpty{
            print("Group created or group id can not be empty")
            return completion()
        }
        self.getGroupDetail(groupCreatedBy: gruopNode, groupId: gruopIdNode) { [weak self] group in
            if userId != String.getstring(group.adminId){
                print("only admin can add member to this group")
                return completion()
            }
            self?.groupReference.child(gruopNode).child(gruopIdNode).updateChildValues([ChatParameter.Users:chatSharedInstanse.getStringArray(group.user) + addUsers])
            self?.groupReference.child(gruopNode).child(gruopIdNode).child(ChatParameter.information).updateChildValues([ChatParameter.Users:chatSharedInstanse.getStringArray(group.user) + addUsers])
            
            let message = MessageClass()
            message.receiverId = String.getstring(group.userid)
            message.senderId = String.getstring(group.adminId)
            message.senderName = String.getstring(group.adminName)
            message.mediaType = .join
            message.message =  addUsers.joined(separator: ",")
            message.receiverName = String.getstring(group.name)
            self?.sendMessageToGroup(messageDic:message, senderid :String.getstring(group.adminId)  , groupid: gruopIdNode,users: chatSharedInstanse.getStringArray(group.user) + addUsers)
            completion()
        }
    }
    
    //MARK:- Block Member into Group By admin
    func blockMemberInGroup(groupCreatedBy:String, groupId:String ,blockUsers:[String],completion: @escaping (()->Void)){
        let userDetails = UsersState(userdata: chatUserDefault.getuserDetails())
        let userId = String.getstring(userDetails.userId)
        if userId.isEmpty{
            print("user id can not be empty")
            return completion()
        }
        let gruopNode = String.getstring(groupCreatedBy)
        let gruopIdNode = String.getstring(groupId)
        if gruopNode.isEmpty && gruopIdNode.isEmpty{
            print("Group created or group id can not be empty")
            return completion()
        }
        self.getGroupDetail(groupCreatedBy: gruopNode, groupId: gruopIdNode) { [weak self] group in
            if userId != String.getstring(group.adminId){
                print("only admin can add member to this group")
                return completion()
            }
            self?.groupReference.child(gruopNode).child(gruopIdNode).updateChildValues([ChatParameter.blockUsers:chatSharedInstanse.getStringArray(group.blockedUser) + blockUsers])
            
            completion()
        }
    }
    
    //MARK:- Block Member into Group By admin
    func unblockMemberInGroup(groupCreatedBy:String, groupId:String ,unblockUsers:[String],completion: @escaping (()->Void)){
        let userDetails = UsersState(userdata: chatUserDefault.getuserDetails())
        let userId = String.getstring(userDetails.userId)
        if userId.isEmpty{
            print("user id can not be empty")
            return completion()
        }
        let gruopNode = String.getstring(groupCreatedBy)
        let gruopIdNode = String.getstring(groupId)
        if gruopNode.isEmpty && gruopIdNode.isEmpty{
            print("Group created or group id can not be empty")
            return completion()
        }
        self.getGroupDetail(groupCreatedBy: gruopNode, groupId: gruopIdNode) { [weak self] group in
            if userId != String.getstring(group.adminId){
                print("only admin can add member to this group")
                return completion()
            }
            let blockedUser = group.blockedUser?.filter{String.getstring($0) != unblockUsers[0]}
            self?.groupReference.child(gruopNode).child(gruopIdNode).updateChildValues([ChatParameter.blockUsers:chatSharedInstanse.getStringArray(blockedUser)])
            completion()
        }
    }
    
    //MARK: - Remove member from group By admin
    func removeMemberFromGroup(groupCreatedBy:String, groupId:String ,removeUsers:[String]){
        let userDetails = UsersState(userdata: chatUserDefault.getuserDetails())
        let userId = String.getstring(userDetails.userId)
        if userId.isEmpty{
            print("user id can not be empty")
            return
        }
        let gruopNode = String.getstring(groupCreatedBy)
        let gruopIdNode = String.getstring(groupId)
        if gruopNode.isEmpty && gruopIdNode.isEmpty{
            print("Group created or group id can not be empty")
            return
        }
        self.getGroupDetail(groupCreatedBy: gruopNode, groupId: gruopIdNode) { [weak self] group in
            if userId != String.getstring(group.adminId){
                print("only admin can add member to this group")
                return
            }
            let message = MessageClass()
            message.receiverId = String.getstring(group.userid)
            message.senderId   = String.getstring(group.adminId)
            message.senderName = String.getstring(group.adminName)
            message.mediaType  = .kickOutByAdmin
            message.message    =  removeUsers.joined(separator: ",")
            message.receiverName = String.getstring(group.name)
            self?.sendMessageToGroup(messageDic:message, senderid :String.getstring(group.adminId)  , groupid: gruopIdNode,users: chatSharedInstanse.getStringArray(group.user))
            let filteredUser = chatSharedInstanse.getStringArray(group.user).filter{ !(removeUsers.contains($0))}
            self?.groupReference.child(gruopNode).child(gruopIdNode).updateChildValues([ChatParameter.Users:filteredUser])
            self?.groupReference.child(gruopNode).child(gruopIdNode).child(ChatParameter.information).updateChildValues([ChatParameter.Users:filteredUser])
        }
    }
    
    //MARK: - Remove member from group By admin
    func leaveMemberFromGroup(groupCreatedBy:String, groupId:String){
        let userDetails = UsersState(userdata: chatUserDefault.getuserDetails())
        let userId = String.getstring(userDetails.userId)
        if userId.isEmpty{
            print("user id can not be empty")
            return
        }
        let gruopNode = String.getstring(groupCreatedBy)
        let gruopIdNode = String.getstring(groupId)
        if gruopNode.isEmpty && gruopIdNode.isEmpty{
            print("Group created or group id can not be empty")
            return
        }
        self.getGroupDetail(groupCreatedBy: gruopNode, groupId: gruopIdNode) { [weak self] group in
            let message = MessageClass()
            message.receiverId = String.getstring(group.userid)
            message.senderId   = userId
            message.senderName = String.getstring(userDetails.name)
            message.mediaType  = .leave
            message.message    =  userId
            message.receiverName = String.getstring(group.name)
            self?.sendMessageToGroup(messageDic:message, senderid :String.getstring(userId), groupid: gruopIdNode,users: chatSharedInstanse.getStringArray(group.user))
            let filteredUser = chatSharedInstanse.getStringArray(group.user).filter{ $0 != userId}
            self?.groupReference.child(gruopNode).child(gruopIdNode).updateChildValues([ChatParameter.Users:filteredUser])
            self?.groupReference.child(gruopNode).child(gruopIdNode).child(ChatParameter.information).updateChildValues([ChatParameter.Users:filteredUser])
        }
    }
    
    //MARK: - Edit Group Detail from group By admin
    func updateGroupDetails(group: GroupModel, completion :@escaping (()->Void)){
        let gruopIdNode = String.getstring(group.userid)
        let gruopNode = String.getstring(group.createdBy)
        if gruopNode.isEmpty && gruopIdNode.isEmpty{
            print("Group created or group id can not be empty")
            return
        }
        userReference.child(gruopIdNode).updateChildValues([ChatParameter.name:String.getstring(group.name),ChatParameter.profileImage:String.getstring(group.imageUrl)])
        groupReference.child(gruopNode).child(gruopIdNode).child(ChatParameter.information).updateChildValues([ChatParameter.name:String.getstring(group.name),ChatParameter.profileImage:String.getstring(group.imageUrl)])
        recentReference.child(gruopNode).child(gruopIdNode).child(ChatParameter.information).updateChildValues([ChatParameter.name:String.getstring(group.name),ChatParameter.profileImage:String.getstring(group.imageUrl)])
        completion()
        
    }
}
           
