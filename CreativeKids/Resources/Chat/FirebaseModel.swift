//
//  FirebaseModel.swift
//  Chat Demo
//
//  Created by Creative Kids on 28/02/22.
//

import Foundation

import UIKit

class ChatSharedClass: NSObject {
    static let sharedInstance = ChatSharedClass()
    private override init() { }
    func getArray(_ array: Any?) -> [Any] {
        guard let arr = array as? [Any] else {
            return []
        }
        return arr
    }
    func getStringArray(_ array: Any?) -> [String] {
        guard let arr = array as? [String] else {
            return []
        }
        return arr
    }
    func getDictionary(_ dictData: Any?) -> Dictionary<String, Any> {
        guard let dict = dictData as? Dictionary<String, Any> else {
            guard let arr = dictData as? [Any] else {
                return ["":""]
            }
            return getDictionary(arr.count > 0 ? arr[0] : ["":""])
        }
        return dict
    }
}

let chatSharedInstanse = ChatSharedClass.sharedInstance
let chatUserDefault = UserDefaults.standard
//MARK:- Class for messgaes For Chat Halper
class MessageClass {
    var uid:String?
    var senderId :String?
    var senderName :String?
    var receiverId :String?
    var receiverName :String?
    var message:String?
    var isDeleted:String?
    var sendingTime =  Int()
    var isOnline = Int()
    var mediaType:messageType?
    var thumnilImageurl : String?
    var location : [String:Any]?
    var starMessage:String?
    var messageFrom:MessageFrom?
    var userDetails = chatUserDefault.getuserDetails()
    var readStatus:String?
    var contantName:String?
    var contactNumber:String?
    var fileName:String?
    init() { }
    init(uid :String , messageData:[String:Any]) {
        self.userDetails         = chatUserDefault.getuserDetails()
        self.uid                 = String.getstring(uid)
        self.readStatus          = String.getstring(messageData[ChatParameter.readStatus])
        self.senderId            = String.getstring(messageData[ChatParameter.senderId])
        self.senderName          = String.getstring(messageData[ChatParameter.senderName])
        self.receiverId          = String.getstring(messageData[ChatParameter.receiverId])
        self.receiverName        = String.getstring(messageData[ChatParameter.receiverName])
        self.message             = String.getstring(messageData[ChatParameter.content])
        self.isDeleted           = String.getstring(messageData[ChatParameter.isDeleted])
        self.sendingTime         = Int.getint(messageData[ChatParameter.timeStamp])
        self.thumnilImageurl     = String.getstring(messageData[ChatParameter.thumnilImageurl])
        self.location            = chatSharedInstanse.getDictionary(messageData[ChatParameter.location])
        self.starMessage         = String.getstring(messageData[ChatParameter.starMessage])
        self.contantName         = String.getstring(messageData[ChatParameter.contactName])
        self.contactNumber       = String.getstring(messageData[ChatParameter.contactNumber])
        self.messageFrom         = String.getstring(messageData[ChatParameter.senderId]) == String.getstring(userDetails[ChatParameter.userId]) ? .sender : .receiver
        self.fileName            = String.getstring(messageData[ChatParameter.fileName])
        //MARK:- Switch for media type
        switch  String.getstring(messageData[ChatParameter.mediaType]) {
        case "text":
            self.mediaType = .text
        case "photos":
            self.mediaType = .photos
        case "audio":
            self.mediaType = .audio
        case "video":
            self.mediaType = .video
        case "pdf":
            self.mediaType = .pdf
        case "location":
            self.mediaType = .location
        case "contact" :
            self.mediaType = .contact
        case "join" :
            self.mediaType = .join
        case "leave" :
            self.mediaType = .leave
        case "kickOutByAdmin" :
            self.mediaType = .kickOutByAdmin
        case "kickOutBySubAdmin" :
            self.mediaType = .kickOutBySubAdmin
        case "anousement" :
            self.mediaType = .anousement
        case "create" :
            self.mediaType = .create
        default:
            self.mediaType = .text
        }
    }
    
    func createDictonary (objects:MessageClass?) -> Dictionary<String , Any> {
        let params : [String:Any] = [
            ChatParameter.uid                       : objects?.uid ?? "",
            ChatParameter.senderId                  : objects?.senderId ?? "" ,
            ChatParameter.senderName                : objects?.senderName ?? "",
            ChatParameter.receiverId                : objects?.receiverId  ?? "",
            ChatParameter.receiverName              : objects?.receiverName ?? "",
            ChatParameter.content                   : objects?.message ?? "",
            ChatParameter.isDeleted                 : objects?.isDeleted ?? "",
            ChatParameter.timeStamp                 : Int(Date().timeIntervalSince1970 * 1000),
            ChatParameter.mediaType                 : objects?.mediaType?.rawValue ?? "",
            ChatParameter.thumnilImageurl           : objects?.thumnilImageurl ?? "",
            ChatParameter.fileName                  : objects?.fileName ?? ""
            // ChatParameter.readStatus               : objects?.readStatus ?? ""
        ]
        return params
    }
}


//MARK:- Class for Resent Users
struct ResentUsers {
    var senderId :String?
    var receiverId:String?
    var receiverName :String?
    var lastMessage:String?
    var sendingTime =  Int()
    var imageUrl :String?
    var userOnlinetime:String?
    var mediaType:String?
    var readState :String?
    var isOnline :Bool = false
    var createdBy:String?
    var readCount:String?
    var readStatus:String?
    var uid:String?
    init() { }
    init(userdata:[String:Any]) {
        self.senderId         = String.getstring(userdata[ChatParameter.senderId])
        self.receiverId       = String.getstring(userdata[ChatParameter.receiverId])
        self.receiverName     = String.getstring(userdata[ChatParameter.receiverName])
        self.lastMessage      = String.getstring(userdata[ChatParameter.lastMessage])
        self.sendingTime      = Int.getint(userdata[ChatParameter.timeStamp])
        self.imageUrl         = String.getstring(userdata[ChatParameter.profileImage])
        self.readState        = String.getstring(userdata[ChatParameter.readState])
        self.createdBy        = String.getstring(userdata[ChatParameter.createdBy])
        self.mediaType        = String.getstring(userdata[ChatParameter.mediaType])
        self.userOnlinetime   = String.getstring(userdata[ChatParameter.userOnlinetime])
        self.uid              = String.getstring(userdata[ChatParameter.uid])
        self.readCount        = String.getstring(userdata[ChatParameter.unreadCount])
        
    }
    //MARK:- Func for createDictonary
    func createDictonary(objects:ResentUsers?) -> Dictionary<String , Any> {
        let params : [String:Any] = [
            ChatParameter.senderId                      : objects?.senderId ?? "",
            ChatParameter.receiverId                    : objects?.receiverId ?? "" ,
            ChatParameter.receiverName                  : objects?.receiverName ?? "" ,
            ChatParameter.lastMessage                   : objects?.lastMessage ?? "" ,
            ChatParameter.timeStamp                     : objects?.sendingTime ?? "" ,
            ChatParameter.profileImage                  : objects?.imageUrl ?? "",
            ChatParameter.readState                     : objects?.readState ?? "" ,
            ChatParameter.mediaType                     : objects?.mediaType ?? "" ,
            ChatParameter.userOnlinetime                : objects?.userOnlinetime ?? "" ,
            ChatParameter.createdBy                     : objects?.createdBy ?? "" ,
            ChatParameter.uid                           : objects?.uid ?? "",
            ChatParameter.readStatus                    : objects?.readStatus ?? "",
            ChatParameter.unreadCount                   : objects?.readCount ?? ""
            
        ]
        return params
    }
}


//MARK:- Class for Group Model
class GroupModel {
    var adminId: String?
    var adminName:String?
    var userid :String?
    var name :String?
    var createdBy : String?
    var lastmessage:String?
    var imageUrl :String?
    var isOnline :String?
    var user :[String]?
    var blockedUser :[String]?
    init(adminId:String, adminName:String, userid:String , name :String , imageUrl:String? , user:[String], createdBy:String, blockedUser:[String] = []){
        self.adminId    =  String.getstring(adminId)
        self.createdBy  =  String.getstring(createdBy)
        self.userid    =  String.getstring(userid)
        self.name      = String.getstring(name)
        self.imageUrl  = String.getstring(imageUrl)
        self.adminName = String.getstring(adminName)
        self.user      = user
        self.blockedUser = blockedUser
    }
    init(data:[String:Any]){
        self.adminId = String.getstring(data[ChatParameter.groupAdminId])
        self.adminName = String.getstring(data[ChatParameter.groupAdminName])
        self.userid = String.getstring(data[ChatParameter.userId])
        self.name = String.getstring(data[ChatParameter.name])
        self.createdBy = String.getstring(data[ChatParameter.CreatedBy])
        self.lastmessage = String.getstring(data[ChatParameter.lastmessage])
        self.imageUrl = String.getstring(data[ChatParameter.profileImage])
        self.isOnline = String.getstring(data[ChatParameter.isOnline])
        self.user = chatSharedInstanse.getStringArray(data[ChatParameter.Users])
        self.blockedUser = chatSharedInstanse.getStringArray(data[ChatParameter.blockUsers])
    }
    func createGroupDic(groupObject:GroupModel) -> Dictionary<String,Any>{
        var dict:[String:Any] = [:]
        dict[ChatParameter.groupAdminId] = String.getstring(groupObject.adminId)
        dict[ChatParameter.groupAdminName] = String.getstring(groupObject.adminName)
        dict[ChatParameter.userId] = String.getstring(groupObject.userid)
        dict[ChatParameter.timeStamp] = String.getstring(Int(Date().timeIntervalSince1970 * 1000))
        dict[ChatParameter.name] = String.getstring(groupObject.name)
        dict[ChatParameter.CreatedBy] = String.getstring(groupObject.createdBy)
        dict[ChatParameter.lastmessage] = String.getstring(groupObject.lastmessage)
        dict[ChatParameter.profileImage] = String.getstring(groupObject.imageUrl)
        dict[ChatParameter.isOnline] = String.getstring(groupObject.isOnline)
        dict[ChatParameter.Users] = chatSharedInstanse.getStringArray(groupObject.user)
        dict[ChatParameter.blockUsers] = chatSharedInstanse.getStringArray(groupObject.blockedUser)
        return dict
    }
}


//MARK:- Class for Chat Backup
class ChatbackupOnetoOne {
    var Senderid :String?
    var Receiverid :String?
    var Message:String?
    var isDeleted:String?
    var uid:String?
    var SendingTime =  Int()
    var isOnline = Int()
    var mediatype:String?
    var thumnilimageurl : String?
    var Sendername :String?
    var location : [String:Any]?
    
    init(uid :String , messageData:[String:Any]) {
        self.Senderid        = String.getstring(messageData[ChatParameter.senderid])
        self.Message         = String.getstring(messageData[ChatParameter.content])
        self.SendingTime     = Int.getint(messageData[ChatParameter.timeStamp])
        self.Receiverid      = String.getstring(messageData[ChatParameter.receiverid])
        self.isDeleted       = String.getstring(messageData[ChatParameter.isDeleted])
        self.uid             = String.getstring(uid)
        self.mediatype       = String.getstring(messageData[ChatParameter.mediatype])
        self.Sendername      = String.getstring(messageData[ChatParameter.sendername])
        self.thumnilimageurl = String.getstring(messageData[ChatParameter.thumnilimageurl])
        self.location        = chatSharedInstanse.getDictionary(messageData[ChatParameter.location])
    }
}

    enum messageType:String {
        case text
        case photos
        case video
        case pdf
        case location
        case audio
        case contact
        case join
        case leave
        case create
        case anousement
        case kickOutByAdmin
        case kickOutBySubAdmin
    }

enum MessageReferanceType:String{
    case oneToOne
    case group
}

enum MessageFrom:String {
    case sender , receiver
}

extension String {
    static func getstring(_ message: Any?) -> String {
        guard let strMessage = message as? String else {
            guard let doubleValue = message as? Double else {
                guard let intValue = message as? Int else {
                    guard let int64Value = message as? Int64 else{
                        return ""
                    }
                    return String(int64Value)
                }
                return String(intValue)
            }
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 2
            formatter.minimumIntegerDigits = 1
            guard let formattedNumber = formatter.string(from: NSNumber(value: doubleValue)) else {
                return ""
            }
            return formattedNumber
        }
        return strMessage.stringByTrimmingWhiteSpaceAndNewLine()
    }
    
    static func toString(_ message: Any?) -> String {
        guard let strMessage = message as? String else {
            guard let doubleValue = message as? Double else {
                guard let intValue = message as? Int else {
                    guard let int64Value = message as? Int64 else{
                        return ""
                    }
                    return String(int64Value)
                }
                return String(intValue)
            }
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 2
            formatter.minimumIntegerDigits = 1
            guard let formattedNumber = formatter.string(from: NSNumber(value: doubleValue)) else {
                return ""
            }
            return formattedNumber
        }
        return strMessage.stringByTrimmingWhiteSpaceAndNewLine()
    }
}

extension Int {
    static func getint(_ value: Any?) -> Int {
        guard let intValue = value as? Int else {
            let strInt = String.getstring(value)
            guard let intValueOfString = Int(strInt) else { return 0 }
            return intValueOfString
        }
        return intValue
    }
    func dateFromTimeStamp() -> Date {
        return Date(timeIntervalSince1970: TimeInterval(self)/1000)
    }
}

//MARK:- Class for User Information
class UsersState {
    var userId:String?
    var `class`:String?
    var schoolId:String?
    var classSectionId:String?
    var schoolName:String?
    var name :String?
    var first_name:String?
    var last_name:String?
    var email:String?
    var mobile_number:String?
    var profile_image:String?
    var deviceID:String?
    var onlineTime =  Int()
    var isOnline :Bool?
    var blockUsers :[String]?
    var userType:String?
    var isSelected = false
    var messageReferanceType = "oneToOne"
    var createdBy:String?
    
    
    init() {}
    init(userdata:[String:Any] , isSelected : Bool) {
        self.userId            = String.getstring(userdata[ChatParameter.userId])
        self.class             = String.getstring(userdata[ChatParameter.className])
        self.schoolId          = String.getstring(userdata[ChatParameter.schoolId])
        self.classSectionId    = String.getstring(userdata[ChatParameter.classSecId])
        self.schoolName        = String.getstring(userdata[ChatParameter.schoolName])
        self.first_name        = String.getstring(userdata[ChatParameter.firstName])
        self.last_name         = String.getstring(userdata[ChatParameter.lastName])
        self.email             = String.getstring(userdata[ChatParameter.email])
        self.name              = String.getstring(userdata[ChatParameter.name])
        self.mobile_number     = String.getstring(userdata[ChatParameter.mobileNumber])
        self.profile_image     = String.getstring(userdata[ChatParameter.profileImage])
        self.onlineTime        = Int.getint(userdata[ChatParameter.timeStamp])
        self.blockUsers        = chatSharedInstanse.getStringArray(userdata[ChatParameter.blockUsers])
        self.isOnline          = String.getstring(userdata[ChatParameter.isOnline]) == "1"
        self.deviceID          = String.getstring(userdata[ChatParameter.deviceID])
        self.userType          = String.getstring(userdata[ChatParameter.userType])
        self.isSelected        = isSelected
        self.messageReferanceType = String.getstring(userdata[ChatParameter.userReferanceType]) == "" ? "oneToOne" : String.getstring(userdata[ChatParameter.userReferanceType])
        self.createdBy = String.getstring(userdata[ChatParameter.createdBy])
    }
    init(userdata:[String:Any]) {
        self.userId            = String.getstring(userdata[ChatParameter.userId])
        self.class             = String.getstring(userdata[ChatParameter.className])
        self.schoolId          = String.getstring(userdata[ChatParameter.schoolId])
        self.classSectionId    = String.getstring(userdata[ChatParameter.classSecId])
        self.schoolName        = String.getstring(userdata[ChatParameter.schoolName])
        self.first_name        = String.getstring(userdata[ChatParameter.firstName])
        self.last_name         = String.getstring(userdata[ChatParameter.lastName])
        self.email             = String.getstring(userdata[ChatParameter.email])
        self.name              = String.getstring(userdata[ChatParameter.name])
        self.mobile_number     = String.getstring(userdata[ChatParameter.mobileNumber])
        self.profile_image     = String.getstring(userdata[ChatParameter.profileImage])
        self.onlineTime        = Int.getint(userdata[ChatParameter.timeStamp])
        self.blockUsers        = chatSharedInstanse.getStringArray(userdata[ChatParameter.blockUsers])
        self.isOnline          = String.getstring(userdata[ChatParameter.isOnline]) == "1"
        self.deviceID          = String.getstring(userdata[ChatParameter.deviceID])
        self.userType          = String.getstring(userdata[ChatParameter.userType])
        self.messageReferanceType = String.getstring(userdata[ChatParameter.userReferanceType]) == "" ? "oneToOne" : String.getstring(userdata[ChatParameter.userReferanceType])
        self.createdBy = String.getstring(userdata[ChatParameter.createdBy])
    }
    func createDictonary (objects:UsersState?) -> Dictionary<String , Any> {
        let params : [String:Any] = [
            ChatParameter.userId                      : objects?.userId ?? "",
            ChatParameter.schoolId                    : objects?.schoolId ?? "",
            ChatParameter.schoolName                  : objects?.schoolName ?? "",
            ChatParameter.userType                    : objects?.userType ?? "",
            ChatParameter.className                   : objects?.class ?? "",
            ChatParameter.name                        : String.getstring(objects?.name),
            ChatParameter.profileImage                : objects?.profile_image ?? "",
            ChatParameter.timeStamp                   : Int(Date().timeIntervalSince1970 * 1000) ,
            ChatParameter.deviceID                    : UIDevice.current.identifierForVendor?.uuidString ?? "",
            ChatParameter.mobileNumber                : objects?.mobile_number ?? "",
            ChatParameter.userReferanceType           : objects?.messageReferanceType ?? "oneToOne",
            ChatParameter.createdBy                   : objects?.createdBy ?? ""
        ]
        return params
    }
}

class MessageGroup {
    var sendingTime:Int?
    var message:[MessageClass]?
    init(time:Int? , message:[MessageClass]?) {
        self.sendingTime = time
        self.message = message
    }
}

enum UserType:String{
    case Student
    case Teacher
}

//MARK: - Structure For String Data
struct ChatParameter {
    static let UserSeenCount              = "userSeenCount"
    static let CreatedBy                  = "createdBy"
    static let Groups                     = "groups"
    static let Images                     = "Images"
    static let content                    = "content"
    static let videoName                  = "videoName"
    static let lastmessage                = "lastMessage"
    static let senderid                   = "senderId"
    static let receiverid                 = "receiverId"
    static let Message                    = "messages"
    static let mediatype                  = "mediaType"
    static let type                       = "type"
    static let mediaurl                   = "mediaUrl"
    static let Pdfs                       = "pdfs"
    static let isDeleted                  = "isDeleted"
    static let thumnilimageurl            = "thumnilImageurl"
    static let ResentMessage              = "resentMessage"
    static let UserState                  = "userState"
    static let readState                  = "readState"
    static let timeStamp                  = "timeStamp"
    static let profileImage              = "profileImage"
    static let OnlineState                = "onlineState"
    static let alertmessage               = "Some Error node  not Found in Send data to firebase"
    static let Users                      = "users"
    static let information                = "information"
    static let emptyString                = ""
    static let saparaterString            = "_"
    static let nomessagesdata             = "No Data found"
    static let nobackupdata               = "No Backup Data found"
    static let blockUsers                 = "blockUsers"
    static let sendername                 = "senderName"
    static let location                   = "location"
    static let latitude                   = "latitude"
    static let longitude                  = "longitude"
    static let starmessage                = "starMessage"
    static let uid                        = "uid"
    static let senderId                   = "senderId"
    static let senderName                 = "senderName"
    static let receiverId                 = "receiverid"
    static let name                       = "name"
    static let receiverName               = "receiverName"
    static let mediaType                  = "mediaType"
    static let thumnilImageurl            = "thumnilImageurl"
    static let starMessage                = "starMessage"
    static let lastMessage                = "lastMessage"
    static let createdBy                  = "createdBy"
    static let userId                     = "userId"
    static let isOnline                   = "onlineState"
    static let firstName                  = "firstName"
    static let lastName                   = "lastName"
    static let email                      = "email"
    static let mobileNumber               = "mobileNumber"
    static let deviceID                   = "deviceID"
    static let friendList                 = "friendList"
    static let userOnlinetime             = "userOnlinetime"
    static let uploadData                 = "uploadData"
    static let readStatus                 = "readStatus"
    static let contactName                = "contactName"
    static let contactNumber              = "contactNumber"
    static let userReferanceType          = "referanceType"
    static let groupMessageSeen           = "groupMessageSenn"
    static let unreadCount                = "unreadCount"
    static let groupAdminId               = "groupAdminId"
    static let groupAdminName             = "groupAdminName"
    static let schoolId                   = "schoolId"
    static let classSecId                 = "classSecId"
    static let className                  = "className"
    static let schoolName                 = "schoolName"
    static let userType                   = "userType"
    static let fileName                   = "fileName"
}
