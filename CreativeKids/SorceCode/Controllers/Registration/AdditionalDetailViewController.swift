//
//  AdditionalDetailViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 24/08/21.
//

import UIKit

class AdditionalDetailViewController: UIViewController {

    @IBOutlet weak var textFieldGuardianEmailId: UITextField!
    @IBOutlet weak var textFieldTeacherEmailId: UITextField!
    var detailViewModel:AdditionalDetailViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func submitButtonTapped(_ sender:UIButton){
        detailViewModel = AdditionalDetailViewModel(viewController: self)
    }
    

}
