//
//  OfferPopUpViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/03/21.
//

import UIKit
import CoreLocation
import AVFoundation
import AVKit
import SDWebImage
import AssetsLibrary

//https://creativekidssolutions.com/api/student/?subid=202&eng=eng&usidnotsubs=niteshjha1234@gmail.com

class OfferPopUpViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imageOffer: UIImageView!
    @IBOutlet weak var videoView: UIView!
    
    var offerImageUrl = ""
    var player : AVPlayer!
    var avPlayerLayer : AVPlayerLayer?
    var playerItem:AVPlayerItem!
    var activityIndicator = UIActivityIndicatorView()
    var assetWriter:AVAssetWriter?
     var assetReader:AVAssetReader?
     let bitrate:NSNumber = NSNumber(value:250000)

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialViewLoad()
        // apiCheck()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        avPlayerLayer?.frame = videoView.layer.bounds
    }
    
    //MARK: - Methods
    func initialViewLoad(){
        launchVideoAndImage()
        let gestureOnMainView = UITapGestureRecognizer(target: self, action: #selector(tapOnMainView(gesture:)))
        viewMain.isUserInteractionEnabled  = true
        viewMain.addGestureRecognizer(gestureOnMainView)
        //imageOffer.downlodeImage(serviceurl: "https://creativekidssolutions.com/content/img/Free_Pop_up.png", placeHolder: UIImage())
    }
    func launchVideoAndImage() {
        //offerImageUrl = "https://sample-videos.com/gif/3.gif"
        let offerImageUrlCheck = offerImageUrl
        let videoTypeCheck = offerImageUrlCheck.lowercased().contains(".mp4")
        if videoTypeCheck {
            self.videoView.backgroundColor = .black
            guard let url = URL(string: offerImageUrl) else{return}
            playerItem = AVPlayerItem(url: url)
            imageOffer.isHidden = true
            player = AVPlayer(playerItem: playerItem)
            avPlayerLayer = AVPlayerLayer(player: player)
            avPlayerLayer?.videoGravity = .resize
            avPlayerLayer?.repeatCount = 2
            player.volume = 5
            player.addObserver(self, forKeyPath: "timeControlStatus", options: [.old,.new], context: nil)
            videoView.layer.addSublayer(avPlayerLayer ?? AVPlayerLayer())
            player.play()
            
        } else if offerImageUrlCheck.lowercased().contains(".gif") {
            imageOffer.isHidden = true
            self.videoView.backgroundColor = .systemIndigo
            self.showActivityIndicator(isLoading: true)
            DispatchQueue.main.async {
                let imageURL = UIImage.gifImageWithURL(self.offerImageUrl)
                self.showActivityIndicator(isLoading: false)
                let imageView3 = UIImageView(image: imageURL)
                imageView3.frame = self.videoView.bounds
                self.videoView.addSubview(imageView3)
            }
        } else {
            imageOffer.isHidden = false
           // self.imageOffer.sd_setImage(with: URL(string: offerImageUrl), placeholderImage: UIImage(), options: .continueInBackground, completed: nil)
            self.imageOffer.downloadImageFromURL(urlString: offerImageUrl)
        }
    }
    //MARK: - TIME CONTROL OVSERVER
    override internal func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "timeControlStatus", let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
            let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
            let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
            if newStatus != oldStatus {
                DispatchQueue.main.async {[weak self] in
                    if newStatus == .playing || newStatus == .paused {
                        self?.showActivityIndicator(isLoading: false)
                    } else {
                        self?.showActivityIndicator(isLoading: true)
                    }
                }
            }
        }
    }
    
    //MARK: - View Animate
    @objc func tapOnMainView(gesture: UITapGestureRecognizer){
        UIView.transition(with: view, duration: 1, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: true, completion: nil)
        }, completion: {_ in
            LocationManager.sharedInstance.locationSetup()
        })
    }
    //MARK: - View IBActions
    @IBAction func buttonCloseTapped(_ sender: UIButton) {
        UIView.transition(with: view, duration: 1, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: true, completion: nil)
        }, completion: {_ in
            LocationManager.sharedInstance.locationSetup()
        })
    }
    
    func showActivityIndicator(isLoading: Bool) {
        if isLoading {
            activityIndicator.alpha = 1.0
            activityIndicator.color = .white
            activityIndicator.style = .large
            videoView.addSubview(activityIndicator)
            activityIndicator.center = CGPoint(x: videoView.size.width / 2, y: videoView.size.height / 2)
            activityIndicator.startAnimating()
        } else {
            DispatchQueue.main.async {
                self.activityIndicator.removeFromSuperview()
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    
    //    func apiCheck(){
    //        let session = URLSession.shared
    //        var urlString = "https://creativekidssolutions.com/api/student/?Email=prashantcoolboy234@gmail.com&clsa=L.K.G (B)"
    //        urlString = urlString.replacingOccurrences(of: " ", with: "%20")
    //        let url = URL(string: urlString)!
    //        var url1 = URLRequest(url: url)
    //        url1.httpMethod = "GET"
    //        url1.setValue("application/json", forHTTPHeaderField: "Content-Type")
    //        let task1 = session.dataTask(with: url1) { (data, urlResponce, error) in
    //            print(data)
    //            if error != nil || data == nil{
    //                print("client error")
    //            }
    //            print((urlResponce as? HTTPURLResponse)?.statusCode)
    //            guard let response = urlResponce as? HTTPURLResponse, (200...399).contains(response.statusCode) else {
    //                    print("Server error!")
    //                    return
    //                }
    //
    //                guard let mime = response.mimeType, mime == "application/json" else {
    //                    print("Wrong MIME type!")
    //                    return
    //                }
    //
    //                do {
    //                    let json = try JSONSerialization.jsonObject(with: data!, options: [])
    //                    print(json)
    //                } catch {
    //                    print("JSON error: \(error.localizedDescription)")
    //                }
    //        }
    //        task1.resume()
    //}
}
