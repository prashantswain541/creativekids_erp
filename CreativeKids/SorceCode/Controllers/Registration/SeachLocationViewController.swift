//
//  SeachLocationViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 04/02/22.
//

import UIKit
import MapKit

class SeachLocationViewController: UIViewController {

    @IBOutlet weak var searchLocationTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchCompleter = MKLocalSearchCompleter()
    var searchResults = [MKLocalSearchCompletion]()
    var locationPassCallBack:((String,String,String)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.becomeFirstResponder()
        searchLocationTableView.delegate = self
        searchLocationTableView.dataSource = self
        searchBar.delegate = self
        searchCompleter.delegate = self
        // Do any additional setup after loading the view.
    }

}
extension SeachLocationViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchCompleter.queryFragment = searchText
    }
}

extension SeachLocationViewController:MKLocalSearchCompleterDelegate{
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        searchLocationTableView.reloadData()
    }
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        // showSimpleAlert(message: "\(error.localizedDescription)")
    }
}

extension SeachLocationViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = self.searchResults[indexPath.row]
        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = searchResult.title
        cell.detailTextLabel?.text = searchResult.subtitle
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedLocation = self.searchResults[indexPath.row]
        let getCityState = getStateCity(title: selectedLocation.title, subtitle: selectedLocation.subtitle)
        print("\(getCityState.0)--\(getCityState.1)")
//        print(selectedLocation.subtitle)
        self.dismiss(animated: true){
            self.locationPassCallBack?(getCityState.0, getCityState.1,"\(selectedLocation.title) \(selectedLocation.subtitle)")
        }
//        let searchRequest = MKLocalSearch.Request(completion: selectedLocation)
//        let search = MKLocalSearch(request: searchRequest)
//        search.start{ (responce,error) in
//            let cordinate = responce?.mapItems.first?.placemark.coordinate
//            print("\(cordinate?.latitude) , \(cordinate?.longitude)")
////            self.convertLatLongToAddress(latitude: cordinate?.latitude ?? 0.0, longitude: cordinate?.longitude ?? 0.0)
//        }
    }
    
    func getStateCity(title:String,subtitle:String) -> (String,String){
        let getState = subtitle.split(separator: ",")
        let getCity = title.split(separator: ",")
        var state = ""
        var city = ""
        print(getState)
        if getState.count > 2{
            if getState.count > 3{
                if let checkPincode = Int(String(getState[getState.count - 3]).replacingOccurrences(of: " ", with: "")){
                    state =  String(getState[getState.count - 2])
                    city = String(getState[getState.count - 4])
                    return (city,state)
                }else{
                    state =  String(getState[getState.count - 2])
                    city = String(getState[getState.count - 3])
                    return (city,state)
                }
            }else{
                if let checkPincode = Int(String(getState[getState.count - 2]).replacingOccurrences(of: " ", with: "")){
                    state =  String(getState[getState.count - 3])
                    city = String(getCity[getCity.count - 1])
                    return (city,state)
                }else{
                    state =  String(getState[getState.count - 2])
                    city = String(getState[getState.count - 3])
                    return (city,state)
                }
            }
        }else{
            if getState.count > 1{
                state =  String(getState[getState.count - 2])
                city = String(getCity[getCity.count - 1])
                return (city,state)
            }else{
                if getState.count == 1{
                    state =  String(getState[getState.count - 1])
                    city = String(getCity[getCity.count - 1])
                    return (city,state)
                }else{
                    state = ""
                    city = String(getCity[getCity.count - 1])
                    return (city,state)
                }
            }
        }
    }
    
}


//func convertLatLongToAddress(latitude:Double,longitude:Double){
//
//        let geoCoder = CLGeocoder()
//        let location = CLLocation(latitude: latitude, longitude: longitude)
//        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
//
//            // Place details
//            var placeMark: CLPlacemark!
//            placeMark = placemarks?[0]
//            print(placeMark.administrativeArea)
//
//            var addressString : String = ""
//             if placeMark.subLocality != nil {
//                 addressString = addressString + placeMark.subLocality! + ", "
//             }
//             if placeMark.thoroughfare != nil {
//                 addressString = addressString + placeMark.thoroughfare! + ", "
//             }
//             if placeMark.locality != nil {
//                 addressString = addressString + placeMark.locality! + ", "
//             }
//             if placeMark.country != nil {
//                 addressString = addressString + placeMark.country! + ", "
//             }
//             if placeMark.postalCode != nil {
//                 addressString = addressString + placeMark.postalCode! + " "
//             }
//            print(addressString)
//
//            // Location name
//            if let locationName = placeMark.location {
//                print(locationName)
//            }
//            // Street address
//            if let street = placeMark.thoroughfare {
//                print(street)
//            }
//            // City
//            if let city = placeMark.locality {
//                print(city)
//            }
//            // State
//            if let state = placeMark.administrativeArea {
//                print(state)
//            }
//            // Zip code
//            if let zipCode = placeMark.postalCode {
//                print(zipCode)
//            }
//            // Country
//            if let country = placeMark.country {
//                print(country)
//            }
//        })
//
//    }
