//
//  WalkThroughViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 17/03/21.
// // 7F2D97D1-C7B9-469B-B695-DB99AAA6EB58 :- 8
// // 32C282FD-9AD8-48FC-A127-7CA2B511D7DC :- 12 pro max

import UIKit
import Photos
import AVFoundation
import AVKit
import AssetsLibrary
import CoreData

class WalkThroughViewController: UIViewController,UIScrollViewDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionViewWalkThrough: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imageViewStartingLearning: UIImageView!
    
    //MARK: - Variables
    var collectionImage = [#imageLiteral(resourceName: "s_img_1"),#imageLiteral(resourceName: "s_img_2"),#imageLiteral(resourceName: "s_img_3"),#imageLiteral(resourceName: "s_img_4")]
    var currentIndex = 0
    var timer: Timer?
    var screen = ScreenProtector()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTimer()
        AppUtility.lockOrientation(.portrait)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer?.invalidate()
    }
    
    //MARK: - Methods
    fileprivate func initialView(){
        pageControl.numberOfPages = collectionImage.count
        pageControl.currentPage = 0
        collectionViewWalkThrough.delegate = self
        collectionViewWalkThrough.dataSource = self
        let tapOnLearn = UITapGestureRecognizer(target: self, action: #selector(moveTologin))
        imageViewStartingLearning.addGestureRecognizer(tapOnLearn)
        getOfferImageApi()
    }
    
    func showPopUp(popUpUrl:String){
        let controller = OfferPopUpViewController.getController(storyboard: .Registration)
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = .overCurrentContext
        controller.offerImageUrl = popUpUrl
        if let alreadyPresentedController = kSharedSceneDelegate?.window?.rootViewController?.presentedViewController{
            alreadyPresentedController.present(controller, animated: true)
        }else{
            kSharedSceneDelegate?.window?.rootViewController?.present(controller, animated: true)
        }
    }
    
    //MARK: -  Move To Home Screen
    @objc func moveTologin(gesture:UITapGestureRecognizer){
        if String.getString(kSharedUserDefaults.getUserClass()["className"]) == "" {
            let controller = ClassSelectionViewController.getController(storyboard: .Registration)
            controller.modalPresentationStyle = .overFullScreen
            controller.cameFor = .classSelected
            self.navigationController?.present(controller, animated: true, completion: nil)
            cameFrom = .sideMenu
        }else{
            cameFrom = .login
            if String.getString(kSharedUserDefaults.getUserClass()["className"]) == ClassName.demo{
                kSharedUserDefaults.setUserClass(userClass: ["className":"","classType":ClassType.cordova])
                self.timer?.invalidate()
                if kSharedUserDefaults.isUserLoggedIn(){
                    self.matchUniqueId()
                }else{
                    self.screenMoveToHome()
                }
            }else{
                self.timer?.invalidate()
                if kSharedUserDefaults.isUserLoggedIn(){
                    self.matchUniqueId()
                }else{
                    kSharedAppManager.moveToHome()
                }
            }
        }
    }
    
    //MARK: -  Match Id for logout and login
    func matchUniqueId(){
        var serviceURL:String = ""
        switch String.getstring(kUserData.role){
        case String.getstring(UserRole.schoolTeacher.rawValue):
            serviceURL = "usehere/?email=\(kUserData.userEmail ?? "")&uniqNO=\(getUDID.getDeviceIdFromKeyChain())".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        case String.getstring(UserRole.schoolStudent.rawValue):
            serviceURL = "usehere/?Email=\(kUserData.userEmail ?? "")&uniqNO=\(getUDID.getDeviceIdFromKeyChain())".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        case String.getstring(UserRole.normalUser.rawValue):
            serviceURL = "usehere/?Email=\(kUserData.userEmail ?? "")&uniqNO=\(getUDID.getDeviceIdFromKeyChain())".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        default:
            return
        }
        var request = URLRequest(url: URL(string: kBaseUrl + serviceURL)!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        print(kBaseUrl + serviceURL)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data else {
                    print(String(describing: error))
                    return
                }
                print(String(data: data, encoding: .utf8)!)
                
                guard let responseData = response as? HTTPURLResponse else {return}
                
                if responseData.statusCode == 200 {
                    if String.getString(getUDID.getDeviceIdFromKeyChain()) == String(data: data, encoding: .utf8)! {
                        self.screenMoveToHome()
                    }else{
                        kSharedAppManager.logout()
                    }
                }
            }
            
        }
        
        task.resume()
        //        BaseController.shared.postToServerAPI(url: serviceURL, params: [:], type: .GET,showHud: false) { (response, statusCode) in
        //            if statusCode == 200 {
        //                let responceMessage = String.getString(response[getResponce])
        //                if String.getString(getUDID.getDeviceIdFromKeyChain()) == responceMessage {
        //                    self.screenMoveToHome()
        //                }else{
        //                    kSharedAppManager.logout()
        //                }
        //            } else {
        //                showAlertMessage.alert(message: "No Data Found")
        //            }
        //        }
    }
    
    func screenMoveToHome() {
        let  loginData = kSharedUserDefaults.getLoggedInUserDetails()["role"] as? String
        switch loginData {
        case "1":
            kSharedAppManager.moveToStudentDashboard()
        case "2":
            kSharedAppManager.moveToTeacherDashboard()
        case "3":
            kSharedAppManager.moveToHome()
        default:
            kSharedAppManager.moveToHome()
        }
        kSharedUserDefaults.setUserLoggedIn(userLoggedIn: true)
    }
    //9A09D1B0-CCEC-44C5-AD60-31C8217CA6CD
    //MARK: - UIScrollViewDelegate Method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let indexpath = collectionViewWalkThrough.indexPathsForVisibleItems
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            collectionViewWalkThrough.scrollToItem(at: indexpath.first!, at: .centeredVertically, animated: true)
        }else{
            collectionViewWalkThrough.scrollToItem(at: indexpath.first!, at: .centeredHorizontally, animated: true)
        }
        pageControl.currentPage = indexpath.first!.item
        currentIndex = indexpath.first!.item
    }
    
    func setTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
    }
    
    //MARK: -  Auto Scroll Methods
    @objc func autoScroll() {
        if self.currentIndex < self.collectionImage.count - 1 {
            self.currentIndex += 1
            let indexPath = IndexPath(item: currentIndex, section: 0)
            if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
                self.collectionViewWalkThrough.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
            }else{
                self.collectionViewWalkThrough.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
            self.pageControl.currentPage = self.currentIndex
        }else{
            self.currentIndex = 0
            self.pageControl.currentPage = self.currentIndex
            
            if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
                self.collectionViewWalkThrough.scrollToItem(at: IndexPath(item: self.currentIndex, section: 0), at: .centeredVertically, animated: true)
            }else{
                self.collectionViewWalkThrough.scrollToItem(at: IndexPath(item: self.currentIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
}

//MARK: - UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
extension WalkThroughViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        collectionImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewWalkThrough.dequeueReusableCell(withReuseIdentifier: "WalkThroughCollectionViewCell", for: indexPath) as! WalkThroughCollectionViewCell
        cell.imageViewSlide.image = collectionImage[indexPath.item]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionViewWalkThrough.width, height: collectionViewWalkThrough.height)
    }
}

extension WalkThroughViewController{
    //MARK: - Get Offer Image Api
    func getOfferImageApi(){
        //        var request = URLRequest(url: URL(string: kBaseUrl + "offerImgold/?offer=ghfgh")!,timeoutInterval: Double.infinity)
        //        request.httpMethod = "GET"
        //
        //        let task = URLSession.shared.dataTask(with: request) { data, response, error in
        //
        //            guard let dataVal = data else {
        //                print(String(describing: error))
        //                return
        //            }
        //            print(String(data: dataVal, encoding: .utf8)!)
        //
        //            guard let responseData = response as? HTTPURLResponse else {return}
        //
        //            if responseData.statusCode == 200 {
        //                do {
        //                    let json = try JSONSerialization.jsonObject(with: dataVal) as? [String: Any]
        //                    let kresponce = kSharedInstance.getArray(withDictionary: json?[getResponce])
        //                    let popUpImageUrl = kSharedInstance.getDictionary(kresponce[0])
        //                    self.showPopUp(popUpUrl: popUpImageUrl["img"] as? String ?? "")
        //                    print("response: ------\(kresponce)")
        //                } catch {
        //                    print("errorMsg")
        //                }
        //
        //            }else if responseData.statusCode == 404 {
        //                print("No PopUp")
        //            } else {
        //                showAlertMessage.alert(message: "Something went wrong")
        //            }
        //        }
        //
        //        task.resume()
        let serviceName = kBaseUrl + "offerImg?offers=Yes"
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET,showHud: false) { (response, statusCode) in
            if statusCode == 200 {
                let kresponce = kSharedInstance.getArray(withDictionary: response[getResponce])
                let popUpImageUrl = kSharedInstance.getDictionary(kresponce[0])
                self.showPopUp(popUpUrl: popUpImageUrl["img"] as? String ?? "")
                print("response: ------\(response)")
            } else if statusCode == 404{
                print("No PopUp")
            }else {
                showAlertMessage.alert(message: "Something went wrong")
            }
        }
    }
    //    func didTakeScreenshot() {n
    //        self.perform(#selector(screen.startPreventingScreenshot), with: nil, afterDelay: 1, inModes: [])
    //    }
}




//MARK:- Cell CLASS
class WalkThroughCollectionViewCell:UICollectionViewCell {
    @IBOutlet weak var imageViewSlide:UIImageView!
    
}
