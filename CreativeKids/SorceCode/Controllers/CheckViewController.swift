//
//  CheckViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/07/22.
//

import UIKit
import CoreCharts

class CheckViewController: UIViewController {
    @IBOutlet weak var barChart: HCoreBarChart!
    override func viewDidLoad() {
        super.viewDidLoad()
        barChart.dataSource = self
        // Do any additional setup after loading the view.
    }

}

extension CheckViewController: CoreChartViewDataSource{
    func loadCoreChartData() -> [CoreChartEntry] {
        getTurkeyFamouseCityList()
    }
    
    func getTurkeyFamouseCityList()->[CoreChartEntry] {
            var allCityData = [CoreChartEntry]()
            let cityNames = ["Istanbul","Antalya","Ankara","Trabzon","İzmir"]
            let plateNumber = [34,07,06,61,35]
            
            for index in 0..<cityNames.count {
                
                let newEntry = CoreChartEntry(id: "\(plateNumber[index])",
                                              barTitle: cityNames[index],
                                              barHeight: CGFloat(plateNumber[index]),
                                              barColor: rainbowColor()
                                              )
                                              
                                             
                allCityData.append(newEntry)
                
            }
            
            return allCityData
            
        }
}
