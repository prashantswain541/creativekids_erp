//
//  AssignChaptersViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 24/03/22.
//

import UIKit
import Alamofire

class AssignChaptersViewController: UIViewController {
    
    @IBOutlet weak var textfieldSelectClass: UITextField! {
        didSet {
            textfieldSelectClass.setRightView(image: UIImage(named: "DropDown_1") ?? UIImage())
        }
    }
    @IBOutlet weak var textfieldSelectSubject: UITextField! {
        didSet {
            textfieldSelectSubject.setRightView(image: UIImage(named: "DropDown_1") ?? UIImage())
        }
    }
    
    @IBOutlet weak var tableViewChapter: UITableView!
    
    var assignClassViewModel: AssignChapterViewModel?
    var subjectModel: [SubjectByChapterModel]?
    var chapterModel: [TopicSearchModel]?
    var subjectSelected:SubjectModel?
    var classesArray = [ClassesModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialView()
    }
    
    private func initialView(){
        statusBarColor(headerColor: UIColor.blue)
        let selectClass = UITapGestureRecognizer(target: self, action: #selector(selectClassTapped(_:)))
        self.textfieldSelectClass.addGestureRecognizer(selectClass)
        let selectSubject = UITapGestureRecognizer(target: self, action: #selector(selectSubjectTapped(_:)))
        self.textfieldSelectSubject.addGestureRecognizer(selectSubject)
        tableViewChapter.register(UINib(nibName: TableCellIdentifier.chapter, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.chapter)
    }
    @IBAction func buttonBAckTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func selectClassTapped(_ sender: UITapGestureRecognizer) {
        if !classesArray.isEmpty{
            showClasses()
        }else{
            self.assignClassViewModel = AssignChapterViewModel.init(viewController: self, completionHandler: { classData in
                self.classesArray = classData
                showClasses()
            })
        }
        //MARK:- Show Class Data
        func showClasses(){
            if !classesArray.isEmpty{
                let controller = SelectClassSubjectViewController.getController(storyboard: .TeacherDashboard)
                controller.modalTransitionStyle = .crossDissolve
                controller.modalPresentationStyle = .overCurrentContext
                controller.data = classesArray
                controller.selectedDelegate = self
                controller.field = self.textfieldSelectClass
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    @objc func selectSubjectTapped(_ sender: UITapGestureRecognizer) {
        if !(self.textfieldSelectClass.text?.isEmpty ?? false) {
            self.assignClassViewModel?.getSubject(completion: { subjectData in
                let datasource = subjectData
                if !datasource.isEmpty {
                    let controller = SelectClassSubjectViewController.getController(storyboard: .TeacherDashboard)
                    controller.modalTransitionStyle = .crossDissolve
                    controller.modalPresentationStyle = .overCurrentContext
                    controller.data = datasource
                    controller.field = self.textfieldSelectSubject
                    controller.selectedDelegate = self
                    self.present(controller, animated: true, completion: nil)
                }
            })
        } else {
            showAlertMessage.alert(message: "Please Select Class first!!")
        }
    }
    
    func loadChapters(id:String){
        if !(self.textfieldSelectSubject.text?.isEmpty ?? false) {
            self.assignClassViewModel?.getChapter(subid: String.getString(id), completion: { chapterData in
                self.chapterModel = chapterData
                self.tableViewChapter.reloadData()
            })
        }
    }
}
extension AssignChaptersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.getNumberOfRow(numberofRow: self.chapterModel?.count ?? 0, message: "Data Not Found")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.chapter, for: indexPath) as! ChapterTableViewCell
        let object = chapterModel![indexPath.row]
        cell.cellType = .assign
        cell.configCell(data: object, index: indexPath.row)
        cell.assignChapterCallBack = {
            self.assignChapter(selectedChapter:object)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.isPad {
            return 100
        } else {
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !(NetworkReachabilityManager()?.isReachable ?? false){
            showAlertMessage.alert(message:AlertMessage.knoNetwork)
            return
        }
        let object = chapterModel?[indexPath.row]
        guard let Kdata = object else{return}
        let controller = VideoPlayerViewController.getController(storyboard: .Home)
        controller.cameFrom = .classVideo
        controller.selectedTopic = Kdata
        controller.subjectSelected = self.subjectSelected
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    // MARK: Func for Enable and Disable Chapter
    func assignChapter(selectedChapter: TopicSearchModel){
        let message = selectedChapter.isChapterRead ? "Do you want to disable this chapter" : "Do you want to enable this chapter for students"
        CommonUtils.showAlert(title: "Creative Kids", message: message, firstTitle: "YES", secondTitle: "CANCEL") { buttonTitle in
            switch buttonTitle{
            case "YES":
                if selectedChapter.isChapterRead {
                    print("Api for Disable Chapter")
                    selectedChapter.isChapterRead = false
                    self.tableViewChapter.reloadData()
                }else{
                    print("Api for enable Chapter")
                    self.assignClassViewModel?.assignChapter(chapter:selectedChapter){
                        selectedChapter.isChapterRead = true
                        self.tableViewChapter.reloadData()
                    }
                }
            case "CANCEL":
                print("Cancel")
                return
            default:
                return
            }
        }
    }
}

extension AssignChaptersViewController:SelectedDataDelegate{
    
    func selecetd(textField: UITextField, item: SelectionDelegate) {
        switch textField{
        case textfieldSelectClass:
            self.textfieldSelectSubject.text = ""
            self.chapterModel = []
            self.tableViewChapter.reloadData()
            textfieldSelectClass.text = item.name
        case textfieldSelectSubject:
            textfieldSelectSubject.text = item.name
            loadChapters(id: item.id ?? "")
        default:
            return
        }
    }
    
    
    
}
