//
//  TeacherSideMenuViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/02/22.
//

import UIKit
import KYDrawerController

class TeacherSideMenuViewController: UIViewController {

    
    //MARK: - IBOutlets
    @IBOutlet weak var tableViewSideMenu: UITableView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var tableTopCons: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: UIView!
    
    //MARK: - Variables
    var sideMenuDataArray = [SideMenuDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showTabbar()
        initialConfig()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabbar()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideTabbar()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setgradient(color1: .blue, color2: .systemIndigo)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
            self.setgradient(color1: .blue, color2: .systemIndigo)
    }
    //MARK: - Methods
    func initialConfig(){
        tableTopCons.constant -= getStatusBarHeight()
        sideMenuDataArray = [SideMenuDataModel(sideTitle: "Home", sideImage: #imageLiteral(resourceName: "change_cls_icon")),
                             SideMenuDataModel(sideTitle: "Contact Us", sideImage: #imageLiteral(resourceName: "contact_us_icon")),
                             SideMenuDataModel(sideTitle: "About Us", sideImage: #imageLiteral(resourceName: "contact_us_icon")),
                             SideMenuDataModel(sideTitle: "Login", sideImage: #imageLiteral(resourceName: "logout_icon")),
                             SideMenuDataModel(sideTitle: "Share App", sideImage: #imageLiteral(resourceName: "share_icon")),
                            ]
        tableViewSideMenu.dataSource = self
        tableViewSideMenu.delegate = self
        tableViewSideMenu.register(UINib(nibName: TableCellIdentifier.sideMenu, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.sideMenu)
    }
    
    func logoutForSchoolApi(){
        let serviceName = "https://creativekidssolutions.com/Auth/tokens/cancel".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        var request = URLRequest(url: URL(string: serviceName)!,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(kSharedUserDefaults.getLoggedInAccessToken(), forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data else {
                    print(String(describing: error))
                    return
                }
                print(String(data: data, encoding: .utf8)!)
                guard let responseData = response as? HTTPURLResponse else {return}
                
                if responseData.statusCode == 200 {
                    let responceMessage = String(data: data, encoding: .utf8)
                    CommonUtils.showToast(message: responceMessage ?? "")
                    kSharedAppManager.logout()
                } else {
                    showAlertMessage.alert(message: "Something not found")
                }
            }
        }
        
        task.resume()
        
//        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .POST) { (response, statusCode) in
//            if statusCode == 200 {
//                let responceMessage = String.getString(response[getResponce])
//                CommonUtils.showToast(message: responceMessage)
//                kSharedAppManager.logout()
//            } else {
//                showAlertMessage.alert(message: "Something not found")
//            }
//        }
    }

}


//MARK: - UITableViewDataSource,UITableViewDelegate
extension TeacherSideMenuViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sideMenuDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.sideMenu, for: indexPath) as! SideMenuTableViewCell
        cell.setConfig(object:sideMenuDataArray[indexPath.row],index:indexPath.row)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1{
            return kSharedUserDefaults.isUserLoggedIn() ? 60 : 0
        }else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPathData = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example

        let currentCell = tableView.cellForRow(at: indexPathData!)! as! SideMenuTableViewCell
        switch indexPath.row {
        case 0:
            if let tabBarController = self.parent as? TeacherTabbarController{
                tabBarController.selectedIndex = 0
            }
        case 1:
            let controller = ContactUsViewController.getController(storyboard: .Home)
            self.navigationController?.pushViewController(controller, animated: true)
        case 3:
            if kSharedUserDefaults.isUserLoggedIn(){
                self.showPopUp(title: kAppName, message: "Are you sure you want to Logout?.", optionTypeCancel: "Cancel", optionTypeOther: "Logout") { _ in
                    self.logoutForSchoolApi()
                }
            }else{
                kSharedAppManager.moveToLogin()
            }
        case 4:
            self.shareApp(appUrl: "https://apps.apple.com/us/app/creative-kids-home-tutor/id1568231840", sorceView: currentCell.labelSideContent, sender: UIButton())
        default:
            return
        }
    }
    
    func setgradient(color1: UIColor, color2: UIColor) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [color1.cgColor, color2.cgColor]
        gradient.locations = [0.0 , 1.5]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.5)
        gradient.frame = self.viewHeader.bounds
        self.viewHeader.layer.insertSublayer(gradient, at: 0)
    }
}
