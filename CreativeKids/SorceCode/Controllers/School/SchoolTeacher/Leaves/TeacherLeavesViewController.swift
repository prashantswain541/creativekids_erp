//
//  TeacherLeavesViewController.swift
//  CreativeKids
//
//  Created by creativekids solutions on 04/10/22.
//

import UIKit

class TeacherLeavesViewController: UIViewController {
    
    @IBOutlet weak var tableViewLeaveList: LeaveListTableView!
    @IBOutlet weak var viewLeaveRequest: UIView!
    
    var cameFrom: UserType!
    var viewModel:LeaveApplicationsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        initialView()
    }
    
    fileprivate func initialView(){
        statusBarColor(headerColor: UIColor.blue)
        viewLeaveRequest.drawShadow()
        tableViewLeaveList.dataPassDelegate = self
        viewModel = LeaveApplicationsViewModel()
        getLeaveList()
    }
    
    func getLeaveList(){
        viewModel?.getLeaveListForTeacehr(completion: { leaveList in
            self.tableViewLeaveList.getData(cell: TableCellIdentifier.leaveList, data: leaveList)
            self.tableViewLeaveList.reloadData()
        })
    }
    
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonApplyTapped(_ sender: UIButton) {
        let leaveVC = LeaveViewController.getController(storyboard: .StudentDashboard)
        leaveVC.userType = cameFrom
        leaveVC.reloadData = self
        self.navigationController?.pushViewController(leaveVC, animated: true)
    }
}
extension TeacherLeavesViewController: DataSourcesDelegate {
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        guard let kdata = data as? LeaveModel else{return}
        let popupVc = LeavePopupViewController.getController(storyboard: .StudentDashboard)
        popupVc.applicationData = kdata
        popupVc.reloadDataDelegate = self
        popupVc.modalTransitionStyle = .coverVertical
        popupVc.modalPresentationStyle = .overCurrentContext
        self.present(popupVc, animated: false, completion: nil)
    }
}

extension TeacherLeavesViewController:ReloadDataDataSources{
    func reload() {
        getLeaveList()
    }
}
