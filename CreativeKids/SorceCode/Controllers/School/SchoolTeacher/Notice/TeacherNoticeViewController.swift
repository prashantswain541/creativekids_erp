//
//  TeacherNoticeViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 09/05/22.
//

import UIKit


class TeacherNoticeViewController: UIViewController {
    
    var noticeArray = [NoticeModel](){
        didSet{
            tableViewNotice.reloadData()
        }
    }
    @IBOutlet weak var viewCalander: UIView!
    @IBOutlet weak var tableViewNotice: UITableView!
    var viewModel:NoticeViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showTabbar()
        statusBarColor(headerColor: UIColor.blue)
        initialView()
        viewCalander.drawShadow()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabbar()
        viewModel = NoticeViewModel{ [weak self] noticeArray in
             self?.noticeArray = noticeArray
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideTabbar()
    }
    
    func initialView(){
        tableViewNotice.register(UINib(nibName: TableCellIdentifier.noticeCell, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.noticeCell)
        tableViewNotice.delegate = self
        tableViewNotice.dataSource = self
    }
    
    @IBAction func buttonCalenderTapped(_ sender: UIButton) {
        let controller = CalenderViewController.getController(storyboard: .StudentDashboard)
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = .overCurrentContext
        controller.selectionType = .single
        controller.delegate = self
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
}
extension TeacherNoticeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noticeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.noticeCell, for: indexPath) as! NoticeTableViewCell
        cell.cellConfig(data: noticeArray[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedIndex = noticeArray[indexPath.row]
        let controller = NoticeViewPopUpViewController.getController(storyboard: .StudentDashboard)
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = .overCurrentContext
        controller.notice = selectedIndex
        self.navigationController?.present(controller, animated: true)
    }
}

extension TeacherNoticeViewController:SelectedDateDelegate{
    func selectedDate(fromDate: Date?, toDate: Date?) {
        guard let fromDt = fromDate else{return}
        viewModel?.getNotice(date: fromDt, usertype: .Teacher)
    }
    
    
}
