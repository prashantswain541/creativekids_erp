//
//  SelectClassSubjectViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 31/08/22.
//

import UIKit

protocol SelectedDataDelegate{
    func selecetd(textField:UITextField,item:SelectionDelegate)
}

class SelectClassSubjectViewController: UIViewController {
    
    @IBOutlet weak var viewGesture: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var collectionViewSelectedClass: SelectionCollectionView!
    @IBOutlet weak var heightCons: NSLayoutConstraint!
    var selectedDelegate:SelectedDataDelegate?
    var data = [SelectionDelegate]()
    var field:UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewSelectedClass.dataPassDelegate = self
        viewGesture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismiss(_:))))
        viewGesture.isUserInteractionEnabled = true
        view.addSubview(viewGesture)
        collectionViewSelectedClass.getData(cell: CollectionCellIdentifier.selectedCell, data: data)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        self.viewMain.roundCorner(corners: , raddi: 20)
        self.viewMain.roundCorner([.topLeft,.topRight], radius: 20)
    }
    @objc func dismiss(_ gesture:UIGestureRecognizer){
        self.dismiss(animated: true)
    }
    
}

extension SelectClassSubjectViewController:DataSourcesCollectionDelegate{
    func collectionView(_ collectionView: UICollectionView, height: CGFloat) {
        self.heightCons.constant = height
    }
    func collectionView(_ collectionView: UICollectionView, data: Any?, selectedIndex: IndexPath) {
        guard let kData = data as? SelectionDelegate else {return}
        self.dismiss(animated: true) {
            self.selectedDelegate?.selecetd(textField:self.field,item: kData)
        }
        
    }
}
