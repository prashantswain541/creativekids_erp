//
//  AssignHomeworkDateViewController.swift
//  CreativeKids
//
//  Created by creativekids solutions on 01/09/22.
//

import UIKit

class AssignHomeworkDateViewController: UIViewController {
    
    @IBOutlet weak var assignHomeworkTable: DateHomeworkTableView!
    @IBOutlet weak var buttonAssignWork: UIButton!
    
    var datesArray = [HomeWorkDate]()
    var viewModel:HomeWorkDatesViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = HomeWorkDatesViewModel()
        initialView()
    }
    
    private func initialView(){
        assignHomeworkTable.dataPassDelegate = self
        statusBarColor(headerColor: UIColor.blue)
        viewModel?.dataPassDelegate = self
        viewModel?.homeWorkDatesApiForTeacher()
    }
    @IBAction func assignHomeworkTapped(_ sender: UIButton) {
        let nextVc = PeriodListViewController.getController(storyboard: .TeacherDashboard)
        nextVc.periodFor = .today
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AssignHomeworkDateViewController:DataSourcesDelegate {
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        let controller = PeriodListViewController.getController(storyboard: .TeacherDashboard)
        controller.periodFor = .specificDate
        controller.datesData = self.datesArray[selectedIndex]
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension AssignHomeworkDateViewController:HomeWorkDatesDelegate{
    func onSuccess(_ data: Any) {
        datesArray = data as! [HomeWorkDate]
        self.assignHomeworkTable.getData(cell: TableCellIdentifier.dateHomework, data: datesArray)
//        for data in datesArray {
//            self.buttonAssignWork.isHidden = data.date == String.getCurrentDate(format: "dd-MM-yyyy") ? true : false
//        }
    }
}
