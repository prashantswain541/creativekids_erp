//
//  PeriodListViewController.swift
//  CreativeKids
//
//  Created by creativekids solutions on 02/09/22.


import UIKit
enum PeriodsFor{
    case today
    case specificDate
}

class PeriodListViewController: UIViewController {
    
    @IBOutlet weak var viewAssignedChapter: UIView!
    var viewModel:AssignedHomeWorkModel?
    var periodFor:PeriodsFor = .specificDate
    var datesData: HomeWorkDate?
    @IBOutlet weak var periodListTableView: PeriodListTableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        periodListTableView.dataPassDelegate = self
        statusBarColor(headerColor: UIColor.blue)
        viewModel = AssignedHomeWorkModel()
        viewModel?.dataPassDataSources = self
        viewAssignedChapter.drawShadow()
        initialView()
    }
    
    fileprivate func initialView(){
        switch periodFor{
        case .today:
            viewModel?.getPeriodListToAssignHomeWork()
        case .specificDate:
            let convertDate = String.convertDateString(dateString: datesData?.date ?? "", fromFormat: "dd-MM-yyyy", toFormat: "yyyy-MM-dd")
            viewModel?.getPeriodListToAssignHomeWork(forDate: convertDate)
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonAssignChaptersTapped(_ sender: UIButton) {
        let assignVC = AssignChaptersViewController.getController(storyboard: .TeacherDashboard)
        self.navigationController?.pushViewController(assignVC, animated: true)
    }
}

extension PeriodListViewController:AssignedHomeWorkDelegate{
    func periodData(data: [HomeWorkModel]) {
        switch periodFor{
        case .today:
            periodListTableView.getData(cell: TableCellIdentifier.homeWorkAccPeriod, data: data)
        case .specificDate:
            let filteredData = data.filter{$0.isWorkedAssigned}
            periodListTableView.getData(cell: TableCellIdentifier.homeWorkAccPeriod, data: filteredData)
        }
    }
}

extension PeriodListViewController:DataSourcesDelegate{
    func tableViewCallBack(_ tableView: UITableView, selectedIndex: Int, data: Any?, cell: UITableViewCell) {
        guard let assignedWork = data as? HomeWorkModel else { return}
        guard let selectedCell = cell as? HomeWorkAccDateTableViewCell else{return}
        let nextVC = StudentHomeworkViewController.getController(storyboard: .TeacherDashboard)
        nextVC.assignedData = assignedWork
        nextVC.reloadBackData = self
        switch selectedCell.buttonAssignedWork.tag{
        case 0:
            nextVC.cameFor = .Assiged
        case 1:
            nextVC.cameFor = .Update
            nextVC.imageArr = ["https://picsum.photos/200","https://picsum.photos/200/300"]
        default:
            return
        }
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        guard let assignedTask = data as? HomeWorkModel else{return}
        let controller = HomeWorkStudentsViewController.getController(storyboard: .TeacherDashboard)
        controller.assignedWork = assignedTask
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension PeriodListViewController:ReloadDataDataSources{
    func reload() {
        self.initialView()
    }
    
    
}
