//
//  HomeWorkWithQuestionViewController.swift
//  CreativeKids
//
//  Created by Prashant Swain on 09/10/22.
//

import UIKit
enum TableViewAllData:Int,CaseIterable{
    case question = 0
    case questionImage = 1
    case answerImage = 2
}

class HomeWorkWithQuestionViewController: UIViewController {

    enum screenFor{
        case checkStudentWork
        case checkSubmittedWork
    }
    @IBOutlet weak var viewStudentDetail: UIView!
    @IBOutlet weak var labelClassName: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var questionAnswerTableView: UITableView!
    var viewModel:AssignmentCheckViewModel?
    
    var reloadData:ReloadDataDataSources?
    var studentsDetails:StudentWork?

    var submittedWorkForStudent:AssignmentModel?
    var cameFor:screenFor = .checkStudentWork
    var questionImageArr = [Any]()
    var answerImageArr = [Any]()
    var assignmentQuestion = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initialView()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    
    fileprivate func initialView(){
        questionAnswerTableView.delegate = self
        questionAnswerTableView.dataSource = self
        switch cameFor{
        case.checkStudentWork:
            viewStudentDetail.isHidden = false
            buttonSubmit.isHidden = studentsDetails?.workDetail.isChecked ?? false
            labelName.text = studentsDetails?.studentName ?? ""
            labelClassName.text = "\(studentsDetails?.className ?? "") \(studentsDetails?.section ?? "")"
            viewModel = AssignmentCheckViewModel(studentDetail: studentsDetails)
            viewModel?.dataPassDataSources = self
            viewModel?.workQuestionApi()
            viewModel?.getStudentAssignment()
        case .checkSubmittedWork:
            setUpForStudent()
        }
        
        
    }
    func setUpForStudent(){
        viewStudentDetail.isHidden = true
        buttonSubmit.isHidden = true
        self.questionImageArr = submittedWorkForStudent?.questionImages ?? []
        self.answerImageArr = submittedWorkForStudent?.answerImages ?? []
        questionAnswerTableView.reloadData()
    }

    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSubmitTapped(_ sender: UIButton) {
        viewModel?.assignmentCheckedByTeacherApi()
    }
}

extension HomeWorkWithQuestionViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        TableViewAllData.allCases.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case TableViewAllData.question.rawValue:
            return 1
        case TableViewAllData.questionImage.rawValue:
            return 1
        case TableViewAllData.answerImage.rawValue:
            return answerImageArr.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case TableViewAllData.question.rawValue:
            let cell = questionAnswerTableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.homeWorkQuestionChap, for: indexPath) as! QuestionChapterTableViewCell
            cell.configCell(data: self.assignmentQuestion)
            return cell
        case TableViewAllData.questionImage.rawValue:
            let cell = questionAnswerTableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.homeWorkQuestionImages, for: indexPath) as! QuestionImagesTableViewCell
            cell.configCell(data: questionImageArr)
            cell.dataPassDataBack = self
            return cell
        case TableViewAllData.answerImage.rawValue:
            let cell = questionAnswerTableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.homeWorkAnswer, for: indexPath) as! AnswerImagesTableViewCell
            cell.viewHeader.isHidden = indexPath.row != 0
            cell.configCell(data: answerImageArr[indexPath.row])
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section)
        switch indexPath.section{
        case TableViewAllData.question.rawValue:
            return
        case TableViewAllData.questionImage.rawValue:
            print("yeehhh")
        case TableViewAllData.answerImage.rawValue:
            let controller = ImagePreviewViewController.getController(storyboard: .TeacherDashboard)
            controller.imageArr = answerImageArr
            controller.selcetedIndex = indexPath.row
            self.navigationController?.pushViewController(controller, animated: true)
        default:
            print("yeehhh")
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
}

extension HomeWorkWithQuestionViewController: AssignmentCheckedDataSources{
    func onSuccess(_ data: Any) {
        guard let homeWorkData = data as? AssignmentModel else {return}
        print(homeWorkData)
        self.questionImageArr = homeWorkData.questionImages
        self.assignmentQuestion = homeWorkData.question ?? ""
        DispatchQueue.main.async {
            self.questionAnswerTableView.reloadData()
        }
        
    }
    
    func studentWorked(data: Any) {
        guard let studentWork = data as? [StudentSubmitedWork] else{return}
        print(studentWork)
        self.answerImageArr = studentWork.map{String.toString($0.submittedImage)}
        DispatchQueue.main.async {
            self.questionAnswerTableView.reloadData()
        }
    }
    func checkedStatus(status: Bool) {
        if status{
            reloadData?.reload()
            self.navigationController?.popViewController(animated: true)
        }else{
            reloadData?.reload()
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension HomeWorkWithQuestionViewController: PassNestedCollectionBackDataSources{
    func getSelectedCell(_ tableCell: UITableViewCell, data: Any?, selectedIndex: Int) {
        let controller = ImagePreviewViewController.getController(storyboard: .TeacherDashboard)
        controller.imageArr = questionImageArr
        controller.selcetedIndex = selectedIndex
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
