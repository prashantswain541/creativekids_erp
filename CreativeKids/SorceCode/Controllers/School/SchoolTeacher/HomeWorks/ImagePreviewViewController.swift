//
//  ImagePreviewViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 23/03/22.
//

import UIKit


class ImagePreviewViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonPrevious: UIButton!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var labelCurrentImage: UILabel!
    @IBOutlet weak var labelHeader:UILabel!
    
    
    enum CameFor{
        case Student
        case Teacher
        case Chat
    }
    var reloadData:ReloadDataDataSources?
    var cameFor:CameFor = .Student
    var imageArr = [Any]()
    var selcetedIndex = 0
    var headerName = ""
    var currntIndex:Int = 0 {
        didSet {
            setCurrentImgLabel()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
        initialView()
    }
    
    private func initialView(){
        switch cameFor{
        case .Teacher, .Student:
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.001){
                self.imageCollectionView.scrollToItem(at: IndexPath(item: self.selcetedIndex, section: 0), at: .centeredHorizontally, animated: false)
                self.currntIndex = self.selcetedIndex
            }
        case .Chat:
            buttonNext.isHidden = true
            buttonPrevious.isHidden = true
            labelCurrentImage.text = ""
            labelHeader.text = headerName
        }
    }
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonPhotoChange(_ sender: UIButton) {
        if !imageArr.isEmpty{
            if sender.tag == 0{
                if !(currntIndex == 0){
                    currntIndex -= 1
                }else{
                    return
                }
            }else{
                if !(currntIndex == imageArr.count - 1){
                    currntIndex += 1
                }else{
                    return
                }
            }
            DispatchQueue.main.async {
                self.imageCollectionView.scrollToItem(at: IndexPath(item: self.currntIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    func setCurrentImgLabel(){
        DispatchQueue.main.async {
            self.labelCurrentImage.text = "\(self.currntIndex + 1)/\(self.imageArr.count)"
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView:  UIScrollView) {
        let visibleCellIndex = imageCollectionView.indexPathsForVisibleItems
        currntIndex = visibleCellIndex.first!.item
    }
}

//MARK: COLLECTION DELEGATE AND DATASOURCCE
extension ImagePreviewViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagePreviewCollectionViewCell", for: indexPath) as! ImagePreviewCollectionViewCell
        let object = imageArr[indexPath.item]
        if let imageData = object as? String{
            let imageUrl = imageData.contains("http") ? imageData : (homeWorkImageBaseUrl + imageData)
            cell.imagePreview.downloadImageFromURL(urlString: imageUrl)
        }else{
            if let imageData = object as? UIImage{
                cell.imagePreview.image = imageData
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}
