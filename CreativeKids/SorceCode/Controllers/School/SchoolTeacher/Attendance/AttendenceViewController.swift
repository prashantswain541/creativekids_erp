//
//  AttendenceViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/02/22.
//

import UIKit

enum AttandanceType:Int{
    case none = 0
    case morning = 1
    case afternoon = 2
}

class AttendenceViewController: UIViewController {
    
    @IBOutlet weak var tableViewAttendance: AttendanceTableView!
    @IBOutlet weak var textFieldClass: UITextFieldPaddingView!
    @IBOutlet weak var textFieldSection: UITextFieldPaddingView!
    @IBOutlet weak var imageMorning: UIImageView!
    @IBOutlet weak var imageAfternoon: UIImageView!
    
    
    @IBOutlet weak var viewButtonSubmit: UIView!
    var attandanceType:AttandanceType = .none
    var classes = [Class]()
    var section = [Section]()
    var selctedClass:Class?
    var selectedSection:Section?
    var studentArr = [StudentAttendance]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showTabbar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabbar()
        initialSetUp()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideTabbar()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    
    func initialSetUp(){
        self.tableViewAttendance?.tableDelegate = self
        self.tableViewAttendance.getData(cell: "cell", data: [])
        textFieldClass.text = ""
        textFieldSection.text = ""
        attandanceType = .none
        radioAttandence(type: attandanceType)
        if self.classes.isEmpty{
            commonApi.getClassesBySchool(schoolId:kUserData.schid ?? "") { [weak self] classArray in
                self?.classes = classArray
            }
        }
    }
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func buttonClassTapped(_ sender: UIButton) {
        CommonUtils.showDropDown(sender: sender, dataSources: classes.map{String.getstring($0.className)}) { [weak self] (index, item) in
            self?.textFieldSection.text = ""
            self?.tableViewAttendance.message = "Select Section"
            self?.radioAttandence(type: .none)
            self?.textFieldClass.text = item
            self?.selctedClass = self?.classes[index]
            commonApi.getSectionByClass(classId: String.getstring(self?.classes[index].id)) { [weak self] getSectionArray in
                self?.section = getSectionArray
            }
        }
    }
    
    @IBAction func buttobSectionTapped(_ sender: UIButton) {
        if self.textFieldClass.text?.isEmpty ?? true{
            showAlertMessage.alert(message: "Select class")
            return
        }else{
            CommonUtils.showDropDown(sender: sender, dataSources: section.map{String.getstring($0.sectionName)}) { [weak self] (index, item) in
                self?.tableViewAttendance.message = "Select Attendance"
                self?.radioAttandence(type:.none)
                self?.selectedSection = self?.section[index]
                self?.textFieldSection.text = item
            }
        }
        
    }
    
    
    @IBAction func buttonAttandenceTapped(_ sender: UIButton) {
        attandanceType = AttandanceType(rawValue: sender.tag) ?? .none
        radioAttandence(type: attandanceType)
    }
    
    @IBAction func buttonSubmitTapped(_ sender: UIButton) {
        if attandanceType == .none{
            showAlertMessage.alert(message: "Select Attendance")
            return
        }else if self.textFieldClass.text?.isEmpty ?? true{
            showAlertMessage.alert(message: "Select class")
            return
        }else if self.textFieldSection.text?.isEmpty ?? true{
            showAlertMessage.alert(message: "Select section")
            return
        }else if !isStudentAttendanceMarked(){
            showAlertMessage.alert(message: "Please mark attendance for all student")
            return
        }else{
            let _ = StudentAttendanceViewModel(studentArrendanceList: self.studentArr, attendanceFor: self.attandanceType) { status in
                DispatchQueue.main.async {
                    self.showSimpleAlert(message: "Students attendance mark successfully.")
                }
                
            }
        }
    }
    
    private func radioAttandence(type:AttandanceType){
        if type.rawValue == 0{
            imageMorning.image = UIImage(named: "Radio button")
            imageAfternoon.image = UIImage(named: "Radio button")
            self.studentArr = []
            self.tableViewAttendance.getData(cell: "cell", data: self.studentArr)
            return
        }else if type.rawValue == 1{
            if validation(){
            imageMorning.image = UIImage(named: "Radio button_1")
            imageAfternoon.image = UIImage(named: "Radio button")
            }
        }else{
            if validation(){
            imageAfternoon.image = UIImage(named: "Radio button_1")
            imageMorning.image = UIImage(named: "Radio button")
            }
        }
    
    }
    
    func isStudentAttendanceMarked() -> Bool{
        let unattendanceList = self.studentArr.filter{!$0.isSelected}
        return unattendanceList.isEmpty ? true : false
    }
    
    func validation()->Bool{
        if self.textFieldClass.text?.isEmpty ?? true{
            showAlertMessage.alert(message: "Select class")
            return false
        }else if self.textFieldSection.text?.isEmpty ?? true{
            showAlertMessage.alert(message: "Select section")
            return false
        }else{
            let _ = StudentAttendanceViewModel(selectedClass:String.getstring(self.selctedClass?.id),selectedSection: String.getstring(self.selectedSection?.id), attendanceType: self.attandanceType ) {[weak self] studentArray in
                self?.studentArr = studentArray
                self?.tableViewAttendance.message = self?.studentArr.isEmpty ?? true ? "No Data Found" : ""
                self?.tableViewAttendance.getData(cell: "cell", data: self?.studentArr ?? [])
            }
            return true
        }
    }
}

extension AttendenceViewController:TableDataDelegate{
    func notifyWhenScrollToButtom(status: Bool) {
        viewButtonSubmit.isHidden = !status
    }
}
