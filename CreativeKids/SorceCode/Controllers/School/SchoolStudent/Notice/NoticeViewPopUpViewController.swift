//
//  NoticeViewPopUpViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 23/06/22.
//

import UIKit

class NoticeViewPopUpViewController: UIViewController {

    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var labelNotice: UILabel!

    var notice:NoticeModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialView()
        // Do any additional setup after loading the view.
    }
    
    func initialView(){
        guard let kdata = notice else{return}
        labelNotice.text = String.getstring(kdata.message)
        labelHeader.attributedText = createAttributedText(firstText: kdata.name, secondText: " (\(kdata.designation))")
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    
}
