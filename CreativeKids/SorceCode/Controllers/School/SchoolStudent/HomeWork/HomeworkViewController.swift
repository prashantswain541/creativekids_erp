//
//  HomeworkViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/02/22.
//

import UIKit

class HomeworkViewController: UIViewController {
    
    var viewModel:AssignedWorkForDateViewModel?
    var selectedDate:HomeWorkDate?
    @IBOutlet weak var tableViewHomework: HomeWorkListTableView! {
        didSet {
            tableViewHomework.navigate = self.navigationController
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
        viewModel = AssignedWorkForDateViewModel()
        initialsetUp()
    }
    func initialsetUp(){
        viewModel?.dataPassDelegate = self
        tableViewHomework?.dataPassDelegate = self
        viewModel?.assignedWorkForDateApi(date: selectedDate?.date ?? "")
    }
    @IBAction func bcakTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension HomeworkViewController:AssignedWorkForDateDelegate{
    func onSuccess(_ data: Any) {
        guard let dataArray = data as? [HomeWork] else{return}
        tableViewHomework.getData(cell: "cell", data: dataArray)
    }
}

extension HomeworkViewController:DataSourcesDelegate{
    func tableViewCallBackMultipleAction(_ tableView: UITableView, selectedIndex: Int, data: Any?, cell: UITableViewCell, actionFor: Any) {
        guard let action = actionFor as? Int else{return}
        switch action{
        case 0:
            let homeworkDetails = HomeworkDetailsViewController.getController(storyboard: .StudentDashboard)
            homeworkDetails.cameFor = .Add
            homeworkDetails.reloadBackScreen = self
            homeworkDetails.work = (data as! HomeWork)
            self.navigationController?.pushViewController(homeworkDetails, animated: true)
        case 1:
            let homeworkDetails = HomeworkDetailsViewController.getController(storyboard: .StudentDashboard)
            homeworkDetails.cameFor = .Update
            homeworkDetails.reloadBackScreen = self
            homeworkDetails.work = (data as! HomeWork)
            self.navigationController?.pushViewController(homeworkDetails, animated: true)
        default:
            return
        }
    }
}

extension HomeworkViewController:ReloadDataDataSources{
    func reload() {
        initialsetUp()
    }
}


