//
//  StudentFeesViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 09/03/22.
//

import UIKit

class StudentFeesViewController: UIViewController {

    @IBOutlet weak var tableViewStudentFees: StudentFeesTableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
        fetchData()
    }
    
    private func fetchData(){
     let _ = FeeViewModel(){ [weak self] feesDataArray in
         self?.tableViewStudentFees.getData(cell: TableCellIdentifier.studentFeesCell, data: feesDataArray)
        }
    }
   
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

