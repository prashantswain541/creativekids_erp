//
//  LeaveViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 14/03/22.
//

import UIKit
import IQKeyboardManagerSwift

enum LeaveType:Int{
    case halfDay = 0
    case fullDay = 1
    case longLeave = 2
}

class LeaveViewController: UIViewController {
    
    @IBOutlet weak var textViewSubject: IQTextView!
    @IBOutlet weak var textFieldFromDate: UITextField!
    @IBOutlet weak var textFieldToDate: UITextField!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var textViewBody: IQTextView!
    @IBOutlet var buttonsLeaveType: [UIButton]!
    @IBOutlet weak var labelSignature: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelClass: UILabel!
    
    
    var leaveViewModel:LeaveViewModel?
    var userType:UserType = .Student
    var forDatePicker = 0
    var datepicker = UIDatePicker()
    var fromDate = Date()
    var toDate = Date()
    var leaveType:LeaveType?
    var reloadData:ReloadDataDataSources?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialView()
    }
    
    fileprivate func initialView(){
        textFieldFromDate.delegate = self
        textFieldToDate.delegate = self
        statusBarColor(headerColor: UIColor.blue)
        let tabSignature = UITapGestureRecognizer(target: self, action: #selector(openSignatureView(_ :)))
        self.imageViewSignature.addGestureRecognizer(tabSignature)
        leaveViewModel = LeaveViewModel.init(vc: self)
        labelSignature.text = userType == .Student ? "Guardian Signature" : "Signature"
        self.labelName.text = "Name: \( kUserData.userName ?? "")"
        self.labelClass.text = "Class: \(kUserData.className ?? "")"
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonLeaveTypeTapped(_ sender: UIButton) {
        manageUiLeaveType(sender:sender)
    }
    
    func manageUiLeaveType(sender:UIButton){
        buttonsLeaveType.forEach{$0.isSelected = false}
        sender.isSelected = true
        switch sender.tag {
        case 0:
            self.textFieldToDate.isHidden = true
            self.textFieldFromDate.placeholder = "Date"
            self.leaveType = .halfDay
        case 1:
            self.textFieldToDate.isHidden = true
            self.textFieldFromDate.placeholder = "Date"
            self.leaveType = .fullDay
        case 2:
            self.textFieldToDate.isHidden = false
            self.textFieldFromDate.placeholder = "From Date"
            self.leaveType = .longLeave
        default:
            return
        }
    }
    
    @IBAction func buttonSubmitTapped(_ sender: UIButton) {
            if leaveViewModel?.validation() ?? false{
                leaveViewModel?.leaveApplicationApi(userType:self.userType,leaveType: self.leaveType ?? .halfDay){ status in
                    if status{
                        self.reloadData?.reload()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
    }
}

extension LeaveViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case textFieldFromDate:
            createDatePicker(textField:textField)
        case textFieldToDate:
            if textFieldFromDate.text == ""{
                self.view.endEditing(false)
                self.showSimpleAlert(message: "Please select from date first")
                return
            }else{
                createDatePicker(textField:textField)
            }
        default:
            return
        }
    }
    func createDatePicker(textField:UITextField) {
        let calender = Calendar.current
        self.forDatePicker = textField.tag
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let donebtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donepressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem:UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelbtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelPressed))
        toolbar.setItems([cancelbtn,spaceButton,donebtn], animated: true)
        textField.inputAccessoryView = toolbar
        textField.inputView = datepicker
        datepicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datepicker.preferredDatePickerStyle = .wheels
        }
        if forDatePicker == 1 {
            datepicker.minimumDate = calender.date(byAdding: .day, value: 1, to: fromDate)
        } else {
            datepicker.minimumDate = calender.date(byAdding: .day, value: 0, to: Date())
        }
    }
    
    // Done button presed
    @objc func donepressed() {
        let dateformator = DateFormatter()
       // dateformator.dateFormat = "yyyy-M-dd"
        dateformator.dateFormat = "MM/dd/yyyy"
        if forDatePicker == 0{
            textFieldFromDate.text = dateformator.string(from: datepicker.date)
            fromDate = datepicker.date
            textFieldFromDate.resignFirstResponder()
        }else{
            textFieldToDate.text = dateformator.string(from: datepicker.date)
            toDate = datepicker.date
        }
        self.view.endEditing(true)
        if self.leaveType == .longLeave{
            if self.textFieldToDate.text != "" {
                self.textFieldToDate.resignFirstResponder()
            } else {
                self.textFieldToDate.becomeFirstResponder()
            }
        }
    }
    
    // Cancel button presed
    @objc func cancelPressed(){
        self.view.endEditing(true)
    }
    
    @objc func openSignatureView(_ sender: UITapGestureRecognizer) {
        let signatureVC = SignatureViewController.getController(storyboard: .StudentDashboard)
        signatureVC.signatureDatasource = self
        self.navigationController?.pushViewController(signatureVC, animated: true)
    }
}

extension LeaveViewController: SignatureDatasource {
    func getSignature(_ signature: UIImage) {
        self.imageViewSignature.image = signature
    }
}
