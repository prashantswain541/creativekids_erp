//
//  LeavePopupViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/03/22.
//

import UIKit


class LeavePopupViewController: UIViewController {
    
    @IBOutlet weak var buttonApprove: UIButton!
    @IBOutlet weak var buttonDecline: UIButton!
    
    @IBOutlet weak var labelSubject: UILabel!
    @IBOutlet weak var labelBody: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageSignature: UIImageView!
    
    var reloadDataDelegate:ReloadDataDataSources?
    var applicationAppDecViewModel:ApplicationApproveDeclineViewModel?
    var applicationData:LeaveModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        initialView()
    }
    
    func initialView(){
        statusBarColor(headerColor: UIColor.blue)
        applicationAppDecViewModel = ApplicationApproveDeclineViewModel(vc: self)
        buttonApprove.isHidden = !(applicationData?.leaveFor == .teacherForApproval)
        buttonDecline.setTitle((applicationData?.leaveFor == .teacherForApproval) ? "Decline" : "Close", for: .normal)
        labelSubject.attributedText = createAttributedText(firstText: "Subject: ", secondText: applicationData?.applicationSubject ?? "")
        labelSubject.numberOfLines = 0
        labelBody.text = String.getstring(applicationData?.message ?? "").isEmpty ? applicationData?.leave ?? "" : applicationData?.message ?? ""
        showLeavesDate()
        imageSignature.downlodeImage(serviceurl: applicationData?.signatureImage ?? "", placeHolder: UIImage())
    }
    
    func showLeavesDate(){
        if applicationData?.toDate?.isEmpty ?? true{
            labelDate.attributedText = createAttributedText(firstText: "Leave Date: ", secondText: String.convertDateString(dateString: applicationData?.fromDate ?? "", fromFormat: kUserData.role == "2" ? "yyyy-MM-dd'T'hh:mm:ss": "dd-MM-yyyy", toFormat: "dd-MM-yyyy"))
        }else{
            labelDate.attributedText = createAttributedText(firstText: "Leave Date: ", secondText: String.convertDateString(dateString: applicationData?.fromDate ?? "", fromFormat: kUserData.role == "2" ? "yyyy-MM-dd'T'hh:mm:ss": "dd-MM-yyyy", toFormat: "dd-MM-yyyy") + " - " + String.convertDateString(dateString: applicationData?.toDate ?? "", fromFormat: kUserData.role == "2" ? "yyyy-MM-dd'T'hh:mm:ss": "dd-MM-yyyy", toFormat: "dd-MM-yyyy"))
        }
    }
    @IBAction func buttonCrossTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func buttonApproveTapped(_ sender: UIButton) {
        applicationAppDecViewModel?.approveApplication{ status in
            if status{
                self.reloadDataDelegate?.reload()
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    @IBAction func buttonDeclineTapped(_ sender: UIButton) {
        _ = kSharedUserDefaults.getLoggedInUserDetails()["role"] as? String
        if (applicationData?.leaveFor == .teacherForApproval) {
            applicationAppDecViewModel?.declineApplication{ status in
                if status{
                    self.reloadDataDelegate?.reload()
                    self.dismiss(animated: false, completion: nil)
                }
            }
        }else{
            self.dismiss(animated: false, completion: nil)
        }
    }
}
