//
//  LeaveListViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/03/22.
//

import UIKit

enum LeaveFor{
    case student
    case teacher
}

class LeaveListViewController: UIViewController {
    
    
    @IBOutlet weak var tableViewLeaveList: LeaveListTableView!
    @IBOutlet weak var viewLeaveRequest: UIView!
    @IBOutlet weak var buttonLeaveApply: UIButton!
    
    var cameFrom:UserType = .Student
    var leaveListViewModel:LeaveApplicationsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewLeaveList.dataPassDelegate = self
        self.leaveListViewModel = LeaveApplicationsViewModel()
        initialView()
//      self.tableViewLeaveList.getData(cell: "cell", data: [""])
    }
    
    func initialView(){
        statusBarColor(headerColor: UIColor.blue)
        viewLeaveRequest.drawShadow()
        self.buttonLeaveApply.setImage(self.cameFrom == .Student ? UIImage(named: "Apply_leave") : UIImage(named: "My Leaves"), for: .normal)
        getLeaveList()
    }
    
    func getLeaveList(){
        switch cameFrom{
        case .Student:
            self.leaveListViewModel?.getLeaveListForStudent{ listArray in
                self.tableViewLeaveList.getData(cell: "cell", data: listArray.reversed())
            }
        case .Teacher:
            self.leaveListViewModel?.getStudentLeaveListForTeacher{ listArray in
            self.tableViewLeaveList.getData(cell: "cell", data: listArray)
            }
        }
    }
    
    @IBAction func buttonApplyLeaveTapped(_ sender: UIButton) {
        if cameFrom == .Student {
            let leaveVC = LeaveViewController.getController(storyboard: .StudentDashboard)
            leaveVC.userType = cameFrom
            leaveVC.reloadData = self
            self.navigationController?.pushViewController(leaveVC, animated: true)
        } else {
            let leaveVC = TeacherLeavesViewController.getController(storyboard: .TeacherDashboard)
            leaveVC.cameFrom = cameFrom
            self.navigationController?.pushViewController(leaveVC, animated: true)
        }
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension LeaveListViewController: DataSourcesDelegate {
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        guard let kdata = data as? LeaveModel else{return}
        let popupVc = LeavePopupViewController.getController(storyboard: .StudentDashboard)
        popupVc.applicationData = kdata
        popupVc.reloadDataDelegate = self
        popupVc.modalTransitionStyle = .coverVertical
        popupVc.modalPresentationStyle = .overCurrentContext
        self.present(popupVc, animated: false, completion: nil)
    }
}

extension LeaveListViewController:ReloadDataDataSources{
    func reload() {
        getLeaveList()
    }
}
