//
//  StudentAttendanceViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 05/03/22.
//

import UIKit
import FSCalendar
class StudentAttendanceViewController: UIViewController {
    
    @IBOutlet weak var tableViewAttendance: StudentAttendanceTable!
    @IBOutlet weak var collectionViewAttendance: StudentAttendanceCollectionView!
    
    @IBOutlet weak var textFieldFromDate: UITextField!
    @IBOutlet weak var textFieldToDate: UITextField!
    @IBOutlet weak var labelTotalAttendance: UILabel!
    @IBOutlet weak var labelTotalPresent: UILabel!
    @IBOutlet weak var labelTotalAbsent: UILabel!
    @IBOutlet weak var labelTotalLeave: UILabel!
    @IBOutlet weak var circularProgress: CircularProgressView!
    
    var studentAttendanceViewModel:StudentAttendViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showTabbar()
        statusBarColor(headerColor: UIColor.blue)
//        self.tableViewAttendance.getData(cell: "cell", data: [""])
        initialViewLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabbar()
        self.circularProgress.layoutIfNeeded()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideTabbar()
    }
    
    func initialViewLoad(){
        self.studentAttendanceViewModel = StudentAttendViewModel(vc: self)
        self.studentAttendanceViewModel?.studentAttendanceDetail(completion: { [weak self] attendanceDetail in
            self?.getStudentAttendanceDetail(detail: attendanceDetail)
        })
        self.studentAttendanceViewModel?.callBackDateAccording = { [weak self] detailArray in
            self?.collectionViewAttendance.getData(cell: CollectionCellIdentifier.StudentAttendCell, data: detailArray)
        }
    }
    
    @IBAction func buttonCalenderTapped(_ sender: UIButton) {
        let controller = CalenderViewController.getController(storyboard: .StudentDashboard)
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = .overCurrentContext
        controller.delegate = self
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    
    func getStudentAttendanceDetail(detail:StudentAttendanceDetailModel){
        labelTotalAttendance.attributedText = createAttributedText(firstText: "Total Attendance :", secondText: detail.totalAttendance)
        labelTotalPresent.attributedText = createAttributedText(firstText: "Present :", secondText: detail.totalPresent)
        labelTotalAbsent.attributedText = createAttributedText(firstText: "Absent :", secondText: detail.totalAbsent)
        labelTotalLeave.attributedText = createAttributedText(firstText: "Leave :", secondText: detail.totalLeave)
        circularProgress.trackClr = UIColor.lightGray
        circularProgress.progressClr = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        let totalAttendance = Float(Int.getInt(String.getstring(detail.totalAttendance)))
        let totalpresent = Float(Int.getInt(String.getstring(detail.totalPresent)))
        if totalAttendance == 0{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.circularProgress.setProgressWithAnimation(duration: 1.0, value: Float(1.0), percentageColor: UIColor.black)
            }
        }else{
            let performance = Float(totalpresent/totalAttendance)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.circularProgress.setProgressWithAnimation(duration: 1.0, value:performance, percentageColor: UIColor.black)
            }
        }
        
        
        //createAttributedText
    }
}

extension StudentAttendanceViewController: SelectedDateDelegate{
    func selectedDate(fromDate: Date?, toDate: Date?) {
        if let fmDate = fromDate {
            textFieldFromDate.text = String.getDateToString(date: fmDate, format: "MM/dd/yyyy")
            if let toDAte = toDate {
                textFieldToDate.isHidden = false
                textFieldToDate.text = String.getDateToString(date: toDAte, format: "MM/dd/yyyy")
                self.studentAttendanceViewModel?.studentAttendanceDateAcc()
            }else{
                textFieldToDate.isHidden = true
                textFieldToDate.text = String.getDateToString(date: fmDate, format: "MM/dd/yyyy")
                self.studentAttendanceViewModel?.studentAttendanceDateAcc()
            }
        }
    }
}
