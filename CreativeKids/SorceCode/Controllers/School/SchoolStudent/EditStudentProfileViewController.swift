//
//  EditStudentProfileViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 28/03/22.
//

import UIKit

class EditStudentProfileViewController: UIViewController {
    
    @IBOutlet weak var imageProfile: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
    }
    @IBAction func buttonSelectImageTapped(_ sender: UIButton) {
        ImagePickerHelper.shared.showPickerController { image in
            self.imageProfile.image = image
        }
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
