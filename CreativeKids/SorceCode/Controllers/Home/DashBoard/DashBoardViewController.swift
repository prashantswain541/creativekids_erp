//
//  SubjectViewController.swift
//  CreativeKids(Updated)
//
//  Created by Creative Kids on 12/04/21.
//

import UIKit
import Foundation
import MediaPlayer
import KYDrawerController
import SDWebImage
import Instructions
import Alamofire

enum ModuleFor{
    case normal, student, teacher
}
enum CameFromDashBoard {
    case login, sideMenu, changedClass
}
var moduleFor:ModuleFor = .normal
var cameFrom:CameFromDashBoard = .login

class DashboardViewController: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var viewProgress: UIView!
    @IBOutlet weak var subjectCollectionView: SubjectCollectionView!
    @IBOutlet weak var popularVideosCollectionView: YouTubeCollectionView!
    @IBOutlet weak var continueLearningTableView: ContinueLearningTableView!
    @IBOutlet weak var subjectCollectionHeigh: NSLayoutConstraint!
    
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelSchoolName: UILabel!
    @IBOutlet weak var circularProgress: CircularProgressView!
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var imageViewClassArrow: UIImageView!
    @IBOutlet weak var labelContinueLearning: UILabel!
    
    @IBOutlet weak var buttonClass: UIButton!
    
    @IBOutlet weak var labelChapterRead: UILabel!
    @IBOutlet weak var labelExerciseDone: UILabel!
    @IBOutlet weak var labelTestDone: UILabel!
    @IBOutlet weak var labelUserClass:UILabel!
    
    @IBOutlet weak var progressChapterRead: UIProgressView!
    @IBOutlet weak var progressExerciseDone: UIProgressView!
    @IBOutlet weak var progressTestDone: UIProgressView!
    
    @IBOutlet weak var viewClass: UIView!
    @IBOutlet weak var buttonProgressView:UIButton!
    @IBOutlet weak var buttonSideMenu:UIButton!
    
    @IBOutlet weak var viewGetProgress:UIView!
    @IBOutlet weak var viewProfileImage: UIView!
    @IBOutlet weak var heightContinueLearningTableView: NSLayoutConstraint!
    @IBOutlet weak var topContinueLearningTableView: NSLayoutConstraint!
    @IBOutlet weak var progressSliderWidth:NSLayoutConstraint!
    
    @IBOutlet weak var subjectsLabel: UILabel!
    @IBOutlet weak var continueLearningLabel: UILabel!
    @IBOutlet weak var popularVideosLabel: UILabel!
    
    @IBOutlet weak var upperScrollView: UIScrollView!
    
    @IBOutlet weak var buttonScanWidth: NSLayoutConstraint!
    @IBOutlet weak var buttonScan: UIButton!
    
    //MARK:- variable
    var topicSearchDataArray = [TopicSearchModel]()
    var subjectViewModel:SubjectViewModel?
    var youTubeViewModel:YouTubeListViewModel?
    var topicSearch:TopicSearchViewModel?
    var performanceViewModel:PerformanceViewModel?
    var recentChapter = [TopicSearchModel]()
    var subjectListArray = [SubjectModel]()
    
    
    var firstSubjectCell:SubjectCollectionViewCell?
    var firstYouTubeCell:PopularVideosCollectionViewCell?
    
    // MARK: - Public properties
    var coachMarksController = CoachMarksController()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if !isUserGreetDone{
            getCurrentTimeForGreetUser()
        }
        //textFieldTopicSearch
        setGradient()
        initialLoad()
        setUserDetail()
        self.setOverAllProgressView()
        coachMarksController.overlay.isUserInteractionEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        continueLearningConfig()
        progressSliderWidth.constant = UIDevice.isPhone ? 100 : 150
        setUserDetail()
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setGradient()
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        DispatchQueue.main.async {
            self.viewProfileImage.layer.cornerRadius = self.viewProfileImage.frame.height/2
            self.circularProgress.layoutIfNeeded()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !kSharedUserDefaults.isAppTourDone(){
            startInstructions()
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        coachMarksController.stop(immediately: true)
    }
    
    func startInstructions() {
        coachMarksController.start(in: .window(over: self))
    }
    
    @IBAction func buttonSideMenuTapped(_ sender: UIButton) {
        if (String.getstring(kUserData.role) == DashboardType.Student.userType) || (String.getstring(kUserData.role) == DashboardType.Teacher.userType){
            self.navigationController?.popViewController(animated: true)
        }else{
            if let kyDrawer = self.parent as? KYDrawerController{
                kyDrawer.drawerWidth = UIDevice.isPhone ? self.view.frame.width - 100 : self.view.frame.width - 250
                kyDrawer.setDrawerState(.opened, animated: true)
            }
        }
       
    }
    @IBAction func buttonChangedClass(_ sender: UIButton){
        switch moduleFor{
        case .normal:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                let controller = ClassSelectionViewController.getController(storyboard: .Registration)
                controller.cameFor = .classChanged
                controller.changeClassCallBack = {
                    cameFrom = .changedClass
                    self.setUserSelectedClass()
                    self.getDataFromViewModels()
//                  self.setOverAllProgressView()
                }
                controller.demoClassCallBack = {
                    cameFrom = .changedClass
                    self.setUserSelectedClass()
                    self.getDataFromViewModels()
                }
                self.navigationController?.present(controller, animated: true, completion: nil)
            }
        case .student,.teacher:
            return
        }
        
    }
    
    @IBAction func buttonProgressTapped(_ sender: UIButton) {
        self.showSimpleAlert(message: "Under Development")
//        if kSharedUserDefaults.isUserLoggedIn(){
//            let controller = ProgressReportViewController.getController(storyboard: .Home)
//            controller.subjectListArray = self.subjectListArray
//            self.navigationController?.pushViewController(controller, animated: true)
//        }else{
//            return
//        }
    }

    @IBAction func buttonNotificationTapped(_ sender: UIButton){
        self.showSimpleAlert(message: "Under Development")
//        let controller = CheckViewController.getController(storyboard: .Home)
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func buttonScanTapped(_ sender: UIButton) {
        let scanner = ScannerViewController.getController(storyboard: .Home)
        self.navigationController?.pushViewController(scanner, animated: true)
    }
    
    //MARK: - Set Gradient
    func setGradient() {
        DispatchQueue.main.async {
            self.viewProgress.setgradient(color1: .blue, color2: .systemIndigo)
        }
    }
    
    //MARK: - initialLoad
    func initialLoad(){
        continueLearningTableView.dataPassDelegate = self
        subjectCollectionView.dataSourcesCollection = self
        popularVideosCollectionView.dataSourcesCollection = self
        getDataFromViewModels()
        getYoutubeDataFromViewModels()
        subjectsLabel.setfont(font: 13, fontFamily: futuraFont)
        continueLearningLabel.setfont(font: 13, fontFamily: futuraFont)
        popularVideosLabel.setfont(font: 13, fontFamily: futuraFont)
        if String.getstring(kUserData.role) == DashboardType.Student.userType || String.getstring(kUserData.role) == DashboardType.Teacher.userType{
            buttonSideMenu.setImage(UIImage.init(systemName: "arrow.left"), for: .normal)
        }else{
            checkAdditionEmail()
            buttonSideMenu.setImage(UIImage.init(systemName: "text.justify"), for: .normal)
        }
        upperScrollView.delegate = self
        // self.tableViewChapter.scrollingDelegate = self
    }
    //MARK: - Check number
    func checkAdditionEmail(){
        if kSharedUserDefaults.isUserLoggedIn(){
            if (kUserData.parentEmail?.isEmpty ?? true && kUserData.teacherEmail?.isEmpty ?? true) {
                let controller = AdditionalDetailViewController.getController(storyboard: .Registration)
                controller.modalPresentationStyle = .overCurrentContext
                controller.modalTransitionStyle = .crossDissolve
                self.navigationController?.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - GET DATA FROM VIEW MODEL
    func getDataFromViewModels(){
        switch String.getstring(kUserData.role){
        case DashboardType.Student.userType:
            subjectViewModel = SubjectViewModel(complition: { data in
                self.subjectListArray = data
                self.subjectCollectionView.getData(cell: CollectionCellIdentifier.subject, data: data, language: "English")
                DispatchQueue.main.async {
                    self.subjectCollectionView.reloadData()
                }
            })
        case DashboardType.Teacher.userType:
          return
        default:
        subjectViewModel = SubjectViewModel(viewController: self, complitionHandler:{ data in
                self.subjectListArray = data
                self.subjectCollectionView.getData(cell: CollectionCellIdentifier.subject, data: data, language: "English")
                DispatchQueue.main.async {
                    self.subjectCollectionView.reloadData()
                }
            })
        }
        
    }
    
    //MARK: - GET YouTube Video DATA FROM VIEW MODEL
    func getYoutubeDataFromViewModels(){
        youTubeViewModel = YouTubeListViewModel(){ data in
            self.popularVideosCollectionView.getData(cell: CollectionCellIdentifier.popularVideo, data: data, language: "English")
        }
    }
    //MARK: - GET DATA FOR CONTINUE LEARNING
    func continueLearningConfig(){
        if kSharedUserDefaults.isUserLoggedIn(){
            let saveRecentChapter = kSharedInstance.getDictionary(kSharedUserDefaults.getRecentChapterSeen())
            if saveRecentChapter.keys.contains("") || !saveRecentChapter.keys.contains("selectedSubject") || !saveRecentChapter.keys.contains("recentTopic"){
                self.recentChapter = []
            }else{
                if let recentTopic = kSharedUserDefaults.getRecentChapterSeen()["recentTopic"] as? Dict {
                    self.recentChapter = [TopicSearchModel(data:recentTopic)]
                }else {
                    self.recentChapter = []
                }
            }
        }else{
            self.recentChapter = []
        }
        
        self.continueLearningTableView.getData(cell: TableCellIdentifier.continueLearning, data: self.recentChapter, language: "English")
    }
    
    //MARK: - SET PROGRESS VIEW
    func setOverAllProgressView(){
        circularProgress.trackClr = UIColor.white
        circularProgress.progressClr = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        if kSharedUserDefaults.isUserLoggedIn(){
            performanceViewModel = PerformanceViewModel(viewController: self){ data in
                self.setProgressView(value:data)
            }
        }else{
            self.setProgressView(value:[:])
        }
    }
    
    //MARK: - SET VALUE IN PROGRESS VIEW
    func setProgressView(value:Dict){
        let chapterRead = Int.getInt(String.getString(value["chapter"]))
        let exerciseDone = Int.getInt(String.getString(value["exe"]))
        let testDone = Int.getInt(String.getString(value["ltp"]))
        let overAllPerformance = Int.getInt(String.getString(value["performance"]))
        self.labelChapterRead.text = "\(chapterRead)%"
        self.labelExerciseDone.text = "\(exerciseDone)%"
        self.labelTestDone.text = "\(testDone)%"
        self.progressChapterRead.progress = Float(chapterRead)/100
        self.progressExerciseDone.progress = Float(exerciseDone)/100
        self.progressTestDone.progress = Float(testDone)/100
        let progressInPercentage = Float(overAllPerformance)/100
        //        let totalProgress = Float(chapterRead + exerciseDone + testDone)/300
        let progress = (Double(progressInPercentage))
        //        let twoDecimalPointString = String(format: "%.2f", progress)
        //        print(twoDecimalPointString)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.circularProgress.setProgressWithAnimation(duration: 1.0, value: Float(progress))
        }
    }
    
    //MARK: - SET USERDATA
    fileprivate func setUserDetail(){
        DispatchQueue.main.async {
            self.imageViewProfile.downlodeImage(serviceurl: kUserData.profileImage, placeHolder: #imageLiteral(resourceName: "kid_icon"))
        }
        labelUserName.setfont(font: 14, fontFamily: futuraFont)
        labelSchoolName.setfont(font: 14, fontFamily: futuraFont)
        labelUserName.text = kUserData.userName
        labelSchoolName.text = kUserData.schoolName
        setUserSelectedClass()
        
    }
    //MARK:- SET USER SELECTED CLASS
    func setUserSelectedClass(){
        let userClass = cameFrom == .changedClass ? String.getString(kSharedUserDefaults.getUserClass()["className"]) : (kSharedUserDefaults.isUserLoggedIn() ? kUserData.className : String.getString(kSharedUserDefaults.getUserClass()["className"]))
        labelUserClass.text = userClass == "" ? "Select Class" : (userClass == ClassName.demo ? "Demo" : userClass)  
        imageViewClassArrow.isHidden = !(moduleFor == .normal)
//        buttonClass.setTitle( cameFrom == .changedClass ? String.getString(kSharedUserDefaults.getUserClass()["className"]) : (kSharedUserDefaults.isUserLoggedIn() ? kUserData.className : String.getString(kSharedUserDefaults.getUserClass()["className"])) , for: .normal)
    }
}

// MARK: - CollectionViewViewDataSources
extension DashboardViewController:DataSourcesCollectionDelegate{
    func collectionView(_ collectionView: UICollectionView, height: CGFloat) {
        switch collectionView {
        case subjectCollectionView:
            self.subjectCollectionHeigh.constant = height
        default:
            return
        }
    }
    func collectionView(_ collectionView: UICollectionView, cell: UICollectionViewCell) {
        switch collectionView {
        case subjectCollectionView:
            guard let subjectCell = cell as? SubjectCollectionViewCell else {
               return
            }
            self.firstSubjectCell = subjectCell
        case popularVideosCollectionView:
            guard let youTubeCell = cell as? PopularVideosCollectionViewCell else {
               return
            }
            self.firstYouTubeCell = youTubeCell
        default:
            return
        }
    }
    func collectionView(_ collectionView: UICollectionView, data: Any?) {
        switch collectionView {
        case subjectCollectionView:
            if kSharedUserDefaults.isUserLoggedIn(){
                guard let selectedSubject = data as? SubjectModel else {return}
                let controller = ChapterViewController.getController(storyboard: .Home)
                controller.subjectSelected = selectedSubject
                self.navigationController?.pushViewController(controller, animated: true)
            }else{
                kSharedAppManager.moveToSignup()
            }
        case popularVideosCollectionView:
            guard let object = data as? YoutubeDataModel else{return}
            CommonUtils.playInYoutube(youtubeId: object.videoCode ?? "")
            
        default:
            return
        }
    }
}

//MARK: - TableViewDataSources
extension DashboardViewController:DataSourcesDelegate{
    func tableView(_ tableView: UITableView, height: CGFloat) {
        switch tableView {
        case continueLearningTableView:
            heightContinueLearningTableView.constant = height
            topContinueLearningTableView.constant = height == 0 ? -30 : 10
            labelContinueLearning.isHidden = height == 0 ? true : false
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        switch tableView {
        case continueLearningTableView:
        if !(NetworkReachabilityManager()?.isReachable ?? false){
            showAlertMessage.alert(message:AlertMessage.knoNetwork)
            return
        }
        guard let Kdata = data as? TopicSearchModel else{return}
        let controller = VideoPlayerViewController.getController(storyboard: .Home)
            if let selectedSubject = kSharedUserDefaults.getRecentChapterSeen()["selectedSubject"] as? Dict {
                let selectedObject = SubjectModel(data: selectedSubject)
                controller.subjectSelected = selectedObject
            }
        controller.cameFrom = .classVideo
        controller.selectedTopic = Kdata
        
        self.navigationController?.pushViewController(controller, animated: true)
        default:
            return
        }
    }
}
extension DashboardViewController: UIScrollViewDelegate  {
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {

        print("scrollViewWillBeginDecelerating")

        let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
        if (actualPosition.y > 0){
            // Dragging down

            UIView.animate(withDuration: 3, animations: {
                print("it's going down")
                self.buttonScanWidth.constant = 180
                self.buttonScan.setTitle("  Scan Chapters", for: .normal)
            })
        }else{
            // Dragging up

            UIView.animate(withDuration: 3, animations: {
                print("it's going up")
                self.buttonScanWidth.constant = 30
                self.buttonScan.setTitle("", for: .normal)
            })
        }
    }
}
