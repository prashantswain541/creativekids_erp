//
//  ChapterViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 24/03/21.
//

import UIKit
import Alamofire


class ChapterViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var buttomView: UIView!
    @IBOutlet weak var buttonToggle: UIButton!
    @IBOutlet weak var imageHeader: UIImageView!
    @IBOutlet weak var viewToggleButton: UIView!
    @IBOutlet weak var leadingViewToggleButton: NSLayoutConstraint!
    @IBOutlet weak var heightConsBottomView: NSLayoutConstraint!
    @IBOutlet weak var labelClassName: UILabel!
    @IBOutlet weak var labelSubjectName: UILabel!
    @IBOutlet weak var labelChapterCount: UILabel!
    @IBOutlet weak var tableViewChapter: ChapterTableView!
    @IBOutlet weak var buttonBuy: UIButton!
    
    var chapterArray = [TopicSearchModel]()
    var subjectSelected:SubjectModel?
    var chapterViewModel:ChapterViewModel?
    var orderId:String?
    var lastContentOffset: CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradient()
        initialViewLoad()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppUtility.lockOrientation(.all)
    }
    
    func initialViewLoad(){
        self.buttonByTapped()
        self.buttonBuy.layer.cornerRadius = self.buttonBuy.height/2
        self.imageHeader.isHidden = !(self.subjectSelected?.isSubscribed ?? true)
        self.imageHeader.image =  chapterViewModel?.getHeaderImage(subjectName: subjectSelected?.subjectName ?? "")
        self.labelClassName.text = subjectSelected?.className == ClassName.demo ? "Demo" : subjectSelected?.className
        self.labelSubjectName.text = subjectSelected?.subjectName ?? ""
        checkForBottomView()
        switchButton(sender:buttonToggle)
        tableViewChapter.dataPassDelegate = self
        InAppManager.shared.delegate = self
        InAppManager.shared.statusDelegate = self
        InAppManager.shared.loadProducts()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
        let colorObject = GetSubjectColor.init(subject: subjectSelected?.subjectName ?? "")
        if !labelSubjectName.applyGradientWith(startColor: UIColor(hexString: colorObject.firstColor ?? ""), endColor: UIColor(hexString: colorObject.secondColor ?? "")){
            labelSubjectName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    func buttonByTapped(){
        //        self.buttonBuy.isHidden =  self.subjectSelected?.className == ClassName.demo ? true : (self.subjectSelected?.isSubscribed ?? true)
        //        self.buttonBuy.isHidden = (String.getstring(kUserData.role) == DashboardType.Student.userType || String.getstring(kUserData.role) == DashboardType.Teacher.userType) ? true : false
        self.buttonBuy.isHidden = true
    }
    
    func setGradient() {
        let colorObject = GetSubjectColor.init(subject: subjectSelected?.subjectName ?? "")
        DispatchQueue.main.async {
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.colors = [UIColor(hexString: colorObject.firstColor ?? "").cgColor,UIColor(hexString: colorObject.secondColor ?? "").cgColor]
            gradient.locations = [0.0 , 1.0]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
            gradient.frame = self.buttonBuy.bounds
            gradient.cornerRadius = self.buttonBuy.height/2
            self.buttonBuy.layer.insertSublayer(gradient, at: 0)
        }
    }
    
    @IBAction func buttonBuyTapped(_ sender: UIButton) {
        chapterViewModel?.reloadListDelegate = self
        if let selectedSubject = self.subjectSelected{
            self.getOrderIdApi(subjectObj:selectedSubject ){ [weak self] (success,orderId) in
                if success{
                    self?.orderId = orderId
                    self?.InAppFunction(index: selectedSubject)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
        viewToggleButton.layer.cornerRadius = viewToggleButton.height/2
        buttonToggle.layer.cornerRadius = buttonToggle.height/2
        buttonToggle.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 15)
    }
    
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonToggleTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        switchButton(sender:sender)
    }
    
    
    func checkForBottomView(){
        if  isContantAvailableInBothLang(subejct:subjectSelected?.subjectName ?? " ") && [ClassName.class6,ClassName.class7,ClassName.class8,ClassName.class9,ClassName.class10].contains(subjectSelected?.className){
            buttomView.isHidden = false
            heightConsBottomView.constant = 40
        }else{
            buttomView.isHidden = true
            heightConsBottomView.constant = 0
        }
    }
}


extension ChapterViewController{
    func switchButton(sender:UIButton){
        if sender.isSelected{
            UIView.animate(withDuration: 3) {
                self.leadingViewToggleButton.constant = 5
            }
            viewToggleButton.backgroundColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
            buttonToggle.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            buttonToggle.semanticContentAttribute = .forceRightToLeft
            buttonToggle.setTitle("Switch To Hindi", for: .normal)
            buttonToggle.setTitleColor(#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1), for: .normal)
            chapterViewModel = ChapterViewModel(controller: self, languageSelection: "eng", complitionHandler: { [weak self] (chapterData) in
                self?.chapterArray = chapterData
                self?.tableViewChapter.getData(cell: TableCellIdentifier.chapter, data: self?.chapterArray ?? [])
                self?.labelChapterCount.text = "\(chapterData.count) Chapters"
            })
        }else{
            UIView.animate(withDuration: 8) {
                self.leadingViewToggleButton.constant = 175
            }
            viewToggleButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            buttonToggle.backgroundColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
            buttonToggle.semanticContentAttribute = .forceLeftToRight
            buttonToggle.setTitle("Switch To English", for: .normal)
            buttonToggle.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            chapterViewModel = ChapterViewModel(controller: self, languageSelection: "hnd", complitionHandler: {[weak self] (chapterData) in
                self?.chapterArray = chapterData
                self?.tableViewChapter.getData(cell: TableCellIdentifier.chapter, data: self?.chapterArray ?? [])
                self?.labelChapterCount.text = "\(chapterData.count) Chapters"
            })
        }
    }
}

//MARK:- Purchase Delegate
extension ChapterViewController: IAPSusscessfulDelegate{
    func paymentStatus(isSuccess: Bool) {
        if isSuccess {
            showAlertMessage.alert(message: "Success")
            // self.buy_planApi()
        }
    }
}
extension ChapterViewController: InAppManagerDelegate {
    func inAppLoadingStarted() {
        //print("loading")
        CommonUtils.showHudWithNoInteraction(show: true)
    }
    func inAppLoadingSucceded(productType: ProductType) {
        print(productType.rawValue)
        CommonUtils.showHudWithNoInteraction(show: true)
    }
    func inAppLoadingFailed(error: Error?) {
        //print("loading failed")
        showAlertMessage.alert(message: error?.localizedDescription ?? "")
        print(error?.localizedDescription as Any)
        CommonUtils.showHudWithNoInteraction(show: false)
    }
    func subscriptionStatusUpdated(value: Bool) {
        print(value)
    }
}

extension ChapterViewController:ReloadListDelegate{
    func reloadSubjectList() {
        self.subjectSelected?.isSubscribed = true
        self.initialViewLoad()
        print("Done")
    }
}
extension ChapterViewController: DataSourcesDelegate{
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        if !(NetworkReachabilityManager()?.isReachable ?? false){
            showAlertMessage.alert(message:AlertMessage.knoNetwork)
            return
        }
        guard let Kdata = data as? TopicSearchModel else{return}
        let controller = VideoPlayerViewController.getController(storyboard: .Home)
        controller.cameFrom = .classVideo
        controller.selectedTopic = Kdata
        controller.subjectSelected = self.subjectSelected
        controller.readchapter = { [weak self] reloadStatus in
            if reloadStatus{
                self?.tableViewChapter.reloadData()
            }
        }
        let recentChapterDict:Dict = ["selectedSubject":self.subjectSelected?.dataDict as Any,"recentTopic":Kdata.dataDict]
        kSharedUserDefaults.setRecentChapterSeen(recentChapter: recentChapterDict)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func tableView(_ tableView: UITableView, height: CGFloat) {
    }
    func tableViewCallBack(_ tableView: UITableView, selectedIndex: Int, data: Any?, cell: UITableViewCell) {
        guard let selectedCell = cell as? ChapterTableViewCell else{return}
        selectedCell.showPopUpCallBack = {
            self.showSimpleAlert(message: "Please buy to enjoy all chapters.")
        }
    }
}
