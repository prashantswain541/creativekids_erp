//
//  VoiceAnimatedView.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/09/21.
//

import Foundation
import UIKit

class AnimatedView: UIView {
    var displayLink: CADisplayLink?
    var startTime: CFTimeInterval = 0
    
    // The CAShapeLayer
    
    let shapeLayer: CAShapeLayer = {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.fillColor = UIColor.black.cgColor
        shapeLayer.lineWidth = 3
        return shapeLayer
    }()
    
    // Start the display link
    func startDisplayLink() {
        startTime = CACurrentMediaTime()
        self.displayLink?.invalidate()
        let displayLink = CADisplayLink(target: self, selector:#selector(handleDisplayLink(_:)))
        displayLink.add(to: .main, forMode: .common)
        self.displayLink = displayLink
    }
    
    // Stop the display link
    func stopDisplayLink() {
        displayLink?.invalidate()
    }
    @objc func handleDisplayLink(_ displayLink: CADisplayLink) {
        let elapsed = CACurrentMediaTime() - startTime
        shapeLayer.path = wave(at: elapsed).cgPath
    }
    // Create Wave
    func wave(at elapsed: Double) -> UIBezierPath {
        let elapsed = CGFloat(elapsed)
        let centerY = self.bounds.midY
        let amplitude = 50 - abs(elapsed.remainder(dividingBy: 3)) * 40
        
        func f(_ x: CGFloat) -> CGFloat {
            return sin((x + elapsed) * 4 * .pi) * amplitude + centerY
        }
        
        let path = UIBezierPath()
        let steps = Int(self.bounds.width / 10)
        
        path.move(to: CGPoint(x: 0, y: f(0)))
        for step in 1 ... steps {
            let x = CGFloat(step) / CGFloat(steps)
            path.addLine(to: CGPoint(x: x * self.bounds.width, y: f(x)))
        }
        
        return path
    }
}
