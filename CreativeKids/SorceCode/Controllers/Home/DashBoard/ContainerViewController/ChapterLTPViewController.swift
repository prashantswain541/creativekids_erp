//
//  chapterLTPViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/07/21.
//

import UIKit
import Foundation

class ChapterLTPViewController: UIViewController {

    @IBOutlet weak var chapterLtpTableView: ChapterLTPTableView!
    var parentController:UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        chapterLtpTableView.dataPassDelegate = self
    }

}

extension ChapterLTPViewController:PassDataToChapterLTPViewController{
    func passLTPData(data:Any,parentController:UIViewController) {
        self.parentController = parentController
        guard let LtpData = data as? [ExerciseModel] else {return}
        chapterLtpTableView.getData(cell: TableCellIdentifier.chapterLTPCell, data: LtpData, language: "English")
    }
}

extension ChapterLTPViewController:DataSourcesDelegate{
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        guard let exerciseDetail = data as? ExerciseModel else {return}
        let controller = ExamStartPopUpViewController.getController(storyboard: .Question)
        controller.cameFrom = .ltp
        controller.subjectSelected = (parentController as! VideoPlayerViewController).subjectSelected
        controller.selectedTopic = (parentController as! VideoPlayerViewController).selectedTopic
        controller.exercise = exerciseDetail
        (parentController as! VideoPlayerViewController).cameFromChapterScreen = false
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
