//
//  ChapterVideo.swift
//  CreativeKids
//
//  Created by Creative Kids on 10/07/21.
//

import UIKit

class ChapterVideoViewController: UIViewController {

    @IBOutlet weak var tableViewVideo: VideoTableView!
    var parentController:UIViewController?
    var videoDataArray = [TopicModel]()
    var passSelectedVideoData:PassVideoDataToSources?
    var cametoScan: CametoVide?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewVideo.cametoScan = self.cametoScan
        tableViewVideo.dataPassDelegate = self
    }

}
extension ChapterVideoViewController:PassDataToChapterVideoViewController{
    func reloadTableForNextPrevious() {
        self.tableViewVideo.reloadData()
    }
    
    func passVideoData(data: Any,parentController:UIViewController) {
        self.parentController = parentController
        guard let videoData = data as? [TopicModel] else {return}
        self.videoDataArray = videoData
        self.tableViewVideo.noDataMessage = self.videoDataArray.isEmpty ? "Data not found":""
        self.tableViewVideo.getData(cell: TableCellIdentifier.video, data: self.videoDataArray,language: (self.parentController as! VideoPlayerViewController).selectedTopic?.subjectName ?? "English")
    }
}

extension ChapterVideoViewController:DataSourcesDelegate{
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        self.passSelectedVideoData?.passVideoData(tableView, selectedIndex: selectedIndex, data: data)
    }
}
