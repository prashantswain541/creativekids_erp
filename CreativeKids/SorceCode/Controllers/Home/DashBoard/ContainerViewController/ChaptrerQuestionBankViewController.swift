//
//  chaptrerQuestionBankViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/07/21.
//

import UIKit

class ChaptrerQuestionBankViewController: UIViewController {
    
    @IBOutlet weak var chapterQuestionBankTableView: ChapterQuestionBankTableView!
    var parentController:UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        chapterQuestionBankTableView.dataPassDelegate = self
    }
    
}

extension ChaptrerQuestionBankViewController:PassDataToQuestionBankViewController{
    func passQuestionBankData(data: Any, parentController: UIViewController) {
        self.parentController = parentController
        chapterQuestionBankTableView.getDataWithAdditionalData(cell:TableCellIdentifier.chapterQuestionBank,data:data as? [Any] ?? [],language:"English",otherData:(parentController as! VideoPlayerViewController).practicePaperPdf)
    }
}

extension ChaptrerQuestionBankViewController:DataSourcesDelegate{
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
//        if (parentController as! VideoPlayerViewController).subjectSelected?.sunjectId == "361" {
            let controller = MathsMockTestViewController.getController(storyboard: .Question)
            controller.selectedTopic = (parentController as! VideoPlayerViewController).selectedTopic
            controller.subjectSelected = (parentController as! VideoPlayerViewController).subjectSelected
            controller.mockTest = (parentController as! VideoPlayerViewController).mockTest[selectedIndex]
            (parentController as! VideoPlayerViewController).cameFromChapterScreen = false
            self.navigationController?.pushViewController(controller, animated: true)
//        } else {
//            let controller = QuestionBankViewController.getController(storyboard: .Question)
//            controller.selectedTopic = (parentController as! VideoPlayerViewController).selectedTopic
//            controller.subjectSelected = (parentController as! VideoPlayerViewController).subjectSelected
//            controller.mockTest = (parentController as! VideoPlayerViewController).mockTest[selectedIndex]
//            (parentController as! VideoPlayerViewController).cameFromChapterScreen = false
//            self.navigationController?.pushViewController(controller, animated: true)
//        }
    }
    func tableViewCallBack(_ tableView: UITableView, selectedIndex: Int, data: Any?, cell: UITableViewCell) {
        (self.parentController as! VideoPlayerViewController).cameFromChapterScreen = false
        let controller = EbookViewController.getController(storyboard: .Home)
        controller.pdfExercise = data as? ExerciseModel
        controller.cameFor = .getPracticePaperPdf
        controller.selectedTopic = (parentController as! VideoPlayerViewController).selectedTopic
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
