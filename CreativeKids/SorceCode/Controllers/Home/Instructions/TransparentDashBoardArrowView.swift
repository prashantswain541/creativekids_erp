//
//  TransparentDashBoardArrowView.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/09/21.
//

import Foundation
import UIKit
import Instructions

// Transparent coach mark (text without background, cool arrow)
internal class TransparentDashBoardArrowView: UIImageView, CoachMarkArrowView {
    // MARK: - Initialization
    init(orientation: CoachMarkArrowOrientation) {
        if orientation == .top {
            super.init(image: UIImage(named: "arrow-top"))
        } else {
            super.init(image: UIImage(named: "arrow-bottom"))
        }

        self.translatesAutoresizingMaskIntoConstraints = false

        widthAnchor.constraint(equalToConstant: self.image?.size.width ?? 0).isActive = true
        heightAnchor.constraint(equalToConstant: self.image?.size.width ?? 0).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding.")
    }
}
