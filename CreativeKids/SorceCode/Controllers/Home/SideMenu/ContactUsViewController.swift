//
//  ContactUsViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 21/04/21.
//

import UIKit

class ContactUsViewController: UIViewController {
    
    @IBOutlet weak var viewEmailHeight: NSLayoutConstraint!
    @IBOutlet weak var viewEmailWidth: NSLayoutConstraint!
    @IBOutlet weak var viewMail: UIView!
    @IBOutlet var viewNumber: [UIView]!
    @IBOutlet var numberButton: [UIButton]!
    @IBOutlet var labelPhoneWhatsApp: [UILabel]!
    @IBOutlet weak var labelInstruction: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoadView()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    func initialLoadView(){
        labelInstruction.setfont(font: 12, fontFamily: futuraFont)
        labelPhoneWhatsApp.forEach{$0.setfont(font: 15, fontFamily: futuraFont)}
        if UIDevice.isPhone{
            viewEmailHeight.constant = 40
            viewEmailWidth.constant = 320
        }else{
            viewEmailHeight.constant = 80
            viewEmailWidth.constant = 600
        }
        let openMail = UITapGestureRecognizer(target: self, action: #selector(mailTapped(_:)))
        self.viewMail.addGestureRecognizer(openMail)
    }
    
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func mailTapped(_ sender: UITapGestureRecognizer) {
        openEmail(email: "Info@creativekidssolutions.com")
    }
    @IBAction func numberTapped(_ sender: UIButton) {
        if sender.tag == 1 {
            Common.showActionSheet(vc: self, sender: sender, firstTitle: "Call", secondTitle: "WhatsApp", sourceView: viewNumber[0]) { selctedIndex in
                if selctedIndex == 0 {
                    openDilerPad(number: "9990247714")
                } else {
                    openWhatsApp(number: "9990247714")
                }
            }
        } else if sender.tag == 2 {
            Common.showActionSheet(vc: self, sender: sender, firstTitle: "Call", secondTitle: "WhatsApp", sourceView: viewNumber[1]) { selctedIndex in
                if selctedIndex == 0 {
                    openDilerPad(number: "9667661157")
                } else {
                    openWhatsApp(number: "9667661157")
                }
            }
           // Common.showActionSheet(vc: self,sender: sender, number: "9667661157", sourceView: viewNumber[1])
        } else {
            Common.showActionSheet(vc: self, sender: sender, firstTitle: "Call", secondTitle: "WhatsApp", sourceView: viewNumber[2]) { selctedIndex in
                if selctedIndex == 0 {
                    openDilerPad(number: "9667201157")
                } else {
                    openWhatsApp(number: "9667201157")
                }
            }
          //  Common.showActionSheet(vc: self,sender: sender, number: "9667201157", sourceView: viewNumber[2])
        }
    }
}

