//
//  SideMenuViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 18/03/21.
//

import UIKit
import KYDrawerController
import SDWebImage

class SideMenuViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableViewSideMenu: UITableView!
    @IBOutlet weak var SignInSignUpStack: UIStackView!
    @IBOutlet weak var heightStack: NSLayoutConstraint!
    @IBOutlet weak var bottomStack: NSLayoutConstraint!
    @IBOutlet weak var buttonEditProfile: UIButton!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var tableTopCons: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: UIView!

    //MARK: - Variables
    var sideMenuDataArray = [SideMenuDataModel]()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //setGradient()
        self.setgradient(color1: .blue, color2: .systemIndigo)
        initialConfig()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
        if kSharedUserDefaults.isUserLoggedIn(){
            buttonEditProfile.isHidden = false
            SignInSignUpStack.isHidden = true
            heightStack.constant = 0
        }else{
            buttonEditProfile.isHidden = true
            SignInSignUpStack.isHidden = false
            heightStack.constant = 30
        }
        DispatchQueue.main.async {
            self.imageProfile.downlodeImage(serviceurl: kUserData.profileImage, placeHolder: #imageLiteral(resourceName: "kid_icon"))
        }
        self.labelName.text = kUserData.userName ?? ""
        tableViewSideMenu.reloadData()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setgradient(color1: .blue, color2: .systemIndigo)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
            self.setgradient(color1: .blue, color2: .systemIndigo)
    }
    //MARK: - Actions
    @IBAction func signinTapped(_ sender: UIButton) {
        AppManager.shared.moveToLogin()
    }
    @IBAction func signupTapped(_ sender: UIButton) {
        AppManager.shared.moveToSignup()
    }
    
    //MARK: - Methods
    func initialConfig(){
        tableTopCons.constant -= getStatusBarHeight()
        sideMenuDataArray = [SideMenuDataModel(sideTitle: "Home", sideImage: #imageLiteral(resourceName: "change_cls_icon")),
                             SideMenuDataModel(sideTitle: "Subscribed Subjects", sideImage: #imageLiteral(resourceName: "subs_sub_icon")),
                             SideMenuDataModel(sideTitle: "Contact Us", sideImage: #imageLiteral(resourceName: "contact_us_icon")),
                             SideMenuDataModel(sideTitle: "Login", sideImage: #imageLiteral(resourceName: "logout_icon")),
                             SideMenuDataModel(sideTitle: "Share App", sideImage: #imageLiteral(resourceName: "share_icon")),
                             SideMenuDataModel(sideTitle: "Buy Your Subjects", sideImage: #imageLiteral(resourceName: "subs_sub_icon"))]
        tableViewSideMenu.dataSource = self
        tableViewSideMenu.delegate = self
        tableViewSideMenu.register(UINib(nibName: TableCellIdentifier.sideMenu, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.sideMenu)
    }
    
    //MARK: - Actions
    @IBAction func buttonChangePasswordTapped(_ sender:UIButton){
        //        kSharedAppManager.profileEdit()
        let controller = EditProfileViewController.getController(storyboard: .Home)
        self.navigationController?.pushViewController(controller, animated: true)
        do{
            if let kyDrawer = self.parent as? KYDrawerController{
                kyDrawer.setDrawerState(.closed, animated: false)
            }
        }
    }
    
}

//MARK: - UITableViewDataSource,UITableViewDelegate
extension SideMenuViewController:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sideMenuDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.sideMenu, for: indexPath) as! SideMenuTableViewCell
        cell.setConfig(object:sideMenuDataArray[indexPath.row],index:indexPath.row)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1{
            return kSharedUserDefaults.isUserLoggedIn() ? 60 : 0
        }else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPathData = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example

        let currentCell = tableView.cellForRow(at: indexPathData!)! as! SideMenuTableViewCell
        switch indexPath.row {
        case 0:
            if let kyDrawer = self.parent as? KYDrawerController{
                kyDrawer.setDrawerState(.closed, animated: true)
            }
            cameFrom = .sideMenu
            kSharedAppManager.moveToHome()
            
        case 1:
            if kUserData.subscriptionData.isEmpty{
                showAlertMessage.alert(message: "You have not subscribe any subject")
                return
            }
            let controller = SubscribedSubjectsViewController.getController(storyboard: .Home)
            self.navigationController?.pushViewController(controller, animated: true)
            do{
                if let kyDrawer = self.parent as? KYDrawerController{
                    kyDrawer.setDrawerState(.closed, animated: false)
                }
            }
            
        case 2:
            let controller = ContactUsViewController.getController(storyboard: .Home)
            self.navigationController?.pushViewController(controller, animated: true)
            do{
                if let kyDrawer = self.parent as? KYDrawerController{
                    kyDrawer.setDrawerState(.closed, animated: false)
                }
            }
            
        case 3:
            if kSharedUserDefaults.isUserLoggedIn(){
                self.showPopUp(title: kAppName, message: "Are you sure you want to Logout?.", optionTypeCancel: "Cancel", optionTypeOther: "Logout") { _ in
                    self.logoutApi()
                }
                
            }else{
                kSharedAppManager.moveToLogin()
            }
            
        case 4:
            self.shareApp(appUrl: "https://apps.apple.com/us/app/creative-kids-home-tutor/id1568231840", sorceView: currentCell.labelSideContent, sender: UIButton())
        default:
            if kSharedUserDefaults.isUserLoggedIn(){
                let controller = SubjectBuyViewController.getController(storyboard: .Home)
                self.navigationController?.pushViewController(controller, animated: true)
                do{
                    if let kyDrawer = self.parent as? KYDrawerController{
                        kyDrawer.setDrawerState(.closed, animated: false)
                    }
                }
            }else{
                showSimpleAlert(message: "Login first then you can buy any subject")
            }
        }
    }
    func logoutApi(){
        let serviceUrl = kBaseUrl + "logout/?email=\(kUserData.userEmail ?? "")&lg=abc"
        var request = URLRequest(url: URL(string: serviceUrl)!,timeoutInterval: Double.infinity)
        CommonUtils.showHud(show: true)
        request.httpMethod = "GET"
        print(serviceUrl)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data else {
                  print(String(describing: error))
                  return
                }
                print(String(data: data, encoding: .utf8)!)
                CommonUtils.showHud(show: false)
                  guard let responseData = response as? HTTPURLResponse else {return}
                  if responseData.statusCode == 200 {
                      CommonUtils.showToast(message: "Logout Successfully")
                      kSharedAppManager.logout()
                      
                  }
            }
        }

        task.resume()
//        let serviceName = kStudentBaseUrl + "logout/?email=\(kUserData.email ?? "")&lg=abc".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
//        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
//            if statusCode == 200 {
//                let responceMessage = String.getString(response[getResponce])
//                CommonUtils.showToast(message: responceMessage)
//                kSharedAppManager.logout()
//            } else {
//                showAlertMessage.alert(message: "Something not found")
//            }
//        }
    }
    func showPopUpForLogout(){
        let alert = UIAlertController.init(title: "Creative Kids", message: "Are you sure you want to Logout?.", preferredStyle: .alert)
        let cancel = UIAlertAction.init(title: "Cancel", style: .default){ _ in
            alert.dismiss(animated: true, completion: nil)
        }
        let Logout = UIAlertAction.init(title: "Logout", style: .destructive){ _ in
            alert.dismiss(animated: true){
                self.logoutApi()
            }
        }
        alert.addAction(cancel)
        alert.addAction(Logout)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    func setgradient(color1: UIColor, color2: UIColor) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [color1.cgColor, color2.cgColor]
        gradient.locations = [0.0 , 1.5]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.5)
        gradient.frame = self.viewHeader.bounds
        self.viewHeader.layer.insertSublayer(gradient, at: 0)
    }
}

