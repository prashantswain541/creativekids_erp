//
//  ExamStartPopUpViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 07/04/21.
//

import UIKit

class ExamStartPopUpViewController: UIViewController {
    
    enum CameFrom {
        case excelInExam, ltp
    }
    
    //MARK:- Outlet
    @IBOutlet weak var labelType: UILabel!
    
    //MARK:- VARIABLES
    var cameFrom:CameFrom!
    var subjectSelected:SubjectModel?
    var exercise:ExerciseModel?
    var selectedTopic:TopicSearchModel?
    
    //MARK:- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let colorObject = GetSubjectColor(subject: subjectSelected?.subjectName ?? "")
        if !labelType.applyGradientWith(startColor: UIColor(hexString: colorObject.firstColor ?? ""), endColor: UIColor(hexString: colorObject.secondColor ?? "")){
            labelType.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    fileprivate func initialLoad(){
        switch self.cameFrom {
        case .excelInExam:
            labelType.text = "EXCEL IN EXAM"
        case .ltp:
            labelType.text = "LIVE TEST PAPER"
        default:
            return
        }
    }
    
    //MARK:- Actions
    @IBAction func buttonStartTapped(_ sender: UIButton) {
        switch self.cameFrom {
        case .excelInExam:
            let controller = ExcelExamViewController.getController(storyboard: .Question)
            controller.exercise = self.exercise
            controller.selectedTopic = self.selectedTopic
            controller.subjectSelected = self.subjectSelected
            self.navigationController?.pushViewController(controller, animated: true)
        case .ltp:
            let controller = LTPViewController.getController(storyboard: .Question)
            controller.selectedTopic = self.selectedTopic
            controller.exercise = self.exercise
            controller.subjectSelected = self.subjectSelected
            self.navigationController?.pushViewController(controller, animated: true)
        default:
            return
        }
    }
}
