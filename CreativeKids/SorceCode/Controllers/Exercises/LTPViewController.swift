//
//  LTPViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 25/03/21.
//
//https://creativekidssolutions.com/Content/Subject_icons/Class-7/Hindi%20Vyakaran.png
//https://creativekidssolutions.com/Content/Subject_icons/Class-6/Hindi%20Vyakaran.png
import UIKit

class LTPViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var questionCollectionView: UICollectionView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var questionCountLable: UILabel!
    @IBOutlet weak var questionTypeLable: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    
    //MARK: - Variables
    var LTPDataModel: LTPViewModel?
    var LTPQuestionsModel = [VSAQQuestionModel]()
    var subjectSelected:SubjectModel?
    var exercise:ExerciseModel?
    //    var selectedChapter:ChapterModel?
    var selectedTopic:TopicSearchModel?
    var timer: Timer?
    var targetTime = Date()
    var count = 0
    var ltp: String?
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitial()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
        self.LTPDataModel = LTPViewModel(viewController: self, completionHandler: {
            self.questionCountLable.text = "\(self.count + 1)/\(self.LTPQuestionsModel.count)"
            self.questionCollectionView.reloadData()
            if self.LTPQuestionsModel.count == 0 {
                self.timerStop()
            } else {
                self.setTarget()
            }
        })
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timerStop()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    //MARK: - Initial Setup
    func setupInitial() {
        if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            headerLabel.setfont(font: 16, fontFamily: chanakyaFont)
            headerLabel.text = selectedTopic?.chapterName ?? ""
        }else{
            headerLabel.setfont(font: 16, fontFamily: futuraFont)
            headerLabel.text = selectedTopic?.chapterName ?? ""
        }
        
        questionCollectionView.delegate = self
        questionCollectionView.dataSource = self
        
    }
    //MARK: - SetTargetForTime
    func setTarget(forSeconds seconds: Int = 0) {
        targetTime = Calendar.current.date(byAdding: .second, value: seconds, to: Date()) ?? Date()
        startTimer()
    }
    //MARK: - Start Timer
    func startTimer() {
        timer?.invalidate()
        var sec = Calendar.current.dateComponents([.second], from: Date(), to: self.targetTime).second!
        if sec == 0 {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
                sec += 1
                kSharedUserDefaults.setValue(sec, forKey: "time_count_Background_Exercise")
                self.timeLabel.text = CommonUtils.timeString(time: TimeInterval(sec))
            }
        }
    }
    //MARK: - StopTimer
    func timerStop() {
        timer?.invalidate()
    }
    //MARK: - QuestionsCount + Scroll Collection
    func setQuestionCountsWithScroll(questions: IndexPath) {
        self.count = questions.item + 1
        self.questionCountLable.text = "\(self.count + 1)/\(self.LTPQuestionsModel.count)"
        let scrollIndexPath = IndexPath(item: questions.item + 1, section: 0)
        self.questionCollectionView.scrollToItem(at: scrollIndexPath, at: .centeredHorizontally, animated: true)
    }
    @IBAction func buttonBacktapped(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: VideoPlayerViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}

//MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension LTPViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    //MARK:- COLLECTION numberOfItemsInSection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return LTPQuestionsModel.count
    }
    
    //MARK:- COLLECTION cellForItemAt
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.ltpExercise, for: indexPath) as! LTPCollectionCell
        let object = LTPQuestionsModel[indexPath.item]
        self.questionTypeLable.text = object.topic
        cell.subjectSelected = self.subjectSelected
        cell.configCollectionData(LTPQuestions: object,index: indexPath)
        cell.submitButton.setTitle(indexPath.item == self.LTPQuestionsModel.count - 1 ? "Done": "Submit", for: .normal)
        cell.nextCallback = {
            if indexPath.item == self.LTPQuestionsModel.count - 1 {
                let controller = ResultViewController.getController(storyboard: .Question)
                controller.modelArr = self.LTPQuestionsModel
                controller.subjectSelected = self.subjectSelected
                controller.exercise = self.exercise
                controller.selectedTopic = self.selectedTopic
                kSharedUserDefaults.setValue(self.timeLabel.text, forKey: "time_count")
                self.navigationController?.pushViewController(controller, animated: true)
                self.questionCollectionView.isScrollEnabled = false
            } else {
                self.setQuestionCountsWithScroll(questions: indexPath)
            }
        }
        cell.selectedObject = object
        return cell
    }
    
    //MARK:- COLLECTION HEIGHT
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}
