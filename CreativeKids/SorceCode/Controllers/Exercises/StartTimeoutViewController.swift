//
//  StartTimeoutViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 23/03/21.
//

import UIKit

class StartTimeoutViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var startBtn: UIButton!
    
    //MARK:- Variables
    var dismissCallBack:(()->Void)?
    var changeButtonText = ""
    
    //MARK:- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startBtn.setTitle(changeButtonText, for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    //MARK:- Action
    @IBAction func timeOutBtnTapped(_ sender: UIButton) {
        if startBtn.titleLabel?.text == "Start" {
            self.dismiss(animated: true, completion: {
                self.dismissCallBack?()
            })
        } else {
            self.dismiss(animated: true, completion: {
                self.dismissCallBack?()
            })
        }
    }
}
