//
//  DNDViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 30/03/21.
//

import UIKit
import KDCircularProgress

class DNDViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var questionsTableView: UITableView!
    @IBOutlet weak var timeView: KDCircularProgress!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var labelHeadeerAbhyaas: UILabel!
    
    //MARK: - Vaiables
    var DNDDataModel: DNDViewModel?
    var DNDQuestionsModel = [VSAQQuestionModel]()
    var timer: Timer?
    var targetTime = Date()
    var callBackSubmit: (() -> ())?
    var selectedTopic:TopicModel?
    var exercise:ExerciseModel?
    var bcakgroundTimer: Timer?
    var targetTimeBack = Date()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timerStop()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    //MARK: - InitialSetup
    func setInitialSetup() {
        questionsTableView.delegate = self
        questionsTableView.dataSource = self
        animatedViewSetup()
        
        self.DNDDataModel = DNDViewModel(viewController: self, completionHandler: {
            if self.DNDQuestionsModel.count == 0 {
                self.timerStop()
            } else {
                self.setTarget()
                self.setTargetBackground()
            }
            self.setHeaderLabel()
            self.questionsTableView.reloadData()
        })
    }
    
    func setHeaderLabel() {
        if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            self.labelHeader.setfont(font: 18, fontFamily: chanakyaFont)
            if isContainNumber(checkString: self.selectedTopic?.topic ?? ""){
                self.labelHeader.attributedText =  getNSAtributedStringWithNumber(forConvertString: self.selectedTopic?.topic ?? "")
            }else{
                self.labelHeader.text = self.selectedTopic?.topic ?? ""
            }
            self.labelHeadeerAbhyaas.text = "(\(ConstantHindiText.abhyaas) \(getNumericFromString(stringWithNumber: self.DNDQuestionsModel.first?.exercise ?? "") ?? 0))"
        } else {
            self.labelHeader.setfont(font: 18, fontFamily: futuraFont)
            self.labelHeader.text = (self.selectedTopic?.topic ?? "") + "(\(self.DNDQuestionsModel.first?.exercise ?? ""))".replacingOccurrences(of: "()", with: "")
        }
    }
    
    //MARK: - AnimatedCircularViewSetup
    func animatedViewSetup() {
        timeView.startAngle = -90
        timeView.progressThickness = 0.3
        timeView.trackThickness = 0.3
        timeView.clockwise = true
        timeView.gradientRotateSpeed = 2
        timeView.roundedCorners = false
        timeView.glowMode = .forward
        timeView.glowAmount = 0.5
        timeView.set(colors: .systemGreen)
        timeView.center = CGPoint(x: view.center.x + 10, y: view.center.y + 15)
    }
    //MARK: - AnimatedCircularView
    func animatedView() {
        self.timeView.animate(fromAngle: 360, toAngle: 0, duration: 120) { completed in
            if completed {
                print("animation stopped, completed")
            } else {
                print("animation stopped, was interrupted")
            }
        }
    }
    //MARK: - setTargetForTime
    func setTarget(forSeconds seconds: Int = 120) {
        targetTime = Calendar.current.date(byAdding: .second, value: seconds, to: Date()) ?? Date()
        self.animatedView()
        self.startTimer()
    }
    func startTimer () {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
            let sec = Calendar.current.dateComponents([.second], from: Date(), to: self.targetTime).second!
            let secondFormat = sec < 10 ? "0\(sec)" : "\(sec)"
            self.labelTime.text = "\(secondFormat)"
            if sec == 0 {
                timer.invalidate()
                let controller = StartTimeoutViewController.getController(storyboard: .Question)
                controller.modalTransitionStyle = .crossDissolve
                controller.modalPresentationStyle = .overCurrentContext
                controller.changeButtonText = "TimeOut"
                controller.dismissCallBack = {
                    self.navigationController?.popViewController(animated: true)
                }
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    //MARK: - StopTimer
    func timerStop() {
        timer?.invalidate()
        self.timeView.pauseAnimation()
    }
    //MARK: - SetTargetForTime
    func setTargetBackground(forSeconds seconds: Int = 0) {
        targetTimeBack = Calendar.current.date(byAdding: .second, value: seconds, to: Date()) ?? Date()
        startTimerBackground()
    }
    //MARK: - Start Timer
    func startTimerBackground() {
        var sec = Calendar.current.dateComponents([.second], from: Date(), to: self.targetTimeBack).second!
        if sec == 0 {
            bcakgroundTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
                sec += 1
                //self.timeLabel.text = CommonUtils.timeString(time: TimeInterval(sec))
                kSharedUserDefaults.setValue(sec, forKey: "time_count_Background_Exercise")
            }
        }
    }
    //MARK: - StopTimer
    func timerStopBackground() {
        bcakgroundTimer?.invalidate()
    }
    //MARK: - Button Action
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        self.callBackSubmit?()
    }
    @IBAction func buttonBacktapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: - UITableViewDelegate, UITableViewDataSource
extension DNDViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DNDQuestionsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.dnd, for: indexPath) as! DNDTableViewCell
        self.DNDQuestionsModel[indexPath.row].questionNumber = indexPath.row + 1
        cell.selectedTopic = self.selectedTopic
        cell.configCell(DNDData: DNDQuestionsModel[indexPath.row])
        self.callBackSubmit = {
            let unSelectedQuestion = self.DNDQuestionsModel.filter{$0.textfieldFirst == nil || $0.texrFieldSecond == nil}
            if unSelectedQuestion.count > 0{
                showAlertMessage.alert(message: "Please select option for qustion \(unSelectedQuestion.first?.questionNumber ?? 1)")
            }
            else {
                self.timerStop()
                self.timerStopBackground()
                let controller = DNDResultViewController.getController(storyboard: .Question)
                controller.modelArr = self.DNDQuestionsModel
                controller.camefrom = .DNDType
                controller.exercise = self.exercise
                controller.selectedTopic = self.selectedTopic
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        return cell
    }
}


