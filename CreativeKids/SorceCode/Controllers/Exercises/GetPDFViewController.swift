//
//  GetPDFViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 05/04/21.
//

import UIKit
import WebKit
import Alamofire

class GetPDFViewController: UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var pdfWebView: WKWebView!
    @IBOutlet weak var headerLable: UILabel!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var viewGetPdf: UIView!
    @IBOutlet weak var buttonGetPdf: UIButton!
    @IBOutlet weak var viewbackground: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var percentLbl: UILabel!
    @IBOutlet weak var fileSizeLbl: UILabel!
    
    //MARK: - Variables
    var pdfModelData: PDFModel?
    var pdfViewModel: PDFDataViewModel?
    var exercise:ExerciseModel?
    var selectedTopic:TopicSearchModel?
    var pdfUrl:String?
    //    var selectedChapter:ChapterModel?
    var isAllQuestionCompleted = false
    
    
    private let byteFormatter: ByteCountFormatter = {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = [.useKB, .useMB]
        return formatter
    }()
    var task : URLSessionTask!
    
    var percentageWritten:Float = 0.0
    var taskTotalBytesWritten = 0
    var taskTotalBytesExpectedToWrite = 0
    
    lazy var session : URLSession = {
        let config = URLSessionConfiguration.ephemeral
        config.allowsCellularAccess = false
        let session = Foundation.URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        return session
    }()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        pdfWebView.uiDelegate = self
        pdfWebView.navigationDelegate = self
        viewbackground.isHidden = true
        self.viewbackground.drawShadowwithCorner()
        self.pdfViewModel = PDFDataViewModel(viewController: self, completionHandler: {
            guard let urlString = self.pdfModelData?.pdf else {return}
            self.pdfUrl = urlString
            let url = URLRequest(url: URL(string: urlString)!)
            self.pdfWebView.load(url)
        })
        if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            self.headerLable.setfont(font: 18, fontFamily: chanakyaFont)
        }else{
            self.headerLable.setfont(font: 17, fontFamily: futuraFont)
        }
        self.headerLable.text = self.selectedTopic?.chapterName?.replacingOccurrences(of: "_", with: " ")
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    //MARK: - Action
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        if isAllQuestionCompleted{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: VideoPlayerViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func buttonGetPdgTapped(_ sender: UIButton) {
//        self.viewDownload.isHidden = false
//            savePdf(urlString: self.pdfUrl ?? "", fileName: "ExcelInExam.\(self.selectedTopic?.subjectName ?? "") \(self.selectedTopic?.className ?? "")", completion: {
//                self.viewDownload.isHidden = true
//            })
            if NetworkReachabilityManager()?.isReachable == true {
                self.viewbackground.isHidden = false
                sender.isUserInteractionEnabled = false
                let url = URL(string: self.pdfUrl ?? "")!
                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
                session.downloadTask(with: url).resume()
            } else {
                showAlertMessage.alert(message: "No internet Available")
            }
    }
}
extension GetPDFViewController: WKUIDelegate,WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        CommonUtils.showHud(show: false)
        buttonGetPdf.isUserInteractionEnabled = true
        buttonBack.isUserInteractionEnabled = true
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        CommonUtils.showHud(show: true)
        buttonGetPdf.isUserInteractionEnabled = false
        buttonBack.isUserInteractionEnabled = false
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        CommonUtils.showHud(show: false)
        buttonGetPdf.isUserInteractionEnabled = true
        buttonBack.isUserInteractionEnabled = true
    }
}
//MARK: - URLSessionDownloadDelegate, URLSessionDelegate
extension GetPDFViewController: URLSessionDownloadDelegate, URLSessionDelegate{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        DispatchQueue.main.async {
            self.viewbackground.isHidden = true
        }
        let data = try? Data.init(contentsOf: location)
        if let pdfData = data{
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "\(self.selectedTopic?.subjectName ?? "") \(self.selectedTopic?.chapterName ?? "") \(self.selectedTopic?.className ?? "") \(self.exercise?.exerciseType ?? "").pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            DispatchQueue.main.async {
                do {
                    try pdfData.write(to: actualPath, options: .atomic)
                    CommonUtils.showHud(show: false)
                    showAlertMessage.alert(message: "Pdf saved successfully!\nfile manager -> CreativeKids")
                    self.buttonGetPdf.isUserInteractionEnabled = true
                } catch {
                    self.buttonGetPdf.isUserInteractionEnabled = true
                    print("Pdf could not be saved")
                }
            }
        }else{
            self.buttonGetPdf.isUserInteractionEnabled = true
            showAlertMessage.alert(message: "Pdf saved failed!")
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64) {
        taskTotalBytesWritten = Int(writ)
        taskTotalBytesExpectedToWrite = Int(exp)
        percentageWritten = Float(taskTotalBytesWritten) / Float(taskTotalBytesExpectedToWrite)
        DispatchQueue.main.async {
            let written = self.byteFormatter.string(fromByteCount: Int64(self.taskTotalBytesWritten))
            let expected = self.byteFormatter.string(fromByteCount: Int64(self.taskTotalBytesExpectedToWrite))
            self.fileSizeLbl.text = "\(written) / \(expected)"
            self.progressView.progress = self.percentageWritten
            self.percentLbl.text = String(format: "%.01f", self.percentageWritten*100) + "%"
        }
    }
    
}
