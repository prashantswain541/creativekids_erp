//
//  EbookViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 02/04/21.
//

import UIKit
import WebKit
import Alamofire


class EbookViewController: UIViewController, URLSessionDownloadDelegate, URLSessionDelegate {
    enum PdfFor{
        case getSamplePaperPdf,getPracticePaperPdf, showPdf
    }
    //MARK: - IBOutlets
    @IBOutlet weak var EbookWebView: WKWebView!
    @IBOutlet weak var labelChapterName: UILabel!
    @IBOutlet weak var viewGetPdf: UIView!
    @IBOutlet weak var buttonGetPdf: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var viewbackground: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var percentLbl: UILabel!
    @IBOutlet weak var fileSizeLbl: UILabel!
    
    var cameFor:PdfFor = .showPdf
    //MARK: - Variables
    var pdfExercise:ExerciseModel?
    var selectedTopic:TopicSearchModel?
    var topicModel: [TopicModel]?
    var cameToType: CametoVide?
    var getPdfUrl = "http//:"
    
    private let byteFormatter: ByteCountFormatter = {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = [.useKB, .useMB]
        return formatter
    }()
    var task : URLSessionTask!
    
    var percentageWritten:Float = 0.0
    var taskTotalBytesWritten = 0
    var taskTotalBytesExpectedToWrite = 0
    
    lazy var session : URLSession = {
        let config = URLSessionConfiguration.ephemeral
        config.allowsCellularAccess = false
        let session = Foundation.URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        return session
    }()
    
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        viewbackground.isHidden = true
        EbookWebView.uiDelegate = self
        EbookWebView.navigationDelegate = self
        self.viewbackground.drawShadowwithCorner()
        viewGetPdf.isHidden = cameFor == .showPdf ? true : false
        if cameFor == .getSamplePaperPdf {
            self.labelChapterName.setfont(font: 17, fontFamily: futuraFont)
            self.labelChapterName.text = selectedTopic?.subjectName ?? ""
        }else{
            if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
                self.labelChapterName.setfont(font: 18, fontFamily: chanakyaFont)
            }else{
                self.labelChapterName.setfont(font: 17, fontFamily: futuraFont)
            }
            self.labelChapterName.text = selectedTopic?.chapterName ?? ""
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewDidAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        if cameFor == .getSamplePaperPdf {
            let urlRequest = URLRequest(url: URL(string: self.selectedTopic?.samplePaperPdf ?? "")!)
            self.EbookWebView.load(urlRequest)
        }else if cameFor == .getPracticePaperPdf{
            getPracticePaperPdfApi()
        } else {
            getEbookPdfApi()
        }
    }
    @IBAction func buttonGetPdgTapped(_ sender: UIButton) {
        if cameFor == .getSamplePaperPdf {
            if NetworkReachabilityManager()?.isReachable == true {
                self.viewbackground.isHidden = false
                sender.isUserInteractionEnabled = false
                let url = URL(string: self.selectedTopic?.samplePaperPdf ?? "")!
                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
                session.downloadTask(with: url).resume()
            } else {
                showAlertMessage.alert(message: "No internet Available")
            }
        }else{
            if NetworkReachabilityManager()?.isReachable == true {
                self.viewbackground.isHidden = false
                sender.isUserInteractionEnabled = false
                let url = URL(string: getPdfUrl)!
                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
                session.downloadTask(with: url).resume()
            } else {
                showAlertMessage.alert(message: "No internet Available")
            }
        }
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64) {
        taskTotalBytesWritten = Int(writ)
        taskTotalBytesExpectedToWrite = Int(exp)
        percentageWritten = Float(taskTotalBytesWritten) / Float(taskTotalBytesExpectedToWrite)
        DispatchQueue.main.async {
            let written = self.byteFormatter.string(fromByteCount: Int64(self.taskTotalBytesWritten))
            let expected = self.byteFormatter.string(fromByteCount: Int64(self.taskTotalBytesExpectedToWrite))
            self.fileSizeLbl.text = "\(written) / \(expected)"
            self.progressView.progress = self.percentageWritten
            self.percentLbl.text = String(format: "%.01f", self.percentageWritten*100) + "%"
        }
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        DispatchQueue.main.async {
            self.viewbackground.isHidden = true
        }
        let data = try? Data.init(contentsOf: location)
        if let pdfData = data{
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            var pdfNameFromUrl = ""
            if cameFor == .getSamplePaperPdf{
                pdfNameFromUrl = "\(self.selectedTopic?.subjectName ?? "") \(self.selectedTopic?.className ?? "") \(self.pdfExercise?.exerciseType ?? "").pdf"
            }else{
                pdfNameFromUrl = "\(self.selectedTopic?.subjectName ?? "") \(self.selectedTopic?.chapterName ?? "") \(pdfExercise?.exercise ?? "") (\(self.selectedTopic?.className ?? "")).pdf"
            }
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            DispatchQueue.main.async {
                do {
                    try pdfData.write(to: actualPath, options: .atomic)
                    CommonUtils.showHud(show: false)
                    showAlertMessage.alert(message: "Pdf saved successfully!\nfile manager -> CreativeKids")
                    self.buttonGetPdf.isUserInteractionEnabled = true
                } catch {
                    self.buttonGetPdf.isUserInteractionEnabled = true
                    print("Pdf could not be saved")
                }
            }
        }else{
            self.buttonGetPdf.isUserInteractionEnabled = true
            showAlertMessage.alert(message: "Pdf saved failed!")
        }
    }
    //MARK: - Actions
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - GET EBook PDF API
    func getEbookPdfApi(){
        var serviceName = ""
        if cameToType == .Scan {
            for typeTopic in self.topicModel ?? [] {
                serviceName = kBaseUrl + "Ebook/?chid=\(String.getString(typeTopic.chapterId))&sid=\(String.getString(typeTopic.subjectId))&cls=\(String.getString(typeTopic.className))".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            }
        } else {
            serviceName = kBaseUrl + "Ebook/?chid=\(String.getString(self.selectedTopic?.chapterNumber ?? ""))&sid=\(String.getString(self.selectedTopic?.sunjectId ?? "0"))&cls=\(String.getString(self.selectedTopic?.className ?? ""))".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }
        
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let pdfData = kSharedInstance.getArray(withDictionary: response[getResponce])
                let pdfUrl = pdfData[0]["ebook"] as? String
                let urlRequest = URLRequest(url: URL(string: pdfUrl ?? "")!)
                self.EbookWebView.load(urlRequest)
            } else {
                showAlertMessage.alert(message: "No PDF Found")
            }
        }
    }
    //MARK: - GET EBook PDF API
    func getPracticePaperPdfApi(){
        let serviceName = "MT_Ques/?chid=\(String.getString(self.selectedTopic?.chapterNumber ?? ""))&sid=\(String.getString(self.selectedTopic?.sunjectId ?? "0"))&cls=\(String.getString(self.selectedTopic?.className ?? ""))&\(self.pdfExercise?.exerciseType?.lowercased() ?? "mt")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let pdfData = kSharedInstance.getArray(withDictionary: response[getResponce])
                let pdfUrl = pdfData[0]["MT_Ques"] as? String
                self.getPdfUrl = pdfUrl ?? "http//:"
                let urlRequest = URLRequest(url: URL(string: pdfUrl ?? "")!)
                self.EbookWebView.load(urlRequest)
            } else {
                showAlertMessage.alert(message: "No PDF Found")
            }
        }
    }
}
extension EbookViewController: WKUIDelegate,WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        CommonUtils.showHud(show: false)
        buttonGetPdf.isUserInteractionEnabled = true
        buttonBack.isUserInteractionEnabled = true
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        CommonUtils.showHud(show: true)
        buttonGetPdf.isUserInteractionEnabled = false
        buttonBack.isUserInteractionEnabled = false
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        CommonUtils.showHud(show: false)
        buttonGetPdf.isUserInteractionEnabled = true
        buttonBack.isUserInteractionEnabled = true
    }
}

