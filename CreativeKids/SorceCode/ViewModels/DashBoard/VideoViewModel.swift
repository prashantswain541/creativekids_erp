//
//  VideoViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 31/03/21.
//

import Foundation
import UIKit

class VideoViewModel{
    var vc:VideoPlayerViewController?
    init(viewController:VideoPlayerViewController,completionHandler: @escaping ((Any)->())) {
        self.vc = viewController
        chapterWiseVideoExerciseApi { (data) in
            completionHandler(data)
        }
    }
    
    //MARK:- Chapter Wise API
    func chapterWiseVideoExerciseApi( completion: @escaping ((Any)->())){
        var serviceName = ""
        if isSchoolUser(){
            serviceName = "getChapterListdemoERP/?subid=\(vc?.selectedTopic?.sunjectId ?? "")&Chapid=\(vc?.selectedTopic?.chapterNumber ?? "")&cls=\(vc?.selectedTopic?.className ?? "")&Userid=\(kUserData.userEmail ?? "")&subname=\(vc?.selectedTopic?.subjectName ?? "")".replacingOccurrences(of: " ", with: "%20")
        }else{
            serviceName = "getChapterListdemo/?subid=\(vc?.selectedTopic?.sunjectId ?? "")&Chapid=\(vc?.selectedTopic?.chapterNumber ?? "")&cls=\(vc?.selectedTopic?.className ?? "")&Email=\(kUserData.userEmail ?? "")&subname=\(vc?.selectedTopic?.subjectName ?? "")".replacingOccurrences(of: " ", with: "%20")
        }
        BaseController.shared.postToServerAPI(url: vc?.camto == .Scan ? String.getString(vc?.scanUrl) : serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                let topicArray = kSharedInstance.getArray(withDictionary: response[getResponce])
                let dictArray = topicArray.map{kSharedInstance.getDictionary($0)}
                self.setSubjectAndTopic(topicData: dictArray.first)
                var topicModelArray = [TopicModel]()
                print(dictArray.count)
                for value in dictArray{
                    if !topicModelArray.isEmpty{
                        var objectExist = false
                        for checkValue in topicModelArray{
                            if String.getString(value["topicid"]) == checkValue.topicId{
                                objectExist = true
                                checkValue.updateValue(data: value)
                                break
                            }
                        }
                        if !objectExist{
                            topicModelArray.append(TopicModel(data: value))
                        }
                    }else{
                        topicModelArray.append(TopicModel(data: value))
                    }
                }
                completion(topicModelArray)
            } else {
                showAlertMessage.alert(message: "Bad request")
            }
        }
    }
// MARK: SET SUBJECT AND TOPIC WHILE CAME FROM SCAN
    func setSubjectAndTopic(topicData: Dict?) {
        if vc?.camto == .Scan {
            guard let topicData else { return }
            let subject = SubjectModel(scanData: topicData)
            let selectedTopic = TopicSearchModel(scanData: topicData)
            vc?.subjectSelected = subject
            vc?.selectedTopic = selectedTopic
            let recentChapterDict:Dict = ["selectedSubject":subject.dataDict as Any,"recentTopic":selectedTopic.dataDict]
            kSharedUserDefaults.setRecentChapterSeen(recentChapter: recentChapterDict)
            
        }
    }
}
