//
//  ChapterViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 25/03/21.
//

import Foundation
import UIKit

protocol ReloadListDelegate{
    func reloadSubjectList()
}

class ChapterViewModel{
    
    var vc:ChapterViewController?
    var reloadListDelegate:ReloadListDelegate?
    init(controller:ChapterViewController,languageSelection:String,complitionHandler: @escaping (([TopicSearchModel])->())) {
        self.vc = controller
        InAppManager.shared.subscribeDataDelegate = self
        chapterApi(language: languageSelection) { (chapterDataInArray) in
            complitionHandler(chapterDataInArray)
        }
    }
    
    func getHeaderImage(subjectName:String)->UIImage{
        switch subjectName {
        case "Rhymes":
            return UIImage(named: "demo_index_side_header") ?? UIImage()
        case "Stories":
            return UIImage(named: "demo_index_side_header") ?? UIImage()
        case "English","English NCERT":
            return UIImage(named: "eng_index_side_header") ?? UIImage()
        case "Maths":
            return UIImage(named: "math_index_side_header") ?? UIImage()
        case "Mathematics","Mathematics NCERT":
            return UIImage(named: "math_index_side_header") ?? UIImage()
        case "EVS":
            return UIImage(named: "evs_index_side_header") ?? UIImage()
        case "Sunshine Term-1":
            return UIImage(named: "demo_index_side_header") ?? UIImage()
        case "Sunshine Term-2":
            return UIImage(named: "demo_index_side_header") ?? UIImage()
        case "English Grammar","English Grammar NCERT":
            return UIImage(named: "grammar_index_side_header") ?? UIImage()
        case "Hindi Vyakaran","Hindi Vyakaran NCERT":
            return UIImage(named: "hindi_index_side_header") ?? UIImage()
        case "Computer":
            return UIImage(named: "demo_index_side_header") ?? UIImage()
        case "Science","Science NCERT":
            return UIImage(named: "sci_index_side_header") ?? UIImage()
        case "Social Science","Social Science NCERT":
            return UIImage(named: "sst_index_side_header") ?? UIImage()
        case "Hindi","Hindi NCERT":
            return UIImage(named: "hindi_index_side_header") ?? UIImage()
        case "Sanskrit":
            return UIImage(named: "demo_index_side_header") ?? UIImage()
        default:
             return UIImage(named: "demo_index_side_header") ?? UIImage()
        }
    }
    
    func chapterApi(language:String,complition: @escaping (([TopicSearchModel])->())){
        var serviceName = "?subid=\(vc?.subjectSelected?.sunjectId ?? "0")&uid=\(kUserData.userEmail ?? "")&eng=English".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        switch String.getstring(kUserData.role){
        case DashboardType.Student.userType:
            serviceName = "getChapterStudent/?subid=\(vc?.subjectSelected?.sunjectId ?? "0")&cls=\(kUserData.className ?? "")&schid=\(kUserData.schid ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        case DashboardType.Teacher.userType:
            return
        case DashboardType.NormalUsers.userType:
            if !(vc?.subjectSelected?.isSubscribed ?? false){
                if language == "hnd"{
                    serviceName = "getChapterhndUpdate/?subid=\(vc?.subjectSelected?.sunjectId ?? "0")&\(language)=\(language)&usidnotsubs=\(kUserData.userEmail ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                } else {
                    serviceName = "getChaptengUpdate/?subid=\(vc?.subjectSelected?.sunjectId ?? "0")&\(language)=\(language)&usidnotsubs=\(kUserData.userEmail ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                }
            }else{
                serviceName = "getChapterhnd/?subid=\(vc?.subjectSelected?.sunjectId ?? "0")&uid=\(kUserData.userEmail ?? "")&\(language)=\(language)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            }
        default:
            return
        }
        
        
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let chapterArray = kSharedInstance.getArray(withDictionary: response[getResponce])
                let fillChapterModel = chapterArray.map{TopicSearchModel(data: $0)}
                complition(fillChapterModel)
            } else {
                showAlertMessage.alert(message: "No data")
            }
        }
    }
}

extension ChapterViewModel:SubscribeDetailDelegate{
    func getSubScribedData(subscribeInfo: [SubscrptionDetails]) {
        print(subscribeInfo.first?.productId as Any)
        print(subscribeInfo.first?.transationId as Any)
        purchaceApi(selectedSubject: vc?.subjectSelected, transactionID: subscribeInfo.first?.transationId ?? ""){ status in
            if status{
                self.userSubscribeApi()
            }
        }
    }
    
    //MARK:- GET Purchase API
    func purchaceApi(selectedSubject:SubjectModel?, transactionID:String, completion: @escaping ((_ status:Bool)->Void)){
        let serviceName = "?subname=\(selectedSubject?.subjectName ?? "")&orderid=\(vc?.orderId ?? "")&userId=\(kUserData.userEmail ?? "")&tsnid=\(transactionID)&subid=\(selectedSubject?.sunjectId ?? "")&cls=\(selectedSubject?.className ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                completion(true)
            } else {
                completion(false)
                showAlertMessage.alert(message: "Invalid credential")
            }
        }
        CommonUtils.showHudWithNoInteraction(show: false)
    }
    
    //MARK: userSubscribeApi
    func userSubscribeApi(){
        let serviceName = "?Email=\(kUserData.userEmail ?? "")&abc=xyz"
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                let saveDataToUserDefalut:[String:Any] = ["data":data]
                kUserData.subscriptionData = data.map{SubjectModel(data: $0)}
                kSharedUserDefaults.setUserSubscribtionDetail(subscribtionDetail: saveDataToUserDefalut)
                self.reloadListDelegate?.reloadSubjectList()
                
            } else {
                showAlertMessage.alert(message: "Unable to get user subscription Data")
            }
        }
    }
}
