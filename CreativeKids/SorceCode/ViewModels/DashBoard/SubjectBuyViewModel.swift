//
//  SubjectBuyViewModel.swift
//  CreativeKids
//
//  Created by Rakesh jha on 28/05/21.
//

import Foundation
import UIKit

class SubjectBuyViewModel{
    
    var vc:SubjectBuyViewController?
    init(viewController:SubjectBuyViewController?,forClass:String,classType:String,complitionHandler: @escaping (([SubjectModel])->())){
        self.vc = viewController
        getSubjectApi(selectedClass:forClass,classType:classType){ data in
            complitionHandler(data)
        }
    }
    
    func getSubjectApi(selectedClass:String,classType:String = "",complition: @escaping (([SubjectModel])->())){
        let serviceUrl = "getSubjectByClassIdCordova/?eid=\(kUserData.userEmail ?? "")&clssa=\(selectedClass)&\(classType)".replacingOccurrences(of: " ", with: "%20")
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let subjectArray = kSharedInstance.getArray(withDictionary: response[getResponce])
                let fillSubjectModel = subjectArray.map{SubjectModel(data: $0)}
                complition(fillSubjectModel)
            } else {
                showAlertMessage.alert(message: "NO Data Found")
            }
        }
    }
}
