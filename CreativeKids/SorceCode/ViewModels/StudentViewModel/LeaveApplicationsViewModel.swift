//
//  LeaveApplicationsViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 09/06/22.
//

import Foundation

class LeaveApplicationsViewModel{
    init() {
    }
    
    func getStudentLeaveListForTeacher(completion: @escaping (([LeaveModel])->Void)) {
        let serviceUrl = "LeaveList/?schid=\(String.getstring(kUserData.schid))&teachid=\(String.getstring(kUserData.userId))&clas=\(String.getstring(kUserData.classId))"
        // let serviceUrl = "?schid=\(String.getstring(kUserData.schid))&teachid=42&clas=\(String.getstring(kUserData.classId))"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            print(returnData)
            let leaveListArray = returnData.map{LeaveModel(data: $0)}
            completion(leaveListArray)
            //print(dataArray)
        }
    }
    
    func getLeaveListForStudent(completion: @escaping (([LeaveModel])->Void)){
        let serviceUrl = "LeaveDetails/\(String.getstring(kUserData.schid))/\(String.getstring(kUserData.classId))/\(String.getstring(kUserData.userId))"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
                           let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            print(returnData)
            var leaveListArray = returnData.map{LeaveModel(Kdata: $0)}
            completion(leaveListArray)
        }
    }
    
    func getLeaveListForTeacehr(completion: @escaping (([LeaveModel])->Void)){
        //        let serviceUrl = "?schid=\(String.getstring(kUserData.schid))&teachid=\(String.getstring(kUserData.userId))&clas=\(String.getstring(kUserData.classId))"
        let serviceUrl = "TeacherLeaveList/?Tteachid=\(String.getstring(kUserData.userId))&schid=\(String.getstring(kUserData.schid))"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            print(returnData)
            let leaveListArray = returnData.map{LeaveModel(teacherdata:$0)}
            completion(leaveListArray)
        }
    }
}
