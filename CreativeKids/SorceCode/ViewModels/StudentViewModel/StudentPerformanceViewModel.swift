//
//  StudentPerformanceViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 11/10/22.
//

import Foundation

class StudentPerformanceViewModel{
    init(){
        
    }
    
    func getPerformanceApi(complition: @escaping ((Dict)->())){
        if kSharedUserDefaults.isUserLoggedIn(){
            
            let className = kUserData.className ?? ""
            var serviceUrl = ""
             
            if kUserData.role == "1" {
                serviceUrl = "dash/?Email=\(kUserData.userEmail ?? "")&dash=abc&cls=\(className)".replacingOccurrences(of: " ", with: "%20")
            } else if kUserData.role == "2" {
                serviceUrl = "dash/?Email=\(kUserData.userEmail ?? "")&dash=abc&cls=\(className)".replacingOccurrences(of: " ", with: "%20")
            } else {
                serviceUrl = "dashboard/?uid=\(kUserData.userEmail ?? "")&cls=\(className)".replacingOccurrences(of: " ", with: "%20")
            }
            BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let performanceDetailArray = kSharedInstance.getArray(withDictionary: response[getResponce])
                    let performanceDetailInDict = kSharedInstance.getDictionary(performanceDetailArray.first)
                    complition(performanceDetailInDict)
                } else {
                    showAlertMessage.alert(message: "NO Performance Data Found")
                }
            }
        }else{
            complition([:])
        }
    }
}
