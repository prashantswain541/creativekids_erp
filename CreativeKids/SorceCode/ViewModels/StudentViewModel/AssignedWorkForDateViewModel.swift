//
//  AssignedWorkForDateViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 14/10/22.
//

import Foundation
protocol AssignedWorkForDateDelegate{
    func onSuccess(_ data:Any)
}

class AssignedWorkForDateViewModel{
    var dataPassDelegate:AssignedWorkForDateDelegate?
    
    func assignedWorkForDateApi(date:String = String.getCurrentDate(format: "dd-MM-yyyy")){
        let serviceUrl = "HomeWorkDetails/\(kUserData.ClassSecid ?? "")/\(kUserData.schid ?? "")/\(kUserData.userId ?? "")/?_date=\(String.convertDateString(dateString: date, fromFormat: "dd-MM-yyyy", toFormat: "yyyy/MM/dd"))"
            BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                    let HomeWorkArray = data.map{HomeWork(data: $0)}
                    self.dataPassDelegate?.onSuccess(HomeWorkArray)
                } else {
                    showAlertMessage.alert(message: "No HomeWork Found")
                }
            }
    }
}
