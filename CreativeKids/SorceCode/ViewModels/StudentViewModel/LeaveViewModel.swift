//
//  LeaveViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 08/06/22.
//

import Foundation
import UIKit


class LeaveViewModel{
    var vc:LeaveViewController?
    init(vc:LeaveViewController) {
        self.vc = vc
    }
    
    func validation() -> Bool{
        if self.vc?.textViewSubject.text.isEmpty ?? true{
            showAlertMessage.alert(message: "Enter subject for leave application")
            return false
        }else if self.vc?.leaveType == nil{
            showAlertMessage.alert(message: "Select any leave type")
            return false
        }else if checkForDate(){
            return false
        }else if self.vc?.textViewBody.text.isEmpty ?? true{
            showAlertMessage.alert(message: "Enter body for leave application")
            return false
        }else if self.vc?.imageViewSignature.image == nil{
            showAlertMessage.alert(message: "Upload Gardian signature")
            return false
        }else{
            return true
        }
    }

    func checkForDate() -> Bool{
        switch self.vc?.leaveType{
        case .halfDay,.fullDay:
            if self.vc?.textFieldFromDate.text?.isEmpty ?? true{
                showAlertMessage.alert(message: "Select date for leave.")
                return true
            }
            return false
        case .longLeave:
            if self.vc?.textFieldFromDate.text?.isEmpty ?? true{
                showAlertMessage.alert(message: "Select from date for leave.")
                return true
            }else if self.vc?.textFieldToDate.text?.isEmpty ?? true{
                showAlertMessage.alert(message: "Select to date for leave.")
                return true
            }
            return false
        default:
          return true
        }
        
    }
    
    func leaveApplicationApi(userType:UserType, leaveType:LeaveType,completion: @escaping ((Bool)->Void)){
        let userSignature = vc?.imageViewSignature.image ?? UIImage()
        var toDate = leaveType == .longLeave ? (vc?.textFieldToDate.text ?? "") : (leaveType == .fullDay ? vc?.textFieldFromDate.text ?? "" : "")
        var serviceName = ""
        switch userType{
        case .Student:
            serviceName = "user/applyLeave/?stuid=\(kUserData.userId ?? "")&schid=\(kUserData.schid ?? "")&subj=\(vc?.textViewSubject.text ?? "")&techid=\(kUserData.teacherId ?? "")&fromdate=\(vc?.textFieldFromDate.text ?? "")&todate=\(toDate)&reason=\(vc?.textViewBody.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        case .Teacher:
            serviceName = "user/leave/?Teacherid=\(kUserData.userId ?? "")&schoolid=\(kUserData.schid ?? "")&subj=\(vc?.textViewSubject.text ?? "")&fromdate=\(vc?.textFieldFromDate.text ?? "")&todate=\(toDate)&reason=\(vc?.textViewBody.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }

        let postImage:[String:Any] = ["imageName":ApiParameters.signature, "image":userSignature]
        
        BaseController.shared.postToServerRequestMultiPart(serviceName, params: [:], imageParams: [postImage]) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getDictionary(response[getResponce])
                CommonUtils.showToast(message: String.getstring(data["success"]))
                completion(true)
            } else {
                showAlertMessage.alert(message: "Error in Request")
            }
        }
    }
    
}
