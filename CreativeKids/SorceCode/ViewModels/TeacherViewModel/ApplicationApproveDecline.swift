//
//  ApplicationApproveDecline.swift
//  CreativeKids
//
//  Created by Creative Kids on 10/06/22.
//

import Foundation


class ApplicationApproveDeclineViewModel{
    var vc:LeavePopupViewController?
    
    init(vc:LeavePopupViewController) {
        self.vc = vc
    }
    
    func approveApplication(completion:@escaping ((Bool)->Void)){
        let serviceUrl = "approveLeave/?teachid=\(kUserData.userId ?? "")&lvid=\(vc?.applicationData?.id ?? "")"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            if statusCode == 200{
                let returnData = String.getstring(response[getResponce])
                CommonUtils.showToast(message: "Leave Approved Successfully.")
                completion(true)
            }else{
                showAlertMessage.alert(message: "Something went wrong")
            }
        }
    }

    func declineApplication(completion:@escaping ((Bool)->Void)){
        let serviceUrl = "declineLeave/?teachid=\(kUserData.userId ?? "")&lvids=\(vc?.applicationData?.id ?? "")"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            if statusCode == 200{
                let returnData = String.getstring(response[getResponce])
                CommonUtils.showToast(message: "Leave Decline Successfully.")
                completion(true)
            }else{
                showAlertMessage.alert(message: "Something went wrong")
            }
        }
    }
}
