//
//  StudentAttandence.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/05/22.
//

import Foundation

class StudentAttendanceViewModel{
    var studentAttendanceCallback: (([StudentAttendance])->())?
    var attendanceSubmitCallback:  ((Bool)->())?
    
    init(selectedClass:String,selectedSection: String, attendanceType:AttandanceType, completionHandler: @escaping (([StudentAttendance])->())){
        self.studentAttendanceCallback = completionHandler
        getAttandenceDetail(selectedClass: selectedClass,selectedSection:selectedSection, attendanceType: attendanceType)
    }
    init(studentArrendanceList:[StudentAttendance],attendanceFor:AttandanceType, completionHandler: @escaping ((Bool)->())){
        self.attendanceSubmitCallback = completionHandler
        let dict = StudentAttendance.getAttandanceDictionery(studentObjects: studentArrendanceList, attendanceFor: attendanceFor)
        markAttendance(attendanceListDict: dict)
    }
    
    func getAttandenceDetail(selectedClass: String, selectedSection: String, attendanceType:AttandanceType) {
        let serviceUrl = kStudentBaseUrl + "getstuforAttendancenew/?schid=\(kUserData.schid ?? "")&teachid=\(kUserData.userId ?? "")&clas=\(selectedClass)&dt=\(String.getCurrentDate(format: "MM/dd/yyyy"))&secid=\(selectedSection)&periods=\(attendanceType.rawValue)"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            if statusCode == 200{
                let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
                let attandenceData = returnData.map{StudentAttendance(data: $0)}
                self.studentAttendanceCallback?(attandenceData)
            }else{
                showAlertMessage.alert(message: "Something went wrong")
            }
        }
    }
    
    func markAttendance(attendanceListDict:[Dict]){
        CommonUtils.showHudWithNoInteraction(show: true)
        let params: [String: Any] = ["Students": attendanceListDict, "Attend":"studentAttendence", "Dt": String.getCurrentDate(format: "yyyy-MM-dd")]
        var request = URLRequest(url: URL(string: kBaseUrl + "submitAttendance")!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        print(params)
        request.httpMethod = "POST"
        let postData = json(from: params)
        guard let data = postData?.data(using: .utf8) else{return}
        request.httpBody = data
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            CommonUtils.showHudWithNoInteraction(show: false)
          guard let data = data else {
            print(String(describing: error))
            return
          }
            guard let responseData = response as? HTTPURLResponse else {return}
            
            if responseData.statusCode == 200 {
                self.attendanceSubmitCallback?(true)
            }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
        
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
}
