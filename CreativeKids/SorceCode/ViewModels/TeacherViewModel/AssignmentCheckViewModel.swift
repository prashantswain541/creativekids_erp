//
//  AssignmentCheckViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 07/10/22.
//

import Foundation

protocol AssignmentCheckedDataSources{
    func checkedStatus(status:Bool)
    func studentWorked(data:Any)
    func onSuccess(_ data:Any)
}

class AssignmentCheckViewModel{
    var studentWorkedData:HomeWorkModel?
    var studentDetail:StudentWork?
    var dataPassDataSources:AssignmentCheckedDataSources?
    init(worked:HomeWorkModel){
        studentWorkedData = worked
    }
    
    init(studentDetail detail:StudentWork?){
        self.studentDetail = detail
    }
    
    //MARK: Assignment Checked BY USER
    
    func assignmentCheckedByTeacherApi() {
//        let serviceName = kTeacherBaseUrl + "CheckHomework"
        let serviceName = kTeacherBaseUrl + "Check?AssignId=\(studentDetail?.workDetail.workId ?? "")&studentid=\(studentDetail?.studentId ?? "")"
        let params:Dict = [ApiParameters.studentID:studentDetail?.studentId ?? "",ApiParameters.teacherId:kUserData.userId ?? "",ApiParameters.assigmentIdByStudent:studentDetail?.workDetail.workId ?? "",ApiParameters.remark:5]
        BaseController.shared.postToServerAPI(url: serviceName, params: params, type: .POST) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getDictionary(response[getResponce])
                let message = String.getstring(data["message"])
                CommonUtils.showToast(message: message)
                self.dataPassDataSources?.checkedStatus(status: true)
            } else {
                showAlertMessage.alert(message: "Failed to submit try again")
            }
        }
    }
    
    
    func getStudentAssignment() {
        //        let serviceName = kTeacherBaseUrl + "GetHomeWorkByStudentId/\(studentDetail?.studentId ?? "")/1236"
        let serviceName = kTeacherBaseUrl + "GetHomeWorkByStudentId/\(studentDetail?.studentId ?? "")/\(studentDetail?.workDetail.workId ?? "")"
        let params:Dict = [:]
        BaseController.shared.postToServerAPI(url: serviceName, params: params, type: .GET) { (response, statusCode) in
            let data = kSharedInstance.getArray(withDictionary: response[getResponce])
            if statusCode == 200 {
                print("response: ------\(response)")
                let studentSubmittedWork = data.map{StudentSubmitedWork(data: $0)}
                self.dataPassDataSources?.studentWorked(data: studentSubmittedWork)
            } else {
                showAlertMessage.alert(message: "Failed to submit try again")
            }
        }
    }
    
    func workQuestionApi(){
//        let serviceUrl = "GetHomeWorkBySubject/1236"
        let serviceUrl = "GetHomeWorkBySubject/\(studentDetail?.workDetail.workId ?? "")"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getDictionary(response[getResponce])
                let homeWorkData = AssignmentModel(data: data)
                print(homeWorkData)
                self.dataPassDataSources?.onSuccess(homeWorkData)
            } else {
                showAlertMessage.alert(message: "No HomeWork Question are found.")
            }
        }
    }
}
