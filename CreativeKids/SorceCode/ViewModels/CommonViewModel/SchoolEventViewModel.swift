//
//  SchoolEventViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/09/22.
//

import Foundation

protocol HomeEventDataSources{
    func eventData(data:[Any])
}
class HomeEventViewModel{
    var dataPassDataSources:HomeEventDataSources?
    var youtubeModel:YouTubeListViewModel?
    init(){
        homeEventApi()
    }
    
    // MARK: GET EVENT IMAGES
        func homeEventApi(){
            let serviceName = "Gallery/\(kUserData.schid ?? "")"
            BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                    let periodDataArray = data.map{EventModel(data: $0)}
                    print(periodDataArray.count)
                    if periodDataArray.isEmpty{
                        self.getYoutubeData()
                    }else{
                        self.dataPassDataSources?.eventData(data: periodDataArray)
                    }
                }else if statusCode == 404{
                    print("data not found")
                    self.getYoutubeData()
                }
                else {
                    showAlertMessage.alert(message: "Something went wrong")
                }
            }
        }
    
    // MARK: Get Popular Video When School Event Folder is Empty
    func getYoutubeData(){
        youtubeModel = YouTubeListViewModel(){ data in
            self.dataPassDataSources?.eventData(data: data)
        }
    }
    
}
