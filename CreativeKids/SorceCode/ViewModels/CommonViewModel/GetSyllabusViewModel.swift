//
//  GetSyllabusViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 14/10/22.
//

import Foundation

protocol GetSyllabusDelegate{
    func onSuccess(_ data:Any)
}

class GetSyllabusViewModel{
    
    var dataPassDelegate:GetSyllabusDelegate?
    func getSyllabusForStudentApi(){
        let serviceUrl = "GetSyllabus/\(kUserData.classId ?? "")/\(kUserData.ClassSecid ?? "")/\(kUserData.schid ?? "")"
            BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                    print(data)
                    self.dataPassDelegate?.onSuccess(data)
                } else {
                    showAlertMessage.alert(message: "No Syllabus found")
                }
            }
    }
    
    func getSyllabusForTeacherApi(){
        let serviceUrl = kBaseUrl + "GetSyllabus/\(kUserData.classId ?? "")/\(kUserData.ClassSecid ?? "")/\(kUserData.schid ?? "")"
            BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let data = kSharedInstance.getArray(response[getResponce])
                    print(data)
                    self.dataPassDelegate?.onSuccess(data)
                } else {
                    showAlertMessage.alert(message: "No Syllabus found")
                }
            }
    }
}
