//
//  HomeWorkDatesViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 14/10/22.
//

import Foundation

protocol HomeWorkDatesDelegate{
    func onSuccess(_ data:Any)
}

class HomeWorkDatesViewModel{
    var dataPassDelegate:HomeWorkDatesDelegate?
    func homeWorkDatesApi(){
        let serviceUrl = "getAssignmentDates/?ClassId=\(kUserData.ClassSecid ?? "")"
//        var request = URLRequest(url: URL(string: "https://creativekidssolutions.com/students/getAssignmentDates/?ClassId=1")!,timeoutInterval: Double.infinity)
//        request.httpMethod = "GET"
//
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//          guard let data = data else {
//            print(String(describing: error))
//            return
//          }
//          print(String(data: data, encoding: .utf8)!)
//        }
//
//        task.resume()

                BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
                    if statusCode == 200 {
                        //print("response: ------\(response)")
                        let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                        let datesArray = data.map{HomeWorkDate(data: $0)}
                        self.dataPassDelegate?.onSuccess(datesArray)
                    } else {
                        showAlertMessage.alert(message: "No HomeWork Found")
                    }
                }
    }
    func homeWorkDatesApiForTeacher(){
        let serviceUrl = kTeacherBaseUrl + "HomeworkDates/\(kUserData.schid ?? "")/\(kUserData.teacherId ?? "")"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                let datesArray = data.map{HomeWorkDate(data: $0)}
                self.dataPassDelegate?.onSuccess(datesArray)
            } else {
                showAlertMessage.alert(message: "No HomeWork Found")
            }
        }
    }
}
