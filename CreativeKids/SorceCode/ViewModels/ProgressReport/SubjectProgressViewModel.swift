//
//  SubjectProgressViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 17/07/21.
//

import Foundation
import UIKit


class SubjectProgressViewModel {
    
    var completion:(((PerformanceModel,PerformanceModel))->Void)?
    var vc:ProgressReportViewController?
    init(viewController:UIViewController,complitionHandler:@escaping (((PerformanceModel,PerformanceModel))->Void)){
        self.completion = complitionHandler
        self.vc = viewController as? ProgressReportViewController
        getPerformanceApi()
    }
    init(){
        
    }
    
    func getPerformanceApi(){
        let selectedSubjectObject = vc?.subjectListArray.filter{$0.isSelected}
        let serviceUrl = "dashSub/?email=\(kUserData.userEmail ?? "")&cls=\(selectedSubjectObject?.first?.className ?? "")&sid=\(selectedSubjectObject?.first?.sunjectId ?? "")".replacingOccurrences(of: " ", with: "%20")
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let performanceDetailDict = kSharedInstance.getDictionary(response[getResponce])
                let performanceDetailArray = kSharedInstance.getArray(withDictionary: performanceDetailDict["lstdash1"])
                let performanceTimeDetailArray = kSharedInstance.getArray(withDictionary: performanceDetailDict["dashbottom"])
                let performanceModelArray = performanceDetailArray.map{PerformanceModel(data: $0)}
                let performanceTimeModelArray = performanceTimeDetailArray.map{PerformanceModel(data: $0)}
                print(performanceModelArray)
                print(performanceTimeModelArray)
                self.completion?((performanceModelArray.first!,performanceTimeModelArray.first!))
            } else {
                showAlertMessage.alert(message: "NO Data Found")
            }
        }
    }
    
    //http://api.cordovalearningsolutions.com/Students/GetPerformance/studentId/schoolId
    
    func getStuPerformanceApi(){
        let serviceUrl = kStudentBaseUrl + "GetPerformance/\(kUserData.userId ?? "")/\(kUserData.schid ?? "")"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                let datesArray = data.map{HomeWorkDate(data: $0)}
//                self.dataPassDelegate?.onSuccess(datesArray)
            } else {
                showAlertMessage.alert(message: "No Performance Found")
            }
        }
    }
}
