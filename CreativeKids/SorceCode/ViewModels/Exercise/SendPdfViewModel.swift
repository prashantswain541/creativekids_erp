//
//  SendPdfViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 21/08/21.
//

import Foundation
import UIKit
class SentPDFViewModel {
    var previewPdfPath:String?
    var resultPdfPath:String?
    var exerciseResultPdfPath:String?
    var controller:UIViewController?
    
    //MARK: SEND LTP RESULT View
    init(vc:UIViewController,previewPdfPath:String, resultPdfPath:String) {
        self.controller = vc
        self.previewPdfPath = previewPdfPath
        self.resultPdfPath = resultPdfPath
        postPDFApi()
    }
    
    //MARK: SEND LTP RESULT View
    init(vc:UIViewController,exricsePdfPath:String) {
        self.controller = vc
        self.exerciseResultPdfPath = exricsePdfPath
        postexercisePDFApi()
    }
    
    //MARK:- API for ltp result
    public func postPDFApi() {
        guard let viewController = controller as? ReviewViewController else{return}
        let previewPdfUrl = URL(fileURLWithPath: previewPdfPath!)
        let resultPdfUrl = URL(fileURLWithPath: resultPdfPath!)
        let previewImage:[String:Any] = ["imageName":ApiParameters.previewPdf, "image":previewPdfUrl]
        let resultImage:[String:Any] = ["imageName":ApiParameters.resultPdf, "image":resultPdfUrl]
        let serviceUrl = "user/Postpdf/?email=\(kUserData.userEmail ?? "")&type=\(viewController.exercise?.exercise ?? "LTP")&parentemail=\(kUserData.parentEmail ?? "")&teachemail=\(kUserData.teacherEmail ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerRequestMultiPart(serviceUrl, params: [:], imageParams: [previewImage,resultImage],showHud: false) { kResponse, statusCode in
            print("PDF Sent")
        }
    }
    
    //MARK:- API for exercise result
    public func postexercisePDFApi() {
        guard let viewController = controller as? DNDResultViewController else{return}
        let resultPdfUrl = URL(fileURLWithPath: exerciseResultPdfPath!)
        let resultImage:[String:Any] = ["imageName":ApiParameters.resultPdf, "image":resultPdfUrl]
        let serviceUrl = "user/Postpdf/?email=\(kUserData.userEmail ?? "")&type=\(viewController.exercise?.exercise ?? "Exercise")&parentemail=\(kUserData.parentEmail ?? "")&teachemail=\(kUserData.teacherEmail ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerRequestMultiPart(serviceUrl, params: [:], imageParams: [resultImage],showHud: false) { kResponse, statusCode in
            print("PDF Sent")
        }
    }
}
