//
//  ChangeProfile.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/03/21.
//

import Foundation
import UIKit
 
class ChangeProfileViewModel{
    
    var vc:EditProfileViewController?
    init(viewController:EditProfileViewController){
        self.vc = viewController
        setModel()
    }
    func setModel(){
        validationField()
    }
    
    func validationField(){
        if String.getString(vc?.textFieldName.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kName)
            return
        }
        self.vc?.view.endEditing(true)
        changeProfileApi()
//      uploadImage(image:vc?.imageProfile.image ?? UIImage())
    }
    
    func changeProfileApi(){
        let userImage = vc?.imageProfile.image ?? UIImage()
        let serviceName = "user/PostUserImage?id=\(kUserData.userId ?? "")&name=\(vc?.textFieldName.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let postImage:[String:Any] = ["imageName":ApiParameters.kprofilepic, "image":userImage]
        
        BaseController.shared.postToServerRequestMultiPart(serviceName, params: [:], imageParams: [postImage]) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getDictionary(response[getResponce])
                kUserData.profileImage = String.getString(data["img"])
                kUserData.userName = String.getString(data["Name"])
                kSharedAppManager.moveToHome()
                CommonUtils.showToast(message: "Profile update successFully")
            } else {
                showAlertMessage.alert(message: "Error in update profile")
            }
        }
    }

}
