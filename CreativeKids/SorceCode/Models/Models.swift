//
//  Models.swift
//  CreativeKids
//
//  Created by Creative Kids on 19/03/21.
//

import Foundation
import UIKit

class ClassModel{
    var name:String?
    var classKey:String?
    var classImage:UIImage?
    init(key: String, name:String,classImage:UIImage) {
        self.classKey = key
        self.name = name
        self.classImage = classImage
    }
}

class SubjectModel{
    var className:String?
    var sunjectId:String?
    var subjectName:String?
    var subjectImage:String?
    var subjectPrice:String?
    var isSubscribed = false
    var isSelected = false
    var dataDict:Dict = [:]
    init(data:Dict) {
        self.className = String.getString(data["class"])
        self.sunjectId = String.getString(data["sid"])
        self.subjectName = String.getString(data["titlename"])
        self.subjectImage = String.getString(data["img"])
        self.subjectPrice = String.getString(data["pric"])
        self.isSubscribed = String.getString(data["subscribe"]) != "0"
        if self.sunjectId == ""{
            self.sunjectId = String.getString(data["PID"])
        }
        dataDict = data
    }
    init(scanData:Dict){
        let createDict = ["titlename":String.getstring(scanData["subname"]),
                          "sid":String.getstring(scanData["subid"]),
                          "class":String.getstring(scanData["classsname"])]
        subjectName = String.getstring(scanData["subname"])
        sunjectId = String.getstring(scanData["subid"])
        className = String.getstring(scanData["classsname"])
        dataDict = createDict
    }
}

class SubscribeSubjectModel{
    var className:String?
    var subjectSubscribe = [SubjectModel]()
    init(data:SubjectModel) {
        className = String.getString(data.className)
        subjectSubscribe = [data]
    }
}


class SideMenuDataModel {
    var title:String?
    var sideImage:UIImage?
    init(sideTitle:String,sideImage:UIImage){
        title = sideTitle
        self.sideImage = sideImage
    }
}


class TopicModel{
    var isSelected = false
    var className:String?
    var subjectId:String?
    var topicId:String?
    var videoUrlString:String?
    var topic:String?
    var chapterId:String?
    var videoThumb:String?
    var ltp: String?
    var status = false
    var exercisesPdfExam = [ExerciseModel]()
    var exercise = [ExerciseModel]()
    var LtpExercise = [ExerciseModel]()
    var mockTest = [ExerciseModel]()
    var practicePaperPdf = [ExerciseModel]()
    var subjectName:String?
    var chapterName: String?
    init(data:Dict){
        self.topicId = String.getString(data["topicid"])
        self.subjectId = String.getString(data["subid"])
        self.className = String.getString(data["classsname"])
        self.topic = String.getString(data["topic"])
        self.videoUrlString = String.getString(data["video"])
        self.chapterId = String.getString(data["chapterid"])
        self.videoThumb = String.getString(data["thumb"])
        self.ltp = String.getString(data["ltp"])
        self.subjectName = String.getString(data["subname"])
        self.chapterName = String.getString(data["chapterName"])
        self.status = String.getString(data["status"]) != "0"
        if ["Solutions","E-Book","E-book","ebook"].contains(String.getString(data["type"])){     // CHECK
            exercisesPdfExam.append(ExerciseModel(type: String.getString(data["type"]), exercise: String.getString(data["type"])))
        }else if String.getString(data["type"]) == "LTP"{
            LtpExercise.append(ExerciseModel(type: String.getString(data["type"]), exercise: String.getString(data["ltp"])))
        }else if String.getString(data["type"]) == "MT"{
            mockTest.append(ExerciseModel(type: String.getString(data["type"]), exercise: String.getString(data["qb"])))
        }else if String.getString(data["type"]) == "MT_Ques"{
            practicePaperPdf.append(ExerciseModel(type: String.getString(data["type"]), exercise: String.getString(data["type"])))
        }else if String.getString(data["exercise1"]) != ""{
            exercise.append(ExerciseModel(type: String.getString(data["type"]), exercise: String.getString(data["exercise1"])))
        }
    }
    func updateValue(data:Dict){
        if ["Solutions","E-Book","E-book"].contains(String.getString(data["type"])){
            exercisesPdfExam.append(ExerciseModel(type: String.getString(data["type"]), exercise: String.getString(data["type"])))
        }else if String.getString(data["type"]) == "LTP"{
            LtpExercise.append(ExerciseModel(type: String.getString(data["type"]), exercise: String.getString(data["ltp"])))
        }else if String.getString(data["type"]) == "MT"{
            mockTest.append(ExerciseModel(type: String.getString(data["type"]), exercise: String.getString(data["qb"])))
        }else if String.getString(data["type"]) == "MT_Ques"{
            practicePaperPdf.append(ExerciseModel(type: String.getString(data["type"]), exercise: String.getString(data["type"])))
        }else if String.getString(data["type"]) != ""{
            exercise.append(ExerciseModel(type: String.getString(data["type"]), exercise: String.getString(data["exercise1"])))
        }
    }
}

class ExerciseModel{
    var exerciseType:String?
    var exercise:String?
    var exerciseNumber: Int?
    init(type:String,exercise:String) {
        self.exerciseType = type
        self.exercise = exercise
    }
    
}

class DemoModel{
    var id:String?
    var videoThumb:String?
    var name:String?
    var videoURL:String?
    init(data:Dict){
        self.id = String.getString(data["id"])
        self.videoThumb = String.getString(data["thumbnail"])
        self.name = String.getString(data["name"])
        self.videoURL = String.getString(data["path"])
    }
}

class YoutubeDataModel{
    var titleName:String?
    var id:String?
    var videoThumb:String?
    var videoCode:String?
    init(data:Dict){
        self.titleName = String.getString(data["name"])
        self.id = String.getString(data["id"])
        self.videoThumb = String.getString(data["thumbnail"])
        self.videoCode = String.getString(data["vidcode"])
    }
}

class TopicSearchModel{
    var className:String?
    var sunjectId:String?
    var subjectName:String?
    var firstColor:String?
    var secondColor:String?
    var chapterUniqueIdId:String?
    var downloadStatus:Bool?
    var chapterNumber:String?
    var chapterName:String?
    var topicId:String?
    var topicCount:String?
    var exerciseCount:String?
    var lptCount:String?
    var chapterBgImage:UIImage?
    var chapterEnableCount:Int?
    var showTopic:String?
    var topicName:String?
    var isSubscribed = false
    var isChapterRead = false
    var dataDict:Dict = [:]
    var samplePaperPdf:String?
    var isSelected = false
    
    init(data:Dict) {
        className = String.getString(data["classsname"])
        sunjectId = String.getString(data["subid"])
        subjectName = String.getString(data["subname"])
        chapterUniqueIdId = String.getString(data["chapterid"])
        chapterNumber = String.getString(data["chapterid"])
        chapterName = String.getString(data["chapterName"])
        topicId = String.getString(data["topicid"])
        topicCount = String.getString(data["topicid"])
        exerciseCount = String.getString(data["exe"])
        lptCount = String.getString(data["ltp"])
        chapterEnableCount = Int.getInt(data["enablechapter"])
        showTopic = "\(chapterName ?? "") in \(subjectName ?? "")"
        isChapterRead = String.getString(data["status"]) != "0"
        samplePaperPdf = String.getString(data["samplepaper"])
        dataDict = data
    }
    
    init(scanData:Dict){
        className = String.getstring(scanData["classsname"])
        sunjectId = String.getstring(scanData["subid"])
        subjectName = String.getstring(scanData["subname"])
        chapterName = String.getstring(scanData["chapterName"])
        chapterNumber = String.getstring(scanData["chapterid"])
        dataDict = scanData
    }
}

class GetSubjectColor{
    var firstColor:String? 
    var secondColor:String?
    var chapterImage:UIImage?
    init(subject:String){
        switch subject {
        case "Rhymes":
            firstColor   = "008AFF"
            secondColor  = "2AE9E7"
            chapterImage = UIImage(named: "demo_index_btn-1") ?? UIImage()
        case "Stories":
            firstColor   = "008AFF"
            secondColor  = "2AE9E7"
            chapterImage = UIImage(named: "demo_index_btn-1") ?? UIImage()
        case "English":
            firstColor   = "AD6BE9"
            secondColor  = "FF70C6"
            chapterImage = UIImage(named: "eng_index_btn-1") ?? UIImage()
        case "Maths":
            firstColor   = "0087FF"
            secondColor  = "00C9FF"
            chapterImage = UIImage(named: "math_index_btn-1") ?? UIImage()
        case "Mathematics","Mathematics NCERT":
            firstColor   = "0087FF"
            secondColor  = "00C9FF"
            chapterImage = UIImage(named: "math_index_btn-1") ?? UIImage()
        case "EVS":
            firstColor   = "FF7400"
            secondColor  = "FFAD00"
            chapterImage = UIImage(named: "evs_index_btn-1") ?? UIImage()
        case "Sunshine Term-1":
            firstColor   = "008AFF"
            secondColor  = "2AE9E7"
            chapterImage = UIImage(named: "demo_index_btn-1") ?? UIImage()
        case "Sunshine Term-2":
            firstColor   = "008AFF"
            secondColor  = "2AE9E7"
            chapterImage = UIImage(named: "demo_index_btn-1") ?? UIImage()
        case "English Grammar","English Grammar NCERT":
            firstColor   = "A364FF"
            secondColor  = "E99FFF"
            chapterImage = UIImage(named: "grammar_index_btn-1") ?? UIImage()
        case "Hindi Vyakaran","Hindi Vyakaran NCERT":
            firstColor   = "FF313F"
            secondColor  = "FF5B00"
            chapterImage = UIImage(named: "hindi_index_btn-1") ?? UIImage()
        case "Computer":
            firstColor   = "008AFF"
            secondColor  = "2AE9E7"
            chapterImage = UIImage(named: "demo_index_btn-1") ?? UIImage()
        case "Science","Science NCERT":
            firstColor   = "008E00"
            secondColor  = "66CC00"
            chapterImage = UIImage(named: "sci_index_btn-1") ?? UIImage()
        case "Social Science","Social Science NCERT":
            firstColor   = "41B400"
            secondColor  = "66CC00"
            chapterImage = UIImage(named: "sst_index_btn-1") ?? UIImage()
        case "Hindi","Hindi NCERT":
            firstColor   = "FF313F"
            secondColor  = "FF5B00"
            chapterImage = UIImage(named: "hindi_index_btn-1") ?? UIImage()
        case "Sanskrit":
            firstColor   = "008AFF"
            secondColor  = "2AE9E7"
            chapterImage = UIImage(named: "demo_index_btn-1") ?? UIImage()
        default:
            firstColor   = "008AFF"
            secondColor  = "2AE9E7"
            chapterImage = UIImage(named: "demo_index_btn-1") ?? UIImage()
        }
    }
}

class ReportSubjectModel{
    var subjectImage:UIImage?
    var subjectName:String?
    var isSelected:Bool = false
    init(name:String,isSelected:Bool,image:UIImage) {
        self.subjectName = name
        self.isSelected = isSelected
        self.subjectImage = image
    
    }
}

class SegmentModel{
    var isSelected = false
    var contentText = ""
    var type:SegmentType?
    var data:Any?
    init(isSelected:Bool,contentText:String,data:Any?,type:SegmentType) {
        self.isSelected = isSelected
        self.contentText = contentText
        self.data = data
        self.type = type
    }
}


class PerformanceModel{
    var performance:String?
    var chapterRead:String?
    var exerciseRead:String?
    var ltpRead:String?
    var totalTime:String?
    var totalQuestion:String?
    var totalMarks:String?
    var rightAnswer:String?
    var obtainMarks:String?
    
    init(data:Dict){
        performance = String.getString(data["performance"])
        chapterRead = String.getString(data["readchap"])
        exerciseRead = String.getString(data["exe"])
        ltpRead = String.getString(data["ltp"])
        totalTime = String.getString(data["time"])
        totalQuestion = String.getString(data["totalQue"])
        totalMarks = String.getString(data["totalmarks"])
        rightAnswer = String.getString(data["right"])
        obtainMarks = String.getString(data["obtainmarks"])
    }
}

class PlayerSpeedModel{
    var name = ""
    var speedType:SpeedType = .normal
    var isSelected = false
    init(type:SpeedType, isSelected:Bool = false){
        self.speedType = type
        self.isSelected = isSelected
        self.name = "\(type.rawValue)x"
    }
}
