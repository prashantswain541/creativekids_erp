//
//  SchoolCommonModels.swift
//  CreativeKids
//
//  Created by Prashant Swain on 17/10/22.
//

import Foundation

class AssignmentModel{
    var assignmentId:String?
    var question:String?
    var chapterName:String?
    var subjectName:String?
    var questionImages = [String]()
    var answerImages = [String]()
     
    init(data:Dict){
        assignmentId = String.getString(data["assignmentId"])
        question = String.getString(data["question"])
        chapterName = String.getString(data["chapterName"])
        subjectName = String.getString(data["subject"])
        let queImages = kSharedInstance.getArray(withDictionary: data["questionImages"])
        let ansImages = kSharedInstance.getArray(withDictionary: data["answerImages"])
        questionImages = queImages.map{String.getString($0["_image"]).contains("http") ? String.getString($0["_image"]) : "https://" + String.getString($0["_image"])}
        answerImages = ansImages.map{String.getString($0["_image"]).contains("http") ? String.getString($0["_image"]) : "https://" + String.getString($0["_image"])}
    }
}
