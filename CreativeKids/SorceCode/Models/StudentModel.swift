//
//  StudentModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 18/10/22.
//

import Foundation

struct HomeWorkDate{
    var date:String
    var status:String
    init(data:Dict){
        date = String.getString(data["date"])
        status = String.getString(data["status"])
    }
}

class HomeWork{
    var periodNo:String?
    var subjectName:String?
    var subjectId: String?
    var date:String?
    var teacherName:String?
    var teacherId:String?
    var topicName:String?
    var question:String?
    var isHomeWorkChecked:Bool = false
    var assignmentId:String?
    var isSubmitedByStudent:Bool = false
    var studentHomeworkDetails: StudentHomeworkDetails?
    
    init(data:Dict){
        periodNo = String.getString(data["periodNo"])
        subjectName = String.getString(data["subject"])
        subjectId = String.getString(data["subjectId"])
        date = String.getString(data["date"])
        teacherName = String.getString(data["teacherName"])
        teacherId = String.getString(data["teacherId"])
        topicName = String.getString(data["topicCovered"])
        question = String.getString(data["question"])
        isHomeWorkChecked = String.getString(data["isHomeworkChecked"]) == "1"
        assignmentId = String.getString(data["assignmentId"])
        isSubmitedByStudent = String.getString(data["isSubmittedByStudent"]) == "1"
        studentHomeworkDetails = StudentHomeworkDetails.init(data: data["studentSubmittedworkDetail"] as? [String:Any] ?? [:])
    }
}
class StudentHomeworkDetails {
    var assignmentId: String?
    var isHomeworkChecked:Bool? = false
    var isSubmittedByStudent:Bool? = false
    var remarks: String?
    var workSubmittedId: String?
    
    init(data:Dict){
        assignmentId = String.getString(data["assignmentId"])
        isHomeworkChecked = String.getString(data["isHomeworkChecked"]) == "1"
        remarks = String.getString(data["remarks"])
        isSubmittedByStudent = String.getString(data["isSubmittedByStudent"]) == "1"
        workSubmittedId = String.getString(data["workSubmittedId"])
    }
}
