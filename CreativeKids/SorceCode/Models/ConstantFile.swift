//
//  ConstantFile.swift
//  CreativeKids
//
//  Created by Creative Kids on 04/06/21.
//

import Foundation
import UIKit
import KDCircularProgress
import AVKit

var getResponce = "returnData"
var hindiSubjectArray = ["Hindi","Sanskrit","Hindi Vyakaran","Hindi Grammar-NCERT","Hindi Vyakaran NCERT", "Hindi NCERT","Hindi Vyakaran-NCERT", "HindiNCERT-10"]
var hindiEnglishSubjectArray = ["Social Science NCERT","Science NCERT"]
var imageType = [".jpg", ".png"]
var launchVideoType = [".mp4", ".gif"]
var chanakyaFont = "WalkmanChanakya901Normal"
var futuraFont = "Futura Medium"

//MARK: Reload Previos Data 
protocol ReloadDataDataSources{
    func reload()
}

//MARK:- ClassName
class ClassName{
    static let demo    = "Demo"
    static let nursery  = "Nursery (A)"
    static let lkg     = "L.K.G (B)"
    static let ukg     = "U.K.G (C)"
    static let class1  = "Class-1"
    static let class2  = "Class-2"
    static let class3  = "Class-3"
    static let class4  = "Class-4"
    static let class5  = "Class-5"
    static let class6  = "Class-6"
    static let class7  = "Class-7"
    static let class8  = "Class-8"
    static let class9  = "Class-9"
    static let class10 = "Class-10"
}
//MARK:- Class Type
class ClassType{
    static let cordova = "Cordova"
    static let ncert = "NCERT"
}

//MARK:- TABLEVIEWCELL IDENTIFIER
class TableCellIdentifier{
    static let dnd                 = "DNDTableViewCell"
    static let sideMenu              = "SideMenuTableViewCell"
    static let chapter               = "ChapterTableViewCell"
    static let video                = "VideoTableViewCell"
    static let review               = "ReviewTableViewCell"
    static let topicSearch            = "TopicSearchTableViewCell"
    static let continueLearning         = "ContinueLearningTableVeiwCell"
    static let chapterProgress          = "ChapterProgressTableCell"
    static let subscribe             = "SubscribeTableViewCell"
    static let chapterExercisecell       = "ChapterExerciseTableViewCell"
    static let chapterLTPCell      = "ChapterLTPTableViewCell"
    static let chapterQuestionBank = "ChapterQuestionBankTableViewCell"
    static let mockTestReviewCell  = "MockTestReviewTableViewCell"
    static let playBackSpeedCell   = "PlaybackSpeedTableViewCell"
    static let teacherTodayTaskCell = "TableViewCellTodayClasses"
    static let studentFeesCell = "StudentFeesTableViewCell"
    static let studentHomework = "HomeWorkListTableCell"
    static let studentAttendance = "StudentAttendanceTableCell"
    static let attendance = "AttendanceTableViewCell"
    static let leaveList = "LeaveListTableViewCell"
    static let studentHomeworkList = "StudentHomeworkListTableCell"
    static let viewHomework = "ViewHomeworkTableViewCell"
    static let noticeCell = "NoticeTableViewCell"
    static let dateHomework = "DateHomeworkTableViewCell"
    static let periodList = "PeriodListTableViewCell"
    static let homeWorkAccPeriod = "HomeWorkAccDateTableViewCell"
    static let teacherleaveList = "TeacherLeavesTableViewCell"
    static let homeWorkQuestionChap = "QuestionChapterTableViewCell"
    static let homeWorkQuestionImages = "QuestionImagesTableViewCell"
    static let homeWorkAnswer          = "AnswerImagesTableViewCell"
    
}

//MARK:- COLLECTIONVIEWCELL IDENTIFIER
class CollectionCellIdentifier{
    
    static let subject                 = "SubjectCollectionViewCell"
    static let popularVideo            = "PopularVideosCollectionViewCell"
    static let overAllRating           = "OverAllRatingCollectionViewcell"
    static let subscribeSubject        = "SubScribeSubjectCollectionViewCell"
    static let quiz                    = "QuizCollectionViewCell"
    static let ltp                     = "LtpCollectionViewCell"
    static let exercise                = "ExerciseCollectionViewCell"
    static let excelExam               = "ExcelExamCollectionViewCell"
    static let mcq                     = "McqCollectionCell"
    static let shortAnswer             = "ShortAnswerCell"
    static let progressReport          = "ProgressReportCollectionViewCell"
    static let subjectPerformance      = "SubjectPerformanceCollectionViewCell"
    static let allSubjectPerformance   = "AllSubjectPerformanceCollectionViewCell"
    static let ltpExercise             = "LTPCollectionCell"
    static let chapterExercise         = "ChapterExerciseCollectionViewCell"
    static let questionBank            = "QuestionBankCollectionCell"
    static let questionBankMath        = "MathsMockTestCollectionViewCell"
    static let homeworkDetails         = "HomeworkImageCollectionCell"
    static let periods                 = "PeriodsCollectionViewCell"
    static let StudentAttendCell       = "StudentAttendanceCollectionViewCell"
    static let typesCollection       = "TypesCollectionViewCell"
    static let selectedCell         = "SelectedCell"
    static let studentWorkList       = "StudentWorkCollectionViewCell"
    static let eventCell           = "StudentDashboardCollectionCell"
    static let questionImageCell     = "HomeWorkCollectionViewCell"
}

enum SegmentType:Int{
    case video = 0
    case exercise
    case LTP
    case questionBank
}

//MARK:- NSLayoutConstraint for change MultiPlier Constraints
extension NSLayoutConstraint {
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

//MARK:- CHECK FOR HINDI LANGUAGE
func isHindiSubject(subejct:String)->Bool{
    return subejct.contains("Hindi") || subejct.contains("Sanskrit") || subejct.contains("Asha")
    //    return hindiSubjectArray.contains(subejct)
}

//MARK: Check Video & Image in Launch Screen
//func isImageAndVideo(launchType: String) -> Bool {
//
//}

//MARK:- CHECK FOR CONTENT AVAILABLE IN BOTH LANGUAGE
func isContantAvailableInBothLang(subejct:String)->Bool{
    return hindiEnglishSubjectArray.contains(subejct)
}

class ConstantHindiText{
    static let abhyaas = "vH;kl"
    static let bhaag = "Hkkx"
    static let paath = "ikB"
}

//MARK:- CHECK FOR STRING CONTAINING NUMBER
func isContainNumber(checkString:String)->Bool{
    let decimalCharacters = CharacterSet.decimalDigits
    let decimalRange = checkString.rangeOfCharacter(from: decimalCharacters)
    return decimalRange != nil ? true : false
}

//MARK:- REMOVE NUMBER FROM STRING
func removeNumberFromString(stringWithNumber:String)->String{
    let removedNumberString = stringWithNumber.components(separatedBy: CharacterSet.decimalDigits).joined()
    return removedNumberString
}

//MARK:- GET NUMBER FROM STRING
func getNumericFromString(stringWithNumber:String)->Int?{
    let newString = stringWithNumber
        .components(separatedBy:CharacterSet.decimalDigits.inverted)
        .joined()
    return Int(newString)
}

//MARK:- GET ATTRIBUTED STRING SUFFIX WITH NUMBER CONTAINING NUMBER IN STRING
func getNSAtributedStringWithNumber(forConvertString:String)->NSAttributedString{
    let myChapterNumber = "\(getNumericFromString(stringWithNumber: forConvertString) ?? 0)"
    let myNumberAttribute = [ NSAttributedString.Key.font: UIFont(name: futuraFont, size: 11.0)!]
    let myAttrNumberString = NSAttributedString(string: myChapterNumber, attributes: myNumberAttribute)
    let removedNumber = removeNumberFromString(stringWithNumber: forConvertString)
    let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!]
    let myAttrQuestionString = NSAttributedString(string: removedNumber, attributes: myAttribute)
    return myAttrQuestionString + myAttrNumberString
}

//MARK:- GET ATTRIBUTED STRING PREFIX WITH NUMBER CONTAINING NUMBER IN STRING
func getNSAtributedNumberWithString(forConvertString:String)->NSAttributedString{
    let myChapterNumber = "\(getNumericFromString(stringWithNumber: forConvertString) ?? 0)" == "0" ? "" : "\(getNumericFromString(stringWithNumber: forConvertString) ?? 0)"
    let myNumberAttribute = [NSAttributedString.Key.font: UIFont(name: futuraFont, size: 11.0)!]
    let myAttrNumberString = NSAttributedString(string: myChapterNumber, attributes: myNumberAttribute)
    let removedNumber = removeNumberFromString(stringWithNumber: forConvertString)
    let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!]
    let myAttrQuestionString = NSAttributedString(string: removedNumber, attributes: myAttribute)
    return myAttrNumberString + myAttrQuestionString
}

//MARK:- GET ATTRIBUTED STRING PREFIX WITH NUMBER CONTAINING NUMBER IN STRING AT FIRST INDEX
func makeNumericString(StringWithNumber:String)->NSAttributedString{
    var numberInString = StringWithNumber
    let number = numberInString.remove(at: numberInString.startIndex)
    let myNumberAttribute = [NSAttributedString.Key.font: UIFont(name: futuraFont, size: 11.0)!]
    let myAttrNumberString = NSAttributedString(string: String(number), attributes: myNumberAttribute)
    let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!]
    let myAttrQuestionString = NSAttributedString(string: numberInString, attributes: myAttribute)
    return myAttrNumberString + myAttrQuestionString
}

//MARK:- CHECK STRING CONTAINING NUMBER AT FIRST INDEX
func checkStringFirstIndexContainingNumber(checkString:String)->NSAttributedString{
    let firstCharacter = checkString.first
    if Int.getInt(String(firstCharacter ?? "0")) == 0{
        return NSAttributedString(string: checkString)
    }else{
        return makeNumericString(StringWithNumber: checkString)
    }
}

func checkStringContainedNumber(checkString:String)->Bool{
    let firstCharacter = String(checkString.first ?? "z")
    if Int.getIntValue((firstCharacter)) == 101{
        return false
    }else{
        return true
    }
}
func buttomBorder(label: UILabel, color: UIColor = .lightGray) -> UILabel {
    // For Buttom Border
    let frame = label.frame
    let bottomLayer = CALayer()
    bottomLayer.frame = CGRect(x: 0, y: frame.height - 1, width: frame.width, height: 1)
    bottomLayer.backgroundColor = color.cgColor
    label.layer.addSublayer(bottomLayer)
    
    return label
    
}

//*//*//*////*//*//*////*//*//*////*//*//*////*//*//*////*//*//*////*//*//*////*//*//*//
func removePreFixNumberFromString(rawString:String)->(startNumber:String,remainString:String){
    var numbers = ""
    var checkString = rawString
    while checkStringContainedNumber(checkString:checkString) {
        numbers += String(checkString.remove(at: checkString.startIndex))
    }
    return(numbers,checkString)
}

func makeNewNumericString(StringWithNumber:String)->NSAttributedString{
    let checkString = removePreFixNumberFromString(rawString: StringWithNumber)
    let myNumberAttribute = [NSAttributedString.Key.font: UIFont(name: futuraFont, size: 11.0)!]
    let myAttrNumberString = NSAttributedString(string: checkString.startNumber, attributes: myNumberAttribute)
    let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!]
    let myAttrQuestionString = NSAttributedString(string: checkString.remainString, attributes: myAttribute)
    return myAttrNumberString + myAttrQuestionString
}

//Mark: Check 3 and 7
func makeNewNumericStringFor3And7(StringWithNumber:String)->NSAttributedString{
    if checkStringContain3and7(stringWithNumber:StringWithNumber){
        let checkString = makeAttributedStringWith3and7(rawString:StringWithNumber.stringByTrimmingWhiteSpaceAndNewLine())
        
        let myBeginAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        let myBeginString = checkString.0.html2Attributed!
        let mybeginAttributedString = NSMutableAttributedString(attributedString: myBeginString)
        mybeginAttributedString.addAttributes(myBeginAttribute, range: NSRange(location: 0, length: mybeginAttributedString.length))
        // let newLineAttribute = NSAttributedString(string: "\n")
        
        let myNumberAttribute = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
        let myAttrNumberString = NSAttributedString(string: checkString.1, attributes: myNumberAttribute)
        
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        //        let myAttrQuestionString = NSAttributedString(string: checkString.2.replacingOccurrences(of: "\n", with: ""), attributes: myAttribute)
        let myAttrQuestionString = checkString.2.html2Attributed!
        let myendingAttributedString = NSMutableAttributedString(attributedString: myAttrQuestionString)
        myendingAttributedString.addAttributes(myAttribute, range: NSRange(location: 0, length: myendingAttributedString.length))
        
        return mybeginAttributedString + myAttrNumberString + myendingAttributedString
        
    }
    else if checkStringContain7and3(stringWithNumber:StringWithNumber){
        let checkString = makeAttributedStringWith7and3(rawString:StringWithNumber.stringByTrimmingWhiteSpaceAndNewLine())
        
        let myBeginAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        let myBeginString = checkString.0.html2Attributed!
        let mybeginAttributedString = NSMutableAttributedString(attributedString: myBeginString)
        mybeginAttributedString.addAttributes(myBeginAttribute, range: NSRange(location: 0, length: mybeginAttributedString.length))
        //let newLineAttribute = NSAttributedString(string: "\n")
        
        let myNumberAttribute = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
        let myAttrNumberString = NSAttributedString(string: checkString.1, attributes: myNumberAttribute)
        
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        //        let myAttrQuestionString = NSAttributedString(string: checkString.2.replacingOccurrences(of: "\n", with: ""), attributes: myAttribute)
        let myAttrQuestionString = checkString.2.html2Attributed!
        let myendingAttributedString = NSMutableAttributedString(attributedString: myAttrQuestionString)
        myendingAttributedString.addAttributes(myAttribute, range: NSRange(location: 0, length: myendingAttributedString.length))
        
        return mybeginAttributedString + myAttrNumberString + myendingAttributedString
    }
    else if checkStringContain3or7(stringWithNumber:StringWithNumber){
        let checkString = makeAttributedStringWith3or7(rawString:StringWithNumber.stringByTrimmingWhiteSpaceAndNewLine())
        
        let myBeginAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        let myBeginString = checkString.0.html2Attributed!
        let mybeginAttributedString = NSMutableAttributedString(attributedString: myBeginString)
        mybeginAttributedString.addAttributes(myBeginAttribute, range: NSRange(location: 0, length: mybeginAttributedString.length))
        //let newLineAttribute = NSAttributedString(string: "\n")
        
        let myNumberAttribute = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
        let myAttrNumberString = NSAttributedString(string: checkString.1, attributes: myNumberAttribute)
        
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        //        let myAttrQuestionString = NSAttributedString(string: checkString.2.replacingOccurrences(of: "\n", with: ""), attributes: myAttribute)
        let myAttrQuestionString = checkString.2.html2Attributed!
        let myendingAttributedString = NSMutableAttributedString(attributedString: myAttrQuestionString)
        myendingAttributedString.addAttributes(myAttribute, range: NSRange(location: 0, length: myendingAttributedString.length))
        
        return mybeginAttributedString + myAttrNumberString + myendingAttributedString
    }else{
        //        let myAttribute = [NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!]
        //        return NSAttributedString(string: StringWithNumber, attributes: myAttribute)
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        let myBeginString = StringWithNumber.html2Attributed!
        let attributedString = NSMutableAttributedString(attributedString: myBeginString)
        attributedString.addAttributes(myAttribute, range: NSRange(location: 0, length: attributedString.length))
        return attributedString
        
    }
}


//*//*//*////*//*//*////*//*//*////*//*//*////*//*//*////*//*//*////*//*//*////*//*//*//
//MARK:- Make Atttributed String when 3 or 7 contain
func checkStringContain3or7(stringWithNumber:String)->Bool{
    if stringWithNumber.contains("3") ||  stringWithNumber.contains("7"){
        return true
    }else{
        return false
    }
}
//MARK:- Make Atttributed String when 3 and 7 contain
func checkStringContain3and7(stringWithNumber:String)->Bool{
    if stringWithNumber.contains("33") ||  stringWithNumber.contains("37"){
        return true
    }else{
        return false
    }
}
//MARK:- Make Atttributed String when 7 and 3 contain
func checkStringContain7and3(stringWithNumber:String)->Bool{
    if stringWithNumber.contains("73") ||  stringWithNumber.contains("77"){
        return true
    }else{
        return false
    }
}

func makeAttributedStringWith3or7(rawString:String)->(String,String,String){
    let checkFor3: Character = "3"
    let checkFor7: Character = "7"
    if let indexFor3 = rawString.firstIndex(of: checkFor3){
        if indexFor3 == rawString.startIndex {
            let begningString = rawString[..<indexFor3]
            let numberString = rawString[indexFor3]
            let removeCharacterIndex = rawString.index(indexFor3, offsetBy: 1)
            let rawEndString =  rawString[removeCharacterIndex..<rawString.endIndex]
            return (String(begningString),String(numberString),String(rawEndString))
        }else{
            let checkPreviousCharDigit = rawString.index(indexFor3, offsetBy: -1)
            if isContainNumber(checkString: String(rawString[checkPreviousCharDigit])){
                let begningString = rawString[..<checkPreviousCharDigit]
                let numberString = rawString[checkPreviousCharDigit...indexFor3]
                let removeCharacterIndex = rawString.index(indexFor3, offsetBy: 1)
                let rawEndString =  rawString[removeCharacterIndex..<rawString.endIndex]
                return (String(begningString),String(numberString),String(rawEndString))
            }else{
                let begningString = rawString[..<indexFor3]
                let numberString = rawString[indexFor3]
                let removeCharacterIndex = rawString.index(indexFor3, offsetBy: 1)
                let rawEndString =  rawString[removeCharacterIndex..<rawString.endIndex]
                return (String(begningString),String(numberString),String(rawEndString))
            }
        }
        
    }else{
        if let indexFor7 = rawString.firstIndex(of: checkFor7){
            if indexFor7 == rawString.startIndex {
                let begningString = rawString[..<indexFor7]
                let numberString = rawString[indexFor7]
                let removeCharacterIndex = rawString.index(indexFor7, offsetBy: 1)
                let rawEndString =  rawString[removeCharacterIndex..<rawString.endIndex]
                return (String(begningString),String(numberString),String(rawEndString))
            }else{
                let checkPreviousCharDigit = rawString.index(indexFor7, offsetBy: -1)
                if isContainNumber(checkString: String(rawString[checkPreviousCharDigit])){
                    let begningString = rawString[..<checkPreviousCharDigit]
                    let numberString = rawString[checkPreviousCharDigit...indexFor7]
                    let removeCharacterIndex = rawString.index(indexFor7, offsetBy: 1)
                    let rawEndString =  rawString[removeCharacterIndex..<rawString.endIndex]
                    return (String(begningString),String(numberString),String(rawEndString))
                }else{
                    let begningString = rawString[..<indexFor7]
                    let numberString = rawString[indexFor7]
                    let removeCharacterIndex = rawString.index(indexFor7, offsetBy: 1)
                    let rawEndString =  rawString[removeCharacterIndex..<rawString.endIndex]
                    return (String(begningString),String(numberString),String(rawEndString))
                }
            }
        }
    }
    return ("","","")
}

func makeAttributedStringWith3and7(rawString:String)->(String,String,String){
    let checkFor3: Character = "3"
    if let indexFor3 = rawString.firstIndex(of: checkFor3){
        let begningString = rawString[..<indexFor3]
        let removeCharacterIndex = rawString.index(indexFor3, offsetBy: 2)
        let numberString = rawString[indexFor3..<removeCharacterIndex]
        let rawEndString =  rawString[removeCharacterIndex..<rawString.endIndex]
        return (String(begningString),String(numberString),String(rawEndString))
    }else{
        
    }
    return ("","","")
}

func makeAttributedStringWith7and3(rawString:String)->(String,String,String){
    let checkFor7: Character = "7"
    if let indexFor7 = rawString.firstIndex(of: checkFor7){
        let begningString = rawString[..<indexFor7]
        let removeCharacterIndex = rawString.index(indexFor7, offsetBy: 2)
        let numberString = rawString[indexFor7..<removeCharacterIndex]
        let rawEndString =  rawString[removeCharacterIndex..<rawString.endIndex]
        return (String(begningString),String(numberString),String(rawEndString))
    }
    return ("","","")
}


//MARK:- CONCAT TWO ATTRIBUTED STRING
func + (left: NSAttributedString, right: NSAttributedString) -> NSAttributedString
{
    let result = NSMutableAttributedString()
    result.append(left)
    result.append(right)
    return result
}

//MARK:- OPEN WHATSAPP
func openWhatsApp(number: String) {
    let phoneNumber =  number // you need to change this number
    let appURL = URL(string: "whatsapp://send?phone=\(phoneNumber)")!
    if UIApplication.shared.canOpenURL(appURL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
        }
        else {
            UIApplication.shared.openURL(appURL)
        }
    } else {
        // WhatsApp is not installed
        UIApplication.shared.open(URL(string: "https://apps.apple.com/in/app/whatsapp-messenger/id310633997")!)
    }
}

//MARK:- OPEN EMAIL
func openEmail(email: String) {
    let email = email
    
    if let url = URL(string: "mailto:\(email)"),
       UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    } else {
        showAlertMessage.alert(message: "You don't have Mail")
    }
}


func openDilerPad(number: String) {
    if let url = URL(string: "tel://\(number)"),
       UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    } else {
        showAlertMessage.alert(message: "You don't have Phone")
    }
}

//MARK: Create Attributred Text
func createAttributedText(firstText:String, secondText:String) -> NSAttributedString{
    let firstTextAttributed = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)]
    let secondTextAttributed = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
    let firstAttributedString = NSAttributedString(string: firstText, attributes: firstTextAttributed)
    let secondAttributedString = NSAttributedString(string: secondText, attributes: secondTextAttributed)
    return firstAttributedString + secondAttributedString
}

//MARK:- SET GRADIENT COLOR
extension UIView {
    func setgradient(color1: UIColor, color2: UIColor) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [color1.cgColor, color2.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.3)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = self.bounds
        self.layer.insertSublayer(gradient, at: 0)
    }
}

//MARK:- CHECK DEVICE TYPE
public extension UIDevice {
    class var isPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    class var isPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
}

//MARK:- CHECK LOGGED USER

public func isSchoolUser() ->Bool{
    if String.getstring(kUserData.role) == DashboardType.Student.userType{
        return true
    }
    return false
}


class Common: NSObject{
    public class func showActionSheet(vc: UIViewController,sender:UIButton? = nil, firstTitle: String, secondTitle: String, sourceView: UIView, completions: @escaping (Int) -> ()) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: firstTitle, style: .default, handler: { (alert:UIAlertAction!) -> Void in
            completions(0)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: secondTitle, style: .default, handler: { (alert:UIAlertAction!) -> Void in
            completions(1)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        //if iPhone
        if UIDevice.isPhone {
            vc.present(actionSheet, animated: true, completion: nil)
        }
        else {
            //In iPad Change Rect to position Popover
            if let btn = sender{
                actionSheet.popoverPresentationController?.sourceRect = btn.frame
                actionSheet.popoverPresentationController?.sourceView = sourceView
            } else {
                actionSheet.popoverPresentationController?.sourceRect = sourceView.frame
                actionSheet.popoverPresentationController?.sourceView = sourceView
            }
            vc.present(actionSheet, animated: true, completion: nil)
        }
        
    }
}

extension UILabel {
    //MARK:- SET LABEL FONT ACCORDING TO DEVICE TYPE
    func setfont(font: CGFloat, fontFamily:String ) {
        if UIDevice.isPhone {
            self.font = UIFont(name: fontFamily, size:  font)
        } else {
            self.font = UIFont(name: fontFamily, size:  font + 3)
        }
    }
    
    //MARK:- SET LABEL TEXT AS HTML TEXT
    func getfontSize(font: CGFloat)->CGFloat{
        if UIDevice.isPhone {
            return font
        } else {
            return font + CGFloat(3)
        }
    }
    
    //MARK:- SET LABEL TEXT AS HTML TEXT
    func setAttributedText(text:String,colorText:UIColor, font: CGFloat, fontFamily: String = futuraFont){
        if text.contains("<i>"){
            self.attributedText = subItalicString(rawString:text, fontSize:getfontSize(font: font))
        }else{
            self.attributedText = text.html2Attributed
            self.setfont(font: font, fontFamily: fontFamily)
            self.textColor = colorText
        }
    }
}

//MARK:- Get PREVIEW IMAGE ON SLIDE
extension AVAsset {
    func getPreviewImage(for timeInSeconds: Int = 0) -> UIImage? {
        let imageGenerator = AVAssetImageGenerator(asset: self)
        imageGenerator.requestedTimeToleranceBefore = .zero
        imageGenerator.requestedTimeToleranceAfter = .zero
        imageGenerator.appliesPreferredTrackTransform = true
        guard let cgImage = try? imageGenerator.copyCGImage(at: CMTime(value: CMTimeValue(timeInSeconds), timescale: 1), actualTime: nil) else { return nil }
        return UIImage(cgImage: cgImage)
    }
}


//MARK:- Unique Ids For Subjects

enum SubjectId:String{
    
    //Class Nursary
    case nursaryRymes                      = "337"
    case nursaryMath                       = "344"
    case nursaryStory                      = "340"
    case nursaryEnglish                    = "343"
    
    
    //Class LKG"
    case lkgRymes                          = "338"
    case lkgMath                           = "346"
    case lkgStory                          = "341"
    case lkgEnglish                        = "345"
    
    
    //Class UKG"
    case ukgRymes                          = "339"
    case ukgMath                           = "348"
    case ukgStory                          = "342"
    case ukgEnglish                        = "347"
    
    
    //Class 1st"
    case mathClass1                        = "17"
    case evsClass1                         = "45"
    case sunshine1Class1                   = "94"
    case sunshine2Class1                   = "99"
    case englishGrammerClass1              = "194"
    case englishClass1                     = "195"
    case hindiGrammerClass1                = "212"
    case hindiClass1                       = "261"
    case computerClass1                    = "228"
    case scienceClass1                     = "241"
    case sstClass1                         = "249"
    
    
    //Class 2nd"
    case mathClass2                        = "18"
    case evsClass2                         = "46"
    case sunshine1Class2                   = "95"
    case sunshine2Class2                   = "100"
    case englishGrammerClass2              = "205"
    case englishClass2                     = "196"
    case hindiGrammerClass2                = "213"
    case hindiClass2                       = "262"
    case computerClass2                    = "229"
    case scienceClass2                     = "242"
    case sstClass2                         = "250"
    
    
    //Class 3rd"
    case mathClass3                        = "19"
    case evsClass3                         = "47"
    case sunshine1Class3                   = "96"
    case sunshine2Class3                   = "101"
    case englishGrammerClass3              = "206"
    case englishClass3                     = "197"
    case hindiGrammerClass3                = "214"
    case hindiClass3                       = "263"
    case computerClass3                    = "230"
    case scienceClass3                     = "243"
    case sstClass3                         = "251"
    
    
    //Class 4th"
    case mathClass4                        = "20"
    case evsClass4                         = "48"
    case sunshine1Class4                   = "97"
    case sunshine2Class4                   = "102"
    case englishGrammerClass4              = "207"
    case englishClass4                     = "198"
    case hindiGrammerClass4                = "215"
    case hindiClass4                       = "264"
    case computerClass4                    = "231"
    case scienceClass4                     = "244"
    case sstClass4                         = "252"
    
    
    //Class 5th"
    case mathClass5                        = "21"
    case evsClass5                         = "49"
    case sunshine1Class5                   = "98"
    case sunshine2Class5                   = "103"
    case englishGrammerClass5              = "208"
    case englishClass5                     = "199"
    case hindiGrammerClass5                = "216"
    case hindiClass5                       = "265"
    case computerClass5                    = "232"
    case scienceClass5                     = "245"
    case sstClass5                         = "253"
    case sanskritClass5                    = "257"
    
    
    // Cordova Class 6th"
    case mathClass6                        = "22"
    case englishGrammerClass6              = "209"
    case englishClass6                     = "200"
    case hindiGrammerClass6                = "217"
    case hindiClass6                       = "999"
    case computerClass6                    = "233"
    case sstClass6                         = "377"
    case sanskritClass6                    = "258"
    case scienceClass6                     = "380"
    
    
    // Ncert Class 6th"
    case scienceClass6Ncert                = "246"
    case sstClass6Ncert                    = "254"
    case mathClass6Ncert                   = "357"
    case hindiGrammerClass6Ncert           = "367"
    case englishGrammerClass6Ncert         = "372"
    case hindiClass6Ncert                  = "23242"         // Dummy Value
    case englishClass6Ncert                = "23234"         // Dummy Value
    
    
    // Cordova Class 7th"
    case mathClass7                        = "23"
    case englishGrammerClass7              = "210"
    case englishClass7                     = "201"
    case hindiGrammerClass7                = "218"
    case hindiClass7                       = "267"
    case computerClass7                    = "234"
    case sstClass7                         = "378"
    case sanskritClass7                    = "259"
    case scienceClass7                     = "381"
    
    
    // Ncert Class 7th"
    case scienceClass7Ncert                = "247"
    case sstClass7Ncert                    = "255"
    case mathClass7Ncert                   = "358"
    case hindiGrammerClass7Ncert           = "368"
    case englishGrammerClass7Ncert         = "373"
    case hindiClass7Ncert                  = "356546"          // Dummy Value
    case englishClass7Ncert                = "366354"          // Dummy Value
    
    
    // Cordova Class 8th"
    case mathClass8                        = "24"
    case englishGrammerClass8              = "211"
    case englishClass8                     = "202"
    case hindiGrammerClass8                = "219"
    case hindiClass8                       = "268"
    case computerClass8                    = "235"
    case sstClass8                         = "379"
    case sanskritClass8                    = "260"
    case scienceClass8                     = "382"
    
    
    // Ncert Class 8th"
    case scienceClass8Ncert                = "248"
    case sstClass8Ncert                    = "256"
    case mathClass8Ncert                   = "359"
    case hindiGrammerClass8Ncert           = "369"
    case englishGrammerClass8Ncert         = "374"
    case hindiClass8Ncert                  = "356789"          // Dummy Value
    case englishClass8Ncert                = "366987"          // Dummy Value
    
    
    // Ncert Class 9th"
    case scienceClass9Ncert                = "33"
    case sstClass9Ncert                    = "335"
    case mathClass9Ncert                   = "1000"            // Dummy Value
    case hindiGrammerClass9Ncert           = "370"
    case englishGrammerClass9Ncert         = "10001"           // Dummy Value
    case hindiClass9Ncert                  = "355"
    case englishClass9Ncert                = "365"
    
    
    // Ncert Class 10th"
    case scienceClass10Ncert               = "34"
    case sstClass10Ncert                   = "336"
    case mathClass10Ncert                  = "361"
    case hindiGrammerClass10Ncert          = "371"
    case englishGrammerClass10Ncert        = "10002"          // Dummy Value
    case hindiClass10Ncert                 = "356"
    case englishClass10Ncert               = "366"
    
}
func imageTypeValue(question: String) -> Bool {
    let check = imageType.filter{question.contains($0)}
    return check.count > 0
}
func savePdf(urlString:String, fileName:String, completion: @escaping (()->Void)) {
    let url = URL(string: urlString)
    let data = try? Data.init(contentsOf: url!)
    if let pdfData = data{
        let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        let pdfNameFromUrl = "CreativeKids-\(fileName).pdf"
        let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
        do {
            try pdfData.write(to: actualPath, options: .atomic)
            CommonUtils.showHud(show: false)
            completion()
            showAlertMessage.alert(message: "Pdf saved successfully!\nfile manager -> CreativeKids")
            //file is downloaded in app data container, I can find file from x code > devices > MyApp > download Container >This container has the file
        } catch {
            print("Pdf could not be saved")
            completion()
        }
    }else{
        showAlertMessage.alert(message: "Pdf saved failed!")
        completion()
    }
    
}
func subItalicString(rawString:String, fontSize:CGFloat)->NSMutableAttributedString{
    let strstr = rawString.components(separatedBy: "<i>")
    let str1 = strstr.map{$0.components(separatedBy: "</i>")}
    let str2 = str1.flatMap{$0}
    let myAttributedString = NSMutableAttributedString()
    for (inde,textValue) in str2.enumerated(){
        let mybeginAttributedString = NSMutableAttributedString(attributedString: " \(textValue) ".html2Attributed!)
        if (inde % 2) == 0{
            let myfaturaAttribute = [ NSAttributedString.Key.font: UIFont(name: "Futura Medium", size: fontSize)!, NSAttributedString.Key.foregroundColor:UIColor.white]
            mybeginAttributedString.addAttributes(myfaturaAttribute, range: NSRange(location: 0, length: mybeginAttributedString.length))
        }else{
            let myItalicAttribute = [ NSAttributedString.Key.font: UIFont(name: "FuturaBT-MediumItalic", size: fontSize)!, NSAttributedString.Key.foregroundColor:UIColor.white]
            mybeginAttributedString.addAttributes(myItalicAttribute, range: NSRange(location: 0, length: mybeginAttributedString.length))
        }
        myAttributedString.append(mybeginAttributedString)
    }
    return myAttributedString
}
extension UIImageView {
    public func downloadImageFromURL(urlString: String,completion: @escaping ((UIImage?)->Void) = {_ in }) {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        activityIndicator.startAnimating()
        if self.image == nil{
            self.addSubview(activityIndicator)
        }
        
        //MARK:- Check image Store in Cache or not
        if let cachedImage = iimageCache.object(forKey: urlString.replacingOccurrences(of: " ", with: "%20") as NSString) {
            if  let image = cachedImage as? UIImage {
                activityIndicator.removeFromSuperview()
                self.image = image
                completion(image)
                print("Find image on Cache : For Key" , urlString.replacingOccurrences(of: " ", with: "%20"))
                return
            }
        }
        
        print("Conecting to Host with Url:-> \(urlString)")
        if NSURL(string: urlString) != nil{
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "No Error")
                completion(nil)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                if let image = UIImage(data: data!){
                    activityIndicator.removeFromSuperview()
                    self.image = image
                    iimageCache.setObject(image, forKey: urlString.replacingOccurrences(of: " ", with: "%20") as NSString)
                    completion(image)
                }
            })
            
        }).resume()
        }
    }
}
extension UIView {
    func addShadowStackView() {
        let radius: CGFloat = self.frame.width / 2.0 //change it to .height if you need spread for height
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 2.5 * radius, height: self.frame.height))
        //Change 2.1 to amount of spread you need and for height replace the code for height
        
        self.layer.cornerRadius = 5
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)  //Here you control x and y
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 3.0 //Here your control your blur
        self.layer.masksToBounds =  false
        self.layer.shadowPath = shadowPath.cgPath
    }
}

func getPicture(controller:UIViewController,sourceView:UIView, completion: @escaping ((UIImage?)->Void)){
    if UIDevice.isPhone{
        ImagePickerHelper.shared.showPickerController { (image) -> (Void) in
            let returnedImage = image
            completion(returnedImage)
        }
    }else{
        Common.showActionSheet(vc: controller, firstTitle: "Camera", secondTitle: "Photo Library", sourceView: sourceView) { selectedIndex in
            if selectedIndex == 0 {
                ImagePickerHelper.shared.picForiPad(forCamera: true) { (image) -> (Void) in
                    DispatchQueue.main.async {
                        let returnedImage = image
                        completion(returnedImage)
                    }
                }
            } else {
                ImagePickerHelper.shared.picForiPad(forCamera: false) { (image) -> (Void) in
                    let returnedImage = image
                    completion(returnedImage)
                }
            }
        }
    }
}

//MARK: GET TIME IN SECOND FORM STRING

func checkTimeForAttendance(data:String,format:String) -> Bool{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = format
    let date = dateformatter.date(from: data)!
    let currentDateInString = String.getCurrentDate(format: format)
    print(data)
    print(date.hour)
    print(date.minute)
    print(currentDateInString)
    let nowDate = dateformatter.date(from: currentDateInString)!
    print("DifferenceInSeconds-----------:\(nowDate - date)")
    let components = Calendar.current.dateComponents([.second,.hour,.minute], from: nowDate, to: date)
    print(components.hour)
    print(components.minute)
    if components.hour < 0{
        return true
    }else if components.hour == 0{
        return components.minute! <= 0
    }else{
      return false
    }
}

// MARK: GetDateTimeFromTimeStamp

func getDateTimeFromTimeStamp(timeStamp:Int?)->String{
    let date1 =  timeStamp?.dateFromTimeStamp()
    if date1?.isToday() ?? false {
        return "Today"
    }else if date1?.isYesterday() ?? false {
        return "Yesterday"
    }else {
        let dateStr1  = date1?.toString(withFormat: "dd MMM YYYY")
        return dateStr1 ?? ""
    }
}

