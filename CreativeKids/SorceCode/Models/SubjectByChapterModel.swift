//
//  ChapterBySubjectModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 29/03/22.
//

import Foundation
import UIKit

protocol SelectionDelegate{
    var name:String?{get set}
    var id:String?{get set}
}

class SubjectByChapterModel:SelectionDelegate {
    var name: String?
    var id: String?
    var CLASS: String?
    var titlename: String?
    var pric: String?
    var SID: String?
    var img: String?
    
    init(subjectData: [String: Any]) {
        name = String.getString(subjectData["titlename"])
        id = String.getString(subjectData["sid"])
        CLASS = String.getString(subjectData["CLASS"])
        titlename = String.getString(subjectData["titlename"])
        pric = String.getString(subjectData["pric"])
        SID = String.getString(subjectData["SID"])
        img = String.getString(subjectData["img"])
        
    }
}

class ClassesModel:SelectionDelegate{
    var name: String?
    var id: String?
    init(data:Dict){
        name = String.getString(data["classname"])
    }
}
