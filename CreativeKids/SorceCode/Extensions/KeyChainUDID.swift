//
//  KeyChainUDID.swift
//  CreativeKids
//
//  Created by Creative Kids on 08/06/21.
//

import Foundation
import UIKit
import JNKeychain

var getUDID = KeyChainManager.shared

class KeyChainManager:NSObject{
    static let shared = KeyChainManager()
    func getDeviceIdFromKeyChain()-> String{
        //try to get value from keychain
        var deviceUDID = self.keychain_valueForKey("keychainDeviceUDID") as? String
        if deviceUDID == nil{
            deviceUDID = UIDevice.current.identifierForVendor?.uuidString
            // SAVE NEW VALUE IN KEYCHAIN
            self.keychain_setObject(deviceUDID! as AnyObject, forKey:"keychainDeviceUDID")
        }
        return deviceUDID ?? ""
    }
    
    func keychain_setObject(_ object:AnyObject, forKey:String){
        let result = JNKeychain.saveValue(object,forKey:forKey)
        if !result{
            print("keyChain saving: Something went worng")
        }
    }
    
    func keychain_deleteObjectFrom(_ key:String) -> Bool{
        let result = JNKeychain.deleteValue(forKey:key)
        return result
    }
    
    func keychain_valueForKey(_ key:String) -> AnyObject?{
        let value = JNKeychain.loadValue(forKey:key)
        return value as AnyObject?
    }
}
