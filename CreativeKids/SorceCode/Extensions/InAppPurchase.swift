//
//  InAppPurchase.swift
//  CreativeKids
//
//  Created by Rakesh jha on 22/05/21.
//

import Foundation

extension ChapterViewController{
    
    func appinPurchase(subject:ProductType){
        InAppManager.shared.purchaseProduct(productType: subject)
    }
    
    func InAppFunction(index: SubjectModel?) {
        
        switch index?.className {
        case ClassName.nursery:
            purchaceForNursery(subjectId: index?.sunjectId ?? "")
        case ClassName.lkg:
            purchaceForLKG(subjectId: index?.sunjectId ?? "")
        case ClassName.ukg:
            purchaceForUKG(subjectId: index?.sunjectId ?? "")
        case ClassName.class1:
            purchaceForClass1(subjectId: index?.sunjectId ?? "")
        case ClassName.class2:
            purchaceForClass2(subjectId: index?.sunjectId ?? "")
        case ClassName.class3:
            purchaceForClass3(subjectId: index?.sunjectId ?? "")
        case ClassName.class4:
            purchaceForClass4(subjectId: index?.sunjectId ?? "")
        case ClassName.class5:
            purchaceForClass5(subjectId: index?.sunjectId ?? "")
        case ClassName.class6:
            purchaceForClass6(subjectId: index?.sunjectId ?? "")
        case ClassName.class7:
            purchaceForClass7(subjectId: index?.sunjectId ?? "")
        case ClassName.class8:
            purchaceForClass8(subjectId: index?.sunjectId ?? "")
        case ClassName.class9:
            purchaceForClass9(subjectId: index?.sunjectId ?? "")
        case ClassName.class10:
            purchaceForClass10(subjectId: index?.sunjectId ?? "")
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS NURSERY
    func purchaceForNursery(subjectId:String){
        switch subjectId {
        case SubjectId.nursaryRymes.rawValue:
            self.appinPurchase(subject: .NursaryRymes)
        case SubjectId.nursaryMath.rawValue:
            self.appinPurchase(subject: .NursaryMath)
        case SubjectId.nursaryStory.rawValue:
            self.appinPurchase(subject: .NursaryStories)
        case SubjectId.nursaryEnglish.rawValue:
            self.appinPurchase(subject: .NursaryEnglish)
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS LKG
    func purchaceForLKG(subjectId:String){
        switch subjectId {
        case SubjectId.lkgRymes.rawValue:
            self.appinPurchase(subject: .LKGRymes)
        case SubjectId.lkgMath.rawValue:
            self.appinPurchase(subject: .LKGMath)
        case SubjectId.lkgStory.rawValue:
            self.appinPurchase(subject: .LKGStories)
        case SubjectId.lkgEnglish.rawValue:
            self.appinPurchase(subject: .LKGEnglish)
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS UKG
    func purchaceForUKG(subjectId:String){
        switch subjectId {
        case SubjectId.ukgRymes.rawValue:
            self.appinPurchase(subject: .UKGRymes)
        case SubjectId.ukgMath.rawValue:
            self.appinPurchase(subject: .UKGMath)
        case SubjectId.ukgStory.rawValue:
            self.appinPurchase(subject: .UKGStories)
        case SubjectId.ukgEnglish.rawValue:
            self.appinPurchase(subject: .UKGEnglish)
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS CLASS 1
    func purchaceForClass1(subjectId:String){
        switch subjectId {
        case SubjectId.mathClass1.rawValue:
            self.appinPurchase(subject: .Class1Math)
        case SubjectId.evsClass1.rawValue:
            self.appinPurchase(subject: .Class1Evs)
        case SubjectId.sunshine1Class1.rawValue:
            self.appinPurchase(subject: .Class1SunShine_Term_1)
        case SubjectId.sunshine2Class1.rawValue:
            self.appinPurchase(subject: .Class1SunShine_Term_2)
        case SubjectId.englishGrammerClass1.rawValue:
            self.appinPurchase(subject: .Class1English_Grammer)
        case SubjectId.englishClass1.rawValue:
            self.appinPurchase(subject: .Class1English)
        case SubjectId.hindiGrammerClass1.rawValue:
            self.appinPurchase(subject: .Class1Hindi_Vyalkaran)
        case SubjectId.computerClass1.rawValue:
            self.appinPurchase(subject: .Class1Computer)
        case SubjectId.scienceClass1.rawValue:
            self.appinPurchase(subject: .Class1Science)
        case SubjectId.sstClass1.rawValue:
            self.appinPurchase(subject: .Class1Social_Science)
        case SubjectId.hindiClass1.rawValue:
            self.appinPurchase(subject: .Class1Hindi)
        case "Sanskrit":
            return
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS CLASS 2
    func purchaceForClass2(subjectId:String){
        switch subjectId {
        case SubjectId.mathClass2.rawValue:
            self.appinPurchase(subject: .Class2Math)
        case SubjectId.evsClass2.rawValue:
            self.appinPurchase(subject: .Class2Evs)
        case SubjectId.sunshine1Class2.rawValue:
            self.appinPurchase(subject: .Class2SunShine_Term_1)
        case SubjectId.sunshine2Class2.rawValue:
            self.appinPurchase(subject: .Class2SunShine_Term_2)
        case SubjectId.englishGrammerClass2.rawValue:
            self.appinPurchase(subject: .Class2English_Grammer)
        case SubjectId.englishClass2.rawValue:
            self.appinPurchase(subject: .Class2English)
        case SubjectId.hindiGrammerClass2.rawValue:
            self.appinPurchase(subject: .Class2Hindi_Vyalkaran)
        case SubjectId.computerClass2.rawValue:
            self.appinPurchase(subject: .Class2Computer)
        case SubjectId.scienceClass2.rawValue:
            self.appinPurchase(subject: .Class2Science)
        case SubjectId.sstClass2.rawValue:
            self.appinPurchase(subject: .Class2Social_Science)
        case SubjectId.hindiClass2.rawValue:
            self.appinPurchase(subject: .Class2Hindi)
        case "Sanskrit":
            return
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS CLASS 3
    func purchaceForClass3(subjectId:String){
        switch subjectId {
        case SubjectId.mathClass3.rawValue:
            self.appinPurchase(subject: .Class3Math)
        case SubjectId.evsClass3.rawValue:
            self.appinPurchase(subject: .Class3Evs)
        case SubjectId.sunshine1Class3.rawValue:
            self.appinPurchase(subject: .Class3SunShine_Term_1)
        case SubjectId.sunshine2Class3.rawValue:
            self.appinPurchase(subject: .Class3SunShine_Term_2)
        case SubjectId.englishGrammerClass3.rawValue:
            self.appinPurchase(subject: .Class3English_Grammer)
        case SubjectId.englishClass3.rawValue:
            self.appinPurchase(subject: .Class3English)
        case SubjectId.hindiGrammerClass3.rawValue:
            self.appinPurchase(subject: .Class3Hindi_Vyalkaran)
        case SubjectId.computerClass3.rawValue:
            self.appinPurchase(subject: .Class3Computer)
        case SubjectId.scienceClass3.rawValue:
            self.appinPurchase(subject: .Class3Science)
        case SubjectId.sstClass3.rawValue:
            self.appinPurchase(subject: .Class3Social_Science)
        case SubjectId.hindiClass3.rawValue:
            self.appinPurchase(subject: .Class3Hindi)
        case "Sanskrit":
            return
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS CLASS 4
    func purchaceForClass4(subjectId:String){
        switch subjectId {
        case SubjectId.mathClass4.rawValue:
            self.appinPurchase(subject: .Class4Math)
        case SubjectId.evsClass4.rawValue:
            self.appinPurchase(subject: .Class4Evs)
        case SubjectId.sunshine1Class4.rawValue:
            self.appinPurchase(subject: .Class4SunShine_Term_1)
        case SubjectId.sunshine2Class4.rawValue:
            self.appinPurchase(subject: .Class4SunShine_Term_2)
        case SubjectId.englishGrammerClass4.rawValue:
            self.appinPurchase(subject: .Class4English_Grammer)
        case SubjectId.englishClass4.rawValue:
            self.appinPurchase(subject: .Class4English)
        case SubjectId.hindiGrammerClass4.rawValue:
            self.appinPurchase(subject: .Class4Hindi_Vyalkaran)
        case SubjectId.computerClass4.rawValue:
            self.appinPurchase(subject: .Class4Computer)
        case SubjectId.scienceClass4.rawValue:
            self.appinPurchase(subject: .Class4Science)
        case SubjectId.sstClass4.rawValue:
            self.appinPurchase(subject: .Class4Social_Science)
        case SubjectId.hindiClass4.rawValue:
            self.appinPurchase(subject: .Class4Hindi)
        case "Sanskrit":
            return
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS CLASS 5
    func purchaceForClass5(subjectId:String){
        switch subjectId {
        case SubjectId.mathClass5.rawValue:
            self.appinPurchase(subject: .Class5Math)
        case SubjectId.evsClass5.rawValue:
            self.appinPurchase(subject: .Class5Evs)
        case SubjectId.sunshine1Class5.rawValue:
            self.appinPurchase(subject: .Class5SunShine_Term_1)
        case SubjectId.sunshine2Class5.rawValue:
            self.appinPurchase(subject: .Class5SunShine_Term_2)
        case SubjectId.englishGrammerClass5.rawValue:
            self.appinPurchase(subject: .Class5English_Grammer)
        case SubjectId.englishClass5.rawValue:
            self.appinPurchase(subject: .Class5English)
        case SubjectId.hindiGrammerClass5.rawValue:
            self.appinPurchase(subject: .Class5Hindi_Vyalkaran)
        case SubjectId.computerClass5.rawValue:
            self.appinPurchase(subject: .Class5Computer)
        case SubjectId.scienceClass5.rawValue:
            self.appinPurchase(subject: .Class5Science)
        case SubjectId.sstClass5.rawValue:
            self.appinPurchase(subject: .Class5Social_Science)
        case SubjectId.hindiClass5.rawValue:
            self.appinPurchase(subject: .Class5Hindi)
        case SubjectId.sanskritClass5.rawValue:
            self.appinPurchase(subject: .Class5Sanskrit)
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS CLASS 6
    func purchaceForClass6(subjectId:String){
        switch subjectId {
        case SubjectId.mathClass6.rawValue:
            self.appinPurchase(subject: .Class6Math)
        case SubjectId.englishGrammerClass6.rawValue:
            self.appinPurchase(subject: .Class6English_Grammer)
        case SubjectId.englishClass6.rawValue:
            self.appinPurchase(subject: .Class6English)
        case SubjectId.hindiGrammerClass6.rawValue:
            self.appinPurchase(subject: .Class6Hindi_Vyalkaran)
        case SubjectId.computerClass6.rawValue:
            self.appinPurchase(subject: .Class6Computer)
        case SubjectId.scienceClass6.rawValue:
            self.appinPurchase(subject: .Class6Science)
        case SubjectId.sstClass6.rawValue:
            self.appinPurchase(subject: .Class6Social_Science)
        case SubjectId.hindiClass6.rawValue:
            self.appinPurchase(subject: .Class6Hindi)
        case SubjectId.sanskritClass6.rawValue:
            self.appinPurchase(subject: .Class6Sanskrit)
        // NCERT SUBJECT
        case SubjectId.mathClass6Ncert.rawValue:
            self.appinPurchase(subject: .Class6Math_NCERT)
        case SubjectId.englishGrammerClass6Ncert.rawValue:
            self.appinPurchase(subject: .Class6English_Grammer_NCERT)
        case SubjectId.englishClass6Ncert.rawValue:
            self.appinPurchase(subject: .Class6English_NCERT)
        case SubjectId.hindiGrammerClass6Ncert.rawValue:
            self.appinPurchase(subject: .Class6Hindi_Vyalkaran_NCERT)
        case SubjectId.scienceClass6Ncert.rawValue:
            self.appinPurchase(subject: .Class6Science_NCERT)
        case SubjectId.sstClass6Ncert.rawValue:
            self.appinPurchase(subject: .Class6Social_Science_NCERT)
        case SubjectId.hindiClass6Ncert.rawValue:
            self.appinPurchase(subject: .Class6Hindi_NCERT)
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS CLASS 7
    func purchaceForClass7(subjectId:String){
        switch subjectId {
        case SubjectId.mathClass7.rawValue:
            self.appinPurchase(subject: .Class7Math)
        case SubjectId.englishGrammerClass7.rawValue:
            self.appinPurchase(subject: .Class7English_Grammer)
        case SubjectId.englishClass7.rawValue:
            self.appinPurchase(subject: .Class7English)
        case SubjectId.hindiGrammerClass7.rawValue:
            self.appinPurchase(subject: .Class7Hindi_Vyalkaran)
        case SubjectId.computerClass7.rawValue:
            self.appinPurchase(subject: .Class7Computer)
        case SubjectId.scienceClass7.rawValue:
            self.appinPurchase(subject: .Class7Science)
        case SubjectId.sstClass7.rawValue:
            self.appinPurchase(subject: .Class7Social_Science)
        case SubjectId.hindiClass7.rawValue:
            self.appinPurchase(subject: .Class7Hindi)
        case SubjectId.sanskritClass7.rawValue:
            self.appinPurchase(subject: .Class7Sanskrit)
        // NCERT SUBJECT
        case SubjectId.mathClass7Ncert.rawValue:
            self.appinPurchase(subject: .Class7Math)
        case SubjectId.englishGrammerClass7Ncert.rawValue:
            self.appinPurchase(subject: .Class7English_Grammer_NCERT)
        case SubjectId.englishClass7Ncert.rawValue:
            self.appinPurchase(subject: .Class7English_NCERT)
        case SubjectId.hindiGrammerClass7Ncert.rawValue:
            self.appinPurchase(subject: .Class7Hindi_Vyalkaran_NCERT)
        case SubjectId.scienceClass7Ncert.rawValue:
            self.appinPurchase(subject: .Class7Science_NCERT)
        case SubjectId.sstClass7Ncert.rawValue:
            self.appinPurchase(subject: .Class7Social_Science_NCERT)
        case SubjectId.hindiClass7Ncert.rawValue:
            self.appinPurchase(subject: .Class7Hindi_NCERT)
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS CLASS 8
    func purchaceForClass8(subjectId:String){
        switch subjectId {
        case SubjectId.mathClass8.rawValue:
            self.appinPurchase(subject: .Class8Math)
        case SubjectId.englishGrammerClass8.rawValue:
            self.appinPurchase(subject: .Class8English_Grammer)
        case SubjectId.englishClass8.rawValue:
            self.appinPurchase(subject: .Class8English)
        case SubjectId.hindiGrammerClass8.rawValue:
            self.appinPurchase(subject: .Class8Hindi_Vyalkaran)
        case SubjectId.computerClass8.rawValue:
            self.appinPurchase(subject: .Class8Computer)
        case SubjectId.scienceClass8.rawValue:
            self.appinPurchase(subject: .Class8Science)
        case SubjectId.sstClass8.rawValue:
            self.appinPurchase(subject: .Class8Social_Science)
        case SubjectId.hindiClass8.rawValue:
            self.appinPurchase(subject: .Class8Hindi)
        case SubjectId.sanskritClass8.rawValue:
            self.appinPurchase(subject: .Class8Sanskrit)
        // NCERT SUBJECT
        case SubjectId.mathClass8Ncert.rawValue:
            self.appinPurchase(subject: .Class8Math_NCERT)
        case SubjectId.englishGrammerClass8Ncert.rawValue:
            self.appinPurchase(subject: .Class8English_Grammer_NCERT)
        case SubjectId.englishClass8Ncert.rawValue:
            self.appinPurchase(subject: .Class8English_NCERT)
        case SubjectId.hindiGrammerClass8Ncert.rawValue:
            self.appinPurchase(subject: .Class8Hindi_Vyalkaran_NCERT)
        case SubjectId.scienceClass8Ncert.rawValue:
            self.appinPurchase(subject: .Class8Science_NCERT)
        case SubjectId.sstClass8Ncert.rawValue:
            self.appinPurchase(subject: .Class8Social_Science_NCERT)
        case SubjectId.hindiClass8Ncert.rawValue:
            self.appinPurchase(subject: .Class8Hindi_NCERT)
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS CLASS 9
    func purchaceForClass9(subjectId:String){
        switch subjectId {
        case SubjectId.scienceClass9Ncert.rawValue:
            self.appinPurchase(subject: .Class9Science)
        case SubjectId.sstClass9Ncert.rawValue:
            self.appinPurchase(subject: .Class9Social_Science)
        // NCERT SUBJECT
        case SubjectId.mathClass9Ncert.rawValue:
            self.appinPurchase(subject: .Class9Math_NCERT)
        case SubjectId.englishGrammerClass9Ncert.rawValue:
            self.appinPurchase(subject: .Class9English_Grammer_NCERT)
        case SubjectId.englishClass9Ncert.rawValue:
            self.appinPurchase(subject: .Class9English_NCERT)
        case SubjectId.hindiGrammerClass9Ncert.rawValue:
            self.appinPurchase(subject: .Class9Hindi_Vyalkaran_NCERT)
        case SubjectId.hindiClass9Ncert.rawValue:
            self.appinPurchase(subject: .Class9Hindi_NCERT)
        default:
            return
        }
    }
    
    //MARK:- PURCHASE FOR CLASS CLASS 10
    func purchaceForClass10(subjectId:String){
        switch subjectId {
        case SubjectId.scienceClass10Ncert.rawValue:
            self.appinPurchase(subject: .Class10Science)
        case SubjectId.sstClass10Ncert.rawValue:
            self.appinPurchase(subject: .Class10Social_Science)
        // NCERT SUBJECT
        case SubjectId.mathClass10Ncert.rawValue:
            self.appinPurchase(subject: .Class10Math_NCERT)
        case SubjectId.englishGrammerClass10Ncert.rawValue:
            self.appinPurchase(subject: .Class10English_Grammer_NCERT)
        case SubjectId.englishClass10Ncert.rawValue:
            self.appinPurchase(subject: .Class10English_NCERT)
        case SubjectId.hindiGrammerClass10Ncert.rawValue:
            self.appinPurchase(subject: .Class10Hindi_Vyalkaran_NCERT)
        case SubjectId.hindiClass10Ncert.rawValue:
            self.appinPurchase(subject: .Class10Hindi_NCERT)
        default:
            return
        }
    }
    
    //MARK:- GET ORDER ID API
    func getOrderIdApi(subjectObj:SubjectModel,completion: @escaping ((_ status:Bool, _ orderId:String)->Void)){
        let serviceName = "?subname=\(subjectObj.subjectName ?? "")&cls=\(subjectObj.className ?? "")&price=\(subjectObj.subjectPrice ?? "")&pid=\(subjectObj.sunjectId ?? "")&userId=\(kUserData.userEmail ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let orderId = String.getString(response[getResponce])
                completion(true,orderId)
            } else {
                completion(false,"")
                showAlertMessage.alert(message: "Order Id Not Given")
            }
        }
    }
}
