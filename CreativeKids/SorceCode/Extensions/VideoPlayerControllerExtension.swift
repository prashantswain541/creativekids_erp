//
//  VideoPlayerControllerExtension.swift
//  CreativeKids
//
//  Created by Creative Kids on 31/03/21.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import MediaPlayer

//gobackward

enum SpeedType:Float,CaseIterable{
    case veryLow = 0.5
    case low = 0.75
    case normal = 1.0
    case high = 1.5
}

//MARK : - Video pLayer class Extension 
extension VideoPlayerViewController {
    
    //MARK: - initialLoad
    func initViewLoad(){
        viewVideoRate.superview?.sendSubviewToBack(viewVideoRate)
        speedModelArray = SpeedType.allCases.map{PlayerSpeedModel(type: $0)}
        tableViewPlaybackSpeed.getData(cell:TableCellIdentifier.playBackSpeedCell,data:speedModelArray)
        viewBrightNessIndicater.addSubview(self.innerView)
        timeSlider.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        timeSlider.maximumValue = 0.0
        timeSlider.minimumValue = 0.0
        progress.transform = progress.transform.rotated(by: .pi * 1.5)
        self.progress.alpha = 0.0
        self.labelShowVolumePercentage.alpha = 0.0
        self.viewBrightNessIndicater.alpha = 0.0
        self.innerView.alpha = 0.0
        self.playButton.alpha = 0.0
        self.labelShowBrightNessPercentage.alpha = 0.0
        self.buttonPreviousSong.alpha = 0.0
        self.buttonNextSong.alpha = 0.0
        self.progress.setProgress(0.2, animated: true)
        self.videoNameLabel.isHidden = true
        self.labelVideoBhaagNumber.isHidden = true
        addPanGesture()
        setVideoPlayerSize()
    }
    
    //MARK:- View For When User Came fro Demo
    func viewForChapter(){
        self.videoViewModel = VideoViewModel(viewController: self, completionHandler: { [unowned self](data) in
            let responceData = data as! [TopicModel]
            let filteredData = responceData.filter{!(["Solutions","Live Test Paper","LTP","E-Book", "E-book", "Mock Test","MT_Ques", "mT_Ques", ""].contains($0.topic))}
            if !filteredData.isEmpty{
                self.videoUrls = filteredData.map{$0.videoUrlString ?? "https://"}
                self.playVideo(url: self.videoUrls[0], itemNumber: 0)
                self.videoDataArray = filteredData
                passDataToVideoController?.passVideoData(data: self.videoDataArray,parentController: self)
                self.videoDataArray[0].isSelected = true
                self.showTopicName(object: filteredData[0])
            }
            if camto == .Scan {
                for topicData in responceData {
                    if  isHindiSubject(subejct:topicData.subjectName ?? ""){
                        self.labelChapterName.font = UIFont(name: "WalkmanChanakya901Normal", size: 15)
                        self.labelChapterName.attributedText = makeNewNumericStringFor3And7(StringWithNumber:topicData.chapterName ?? "")
                    }else{
                        self.labelChapterName.font = UIFont(name: "Futura Medium", size: 15)
                        self.labelChapterName.text = topicData.chapterName ?? ""
                    }
                }
            }
            self.excelInExam = responceData.flatMap{$0.exercisesPdfExam.filter{$0.exerciseType == "Solutions"}}
            self.ebook = responceData.flatMap{$0.exercisesPdfExam.filter{["E-Book","E-book"].contains($0.exerciseType)}}
            self.ltpExam = responceData.flatMap{$0.LtpExercise.filter{$0.exerciseType == "LTP"}}
            self.practicePaperPdf = responceData.flatMap{$0.practicePaperPdf.map{$0}}
            self.exercises = responceData.flatMap{$0.exercise.map{$0}}
            self.mockTest = responceData.flatMap{$0.mockTest.map{$0}}
            self.manageSegmentView(selectedSegment:0)
            segmentViewData()
            self.loadExercise()
        })
    }
    
    
    //MARK:- VIDEO Config with Url
    func playVideoWithUrl(url:String){
        previousValue = 0
        speedModelArray.forEach{$0.isSelected = false}
        tableViewPlaybackSpeed.reloadData()
        guard let url = URL(string: url) else{return}
        playerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
        playerlayer = AVPlayerLayer(player: player)
        playerlayer.videoGravity = .resize
        playerView.layer.addSublayer(playerlayer)
        addObservers()
        player.volume = Float(20.0)
        speedType = .normal
        player.playImmediately(atRate: speedType.rawValue)
        self.labelShowVolumePercentage.text = "20 %"
        fadePlayerButtomView()
    }
    
    //MARK:- VIDEO Initilize
    func playVideo(url:String,itemNumber:Int){
        player?.pause()
        self.timeSlider.maximumValue = 1.0
        self.timeSlider.minimumValue = 0.0
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.player = nil
            self.playerlayer.removeFromSuperlayer()
            self.playVideoWithUrl(url: url)
            self.currentPlayingVideo = itemNumber
            self.fadeNextVideoPlayButton()
            self.fadePreviousVideoPlayButton()
        }
    }
        
    //MARK:- EBook and Exam LOAD
    func segmentViewData(){
        if !self.videoDataArray.isEmpty{
            self.segmentDataArray.append(SegmentModel(isSelected: true, contentText: "Videos", data: self.videoDataArray,type: SegmentType.video))
        }
        if !self.exercises.isEmpty{
            self.segmentDataArray.append(SegmentModel(isSelected: false, contentText: "Exercise", data: self.exercises, type: SegmentType.exercise))
        }
        if !self.ltpExam.isEmpty{
            self.segmentDataArray.append(SegmentModel(isSelected: false, contentText: "Live test paper", data: ltpExam, type: SegmentType.LTP))
        }
        if !self.mockTest.isEmpty{
            self.segmentDataArray.append(SegmentModel(isSelected: false, contentText: "Practice paper", data: nil, type: SegmentType.questionBank))
        }
        self.exerciseSegmentCollectionView.getData(cell: "ExerciseSegmentCollectionViewCell", data: segmentDataArray, language: "English")
        self.exerciseSegmentCollectionView.dataSourcesCollection = self
    }
    
    //MARK:- EBook and Exam LOAD
    func loadExercise(){
        self.viewExcelInExam.isHidden = self.excelInExam.isEmpty ? true : false
        self.viewEbook.isHidden = self.ebook.isEmpty ? true : false
        self.viewSamplePaper.isHidden = self.selectedTopic?.samplePaperPdf?.isEmpty ?? true ? true : false
        self.passDataToLTPController?.passLTPData(data: self.ltpExam,parentController: self)
    }
    
    //MARK:- Video WISE NAME SET
    func showTopicName(object:TopicModel){
        if  isHindiSubject(subejct:object.subjectName ?? ""){
            self.labelVideoName.font = UIFont(name: "WalkmanChanakya901Normal", size: 15)
            self.videoNameLabel.font = UIFont(name: "WalkmanChanakya901Normal", size: 11)
            if isContainNumber(checkString: object.topic ?? ""){
                self.labelVideoName.text = ConstantHindiText.bhaag
                self.videoNameLabel.text = ConstantHindiText.bhaag
                self.labelBhaagNumber.text = "- \(self.currentPlayingVideo + 1)"
                self.labelVideoBhaagNumber.text = "- \(self.currentPlayingVideo + 1)"
            }else{
                self.videoNameLabel.text = object.topic
                self.labelVideoName.text = object.topic
            }
        }else{
            self.labelVideoName.font = UIFont(name: "Roboto-Medium", size: 15)
            self.videoNameLabel.font = UIFont(name: "Futura Medium", size: 11)
            self.videoNameLabel.text = object.topic
            self.labelVideoName.text = object.topic
        }
    }
    
    func setChapterName(){
        if  isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            self.labelChapterName.font = UIFont(name: "WalkmanChanakya901Normal", size: 15)
            self.labelChapterName.attributedText = makeNewNumericStringFor3And7(StringWithNumber:selectedTopic?.chapterName ?? "")
        }else{
            self.labelChapterName.font = UIFont(name: "Futura Medium", size: 15)
            self.labelChapterName.text = selectedTopic?.chapterName ?? ""
        }
    }
    
    //MARK:- VIDEO CHANGE
    func videoChange(sender:UIButton,isNext:Bool){
        player!.pause()
        self.timeSlider.minimumValue = 0.0
        self.timeSlider.maximumValue = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.player = nil
            self.playerlayer.removeFromSuperlayer()
            let _ = self.videoDataArray.map{$0.isSelected = false}
            self.currentPlayingVideo = isNext ? self.currentPlayingVideo + 1 : self.currentPlayingVideo - 1
            self.videoDataArray[self.currentPlayingVideo].isSelected = true
            self.passDataToVideoController?.reloadTableForNextPrevious()
            self.playVideo(url: self.videoDataArray[self.currentPlayingVideo].videoUrlString ?? "https://", itemNumber: self.currentPlayingVideo)
            self.showTopicName(object: self.videoDataArray[self.currentPlayingVideo])
            self.playButton.isSelected = false
            self.buttonPlayButtom.isSelected = false
            if isNext{
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.fadeNextVideoPlayButton), object: sender)
            self.perform(#selector(self.fadeNextVideoPlayButton), with: sender, afterDelay: 0.2)
            }else{
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector( self.fadePreviousVideoPlayButton), object: sender)
            self.perform(#selector(self.fadePreviousVideoPlayButton), with: sender, afterDelay: 0.2)
            }
        }
    }
    
    //MARK: - TIME CONTROL OVSERVER
    override internal func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "timeControlStatus", let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
            let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
            let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
            if newStatus != oldStatus {
                DispatchQueue.main.async {[weak self] in
                    if newStatus == .playing || newStatus == .paused {
                        self?.indicator.isHidden = true
                    } else {
                        self?.indicator.isHidden = false
                        self?.indicator.startAnimating()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
                            if !(self?.videoPlayingforLongTime ?? false){
                                print("No internet Connection")
                                self?.videoPlayingforLongTime = true
                            }
                        }
                    }
                }
            }
        }
        
        if keyPath == "duration", let duration = player?.currentItem?.duration.seconds, duration > 0.0 {
            self.labelDurationTime.text = getTimeString(from: player.currentItem!.duration)
        }
    }
    
    //MARK:- Add Gesture
    func addPanGesture(){
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(onPan(gestureRecognizer:)))
        viewAddPanGesture.isUserInteractionEnabled = true
        viewAddPanGesture.addGestureRecognizer(panGesture)
        let panGestureBrightness = UIPanGestureRecognizer(target: self, action: #selector(brightnessPan(gestureRecognizer:)))
        viewAddPanGestureBrightNess.isUserInteractionEnabled = true
        viewAddPanGestureBrightNess.addGestureRecognizer(panGestureBrightness)
        viewAddPanGesture.transform.rotated(by: .pi * 2)
    }
    
    //MARK:- Add Observers
    func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        player.currentItem?.addObserver(self, forKeyPath: "duration", options: [.old,.new,.initial], context: nil)
        player.addObserver(self, forKeyPath: "timeControlStatus", options: [.old,.new], context: nil)
        addTimeObserver()
        
    }
    
    //MARK: - Call After Screen Rotate
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.isPhone{
            
            if UIDevice.current.orientation.isLandscape {
                (self.navigationController as? HomeNavigationController)?.voiceView?.isHidden = true
                statusBarColorwithorentation(headerColor: .clear, statusView: self.statusView)
                self.topVideoPlayer.constant = -50
                self.videoNameLabel.isHidden = false
                self.labelVideoBhaagNumber.isHidden = false
                self.viewEbbokTop.isActive = false
                
            } else {
                (self.navigationController as? HomeNavigationController)?.voiceView?.isHidden = false
                print("Portrait")
                statusBarColorwithorentation(headerColor: UIColor.blue, statusView: self.statusView)
                self.topVideoPlayer.constant = 0
                self.videoNameLabel.isHidden = true
                self.labelVideoBhaagNumber.isHidden = true
                self.viewEbbokTop.isActive = true
            }
        }else{
            if UIDevice.current.orientation.isLandscape {
                (self.navigationController as? HomeNavigationController)?.voiceView?.isHidden = true
                statusBarColorwithorentation(headerColor: UIColor.blue, statusView: self.statusView)
                self.topVideoPlayer.constant = -50
                self.videoNameLabel.isHidden = false
                self.labelVideoBhaagNumber.isHidden = false
                self.viewEbbokTop.isActive = false
                
            } else {
                (self.navigationController as? HomeNavigationController)?.voiceView?.isHidden = false
                print("Portrait")
                statusBarColorwithorentation(headerColor: UIColor.blue, statusView: self.statusView)
                self.topVideoPlayer.constant = 0
                self.videoNameLabel.isHidden = true
                self.labelVideoBhaagNumber.isHidden = true
                self.viewEbbokTop.isActive = true
            }
        }
    }
    
    //MARK:- Time Observers
    @objc func appMovedToBackground() {
        DispatchQueue.main.async{
            self.playButton.isSelected = true
            self.buttonPlayButtom.isSelected = true
            self.player?.pause()
            self.isPlaying = false
        }
    }
    
    //MARK:- Time Observers
    func addTimeObserver(){
        let interval = CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        _ = player.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main , using: { [weak self] (time) in
            guard let currentItem = self?.player?.currentItem else{return}
            guard currentItem.status.rawValue == AVPlayerItem.Status.readyToPlay.rawValue else {return}
            self?.timeSlider.maximumValue = Float(currentItem.duration.seconds)
            self?.timeSlider.value = Float(currentItem.currentTime().seconds)
            self?.labelCurrentTime.text = self?.getTimeString(from: currentItem.currentTime())
            self?.currentVideoTotalTimeDuration = Int(currentItem.duration.seconds)
            self?.findOutVideoSeenByTime(currentValue: Int(CMTimeGetSeconds(time))){
                print("\(Int(Int(currentItem.duration.seconds) * 3)/4) --- \(self!.previousValue)")
                ///////////////////////////////// Video Performance/////////////////////////////////////////
                if Int(currentItem.duration.seconds) == Int(CMTimeGetSeconds(time)) || Int(Int(currentItem.duration.seconds) * 3)/4 == self!.previousValue{
                    let userType = String.getString(kUserData.role)
                    switch userType {
                    case String.getstring(UserRole.schoolStudent.rawValue):
                        if Int(currentItem.duration.seconds) == Int(CMTimeGetSeconds(time)){
                            self?.studentVideoPerformance()
                        }
                        if Int(Int(currentItem.duration.seconds) * 3)/4 == self!.previousValue{
                            if !(self?.selectedTopic?.isChapterRead ?? true) && (self?.currentPlayingVideo == ((self?.videoDataArray.count ?? 1) - 1)){
                                DispatchQueue.main.async {
                                    self?.chapterReadStatusApi()
                                }
                            }
                        }
                    case String.getstring(UserRole.normalUser.rawValue):
                        if Int(Int(currentItem.duration.seconds) * 3)/4 == self!.previousValue{
                            if !(self?.selectedTopic?.isChapterRead ?? true) && (self?.currentPlayingVideo == ((self?.videoDataArray.count ?? 1) - 1)){
                                DispatchQueue.main.async {
                                self?.chapterReadStatusApi()
                            }
                         }
                        }
                    default:
                        return
                    }
                    
                }
                
                
                
            }
            if Float(currentItem.currentTime().seconds) == Float(currentItem.duration.seconds){
                self?.buttonPlayButtom.isSelected = true
                self?.playButton.isSelected = false
                self?.playButton.isHidden = true
                self?.buttonReplayVideo.isHidden = false
                self?.isPlaying = false
            }
//            if Float(currentItem.currentTime().seconds) >= Float(currentItem.duration.seconds - 10){}
        })
    }
    
    //MARK:- Count total video Seen Time
    func findOutVideoSeenByTime(currentValue:Int, completion: @escaping (()->Void)){
        if currentValue != 0{
            if !(previousValue >= currentValue){
                previousValue += 1
                completion()
            }
        }
    }
    
    //MARK:- Video Player Size
    func setVideoPlayerSize(){
        if UIDevice.isPhone{
            let statusHeightheight = getStatusBarHeight()
            let estimatedHeight = kScreenHeight - statusHeightheight
            let aspectRatioCons = NSLayoutConstraint(item: self.playerView!, attribute: .height, relatedBy: .equal, toItem: self.playerView, attribute: .width, multiplier: ((kScreenWidth)/estimatedHeight), constant: 0)
            aspectHeightWidth.isActive = false
            aspectRatioCons.isActive = true
            playerView.addConstraint(aspectRatioCons)
        }else{
            let statusHeightheight = getStatusBarHeight()
            let estimatedHeight = kScreenHeight - statusHeightheight
            let aspectRatioCons = NSLayoutConstraint(item: self.playerView!, attribute: .height, relatedBy: .equal, toItem: self.playerView, attribute: .width, multiplier: ((kScreenWidth - 20)/estimatedHeight), constant: 0)
            aspectHeightWidth.isActive = false
            aspectRatioCons.isActive = true
            playerView.addConstraint(aspectRatioCons)
        }
    }
    
    //MARK: PANGESTURE FOR VOLUME
    @objc func onPan(gestureRecognizer: UIPanGestureRecognizer) {
        var panStartPoint = CGPoint()
        let point = gestureRecognizer.location(in: viewAddPanGesture)
        switch gestureRecognizer.state {
        case .began:
            panStartPoint = point
            buttonOnWholeVideoPlayer.isUserInteractionEnabled = false
            fadeVolumeConfig()
            print(panStartPoint)
        case .changed:
            setVolume(point:point.y)
        case .ended:
            buttonOnWholeVideoPlayer.isUserInteractionEnabled = true
        default:
            print("default")
        }
    }
    
    //MARK:- Volume Set
    func setVolume(point:CGFloat){
        let viewHeight = viewAddPanGesture.frame.height
        var boundPoint:CGFloat = 0.0
        if point < 0{
            boundPoint = 0.0
        }else if point > viewHeight{
            boundPoint = viewHeight
        }else{
            boundPoint = point
        }
        let invertPoint = viewHeight - boundPoint
        let getPoint  = Int((invertPoint/viewHeight) * 100)
        self.progress.setProgress(Float(getPoint) / 100, animated: true)
        print(Float(getPoint))
        //        self.player.volume = Float(getPoint)
        MPVolumeView.setVolume(Float(getPoint) / 100)
        self.labelShowVolumePercentage.text = "\(Int(getPoint)) %"
        fadeVolumeConfig()
        
    }
    //MARK:- Fade Volume Indicator
    func fadeVolumeConfig(){
        self.progress.alpha = 1
        self.labelShowVolumePercentage.alpha = 1
        UIView.animate(withDuration: 4,
                       animations: { [weak self] in
                        self?.progress.alpha = 0.0
                        self?.labelShowVolumePercentage.alpha = 0.0
                       }) { [weak self] _ in
            self?.progress.isHidden = false
            self?.labelShowVolumePercentage.isHidden = false
        }
    }
    
    //MARK: PANGESTURE FOR BrightNess
    @objc func brightnessPan(gestureRecognizer: UIPanGestureRecognizer) {
//        var panStartPoint = CGPoint()
        let point = gestureRecognizer.location(in: viewAddPanGestureBrightNess)
        
        switch gestureRecognizer.state {
        case .began:
            buttonOnWholeVideoPlayer.isUserInteractionEnabled = false
            fadeBrightnessConfig()
//            panStartPoint = point
        case .changed:
            setBrightNess(point:point.y)
        case .ended:
            buttonOnWholeVideoPlayer.isUserInteractionEnabled = true
        default:
            print("default")
        }
    }
    //MARK: Methods FOR BrightNess
    func setBrightNess(point:CGFloat){
        let viewHeight = viewAddPanGestureBrightNess.frame.height
        var boundPoint:CGFloat = 0.0
        if point < 0{
            boundPoint = 0.0
        }else if point > viewHeight{
            boundPoint = viewHeight
        }else{
            boundPoint = point
        }
        let invertPoint = viewHeight - boundPoint
        let getPoint  = Int((invertPoint/viewHeight) * 100)
        setBrightNessIndicator(pointChange:getPoint)
        UIScreen.main.brightness = CGFloat(Float(getPoint)/100)
        print(UIScreen.main.brightness*100)
    }
    
    //MARK: Set Indicator FOR BrightNess
    func setBrightNessIndicator(pointChange:Int){
        self.labelShowBrightNessPercentage.text = "\(pointChange) %"
        viewBrightNessIndicater.layer.cornerRadius = viewBrightNessIndicater.frame.height/2
        let widthPoint:Double = Double(viewBrightNessIndicater.frame.height/100)
        let innerWidth = widthPoint*Double(pointChange)
        self.innerView.frame = CGRect(x: 0, y: 0, width: CGFloat(innerWidth), height: viewBrightNessIndicater.frame.height)
        innerView.layer.cornerRadius = innerView.frame.height/2
        innerView.backgroundColor = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
        fadeBrightnessConfig()
    }
    
    //MARK: Fade BrightNess Indicator
    func fadeBrightnessConfig(){
        self.viewBrightNessIndicater.alpha = 1
        self.innerView.alpha = 1
        self.labelShowBrightNessPercentage.alpha = 1
        UIView.animate(withDuration: 4,
                       animations: { [weak self] in
                        self?.viewBrightNessIndicater.alpha = 0.0
                        self?.innerView.alpha = 0.0
                        self?.labelShowBrightNessPercentage.alpha = 0.0
                       }) { [weak self] _ in
            self?.viewBrightNessIndicater.isHidden = false
            self?.innerView.isHidden = false
            self?.labelShowBrightNessPercentage.isHidden = false
        }
    }
    
    //MARK: Hide And Show view on VideoPlayer
    @objc func fadeCenterPlayButton() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fadeCenterPlayButton), object:nil)
        self.playButton.alpha = !(self.buttonReplayVideo.isHidden) ? 0.0 : 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            if self.playButton.isSelected == true || self.buttonPlayButtom.isSelected == true {
                self.playButton.isHidden = false
                self.playButton.alpha = !(self.buttonReplayVideo.isHidden) ? 0.0 : 1.0
            } else {
                UIView.animate(withDuration: 1,
                               animations: { [weak self] in
                                self?.playButton.alpha = 0.0
                               }) { [weak self] _ in
                    self?.playButton.isHidden = false
                }
            }
        }
    }
    
    //MARK: Fade Volume Indicator
    @objc func fadePlayerButtomView(){
        self.viewPlayerButtom.alpha = 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            UIView.animate(withDuration: 0.5,
                           animations: { [weak self] in
                            self?.viewPlayerButtom.alpha = 0.0
                           }) { [weak self] _ in
                self?.viewPlayerButtom.isHidden = false
            }
        }
    }
    
    //MARK: Hide And Show NEXT VIDEO BUTTON on VideoPlayer
    @objc func fadeNextVideoPlayButton() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fadeNextVideoPlayButton), object: nil)
        if currentPlayingVideo == videoUrls.count - 1{
            self.buttonNextSong.alpha = 0.0
            return
        }
        self.buttonNextSong.alpha = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            UIView.animate(withDuration: 1,
                           animations: { [weak self] in
                            self?.buttonNextSong.alpha = 0.0
                           }) { [weak self] _ in
                self?.buttonNextSong.isHidden = false
            }
        }
    }
    
    //MARK: Hide And Show previous VIDEO BUTTON on VideoPlayer
    @objc func fadePreviousVideoPlayButton() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fadePreviousVideoPlayButton), object: nil)
        if  videoUrls.count < 1 || currentPlayingVideo < 1 {
            self.buttonPreviousSong.alpha = 0.0
            return
        }
        self.buttonPreviousSong.alpha = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            UIView.animate(withDuration: 1,
                           animations: { [weak self] in
                            self?.buttonPreviousSong.alpha = 0.0
                           }) { [weak self] _ in
                self?.buttonPreviousSong.isHidden = false
            }
        }
    }
    
    //MARK:- GET TIME IN STRING
    func getTimeString(from time:CMTime) -> String{
        let totalSecond = CMTimeGetSeconds(time)
        let hours = Int(totalSecond/3600)
        let minutes = Int(totalSecond/60) % 60
        let seconds = Int(totalSecond.truncatingRemainder(dividingBy: 60))
        if hours > 0{
            return String(format: "%i:%02i:%02i", arguments: [hours,minutes,seconds])
        }else{
            return String(format: "%02i:%02i", arguments: [minutes,seconds])
        }
    }
    
    //   MARK:- Chamge Status Bar Color with orentation
    func statusBarColorwithorentation(headerColor:UIColor,statusView:UIView){
        if UIDevice.current.orientation.isLandscape {
            statusView.removeFromSuperview()
        }else{
            if #available(iOS 13.0, *){
                let width = kScreenHeight > kScreenWidth ? kScreenHeight : kScreenWidth
                statusView.frame = CGRect(x: 0, y: 0, width: Double(width), height: self.statusHeight)
                statusView.backgroundColor = headerColor
                self.view.addSubview(statusView)
                print("prashant")
            }else{
                statusView.frame = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as! CGRect
                statusView.backgroundColor = headerColor
                self.view.addSubview(statusView)
            }
        }
    }
    
    func chapterReadStatusApi(){
        var serviceName = ""
        if isSchoolUser(){
            serviceName = "?subid=\(self.selectedTopic?.sunjectId ?? "")&chid=\(self.selectedTopic?.chapterNumber ?? "")&Userid=\(kUserData.userEmail ?? "")&cls=\(self.selectedTopic?.className ?? "")".replacingOccurrences(of: " ", with: "%20")
        }else{
            serviceName = "?subid=\(self.selectedTopic?.sunjectId ?? "")&chid=\(self.selectedTopic?.chapterNumber ?? "")&uid=\(kUserData.userEmail ?? "")&cls=\(self.selectedTopic?.className ?? "")".replacingOccurrences(of: " ", with: "%20")
        }
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET,showHud: false) { (response, statusCode) in
            if statusCode == 200 {
                let responceMessage = String.getString(response[getResponce])
                self.selectedTopic?.isChapterRead = true
                print(responceMessage)
            } else {
                print("wrror in responce")
            }
        }
    }
    
    @objc func notifyWithVideoSeen(){
        let userType = String.getString(kUserData.role)
        switch userType {
        case String.getstring(UserRole.schoolStudent.rawValue):
            studentVideoPerformance()
        default:
            return
        }
    }
    
    //MARK: VideoPerformanceApi For School
    func studentVideoPerformance(){
        print("Api Hit for Student Video Performance")
        print("\(self.currentVideoTotalTimeDuration) ----------------- Current video total Time DURATION")
        print("\(self.previousValue) ----------------- Current video totall seen Time DURATION")
        if currentVideoTotalTimeDuration > 0 {
            let viewModel = StudentPerformance()
            viewModel.videoSeenForPerforManceApi(totalVideoTime: self.currentVideoTotalTimeDuration, seenTime: self.previousValue, data: self.videoDataArray[currentPlayingVideo])
        }
    }
}

//Update system volume
extension MPVolumeView {
    static func setVolume(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        volumeView.showsVolumeSlider = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            slider?.value = volume
        }
    }
}


