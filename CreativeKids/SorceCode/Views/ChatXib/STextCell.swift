//
//  STextCell.swift
//  VoiceChat
//
//  Created by Shubham Kaliyar on 06/01/21.
//  Copyright © 2021 SHALINI YADAV. All rights reserved.
//

import UIKit

class STextCell: UITableViewCell {
    
    @IBOutlet weak var txtMessage: UILabel!
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var labelTime: UILabel!
    
    var message:MessageClass?{didSet{ self.updateCell() }}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateCell() {
        viewMain.layer.cornerRadius = 5
        self.txtMessage.text = message?.message
        self.labelTime.text = String.getTime(timeStamp:Double(message?.sendingTime ?? 0))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
