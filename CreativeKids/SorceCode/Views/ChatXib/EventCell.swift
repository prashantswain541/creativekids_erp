//
//  EventCell.swift
//  VoiceChat
//
//  Created by Shubham Kaliyar on 06/01/21.
//  Copyright © 2021 SHALINI YADAV. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    @IBOutlet weak var txtMessage: UILabel!
    @IBOutlet weak var viewBg: UIView!
    
    var message:MessageClass?{didSet{ self.updateCell() }}
    var userDetails = UsersState(userdata: chatUserDefault.getuserDetails())
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.viewBg.sizeToFit()
    }
    
    
    func updateCell() {
        manageMessage(message: message)
    }
    
    func manageMessage(message:MessageClass?){
        switch String.getstring(message?.mediaType?.rawValue){
        case "create":
            create(messageObject: message)
        case "join":
            join(messageObject: message)
        case "kickOutByAdmin":
            kickOutByAdmin(messageObject: message)
        case "leave":
            leave(messageObject: message)
        default:
            return
        }
    }
}

extension EventCell{
    func create(messageObject:MessageClass?){
        let userDetail = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(messageObject?.senderId) == String.getstring(userDetail.userId)
        {
            txtMessage.text = "You created this group"
        }else{
            ChatHalper.shared.userReference.child(String.getstring(messageObject?.senderId)).observe(.value) { [weak self] (snapshot) in
                if snapshot.exists(){
                   let userDetail = chatSharedInstanse.getDictionary(snapshot.value)
                    self?.txtMessage.text = "You were added by \(userDetail[ChatParameter.name] ?? "")"
                }else{
                    self?.txtMessage.text = ""
                }
            }
           
        }
    }
    
    func join(messageObject:MessageClass?){
        let userDetail = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(messageObject?.senderId) == String.getstring(userDetail.userId){
            getAllUserWhenJoinUsers(messageObject:messageObject){ [weak self] (returnedMessage) in
                self?.txtMessage.text = "you added \(returnedMessage) in this group"
            }
            
        }else{
            if String.getstring(messageObject?.message).components(separatedBy: ",").contains(String.getstring(userDetail.userId)){
                ChatHalper.shared.userReference.child(String.getstring(messageObject?.senderId)).observe(.value) { [weak self] (snapshot) in
                    if snapshot.exists(){
                       let userDetail = chatSharedInstanse.getDictionary(snapshot.value)
                        self?.txtMessage.text = "You were added by \(userDetail[ChatParameter.name] ?? "")"
                    }else{
                        self?.txtMessage.text = ""
                    }
                }
            }else{
                getAllUserWhenJoinUsers(messageObject:messageObject){ [weak self] (returnedMessage) in
                    self?.txtMessage.text = "\(returnedMessage) added by admin"
                }
            }
        }
    }
    
    func kickOutByAdmin(messageObject:MessageClass?){
        let userDetail = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(messageObject?.senderId) == String.getstring(userDetail.userId) {
            getAllUserWhenJoinUsers(messageObject:messageObject){ [weak self] (returnedMessage) in
                self?.txtMessage.text = "You removed \(returnedMessage)"
            }
        }else{
            if String.getstring(messageObject?.message).components(separatedBy: ",").contains(String.getstring(userDetail.userId)){
                self.txtMessage.text = "You were removed by admin"
            }else{
                getAllUserWhenJoinUsers(messageObject:messageObject){ [weak self] (returnedMessage) in
                    self?.txtMessage.text = "\(returnedMessage) removed by admin"
                }
            }
        }
    }
    
    func leave(messageObject:MessageClass?){
        let userDetail = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(messageObject?.senderId) == String.getstring(userDetail.userId) {
            txtMessage.text = "you left"
        }else{
            ChatHalper.shared.userReference.child(String.getstring(messageObject?.senderId)).observe(.value) { [weak self] (snapshot) in
                if snapshot.exists(){
                   let userDetail = chatSharedInstanse.getDictionary(snapshot.value)
                    self?.txtMessage.text = "\(userDetail[ChatParameter.name] ?? "") left"
                }else{
                    self?.txtMessage.text = ""
                }
            }
        }
    }
    
    //MARK: - message for join users
    func getAllUserWhenJoinUsers(messageObject:MessageClass?, showMessage: @escaping (_ message:String)->Void){
        var message = ""
        var users = String.getstring(messageObject?.message).components(separatedBy: ",")
        if users.isEmpty{
            users.append(String.getstring(messageObject?.message))
        }
        for user in users{
            ChatHalper.shared.userReference.child(String.getstring(user)).observe(.value) { (snapshot) in
                if snapshot.exists(){
                   let userDetail = chatSharedInstanse.getDictionary(snapshot.value)
                    if users.last == user{
                        message = "\(String.getstring(userDetail[ChatParameter.name]))"
                    }else{
                        message = "\(String.getstring(userDetail[ChatParameter.name])),"
                    }
                    showMessage(message)
                }
            }
        }
        
    }
    
}
