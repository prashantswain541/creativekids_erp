//
//  SDocumentCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 21/06/22.
//

import UIKit

class SDocumentCell: UITableViewCell {

    
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelPdfName: UILabel!
    var message:MessageClass?{didSet{ self.updateCell() }}
    var didTapCallBack:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateCell() {
//        viewMain.layer.cornerRadius = 5
        self.labelPdfName.text = message?.fileName
        print(message?.fileName)
        self.labelTime.text = String.getTime(timeStamp:Double(message?.sendingTime ?? 0))
    }
    
    @IBAction func buttonShowPdfTapped(_ sender: UIButton) {
        didTapCallBack?()
    }
}
