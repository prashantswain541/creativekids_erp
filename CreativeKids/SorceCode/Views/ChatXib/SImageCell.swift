//
//  SImageCell.swift
//  Smooshi
//
//  Created by Shubham Kaliyar on 07/09/20.
//  Copyright © 2020 fluper. All rights reserved.
// https://www.wwdcnotes.com/notes/wwdc20/10165/

import UIKit

class SImageCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imageSend: UIImageView!
    @IBOutlet weak var labelTime: UILabel!
    
    var message:MessageClass?{didSet{DispatchQueue.main.async { self.updateCell() }}}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateCell() {
        self.lblName.text = "You"
        self.imageSend.downlodeImage(serviceurl: String.getstring(self.message?.message), placeHolder: UIImage())
        self.labelTime.text = String.getTime(timeStamp:Double(message?.sendingTime ?? 0))
    }
    @IBAction func showImage(_ sender: UIButton) {
//        let nextvc = ImagePreview()
//        nextvc.imageUrl = String.getstring(self.message?.message)
//        nextvc.modalPresentationStyle = .overFullScreen
//        self.parentViewController?.present(nextvc,animated:true)
        let nextvc = ImagePreviewViewController.getController(storyboard: .TeacherDashboard)
        nextvc.cameFor = .Chat
            guard let imageUrl = self.message?.message else {return}
            nextvc.imageArr = [imageUrl]
        
        nextvc.headerName = "\(self.lblName.text ?? "") on \(getDateTimeFromTimeStamp(timeStamp:  self.message?.sendingTime)),\(self.labelTime.text ?? "")"
        self.parentContainerViewController()?.navigationController?.pushViewController(nextvc, animated: true)
        
    }
}

