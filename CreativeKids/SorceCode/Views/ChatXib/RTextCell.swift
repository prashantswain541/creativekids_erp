//
//  RTextCell.swift
//  VoiceChat
//
//  Created by Shubham Kaliyar on 06/01/21.
//  Copyright © 2021 SHALINI YADAV. All rights reserved.
//

import UIKit

class RTextCell: UITableViewCell {
    @IBOutlet weak var txtMessage: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var labelTime: UILabel!
    
    var message:MessageClass?{didSet{ self.updateCell() }}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func updateCell() {
        viewMain.layer.cornerRadius = 5
        self.lblName.text = message?.senderName
        self.txtMessage.text = message?.message
        self.labelTime.text = String.getTime(timeStamp:Double(message?.sendingTime ?? 0))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
extension String{
 static func getTime(timeStamp :Double) -> String {
      let time = Double(timeStamp) / 1000
      let date = Date(timeIntervalSince1970: time)
      let dateFormatter = DateFormatter()
           dateFormatter.dateStyle = .short
           dateFormatter.timeZone = .current
           //   dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
           //  "July 27, 2019 03:15 PM"
//     dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
           dateFormatter.dateFormat = "hh:mm a"
           dateFormatter.locale =  Locale(identifier:  "en")
           let localDate = dateFormatter.string(from: date)
           return String(localDate)
   }
}
