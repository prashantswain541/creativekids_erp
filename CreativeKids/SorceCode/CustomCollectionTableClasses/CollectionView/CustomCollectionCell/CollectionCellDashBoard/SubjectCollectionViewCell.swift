//
//  SubjectCollectionViewCell.swift
//  CreativeKids(Updated)
//
//  Created by Creative Kids on 12/04/21.
//

import UIKit

class SubjectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageSubject: UIImageView!
    @IBOutlet weak var labelSubjectName: UILabel!
   
    func cellConfig(object:Any,language:String){
        guard let kData = object as? SubjectModel else{return}
        let ImageBaseUrl = (kData.subjectImage ?? "").contains("http") ? "" : kImageBaseUrl
        DispatchQueue.main.async {
            let imageUrl = ImageBaseUrl + ((kData.subjectImage ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
            self.imageSubject.sd_setImage(with: URL(string:imageUrl), placeholderImage: #imageLiteral(resourceName: "placeholder_btn"), options: .continueInBackground, completed: nil)
//            self.imageSubject.downloadImageFromURL(urlString: imageUrl)
        }
        self.labelSubjectName.setfont(font: 13, fontFamily: futuraFont)
        self.labelSubjectName.text = kData.subjectName
    }
}
