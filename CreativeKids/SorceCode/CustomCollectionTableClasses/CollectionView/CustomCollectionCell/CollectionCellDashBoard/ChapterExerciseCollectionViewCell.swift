//
//  ChapterExerciseCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 12/07/21.
//

import UIKit

class ChapterExerciseCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelExerciseNumber: UILabel!
    @IBOutlet weak var ImageExercise: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configCell(cellData: Any,index: Int, language:String){
        guard let data = cellData as? ExerciseModel else {return}
        data.exerciseNumber = index + 1
        exerciseName(type:data.exerciseType ?? "")
        if isHindiSubject(subejct: language){
            self.labelExerciseNumber.setfont(font: 15, fontFamily: chanakyaFont)
            labelExerciseNumber.attributedText =  getNSAtributedStringWithNumber(forConvertString: "\(ConstantHindiText.abhyaas) \(index + 1)")
        }else{
            self.labelExerciseNumber.setfont(font: 13, fontFamily: futuraFont)
            labelExerciseNumber.text = "Exercise \(data.exerciseNumber ?? 1)"
        }
        
    }
    
    func exerciseName(type:String){
        switch type {
        case "MCQ":
            ImageExercise.image = #imageLiteral(resourceName: "mcq_btn")
        case "VSAQ":
            ImageExercise.image = #imageLiteral(resourceName: "vsaq_btn")
        case "SAQ":
            ImageExercise.image = #imageLiteral(resourceName: "saq_btn")
        case "LAQ":
            ImageExercise.image = #imageLiteral(resourceName: "laq_btn")
        default:
            ImageExercise.image = #imageLiteral(resourceName: "dnd_btn")
        }
    }
}
