//
//  SubScribeSubjectCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 02/06/21.
//

import UIKit

class SubScribeSubjectCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageSubject: UIImageView!
    @IBOutlet weak var labelSubjectName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func cellConfig(object:Any,language:String){
        guard let kData = object as? SubjectModel else{return}
        DispatchQueue.main.async {
            self.imageSubject.sd_setImage(with: URL(string:kData.subjectImage ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_btn"), options: .continueInBackground, completed: nil)
        }
        self.labelSubjectName.setfont(font: 13, fontFamily: futuraFont)
        self.labelSubjectName.text = kData.subjectName
    }

}
