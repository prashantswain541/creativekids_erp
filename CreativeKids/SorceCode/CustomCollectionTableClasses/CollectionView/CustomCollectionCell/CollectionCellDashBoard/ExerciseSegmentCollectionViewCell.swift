//
//  ExerciseSegmentCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 12/07/21.
//

import UIKit

class ExerciseSegmentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var labelContent: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backView?.layer.cornerRadius = self.backView.height/2
        self.backView?.layer.borderWidth = 1
        self.backView?.layer.borderColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
    }

    func configCell(cellData: Any,index:Int){
        guard let data = cellData as? SegmentModel else {return}
        self.labelContent.textColor = data.isSelected ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1):#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.backView?.backgroundColor = data.isSelected ? #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1):#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.labelContent.text = data.contentText
    }
}
