//
//  StudentDashboardCollectionCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/09/22.
//

import UIKit
import AVKit

class StudentDashboardCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var dashboardImage: UIImageView!
    @IBOutlet weak var dashboardDataLabel: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var buttonPlayVideo: UIButton!
    var playerController:AVPlayerViewController?
    var callBack:(()->Void)?
    
    func configCell(data:Any) {
        self.viewBackground.drawShadowOnCell()
        self.dashboardImage.contentMode = .scaleToFill
        if data is EventModel{
            guard let eventData = data as? EventModel else{return}
            if eventData.mediaType == .Image{
                setImage(imageData:eventData)
            }else{
                setVideo(videoData:eventData)
            }
        }else if data is YoutubeDataModel{
            guard let youtubeData = data as? YoutubeDataModel else{return}
            setYoutubeData(youtubeData: youtubeData)
        }
        
    }
    
    func setImage(imageData:EventModel){
        dashboardDataLabel.text = imageData.description
        buttonPlayVideo.isHidden = true
        let baseUrl = "https://creativekidssolutions.com/Areas/Teacher/Content/img/Gallery/"
        viewBackground.backgroundColor = .white
        DispatchQueue.main.async {
            self.dashboardImage.downloadImageFromURL(urlString: baseUrl + imageData.imageURL)
        }
    }
    
    func setYoutubeData(youtubeData:YoutubeDataModel){
        dashboardDataLabel.text = youtubeData.titleName
        buttonPlayVideo.isHidden = false
        viewBackground.backgroundColor = .black
        DispatchQueue.main.async {
            self.dashboardImage.downloadImageFromURL(urlString: youtubeData.videoThumb ?? "")
        }
    }
    
    @IBAction func buttonPlayTapped(_ sender: UIButton) {
        callBack?()
    }
    func setVideo(videoData:EventModel){
        dashboardDataLabel.text = videoData.videoTitle
        buttonPlayVideo.isHidden = false
        viewBackground.backgroundColor = .black
        //        playerController = AVPlayerViewController()
        DispatchQueue.main.async {
            videoData.videoURL.getVideoThumbnailYouTube{ [weak self] (thumbNailImage) in
                if let image = thumbNailImage{
                    self?.dashboardImage.downloadImageFromURL(urlString: image)
                }else{
                    
                }
            }
        }
        //            self.setVideo(self.viewBackground, videoURL:videoData.videoURL, playerController: self.playerController!)
    }
}

//extension StudentDashboardCollectionCell:AVPlayerViewControllerDelegate{
//    func setVideo(_ view: UIView, videoURL:String,playerController:AVPlayerViewController){
//        guard let playvideoURL = URL(string: "\(videoURL )") else {return}
//        let player = AVPlayer(url: playvideoURL)
//        // playerController = AVPlayerViewController()
//        playerController.player = player
//        playerController.view.frame = view.bounds
//        playerController.allowsPictureInPicturePlayback = true
//        playerController.delegate = self
//        view.addSubview(playerController.view)
//        playerController.didMove(toParent: self.parentContainerViewController())
//        playerController.player?.pause()
//        playerController.player?.isMuted = true
//        //playerController.play()
//        //self.present(playerController, animated: true, completion: nil)
//    }
//}
