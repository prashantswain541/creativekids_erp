//
//  StudentWorkCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/09/22.
//

import UIKit

class StudentWorkCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewBackGround: UIView!{
        didSet {
            viewBackGround.drawShadowOnCell()
        }
    }
    @IBOutlet weak var viewImageView: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelStudentName: UILabel!
    @IBOutlet weak var labelClass: UILabel!
    @IBOutlet weak var imageChecked: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        viewImageView.layer.cornerRadius = viewImageView.height/2
    }
    
    func configCell(data:Any){
        guard let kData = data as? StudentWork else{return}
        labelStudentName.text = kData.studentName
        labelClass.text = "\(kData.className) \(kData.section)"
        imageChecked.isHidden = !kData.workDetail.isChecked
    }
}
