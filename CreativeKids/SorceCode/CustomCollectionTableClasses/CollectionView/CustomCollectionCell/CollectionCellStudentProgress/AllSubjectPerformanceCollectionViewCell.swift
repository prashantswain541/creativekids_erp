//
//  AllSubjectPerformanceCollectionViewCell.swift
//  CreativeKids
//
//  Created by Rakesh jha on 28/04/21.
//

import UIKit
import PieCharts
class AllSubjectPerformanceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var chartView: PieChart!
    
    let subjectArray = ["social studies","english","hindi","science","history","evs","math"]
    fileprivate static let alpha: CGFloat = 0.5
    let colors = [
        UIColor.yellow.withAlphaComponent(alpha),
        UIColor.green.withAlphaComponent(alpha),
        UIColor.purple.withAlphaComponent(alpha),
        UIColor.cyan.withAlphaComponent(alpha),
        UIColor.darkGray.withAlphaComponent(alpha),
        UIColor.red.withAlphaComponent(alpha),
        UIColor.magenta.withAlphaComponent(alpha),
        UIColor.orange.withAlphaComponent(alpha),
        UIColor.brown.withAlphaComponent(alpha),
        UIColor.lightGray.withAlphaComponent(alpha),
        UIColor.gray.withAlphaComponent(alpha),
    ]
    override func awakeFromNib() {
        super.awakeFromNib()
        chartView.layers = [createPlainTextLayer(), createTextWithLinesLayer()]
        chartView.delegate = self
        chartView.models = createModels()
    }
    
    // MARK: - Models
    fileprivate func createModels() -> [PieSliceModel] {
        let models = subjectArray.map{PieSliceModel(value: 2, color: colors.randomElement() ?? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), subject: $0)}
//        currentColorIndex = models.count
        return models
    }
    
    // MARK: - Layers
    
    fileprivate func createPlainTextLayer() -> PiePlainTextLayer {
        
        let textLayerSettings = PiePlainTextLayerSettings()
        textLayerSettings.viewRadius = 85
        textLayerSettings.hideOnOverflow = true
        textLayerSettings.label.font = UIFont.systemFont(ofSize: 8)
        
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        textLayerSettings.label.textGenerator = {slice in
            return formatter.string(from: slice.data.percentage * 100 as NSNumber).map{"\($0)%"} ?? ""
        }
        
        let textLayer = PiePlainTextLayer()
        textLayer.settings = textLayerSettings
        return textLayer
    }
    
    fileprivate func createTextWithLinesLayer() -> PieLineTextLayer {
        let lineTextLayer = PieLineTextLayer()
        var lineTextLayerSettings = PieLineTextLayerSettings()
        lineTextLayerSettings.lineColor = UIColor.lightGray
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        lineTextLayerSettings.label.font = UIFont.systemFont(ofSize: 10)
        lineTextLayerSettings.label.textGenerator = {slice in
//            return formatter.string(from: slice.data.model.value as NSNumber).map{"\($0)"} ?? ""
            return slice.data.model.subject
        }
        
        lineTextLayer.settings = lineTextLayerSettings
        return lineTextLayer
    }
}

extension AllSubjectPerformanceCollectionViewCell:PieChartDelegate{
    func onSelected(slice: PieSlice, selected: Bool) {
        print("Selected: \(selected), slice: \(slice)")
    }
    
    
}
