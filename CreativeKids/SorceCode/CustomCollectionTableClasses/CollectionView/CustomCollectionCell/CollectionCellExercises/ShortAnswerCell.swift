//
//  ShortAnswerCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/03/21.
//

import UIKit

class ShortAnswerCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var labelHint: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    
    var nextBtnCallback: (() -> ())?
    var selectedTopic:TopicModel?
    
    //MARK: - Configure Cell
    func configCollectionCell(VSAQQuestion: VSAQQuestionModel ,index:IndexPath) {
        
        if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            self.labelQuestion.setfont(font: 18, fontFamily: chanakyaFont)
            self.labelHint.setfont(font: 18, fontFamily: chanakyaFont)
            if isContainNumber(checkString: VSAQQuestion.Questions ?? ""){
                self.labelQuestion.attributedText = checkStringFirstIndexContainingNumber(checkString: VSAQQuestion.Questions?.replacingOccurrences(of: "<br>", with: "\n") ?? "")
            }else{
                self.labelQuestion.text = VSAQQuestion.Questions?.replacingOccurrences(of: "<br>", with: "\n")
            }
            self.labelHint.text = "\(VSAQQuestion.option_A?.replacingOccurrences(of: "<br>", with: "\n") ?? "")"
        } else {
                self.labelQuestion.setAttributedText(text: VSAQQuestion.Questions ?? "",colorText: .white, font: 16)
                self.labelHint.setAttributedText(text: VSAQQuestion.option_A ?? "",colorText: .white, font: 16)
        }
       
        self.image.downlodeImage(serviceurl: VSAQQuestion.img, placeHolder: nil)
    }
    
    //MARK: - Button Action
    @IBAction func nextBtnClicked(_ sender: UIButton) {
        self.nextBtnCallback?()
    }
}


