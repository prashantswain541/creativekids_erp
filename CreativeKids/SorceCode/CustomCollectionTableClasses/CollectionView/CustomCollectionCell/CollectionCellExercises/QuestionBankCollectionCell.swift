//
//  QuestionBankCollectionCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/07/21.
//

import UIKit

class QuestionBankCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var labelQuestions: UILabel!
    @IBOutlet var labelOptions: [UILabel]!
    @IBOutlet var buttonOptions: [UIButton]!
    @IBOutlet weak var buttonConfirmNext: UIButton!
    @IBOutlet weak var questionImage: UIImageView!
    @IBOutlet var viewOptions: [UIView]!
    @IBOutlet weak var imageScrollTop: NSLayoutConstraint!
    @IBOutlet weak var imageQuestionTop: NSLayoutConstraint!
    
    var nextCallback: (() -> ())?
    var selectedOptionCallback: (() -> ())?
    var checkAns = 0
    var titleChangeCount = 0
    var mockTestModel = [VSAQQuestionModel]()
    var mockTest: VSAQQuestionModel?
    
    func configCell(modelData: VSAQQuestionModel?, indexPath: IndexPath) {
        self.mockTest = modelData
        self.titleChangeCount = indexPath.item
        self.viewOptions[0].isHidden = modelData?.option_A == "" ? true : false
        self.viewOptions[1].isHidden = modelData?.option_B == "" ? true : false
        self.viewOptions[2].isHidden = modelData?.option_C == "" ? true : false
        self.viewOptions[3].isHidden = modelData?.option_D == "" ? true : false
        
        if isHindiSubject(subejct: modelData?.subname ?? "") {
//            self.labelOptions.forEach{$0.setfont(font: 18, fontFamily: chanakyaFont)}
            self.labelQuestions.attributedText = makeNewNumericStringFor3And7(StringWithNumber:modelData?.Questions ?? "")
            self.labelOptions[0].setAttributedText(text: modelData?.option_A ?? "", colorText: .white, font: 16,fontFamily: chanakyaFont)
            self.labelOptions[1].setAttributedText(text: modelData?.option_B ?? "", colorText: .white, font: 16,fontFamily: chanakyaFont)
            self.labelOptions[2].setAttributedText(text: modelData?.option_C ?? "", colorText: .white, font: 16,fontFamily: chanakyaFont)
            self.labelOptions[3].setAttributedText(text: modelData?.option_D ?? "", colorText: .white, font: 16,fontFamily: chanakyaFont)
            
        } else {
            
            self.labelQuestions.setAttributedText(text: modelData?.Questions ?? "", colorText: .white, font: 18)
            self.questionImage.downlodeImage(serviceurl: modelData?.img ?? "", placeHolder: nil)
            self.labelOptions[0].setAttributedText(text: modelData?.option_A ?? "", colorText: .white, font: 16)
            self.labelOptions[1].setAttributedText(text: modelData?.option_B ?? "", colorText: .white, font: 16)
            self.labelOptions[2].setAttributedText(text: modelData?.option_C ?? "", colorText: .white, font: 16)
            self.labelOptions[3].setAttributedText(text: modelData?.option_D ?? "", colorText: .white, font: 16)
        }
    }
    
    @IBAction func buttonOptionTapped(_ sender: UIButton) {
        viewChangeBacground(sender: sender)
    }
    @IBAction func nextBtnTapped(_ sender: UIButton) {
        // viewChangeBacground(sender: sender)
        self.checkOptionSelection()
        if sender.titleLabel?.text == "Confirm" {
            self.selectedOptionCallback?()
        } else {
            let selectedButton = buttonOptions.filter{$0.isSelected == true}
            mockTest?.userSelectedOption = selectedButton.first!.tag
            self.nextCallback?()
        }
    }
    //MARK: - Validation
    func checkOptionSelection() {
        if !buttonOptions[0].isSelected && !buttonOptions[1].isSelected && !buttonOptions[2].isSelected && !buttonOptions[3].isSelected {
            buttonConfirmNext.setTitle("Confirm", for: .normal)
            showAlertMessage.alert(message: "Please Select One Option")
            return
        } else {
            self.changeBackground()
        }
    }
    //MARK: - For Changing Selected Button Color According to answer
    func changeBackground() {
        buttonOptions[0].isUserInteractionEnabled = false
        buttonOptions[1].isUserInteractionEnabled = false
        buttonOptions[2].isUserInteractionEnabled = false
        buttonOptions[3].isUserInteractionEnabled = false
        if titleChangeCount == mockTestModel.count - 1 {
            self.buttonConfirmNext.setTitle("Done", for: .normal)
        } else {
            self.buttonConfirmNext.setTitle("Next", for: .normal)
        }
        self.buttonOptions[0].setImage(buttonOptions[0].isSelected == true ? (mockTest?.rightAns != buttonOptions[0].tag ? #imageLiteral(resourceName: "qb_red_btn") : #imageLiteral(resourceName: "qb_green_btn")):#imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[1].setImage(buttonOptions[1].isSelected == true ? (mockTest?.rightAns != buttonOptions[1].tag ? #imageLiteral(resourceName: "qb_red_btn") : #imageLiteral(resourceName: "qb_green_btn")):#imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[2].setImage(buttonOptions[2].isSelected == true ? (mockTest?.rightAns != buttonOptions[2].tag ? #imageLiteral(resourceName: "qb_red_btn") : #imageLiteral(resourceName: "qb_green_btn")):#imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[3].setImage(buttonOptions[3].isSelected == true ? (mockTest?.rightAns != buttonOptions[3].tag ? #imageLiteral(resourceName: "qb_red_btn") : #imageLiteral(resourceName: "qb_green_btn")):#imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        
        switch mockTest?.rightAns {
        case 1:
            self.buttonOptions[0].setImage(#imageLiteral(resourceName: "qb_green_btn"), for: .normal)
        case 2:
            self.buttonOptions[1].setImage(#imageLiteral(resourceName: "qb_green_btn"), for: .normal)
        case 3:
            self.buttonOptions[2].setImage(#imageLiteral(resourceName: "qb_green_btn"), for: .normal)
        default:
            self.buttonOptions[3].setImage(#imageLiteral(resourceName: "qb_green_btn"), for: .normal)
        }
    }
    func viewChangeBacground(sender: UIButton) {
        self.buttonOptions[0].setImage(sender.tag == 1 ? #imageLiteral(resourceName: "qb_blue_btn"): #imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[1].setImage(sender.tag == 2 ? #imageLiteral(resourceName: "qb_blue_btn"): #imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[2].setImage(sender.tag == 3 ? #imageLiteral(resourceName: "qb_blue_btn"): #imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[3].setImage(sender.tag == 4 ? #imageLiteral(resourceName: "qb_blue_btn"): #imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        
        self.buttonOptions[0].isSelected = sender.tag == 1 ? true : false
        self.buttonOptions[1].isSelected = sender.tag == 2 ? true : false
        self.buttonOptions[2].isSelected = sender.tag == 3 ? true : false
        self.buttonOptions[3].isSelected = sender.tag == 4 ? true : false
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.buttonOptions.forEach{$0.setImage(#imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)}
        self.buttonOptions.forEach{$0.isUserInteractionEnabled = true}
        self.buttonOptions.forEach{$0.isSelected = false}
    }
}



