//
//  LTPCollectionCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/03/21.
//

import UIKit

class LTPCollectionCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var labelQuestions: UILabel!
    @IBOutlet weak var viewOptionsA: UIView!
    @IBOutlet weak var viewOptionsB: UIView!
    @IBOutlet weak var viewOptionsC: UIView!
    @IBOutlet weak var viewOptionsD: UIView!
    @IBOutlet var buttonOptions: [UIButton]!
    @IBOutlet var labelOptions: [UILabel]!
    @IBOutlet weak var stackOptions: UIStackView!
    @IBOutlet weak var submitButton: UIButton!
    
    var selectedObject:VSAQQuestionModel?
    var nextCallback: (() -> ())?
    var subjectSelected:SubjectModel?

    
    //MARK: - Configure Cell
    func configCollectionData(LTPQuestions: VSAQQuestionModel?,index:IndexPath) {
        buttonOptions.forEach{$0.titleLabel?.numberOfLines = 0}
        self.viewOptionsA.isHidden = LTPQuestions?.option_A == "" ? true : false
        self.viewOptionsB.isHidden = LTPQuestions?.option_B == "" ? true : false
        self.viewOptionsC.isHidden = LTPQuestions?.option_C == "" ? true : false
        self.viewOptionsD.isHidden = LTPQuestions?.option_D == "" ? true : false
        
        if isHindiSubject(subejct:subjectSelected?.subjectName ?? ""){
            self.labelQuestions.setfont(font: 18, fontFamily: chanakyaFont)
            self.labelOptions.forEach{$0.setfont(font: 18, fontFamily: chanakyaFont)}
//            self.labelQuestions.attributedText = makeNewNumericString(StringWithNumber: LTPQuestions?.Questions ?? "")
//            self.labelOptions[0].attributedText = makeNewNumericString(StringWithNumber:LTPQuestions?.option_A ?? "")
//            self.labelOptions[1].attributedText = makeNewNumericString(StringWithNumber: LTPQuestions?.option_B ?? "")
//            self.labelOptions[2].attributedText = makeNewNumericString(StringWithNumber: LTPQuestions?.option_C ?? "")
//            self.labelOptions[3].attributedText = makeNewNumericString(StringWithNumber: LTPQuestions?.option_D ?? "")
            self.labelQuestions.attributedText = makeNewNumericStringFor3And7(StringWithNumber: LTPQuestions?.Questions ?? "")
            self.labelOptions[0].attributedText = makeNewNumericStringFor3And7(StringWithNumber:LTPQuestions?.option_A ?? "")
            self.labelOptions[1].attributedText = makeNewNumericStringFor3And7(StringWithNumber: LTPQuestions?.option_B ?? "")
            self.labelOptions[2].attributedText = makeNewNumericStringFor3And7(StringWithNumber: LTPQuestions?.option_C ?? "")
            self.labelOptions[3].attributedText = makeNewNumericStringFor3And7(StringWithNumber: LTPQuestions?.option_D ?? "")
            self.labelQuestions.textColor = .black
            self.labelOptions.forEach{$0.textColor = .black}
            
        } else {
                self.labelOptions[0].setAttributedText(text: LTPQuestions?.option_A ?? "",colorText: .black, font: 14)
                self.labelOptions[1].setAttributedText(text: LTPQuestions?.option_B ?? "",colorText: .black, font: 14)
                self.labelOptions[2].setAttributedText(text: LTPQuestions?.option_C ?? "",colorText: .black, font: 14)
                self.labelOptions[3].setAttributedText(text: LTPQuestions?.option_D ?? "",colorText: .black, font: 14)
                self.labelOptions.forEach{$0.setfont(font: 16, fontFamily: futuraFont)}
                self.labelQuestions.setAttributedText(text: LTPQuestions?.Questions ?? "",colorText: .black, font: 14)
                self.labelQuestions.font =  UIFont.systemFont(ofSize: 17, weight: .heavy)
        }
        stackOptions.layoutIfNeeded()
    }
    //MARK: - Button ACTION
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        checkOptionSelection()
    }
    //MARK: - Validation
    func checkOptionSelection() {
        if !buttonOptions[0].isSelected && !buttonOptions[1].isSelected && !buttonOptions[2].isSelected && !buttonOptions[3].isSelected {
            showAlertMessage.alert(message: "Please Select One Option")
            return
        } else {
            let selectedButton = buttonOptions.filter{$0.isSelected == true}
            selectedObject?.userSelectedOption = selectedButton.first!.tag
            self.nextCallback?()
        }
    }
    //MARK: - For Reuse Cell
    override func prepareForReuse() {
        super.prepareForReuse()
        self.buttonOptions.forEach{$0.isSelected = false}
        self.viewOptionsA.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsB.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsC.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsD.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    //MARK: - Button Action
    @IBAction func buttonOptionsTapped(_ sender: UIButton) {
        self.viewChangeBacground(sender: sender)
        
    }
    //MARK: - For Changing Button Color On Selection
    func viewChangeBacground(sender: UIButton) {
        self.viewOptionsA.backgroundColor = sender.tag == 1 ? #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) :#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsB.backgroundColor = sender.tag == 2 ? #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) :#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsC.backgroundColor = sender.tag == 3 ? #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) :#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsD.backgroundColor = sender.tag == 4 ? #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) :#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.buttonOptions[0].isSelected = sender.tag == 1 ? true : false
        self.buttonOptions[1].isSelected = sender.tag == 2 ? true : false
        self.buttonOptions[2].isSelected = sender.tag == 3 ? true : false
        self.buttonOptions[3].isSelected = sender.tag == 4 ? true : false
        
    }
}

