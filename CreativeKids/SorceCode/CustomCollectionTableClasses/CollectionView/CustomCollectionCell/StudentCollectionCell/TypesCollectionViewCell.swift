//
//  TypesCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 04/07/22.
//

import UIKit

class TypesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewTitle: UIImageView!
    var selectedCallBack:(()->Void)?
    
    func configCell(data: Any) {
        guard let dataValue = data as? TYpesModel else {return}
        self.imageViewTitle.image = dataValue.image
    }
    @IBAction func buttonCellTapped(_ sender: UIButton) {
        selectedCallBack?()
    }
}
