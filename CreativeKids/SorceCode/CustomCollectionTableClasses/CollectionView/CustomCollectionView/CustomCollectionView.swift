//
//  CustomCollectionView.swift
//  CreativeKids
//
//  Created by Creative Kids on 10/07/21.
//

import Foundation
import UIKit

 protocol CollectionDataDelegate {
      func getData(cell:String,data:[Any])
      func getData(cell:String,data:[Any],language:String)
      func getData(cell:UICollectionViewCell,data:[Any],language:String)
}
 protocol DataSourcesCollectionDelegate{
      func collectionView(_ collectionView:UICollectionView,cell:UICollectionViewCell)
      func collectionView(_ collectionView:UICollectionView, data:Any,cell:UICollectionViewCell)
      func collectionView(_ collectionView:UICollectionView,data:Any?)
      func collectionView(_ collectionView:UICollectionView,data:Any?, selectedIndex:IndexPath)
      func collectionView(_ collectionView:UICollectionView,data:Any?, selectedIndex:Int)
      func collectionView(_ collectionView:UICollectionView,height:CGFloat)
}
extension CollectionDataDelegate{
    func getData(cell:String,data:[Any]){}
    func getData(cell:String,data:[Any],language:String){}
    func getData(cell:UICollectionViewCell,data:[Any],language:String){}
}

extension DataSourcesCollectionDelegate{
    func collectionView(_ collectionView:UICollectionView,cell:UICollectionViewCell){}
    func collectionView(_ collectionView:UICollectionView, data:Any,cell:UICollectionViewCell){}
    func collectionView(_ collectionView:UICollectionView,data:Any?){}
    func collectionView(_ collectionView:UICollectionView,data:Any?, selectedIndex:IndexPath){}
    func collectionView(_ collectionView:UICollectionView,data:Any?, selectedIndex:Int){}
    func collectionView(_ collectionView:UICollectionView,height:CGFloat){}
}

extension UICollectionView {
    func getNumberOfCell(numberofCell count:Int? ,message messages:String? , messageColor:UIColor = .black) -> Int {
        var noDataLbl : UILabel?
        noDataLbl = UILabel(frame: self.frame)
        noDataLbl?.textAlignment = .center
        noDataLbl?.font = UIFont.boldSystemFont(ofSize: 18)
        noDataLbl?.numberOfLines = 0
        noDataLbl?.center = self.center
        noDataLbl?.textColor = .black
        noDataLbl?.lineBreakMode = .byTruncatingTail
        self.backgroundView = noDataLbl
        if Int.getInt(count) == 0 {
            noDataLbl?.text = messages
        }
//        noDataLbl?.text = ""
        return Int.getInt(count)
    }
}
