//
//  TypesCollectionView.swift
//  CreativeKids
//
//  Created by Creative Kids on 04/07/22.
//

import UIKit

class TypesCollectionView: UICollectionView, UICollectionViewDelegateFlowLayout,UICollectionViewDelegate {
    
    var item = [Any]()
    var dataPassDelegate:DataSourcesCollectionDelegate?
    var customDataSourecs:CustomCollectionViewDataSources<TypesCollectionViewCell,Any>!
    
    var forCollectionViewHeight:Int{
        get{
            let count = item.count
            return (count % 2) == 0 ? count/2 : (count/2) + 1
        }
    }
    
    var modelData = [TYpesModel(title: "Homework", image: UIImage(named: "Homework") ?? UIImage()), TYpesModel(title: "Syllabus", image: UIImage(named: "Syllabus") ?? UIImage()), TYpesModel(title: "Fees", image: UIImage(named: "Fees") ?? UIImage()), TYpesModel(title: "Digital Tutor", image: UIImage(named: "Didital Tutor_1") ?? UIImage())]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    func initialSetup() {
        self.dataPassDelegate?.collectionView(self, height: UIDevice.isPad ? CGFloat(forCollectionViewHeight) * (kScreenWidth/2.5) + 10 : CGFloat(forCollectionViewHeight) * (kScreenWidth/2) + 10)
        self.customDataSourecs = .init(cellIdentifier: CollectionCellIdentifier.typesCollection, item: self.item, completion: { cell, data, index in
            cell.configCell(data: data)
            cell.selectedCallBack = {
                self.dataPassDelegate?.collectionView(self, data: data, selectedIndex: index)
            }
        })
        
        DispatchQueue.main.async {
            self.delegate = self
            self.dataSource = self.customDataSourecs
            self.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: kScreenWidth/2, height: UIDevice.isPad ? kScreenWidth/2.5 : kScreenWidth/2)
    }
    
    
}
extension TypesCollectionView: CollectionDataDelegate {
    func getData(cell: String, data: [Any]) {
        self.item = data
        initialSetup()
    }
}
class TYpesModel {
    var title: String
    var image: UIImage
    
    init(title: String, image: UIImage) {
        self.title = title
        self.image = image
    }
}
