//
//  StudentDashBoardCollection.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/02/22.
//

import UIKit
import AVKit

protocol EventCollectionDataSources{
    func numberOfCell(_ :UICollectionView,_ numberOfCell:Int)
    func selectedCell(_ :UICollectionView,selectedData:Any)
    func scrollAt(_ indexPath:IndexPath)
    func height(_ :UICollectionView, height:CGFloat)
}

class DashBoardEventCollectionView: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
    var navigation: UINavigationController?
    var dashboardType: DashboardType!
    var eventViewModel:HomeEventViewModel?
    var dataPassDataSources:EventCollectionDataSources?
    var items = [Any]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(UINib(nibName: CollectionCellIdentifier.eventCell, bundle: nil), forCellWithReuseIdentifier: CollectionCellIdentifier.eventCell)
        self.delegate = self
        self.dataSource = self
        eventViewModel = HomeEventViewModel()
        eventViewModel?.dataPassDataSources = self
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.dataPassDataSources?.height(self, height:UIDevice.isPad ? kScreenHeight * 0.25 : kScreenHeight * 0.2)
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.eventCell, for: indexPath) as! StudentDashboardCollectionCell
        cell.configCell(data: self.items[indexPath.row])
        cell.callBack = {
            self.dataPassDataSources?.selectedCell(self, selectedData: self.items[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    //MARK: - UIScrollViewDelegate Method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let indexpath = self.indexPathsForVisibleItems
        self.dataPassDataSources?.scrollAt(indexpath.first!)
    }
}

extension DashBoardEventCollectionView:HomeEventDataSources{
    func eventData(data:[Any]){
        self.items = data
        self.dataPassDataSources?.numberOfCell(self, self.items.count)
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}


