//
//  PeriodsCollectionView.swift
//  CreativeKids
//
//  Created by Creative Kids on 23/06/22.
//

import UIKit

class PeriodsCollectionView: UICollectionView, UICollectionViewDelegateFlowLayout {
    
    var item = [Any]()
    var dataPassDelegate:DataSourcesCollectionDelegate?
    var customDataSourecs:CustomCollectionViewDataSources<PeriodsCollectionViewCell,Any>!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    func initialSetup() {
        if item.isEmpty{
            dataPassDelegate?.collectionView(self, height: 0.0)
            return
        }
        dataPassDelegate?.collectionView(self, height: UIDevice.isPhone ? 150 : 200)
        self.customDataSourecs = .init(cellIdentifier: CollectionCellIdentifier.periods, item: self.item, completion: { cell, data, index in
            cell.configCell(data: data)
        })
        
        DispatchQueue.main.async {
            self.delegate = self
            self.dataSource = self.customDataSourecs
            self.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dataPassDelegate?.collectionView(self, data: item[indexPath.item], selectedIndex: indexPath)
    }
}
extension PeriodsCollectionView: CollectionDataDelegate {
    func getData(cell: String, data: [Any]) {
        self.item = data
        self.initialSetup()
    }
}

