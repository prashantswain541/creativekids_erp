//
//  ProgressReportCollectionView.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/04/21.
//

import UIKit
import Foundation

class ProgressReportCollectionView: UICollectionView {
    override func awakeFromNib() {
        super.awakeFromNib()
        setDelegate()
    }
    var item = [Any]()
    var dataSourcesCollection:DataSourcesCollectionDelegate?
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    //MARK: - Set delegate,dataSources
    private func setDelegate() {
        self.register(UINib(nibName: CollectionCellIdentifier.progressReport, bundle: nil), forCellWithReuseIdentifier: CollectionCellIdentifier.progressReport)
        self.delegate = self
        self.dataSource = self
    }
}

//MARK: - UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
extension ProgressReportCollectionView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.progressReport, for: indexPath) as! ProgressReportCollectionViewCell
        cell.cellConfig(object: item[indexPath.item],index: indexPath.item)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.height, height: self.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dataSourcesCollection?.collectionView(self, data: item[indexPath.item], selectedIndex: indexPath)
    }
}

extension ProgressReportCollectionView: CollectionDataDelegate{
    func getData(cell: String, data: [Any]) {
        self.item = data
        self.reloadData()
    }

}
