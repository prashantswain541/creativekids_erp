//
//  OverAllRatingCollectionVew.swift
//  CreativeKids
//
//  Created by Rakesh jha on 07/05/21.
//

import UIKit
import Foundation

class OverAllRatingCollectionVew: UICollectionView {
    override func awakeFromNib() {
        super.awakeFromNib()
        setDelegate()
    }
    
    var item = [Any]()
    var collectionHeight:CGFloat{
        get{
            let count = ceil(Float(self.item.count)/Float(4))
            return (self.width/4) * CGFloat(count)
        }
    }
    var dataSourcesCollection:DataSourcesCollectionDelegate?
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    //MARK: - Set delegate,dataSources
    private func setDelegate() {
        self.register(UINib(nibName: CollectionCellIdentifier.overAllRating, bundle: nil), forCellWithReuseIdentifier: CollectionCellIdentifier.overAllRating)
        self.delegate = self
        self.dataSource = self
    }
}

//MARK: - UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
extension OverAllRatingCollectionVew: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataSourcesCollection?.collectionView(collectionView, height: collectionHeight)
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.overAllRating, for: indexPath) as! OverAllRatingCollectionViewcell
        cell.cellConfig(object: item[indexPath.item])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.width/4, height: self.width/4)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dataSourcesCollection?.collectionView(self, data: item[indexPath.item], selectedIndex: indexPath)
    }
}

extension OverAllRatingCollectionVew: CollectionDataDelegate{
    func getData(cell: String, data: [Any]) {
        self.item = data
        self.reloadData()
    }
}
