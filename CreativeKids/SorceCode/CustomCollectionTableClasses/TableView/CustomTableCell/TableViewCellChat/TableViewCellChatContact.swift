//
//  TableViewCellChatContact.swift
//  CreativeKids
//
//  Created by Creative Kids on 07/03/22.
//

import UIKit
import Foundation
import CoreMedia
import SDWebImage

class TableViewCellChatContact: UITableViewCell {

    @IBOutlet weak var imageContactPerson: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelLastMessage: UILabel!
    @IBOutlet weak var labelUnReadMeesage: UILabel!
    @IBOutlet weak var viewUnReadCount: UIView!
    @IBOutlet weak var imageChat: UIImageView!
    var currentUser:ResentUsers?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageContactPerson.image = UIImage(named: "individual_chat 1")
        self.viewUnReadCount.isHidden = true
        self.labelUnReadMeesage.text = ""
    }
    func configCell(object:Any){
        guard let kdata = object as? ResentUsers else{return}
        currentUser = kdata
        makeInteraction()
        labelName.text = kdata.receiverName
        setImage(object:kdata)
        switch kdata.mediaType {
        case "text":
            imageChat.isHidden = true
            labelLastMessage.text = kdata.lastMessage
        case "photos":
            imageChat.isHidden = false
            labelLastMessage.text = "photo"
        case "pdf":
            imageChat.isHidden = false
            labelLastMessage.text = "pdf"
        case "join","leave","create","kickOutByAdmin":
            manageLastMessage(user: kdata)
        default:
            imageChat.isHidden = true
            labelLastMessage.text = kdata.lastMessage
        }
        labelTime.text = String.getTime(timeStamp:Double(kdata.sendingTime))
        DispatchQueue.main.async {
            if !(Int.getint(kdata.readCount) == 0) {
                self.viewUnReadCount.isHidden = false
                self.viewUnReadCount.layer.cornerRadius = self.viewUnReadCount.frame.height/2
                self.labelUnReadMeesage.text = String.getstring(kdata.readCount)
            } else {
                self.viewUnReadCount.isHidden = true
                self.labelUnReadMeesage.text = ""
            }
        }
    }
    
    func makeInteraction(){
        if !(String.getstring(self.currentUser?.createdBy).isEmpty) && (String.getstring(kUserData.userId) == String.getstring(self.currentUser?.senderId)){
            let menuInteraction = UIContextMenuInteraction(delegate: self)
            self.addInteraction(menuInteraction)
        }
    }
    
    func setImage(object:ResentUsers){
//      self.imageContactPerson.downlodeImage(serviceurl: String.getstring(object.imageUrl), placeHolder: UIImage(systemName: "person.3"))
        if String.getstring(object.imageUrl).isEmpty{
            self.imageContactPerson.image = String.getstring(object.createdBy).isEmpty ? UIImage(named: "individual_chat 1") : UIImage(named: "Group_Chat 1")
        }else{
            self.imageContactPerson.sd_setImage(with: URL(string: String.getstring(object.imageUrl)))
        }
        
    }
    
    func manageLastMessage(user:ResentUsers){
        switch user.mediaType{
        case "create":
            create(userObject: user)
        case "join":
            join(userObject: user)
        case "kickOutByAdmin":
            kickOutByAdmin(userObject: user)
        case "leave":
            leave(userObject: user)
        default:
            return
        }
    }
}

extension TableViewCellChatContact{
    func create(userObject:ResentUsers){
        let userDetail = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userObject.senderId) == String.getstring(userDetail.userId)
        {
            labelLastMessage.text = "You created this group"
        }else{
            ChatHalper.shared.userReference.child(String.getstring(userObject.senderId)).observe(.value) { [weak self] (snapshot) in
                if snapshot.exists(){
                   let userDetail = chatSharedInstanse.getDictionary(snapshot.value)
                    self?.labelLastMessage.text = "You were added by \(userDetail[ChatParameter.name] ?? "")"
                }
            }
           
        }
    }
    
    func join(userObject:ResentUsers){
        let userDetail = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userObject.senderId) == String.getstring(userDetail.userId){
            labelLastMessage.text = "You added people into this group"
        }else{
            if String.getstring(userObject.lastMessage).components(separatedBy: ",").contains(String.getstring(userDetail.userId)){
                ChatHalper.shared.userReference.child(String.getstring(userObject.senderId)).observe(.value) { [weak self] (snapshot) in
                    if snapshot.exists(){
                       let userDetail = chatSharedInstanse.getDictionary(snapshot.value)
                        self?.labelLastMessage.text = "You were added by \(userDetail[ChatParameter.name] ?? "")"
                    }
                }
            }else{
                self.labelLastMessage.text = "people added by admin"
            }
        }
    }
    
    func kickOutByAdmin(userObject:ResentUsers){
        let userDetail = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userObject.senderId) == String.getstring(userDetail.userId) {
            labelLastMessage.text = "You removed people into this group"
        }else{
            if String.getstring(userObject.lastMessage).components(separatedBy: ",").contains(String.getstring(userDetail.userId)){
                self.labelLastMessage.text = "You were removed by admin"
            }else{
                self.labelLastMessage.text = "people removed by admin"
            }
        }
    }
    
    func leave(userObject:ResentUsers){
        let userDetail = UsersState(userdata: chatUserDefault.getuserDetails())
        if String.getstring(userObject.senderId) == String.getstring(userDetail.userId) {
            labelLastMessage.text = "You left"
        }else{
            ChatHalper.shared.userReference.child(String.getstring(userObject.senderId)).observe(.value) { [weak self] (snapshot) in
                if snapshot.exists(){
                   let userDetail = chatSharedInstanse.getDictionary(snapshot.value)
                    self?.labelLastMessage.text = "\(userDetail[ChatParameter.name] ?? "") left"
                }
            }
        }
    }
    
}

extension TableViewCellChatContact:UIContextMenuInteractionDelegate{
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(actionProvider:  { suggestedAction in
            let gropuDeleteAction = UIAction(title:"Delete") { action in
                if !(String.getstring(self.currentUser?.createdBy).isEmpty){
                    ChatHalper.shared.deleteGroup(user:self.currentUser)
                }
            }
            return UIMenu(title:"Yeah",children: [gropuDeleteAction])
        })
    }
    
    
}
