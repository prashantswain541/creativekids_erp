//
//  PlaybackSpeedTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 31/01/22.
//
import Foundation
import UIKit

class PlaybackSpeedTableViewCell: UITableViewCell {

    @IBOutlet weak var labelSpeed: UILabel!
    @IBOutlet weak var imageSpeedSelection: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configCell(object: Any){
        guard let data = object as? PlayerSpeedModel else {return}
        self.imageSpeedSelection.isHidden = !data.isSelected
        self.labelSpeed.text = data.name
        self.labelSpeed.textColor = data.isSelected ? .systemBlue : .white
    }
}
