//
//  ContinueLearningTableVeiwCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 17/04/21.
//

import UIKit

class ContinueLearningTableVeiwCell: UITableViewCell {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var labelTopicName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(object: Any){
        guard let data = object as? TopicSearchModel else {return}
        if isHindiSubject(subejct:data.subjectName ?? ""){
            self.labelTopicName.setfont(font: 15, fontFamily: chanakyaFont)
        }else{
            self.labelTopicName.setfont(font: 13, fontFamily: futuraFont)
        }
        self.labelTopicName.text = data.chapterName
    }
    
}
