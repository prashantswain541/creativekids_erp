//
//  TopicSearchTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/04/21.
//

import UIKit
import Foundation

class TopicSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTopicName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(object:Any){
        print(object)
        guard let kdata = object as? TopicSearchModel else{return}
        labelTopicName.text = kdata.showTopic
    }
    
}
