//
//  AnswerImagesTableViewCell.swift
//  CreativeKids
//
//  Created by Prashant Swain on 09/10/22.
//

import UIKit
let homeWorkImageBaseUrl = "https://creativekidssolutions.com/"

class AnswerImagesTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewAnswer: UIImageView!
    @IBOutlet weak var viewHeader:UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(data:Any){
        if let imageData = data as? String{
            let imageUrl = imageData.contains("http") ? imageData : (homeWorkImageBaseUrl + imageData)
            imageViewAnswer.downloadImageFromURL(urlString: imageUrl)
        }else{
            if let imageData = data as? UIImage{
                imageViewAnswer.image = imageData
            }
        }
    }
}
