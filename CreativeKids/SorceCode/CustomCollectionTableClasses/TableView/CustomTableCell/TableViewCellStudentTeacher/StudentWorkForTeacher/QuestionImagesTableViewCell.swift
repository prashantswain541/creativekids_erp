//
//  QuestionImagesTableViewCell.swift
//  CreativeKids
//
//  Created by Prashant Swain on 09/10/22.
//

import UIKit
protocol PassNestedCollectionBackDataSources{
    func getSelectedCell(_ tableCell:UITableViewCell,data: Any?,selectedIndex: Int)
}
class QuestionImagesTableViewCell: UITableViewCell {

    @IBOutlet weak var questionCollectionView: HomeWorkCollectioView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var dataPassDataBack:PassNestedCollectionBackDataSources?
    func configCell(data:Any){
        questionCollectionView?.dataPassDelegate = self
        guard let kData = data as? [String] else{return}
        questionCollectionView.getData(cell: CollectionCellIdentifier.questionImageCell, data: kData)
    }
}

extension QuestionImagesTableViewCell:DataSourcesCollectionDelegate{
    func collectionView(_ collectionView: UICollectionView, height: CGFloat) {
        self.heightConstraint.constant = height
    }
    
    func collectionView(_ collectionView: UICollectionView, data: Any?, selectedIndex: Int) {
        dataPassDataBack?.getSelectedCell(self, data: data, selectedIndex: selectedIndex)
    }
}
