//
//  QuestionChapterTableViewCell.swift
//  CreativeKids
//
//  Created by Prashant Swain on 09/10/22.
//

import UIKit

class QuestionChapterTableViewCell: UITableViewCell {

    @IBOutlet weak var labelChapterQuestion: UILabel!
    @IBOutlet weak var viewMain: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configCell(data:Any){
        guard let kData = data as? String else{return}
        labelChapterQuestion.text = kData
    }

}
