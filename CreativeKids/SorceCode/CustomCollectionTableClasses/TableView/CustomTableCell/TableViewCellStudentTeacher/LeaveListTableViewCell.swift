//
//  LeaveListTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/03/22.
//

import UIKit

class LeaveListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIView! {
        didSet {
            viewBackground.drawShadowOnCell()
        }
    }
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var labelStudentName: UILabel!
    @IBOutlet weak var labelSubject: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configCell(data:Any?){
        guard let kData = data as? LeaveModel else{return}
        labelStudentName.attributedText = createAttributedText(firstText: "Student Name: ", secondText: "\(kData.UserName ?? "")")
        labelSubject.attributedText = createAttributedText(firstText: "Subject: ", secondText: "\(kData.applicationSubject ?? "")")
        labelDate.attributedText = createAttributedText(firstText: "Date: ", secondText: String.convertDateString(dateString: "\(kData.date ?? "")", fromFormat: kUserData.role == "2" ? "yyyy-MM-dd'T'hh:mm:ss" : "dd-MM-yyyy", toFormat: "dd-MM-yyyy"))
        statusView(data:kData)
    }
    
    func statusView(data:LeaveModel){
        _ = kSharedUserDefaults.getLoggedInUserDetails()["role"] as? String
        switch data.leaveFor{
        case .teacher, .student:
            labelStudentName.isHidden = true
            viewStatus.isHidden = false
            if !(data.isApproved) && !(data.isDeclined){
                viewStatus.backgroundColor = .blue
                labelStatus.text = "Pending"
            }else if data.isApproved{
                viewStatus.backgroundColor = .systemGreen
                labelStatus.text = "Approved"
            }else if data.isDeclined{
                viewStatus.backgroundColor = .red
                labelStatus.text = "Declined"
            }
        case .teacherForApproval:
            labelStudentName.isHidden = false
            viewStatus.isHidden = true
        }
    }
}
