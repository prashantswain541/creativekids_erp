//
//  HomeWorkListTableCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/02/22.
//

import UIKit

class HomeWorkListTableCell: UITableViewCell {
    
    enum CellType{
        case Add
        case Update
    }
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var buttonHomework: UIButton!
    
    @IBOutlet weak var labelPeriodNumber: UILabel!
    @IBOutlet weak var labelTeacherName: UILabel!
    @IBOutlet weak var labelSubjectName: UILabel!
    @IBOutlet weak var buttonAdd: UIButton!
    
    var studentButtonCallback: ((_:CellType)->())?
    var cellTapped:(()->())?
    var data:HomeWork?
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBackground.drawShadowOnCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configCell(data:Any){
        guard let kData = data as? HomeWork else {return}
        self.data = kData
        labelPeriodNumber.attributedText = createAttributedText(firstText: "Period No.: ", secondText: "\(kData.periodNo ?? "")")
        labelTeacherName.attributedText = createAttributedText(firstText: "Teacher's Name: ", secondText: "\(kData.teacherName ?? "")")
        labelSubjectName.attributedText = createAttributedText(firstText: "Subject: ", secondText: "\(kData.subjectName ?? "")")
        buttonAdd.isHidden = (kData.studentHomeworkDetails?.isHomeworkChecked ?? false) && (kData.studentHomeworkDetails?.isSubmittedByStudent ?? false)
        buttonAdd.setTitle(kData.studentHomeworkDetails?.isSubmittedByStudent ?? false ? "  Update  " : "  Homework  ", for: .normal)
        
    }
    @IBAction func buttonHomeworkTapped(_ sender: UIButton) {
        self.studentButtonCallback?((data?.studentHomeworkDetails?.isSubmittedByStudent ?? false) ? .Update : .Add)
    }
    
    @IBAction func buttonCellTapped(_ sender: UIButton) {
        if (data?.studentHomeworkDetails?.isHomeworkChecked ?? false) && (data?.studentHomeworkDetails?.isSubmittedByStudent ?? false){
            self.cellTapped?()
        }
    }
    
}
