//
//  StudentHomeworkListTableCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 18/05/22.
//

import UIKit

class StudentHomeworkListTableCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIView!  {
        didSet {
            viewBackground.drawShadowOnCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
