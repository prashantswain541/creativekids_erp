//
//  PeriodListTableViewCell.swift
//  CreativeKids
//
//  Created by creativekids solutions on 02/09/22.
//

import UIKit

class PeriodListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIView! {
        didSet {
            viewBackground.drawShadowOnCell()
        }
    }
    
    var addHomeworkCallback: (()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(data:Any){
        guard let kData = data as? HomeWorkModel else{return}
        
    }
    @IBAction func addHomeworkTapped(_ sender: UIButton) {
        self.addHomeworkCallback?()
    }

}
