//
//  DNDTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 30/03/21.
//

import UIKit
import DropDown

class DNDTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var questionStr1: UILabel!
    @IBOutlet weak var questionStr2: UILabel!
    @IBOutlet weak var questionStr3: UILabel!
    @IBOutlet weak var questionTextField1Width: NSLayoutConstraint!
    @IBOutlet weak var questionTextField2Width: NSLayoutConstraint!
    @IBOutlet var questionsTextFields: [UITextField]!
    
    var dataModel: VSAQQuestionModel?
    var selectedTopic:TopicModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        openDropDown()
    }
    //MARK: - Drop Down
    func openDropDown() {
        let tapField1 = UITapGestureRecognizer(target: self, action: #selector(tap1TextField(_:)))
        self.questionsTextFields[0].addGestureRecognizer(tapField1)
        
        let tapField2 = UITapGestureRecognizer(target: self, action: #selector(tap2TextField(_:)))
        self.questionsTextFields[1].addGestureRecognizer(tapField2)
    }
    
    //MARK: - Configure Cell
    func configCell(DNDData: VSAQQuestionModel?) {
        self.dataModel = DNDData
        self.questionsTextFields[0].isHidden = true
        self.questionsTextFields[1].isHidden = true
        self.questionTextField1Width.constant = 0
        self.questionTextField2Width.constant = 0
        
        if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            self.questionStr1.font = UIFont(name: chanakyaFont, size: 18)
            self.questionsTextFields[0].font = UIFont(name: chanakyaFont, size: 18)
            self.questionStr2.font = UIFont(name: chanakyaFont, size: 18)
            self.questionStr3.font = UIFont(name: chanakyaFont, size: 18)
            self.questionsTextFields[1].font = UIFont(name: chanakyaFont, size: 18)
            self.questionsTextFields[0].placeholder = "चयन करें"
            self.questionsTextFields[1].placeholder = "चयन करें"
        } else {
            self.questionStr1.setfont(font: 15, fontFamily: futuraFont)
            self.questionStr2.setfont(font: 15, fontFamily: futuraFont)
            self.questionsTextFields[0].font = UIFont(name: futuraFont , size: 15)
            self.questionsTextFields[1].font = UIFont(name: futuraFont , size: 15)
            self.questionStr3.font = UIFont(name: futuraFont , size: 15)
            self.questionsTextFields[0].placeholder = "Choose"
            self.questionsTextFields[1].placeholder = "Choose"
        }
        if DNDData?.textArrSplit.count == 1 {
            if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
                self.questionStr1.attributedText = makeNewNumericString(StringWithNumber: DNDData?.textArrSplit[0].replacingOccurrences(of: "<br>", with: "\n") ?? "")
            }else{
                
                self.questionStr1.setAttributedText(text: DNDData?.textArrSplit[0].replacingOccurrences(of: "<br>", with: "\n") ?? "", colorText: .black,font: 16)
            }
            DNDData?.textfieldFirst = ""
            DNDData?.texrFieldSecond = ""
        }
        if DNDData?.textArrSplit.count == 2 {
            if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
                self.questionStr1.attributedText = makeNewNumericString(StringWithNumber: DNDData?.textArrSplit[0].replacingOccurrences(of: "<br>", with: "\n") ?? "")
            }else{
                self.questionStr1.setAttributedText(text: DNDData?.textArrSplit[0].replacingOccurrences(of: "<br>", with: "\n") ?? "", colorText: .black,font: 16)
                self.questionStr2.setAttributedText(text: DNDData?.textArrSplit[1].replacingOccurrences(of: "<br>", with: "\n") ?? "", colorText: .black,font: 16)
            }
            self.questionsTextFields[0].isHidden = false
            self.questionTextField1Width.constant = 85
            
            DNDData?.texrFieldSecond = ""
        }
        if DNDData?.textArrSplit.count == 3 {
            if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
                self.questionStr1.attributedText = makeNewNumericString(StringWithNumber: DNDData?.textArrSplit[0].replacingOccurrences(of: "<br>", with: "\n") ?? "")
            }else{
                self.questionStr1.setAttributedText(text: DNDData?.textArrSplit[0].replacingOccurrences(of: "<br>", with: "\n") ?? "", colorText: .black,font: 16)
                self.questionStr2.setAttributedText(text: DNDData?.textArrSplit[1].replacingOccurrences(of: "<br>", with: "\n") ?? "", colorText: .black,font: 16)
                self.questionStr3.setAttributedText(text: DNDData?.textArrSplit[2].replacingOccurrences(of: "<br>", with: "\n") ?? "", colorText: .black,font: 16)
            }
            self.questionsTextFields[0].isHidden = false
            self.questionTextField1Width.constant = 85
            self.questionsTextFields[1].isHidden = false
            self.questionTextField2Width.constant = 85
        }
    }
    
    func getNSAtributedStringWithNumber(forConvertString:String)->NSAttributedString{
        let myChapterNumber = "\(getNumericFromString(stringWithNumber: forConvertString) ?? 0)"
        let myNumberAttribute = [ NSAttributedString.Key.font: UIFont(name: futuraFont, size: 12.0)!]
        let myAttrNumberString = NSAttributedString(string: myChapterNumber, attributes: myNumberAttribute)
        let removedNumber = removeNumberFromString(stringWithNumber: forConvertString)
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!]
        let myAttrQuestionString = NSAttributedString(string: removedNumber, attributes: myAttribute)
        return myAttrNumberString + myAttrQuestionString
    }
    
    
    //MARK: - DropDown Tapgesture
    @objc func tap1TextField(_ sender: UITapGestureRecognizer) {
        var datasource = [String]()
        let options = ["\(dataModel?.option_A ?? "")", "\(dataModel?.option_B ?? "")", "\(dataModel?.option_C ?? "")", "\(dataModel?.option_D ?? "")"]
        options.forEach { option in
            if !option.isEmpty{
                datasource.append(option)
            }
        }
        let dropDown = DropDown()
        dropDown.anchorView = questionsTextFields[0]
        dropDown.dataSource = datasource
        if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            dropDown.textFont = UIFont(name: chanakyaFont, size: 18)!
        } else {
            dropDown.textFont = UIFont(name: futuraFont , size: 18)!
        }
        
        dropDown.selectionAction = { (index: Int, item: String) in
            if isHindiSubject(subejct:self.selectedTopic?.subjectName ?? ""){
                self.questionsTextFields[0].attributedText = makeNewNumericString(StringWithNumber: item)
                self.questionsTextFields[0].font = UIFont(name: chanakyaFont, size: 18) ?? UIFont()
                self.dataModel?.userSelectedOption = index + 1
                self.dataModel?.textfieldFirst = item
            } else {
                self.questionsTextFields[0].text = item
                self.questionsTextFields[0].font = UIFont(name: futuraFont , size: 18)
                self.dataModel?.userSelectedOption = index + 1
                self.dataModel?.textfieldFirst = item
            }
        }
        dropDown.show()
    }
    @objc func tap2TextField(_ sender: UITapGestureRecognizer) {
        var datasource = [String]()
        let options = ["\(dataModel?.option_A ?? "")", "\(dataModel?.option_B ?? "")", "\(dataModel?.option_C ?? "")", "\(dataModel?.option_D ?? "")"]
        options.forEach { option in
            if !option.isEmpty{
                datasource.append(option)
            }
        }
        CommonUtils.showDropDown(sender: questionsTextFields[1], dataSources: datasource) { [unowned self] (index, itemText) in
            if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
                self.questionsTextFields[1].attributedText = makeNewNumericString(StringWithNumber: itemText)
                self.questionsTextFields[1].font = UIFont(name: chanakyaFont, size: 18) ?? UIFont()
                self.dataModel?.userSelectedOption = index + 1
                self.dataModel?.texrFieldSecond = itemText
            } else {
                self.questionsTextFields[1].text = itemText
                self.questionsTextFields[1].font = UIFont(name: futuraFont , size: 18)
                self.dataModel?.userSelectedOption = index + 1
                self.dataModel?.texrFieldSecond = itemText
            }
        }
    }
}
