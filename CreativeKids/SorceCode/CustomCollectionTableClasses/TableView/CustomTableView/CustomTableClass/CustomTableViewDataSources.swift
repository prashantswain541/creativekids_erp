//
//  customTableViewDataSources.swift
//  CreativeKids
//
//  Created by Creative Kids on 18/03/21.
//

import Foundation
import UIKit

class CustomTableViewDataSources<CELL:UITableViewCell,T>:NSObject,UITableViewDataSource{
    
    var cellIdentifier:String?
    var item = [T]()
    var configCell:((CELL,T,Int)->())
    var message:String?
    
    init(cellIdentifier:String,item:[T],message:String = "No data found",completion: @escaping ((CELL,T,Int)->())) {
        self.cellIdentifier = cellIdentifier
        self.message = message
        self.item = item
        self.configCell = completion
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.getNumberOfRow(numberofRow: item.count, message: self.message, messageColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier ?? "", for: indexPath)
            as! CELL
        configCell(cell,item[indexPath.row],indexPath.row)
        return cell
    }
}
 
