//
//  ChapterExerciseTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 12/07/21.
//

import Foundation
import UIKit

class ChapterExerciseTableView:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<ChapterExerciseTableViewCell,Any>!
    var cellObjejct:UITableViewCell?
    var language:String?
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: TableCellIdentifier.chapterExercisecell, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.chapterExercisecell)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.chapterExercisecell, item: self.item, completion: { (cell, data, index) in
            self.cellObjejct = cell
            cell.exerciseCallBackBack = { (exerciseData,selectedExercise) in
                self.dataPassDelegate?.tableViewCallBack(self, selectedIndex: index, data: exerciseData, cell: self.cellObjejct ?? ChapterExerciseTableViewCell())
            }
            self.separatorStyle = .none
            cell.configCell(object: data, language: self.language ?? "", index:  index)
        })
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
}
extension ChapterExerciseTableView:TableDataDelegate {
    func getData(cell: String, data: [Any], language: String) {
        self.tableCellId = cell
        self.item = data
        self.language = language
        initialSet()
    }
}
