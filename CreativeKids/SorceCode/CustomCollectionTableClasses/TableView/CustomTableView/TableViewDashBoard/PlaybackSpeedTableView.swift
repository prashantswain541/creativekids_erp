//
//  PlaybackSpeedTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 31/01/22.
//

import Foundation
import UIKit

class PlaybackSpeedTableView:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<PlaybackSpeedTableViewCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: TableCellIdentifier.playBackSpeedCell, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.playBackSpeedCell)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.playBackSpeedCell, item: self.item, completion: { (cell, data, index) in
            self.separatorStyle = .singleLine
            cell.configCell(object: data)
        })
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.height / CGFloat(item.count)
    }
    
}
extension PlaybackSpeedTableView:TableDataDelegate {
    func getData(cell:String,data:[Any]){
        self.tableCellId = cell
        self.item = data
        initialSet()
    }
}
