//
//  ChapterLTPTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/07/21.
//

import Foundation
import UIKit

class ChapterLTPTableView:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<ChapterLTPTableViewCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: TableCellIdentifier.chapterLTPCell, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.chapterLTPCell)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.chapterLTPCell, item: self.item, completion: { (cell, data, index) in
            self.separatorStyle = .none
            cell.configCell(object: data,index: index)
        })
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
extension ChapterLTPTableView:TableDataDelegate {
    func getData(cell: String, data: [Any], language: String) {
        self.tableCellId = cell
        self.item = data
        initialSet()
    }
}
