//
//  chapterQuestionBankTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/07/21.
//

import Foundation
import UIKit

class ChapterQuestionBankTableView:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var pdfData = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<ChapterQuestionBankTableViewCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: TableCellIdentifier.chapterQuestionBank, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.chapterQuestionBank)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.chapterQuestionBank, item: self.item, completion: { (cell, data, index) in
            self.separatorStyle = .none
            cell.configCell(object: data,index: index)
            cell.buttonGetPdf.isHidden = self.pdfData.count < 1 ? true : false
            cell.getPDFCallback = {
                self.dataPassDelegate?.tableViewCallBack(self, selectedIndex: index, data: data, cell: cell)
            }
        })
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
extension ChapterQuestionBankTableView:TableDataDelegate {
    func getDataWithAdditionalData(cell:String,data:[Any],language:String,otherData:[Any]){
        self.tableCellId = cell
        self.item = data
        self.pdfData = otherData
        initialSet()
    }
}

