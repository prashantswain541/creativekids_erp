//
//  DateHomeworkTableView.swift
//  CreativeKids
//
//  Created by creativekids solutions on 01/09/22.
//

import UIKit

class DateHomeworkTableView: UITableView, UITableViewDelegate {
    
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<DateHomeworkTableViewCell,Any>!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    
    func initialSetup() {
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.dateHomework, item: self.item, completion: { cell, data, index in
            cell.configCell(data: data)
        })
        DispatchQueue.main.async {
            self.delegate = self
            self.dataSource = self.customDataSourecs
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: item[indexPath.row])
    }
}
extension DateHomeworkTableView: TableDataDelegate {
    func getData(cell:String,data:[Any]) {
        self.item = data
        self.initialSetup()
    }
}
