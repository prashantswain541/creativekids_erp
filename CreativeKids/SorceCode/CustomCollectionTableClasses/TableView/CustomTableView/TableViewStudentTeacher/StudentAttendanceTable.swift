//
//  StudentAttendanceTable.swift
//  CreativeKids
//
//  Created by Creative Kids on 05/03/22.
//

import UIKit

class StudentAttendanceTable: UITableView, UITableViewDelegate {
    
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<StudentAttendanceTableCell,Any>!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.delegate = self
        initialSetup()
    }
    
    func initialSetup() {
        self.register(UINib(nibName: TableCellIdentifier.studentAttendance, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.studentAttendance)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.studentAttendance, item: self.item, completion: { cell, data, index in
            
        })
        
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension StudentAttendanceTable:TableDataDelegate {
    func getData(cell:String,data:[Any]){
        self.item = data
        initialSetup()
    }
}
