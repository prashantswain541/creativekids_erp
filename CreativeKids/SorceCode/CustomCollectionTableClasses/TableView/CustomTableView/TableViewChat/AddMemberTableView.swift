//
//  AddMemberTableView.swift
//  Chat Demo
//
//  Created by Creative Kids on 25/03/22.
//

import Foundation
import UIKit

class AddMemberTableView:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var addMemberFor:AddMemberFor = .GroupCreate
    var existanceUser = [String]()
    var customDataSourecs:CustomTableViewDataSources<AddMemberTableViewCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: "AddMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "AddMemberTableViewCell")
        DispatchQueue.main.async {
            ChatHalper.shared.retrivedAllUser { [weak self] users in
                self?.item = users
                switch self?.addMemberFor{
                case .GroupCreate:
                    self?.customDataSourecs = .init(cellIdentifier: "AddMemberTableViewCell", item: self?.item ?? [],message:"User not found", completion: { (cell, data, index) in
                        self?.separatorStyle = .singleLine
                        cell.configCell(object: data)
                        cell.selectedCallBack = { [weak self] in
                            self?.dataPassDelegate?.tableView(self!, selectedIndex: index, data: data)
                        }
                    })
                    DispatchQueue.main.async {
                        self?.dataSource = self?.customDataSourecs
                        self?.delegate = self
                        self?.reloadData()
                    }
                case .GroupUpdate:
                    self?.item.forEach{ user in
                        let object = user as? UsersState
                        if self!.existanceUser.contains(String.getstring(object?.userId)){
                            object?.isSelected = true
                        }else{
                            object?.isSelected = false
                        }
                    }
                    self?.customDataSourecs = .init(cellIdentifier: "AddMemberTableViewCell", item: self?.item ?? [],message:"User not found", completion: { (cell, data, index) in
                        let cellObject = data as? UsersState
                        self?.separatorStyle = .singleLine
                        cell.configCell(object: data)
                        cell.selectedCallBack = { [weak self] in
                            self?.dataPassDelegate?.tableView(self!, selectedIndex: index, data: data)
                        }
                        cell.isUserInteractionEnabled = !(self!.existanceUser.contains(String.getstring(cellObject?.userId)))
                        cell.labelName.textColor = self!.existanceUser.contains(String.getstring(cellObject?.userId)) ? UIColor.lightGray : UIColor.black
                    })
                    DispatchQueue.main.async {
                        self?.dataSource = self?.customDataSourecs
                        self?.delegate = self
                        self?.reloadData()
                    }
                default:
                    return
                }
                
            }
        }
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
//PRASH58791YZ
extension AddMemberTableView:TableDataDelegate {
    func filterDataUsingText(searchBy: String) {
        let searedText = String.getstring(searchBy)
        if searedText.isEmpty{
            self.customDataSourecs.item = self.item
            self.reloadData()
        }else{
            let filteredItem = self.item.filter{String.getstring(($0 as? UsersState)?.name).lowercased().contains(searchBy)}
            self.customDataSourecs.item = filteredItem
            self.reloadData()
        }
    }
}
