//
//  LiveTestPaperTableView.swift
//  CreativeKids
//
//  Created by Rakesh jha on 07/05/21.
//

import Foundation
import UIKit

class LiveTestPaperTableView:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<ChapterProgressTableCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.separatorStyle = .none
        self.register(UINib(nibName: TableCellIdentifier.chapterProgress, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.chapterProgress)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.chapterProgress, item: self.item, completion: { (cell, data, index) in
            cell.configCell(object: data)
        })
        self.dataPassDelegate?.tableView(self, height:CGFloat(self.item.count * 35))
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
}
extension LiveTestPaperTableView:TableDataDelegate {
    func getData(cell: String, data: [Any], language: String) {
        self.tableCellId = cell
        self.item = data
        initialSet()
    }
}
